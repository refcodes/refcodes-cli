module org.refcodes.cli {
	requires org.refcodes.data;
	requires org.refcodes.graphical;
	requires org.refcodes.runtime;
	requires transitive org.refcodes.schema;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.struct;
	requires transitive org.refcodes.textual;

	exports org.refcodes.cli;
}
