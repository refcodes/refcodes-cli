// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The {@link AbstractCondition} is an abstract implementation of the
 * {@link Condition} interface providing the boiler plate when implementing the
 * {@link Condition} interface as done by the {@link AbstractCondition}'s
 * sub-classes.
 */
public abstract class AbstractCondition extends AbstractTerm implements Condition {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected Term[] _children;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates an {@link AbstractCondition}' s sub-class with the according
	 * arguments.
	 * 
	 * @param aDescription The description of this {@link Condition}.
	 * @param aElements The elements managed by the {@link Condition}.
	 */
	public AbstractCondition( String aDescription, Term... aElements ) {
		super( aDescription );
		_children = aElements;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMatchCount() {
		int theMatchCount = 0;
		for ( Term eTerm : _children ) {
			theMatchCount += eTerm.getMatchCount();
		}
		return theMatchCount;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		super.reset();
		for ( Term eTerm : _children ) {
			eTerm.reset();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T extends Operand<?>> T toOperand( String aAlias, Class<T> aType ) {
		Operand<?> eOperand;
		Condition eCondition;
		boolean eTypeMatch;
		boolean eAliasMatch;
		for ( Term eSyntaxable : _children ) {
			if ( eSyntaxable instanceof Operand ) {
				eOperand = (Operand<?>) eSyntaxable;
				eTypeMatch = true;
				eAliasMatch = true;
				if ( aAlias != null && aAlias.length() != 0 ) {
					eAliasMatch = aAlias.equals( eOperand.getAlias() );
				}
				if ( aType != null ) {
					eTypeMatch = aType.isAssignableFrom( eOperand.getClass() );
				}
				if ( eTypeMatch && eAliasMatch ) {
					return (T) eOperand;
				}
			}
			else if ( eSyntaxable instanceof Condition ) {
				eCondition = (Condition) eSyntaxable;
				eOperand = eCondition.toOperand( aAlias, aType );
				if ( eOperand != null ) {
					return (T) eOperand;
				}
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Operand<?>[] toOperands() {
		final List<Operand<?>> theResult = new ArrayList<>();
		for ( Synopsisable eSyntaxable : _children ) {
			if ( eSyntaxable instanceof Operand<?> ) {
				theResult.add( (Operand<?>) eSyntaxable );
			}
			if ( eSyntaxable instanceof Condition ) {
				final Operand<?>[] theOperands = ( (Condition) eSyntaxable ).toOperands();
				if ( theOperands != null && theOperands.length != 0 ) {
					for ( var eOperand : theOperands ) {
						theResult.add( eOperand );
					}
				}
			}
		}
		return theResult.toArray( new Operand<?>[theResult.size()] );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CliSchema toSchema() {
		CliSchema[] theSchemas = null;
		if ( _children != null && _children.length != 0 ) {
			theSchemas = new CliSchema[_children.length];
			for ( int i = 0; i < theSchemas.length; i++ ) {
				theSchemas[i] = _children[i].toSchema();
			}
		}
		final CliSchema theSchema = new CliSchema( getClass(), _description, theSchemas );
		theSchema.put( CliSchema.MATCH_COUNT, getMatchCount() );
		if ( _exception != null ) {
			theSchema.put( CliSchema.EXCEPTION, _exception );
		}
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [children=" + Arrays.toString( _children ) + "]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSyntax( CliContext aCliCtx ) {
		String theSyntax = toSynopsis( aCliCtx );
		if ( theSyntax.length() > 0 && _children != null && _children.length > 1 ) {
			theSyntax = aCliCtx.getSyntaxMetrics().getBeginListSymbol() + " " + theSyntax + " " + aCliCtx.getSyntaxMetrics().getEndListSymbol();
		}
		return theSyntax;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <V> V toValue( String aAlias ) {
		Object eValue;
		for ( Term eElement : _children ) {
			eValue = eElement.toValue( aAlias );
			if ( eValue != null ) {
				return (V) eValue;
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Term[] getChildren() {
		return _children;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isVisible() {
		if ( super.isVisible() ) {
			for ( Term eChild : _children ) {
				if ( eChild.isVisible() ) {
					return true;
				}
			}
		}
		return false;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	//	/**
	//	 * Adds any "ignored" child's exception to the given exception as suppressed
	//	 * exception (if not already added as cause or suppressed exception). An
	//	 * ignored exception may be "hidden" by an {@link AnyCondition} though still
	//	 * be useful when analyzing the cause of arguments parsing failure. In this
	//	 * case when overall parsing if arguments failed, then the ignored exception
	//	 * is added to the suppressed exception list of the exception actually
	//	 * responsible for failure.
	//	 * 
	//	 * @param aCause The {@link Exception} responsible for argument parsing
	//	 *        failure.
	//	 * 
	//	 * @return Returns the given (probably modified) exception to ease usage in
	//	 *         a fluent way.
	//	 */
	//	protected ArgsSyntaxException withAddSuppressed( ArgsSyntaxException aCause ) {
	//		return withAddSuppressed( aCause, this );
	//	}

	//	/**
	//	 * Adds any child's exception to the given exception as suppressed exception
	//	 * (if not already added as cause or suppressed exception).
	//	 * 
	//	 * @param aCause The {@link Exception} responsible for argument parsing
	//	 *        failure.
	//	 * 
	//	 * @param aCondition The {@link Condition} whose children to consider.
	//	 * 
	//	 * @return Returns the given (probably modified) exception to ease usage in
	//	 *         a fluent way.
	//	 */
	//	protected static ArgsSyntaxException withAddSuppressed( ArgsSyntaxException aCause, Condition aCondition ) {
	//		for ( Term eTerm : aCondition.getChildren() ) {
	//			ArgsSyntaxException eException = eTerm.getException();
	//			if ( eException != null ) {
	//				if ( aCause == null ) aCause = eException;
	//				if ( !hasException( aCause, eException ) ) {
	//					aCause.addSuppressed( eException );
	//				}
	//			}
	//		}
	//		return aCause;
	//	}
	//	
	//	private static boolean hasException( ArgsSyntaxException aCause, ArgsSyntaxException aException ) {
	//		return hasException( aCause, aException, new HashSet<>() );
	//	}

	//	private static boolean hasException( ArgsSyntaxException aCause, ArgsSyntaxException aException, Set<ArgsSyntaxException> aVisited ) {
	//		if ( aVisited.contains( aException ) ) return false;
	//		aVisited.add( aException );
	//		if ( aCause == aException ) return true;
	//	
	//		if ( aCause == null ) {
	//			System.out.println( "WTF!" );
	//		}
	//	
	//		if ( aCause.getCause() == aException ) return true;
	//		if ( aCause.getCause() != null && aCause.getCause() instanceof ArgsSyntaxException eCause && hasException( eCause, aException, aVisited ) ) {
	//			return true;
	//		}
	//		if ( aCause.getSuppressed() != null && aCause.getSuppressed().length != 0 ) {
	//			for ( Throwable eSuppressed : aCause.getSuppressed() ) {
	//				if ( aCause == eSuppressed ) return true;
	//				if ( eSuppressed instanceof ArgsSyntaxException eException ) {
	//					if ( aException != eException && hasException( aCause, eException, aVisited ) ) {
	//						return true;
	//					}
	//				}
	//			}
	//		}
	//		return false;
	//	}
}
