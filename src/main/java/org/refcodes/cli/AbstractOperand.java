// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.Arrays;
import java.util.function.Consumer;

import org.refcodes.schema.Schema;
import org.refcodes.struct.Relation;

/**
 * The {@link AbstractOperand} is an abstract implementation of an
 * {@link Operand} providing the boiler plate when implementing the
 * {@link Operand} interface.
 *
 * @param <T> The generic type of the {@link AbstractOperand}'s value.
 */
public abstract class AbstractOperand<T> extends AbstractTerm implements Operand<T> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _alias;
	private String[] _args = null;
	private Class<T> _type;
	private Consumer<Operand<T>> _consumer;
	private T _value = null;
	protected int _matchCount = 0;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link Operand} with the given arguments.
	 * 
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aAlias The {@link Operand}'s name, used for syntax creation.
	 * @param aDescription The description to be used (without any line breaks).
	 */
	public AbstractOperand( Class<T> aType, String aAlias, String aDescription ) {
		this( aType, aAlias, aDescription, null );
	}

	/**
	 * Constructs an {@link Operand} with the given arguments.
	 * 
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aAlias The {@link Operand}'s name, used for syntax creation.
	 * @param aDescription The description to be used (without any line breaks).
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Operand} participated in successfully parsing the command
	 *        line arguments.
	 */
	@SuppressWarnings("unchecked")
	public AbstractOperand( Class<T> aType, String aAlias, String aDescription, Consumer<? extends Operand<T>> aConsumer ) {
		super( aDescription );
		_type = aType;
		_alias = aAlias;
		_consumer = (Consumer<Operand<T>>) aConsumer;
	}

	/**
	 * Constructs an {@link Operand} with the given arguments.
	 *
	 * @param aProperty The key (= alias) and the value for the operand.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 */
	public AbstractOperand( Relation<String, T> aProperty, Class<T> aType ) {
		this( aProperty, aType, null );
	}

	/**
	 * Constructs an {@link Operand} with the given arguments.
	 *
	 * @param aProperty The key (= alias) and the value for the operand.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Option} participated in successfully parsing the command
	 *        line arguments.
	 */
	@SuppressWarnings("unchecked")
	public AbstractOperand( Relation<String, T> aProperty, Class<T> aType, Consumer<? extends Operand<T>> aConsumer ) {
		_alias = aProperty.getKey();
		_value = aProperty.getValue();
		_consumer = (Consumer<Operand<T>>) aConsumer;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compareTo( Operand<?> obj ) {
		if ( obj instanceof Option<?> aOption ) {
			if ( this instanceof Option<?> theOption ) {
				final String otherOption = aOption.getShortOption() != null ? aOption.getShortOption().toString() : aOption.getLongOption();
				final String thisOption = theOption.getShortOption() != null ? theOption.getShortOption().toString() : theOption.getLongOption();
				return thisOption.compareTo( otherOption );
			}
			if ( this instanceof Operand<?> ) {
				return 1;
			}
		}
		if ( obj instanceof Operand<?> ) {
			if ( this instanceof Option<?> ) {
				return -11;
			}
		}
		if ( getAlias() != null && obj.getAlias() != null ) {
			return getAlias().compareTo( obj.getAlias() );
		}
		return toSyntax().compareTo( obj.toSyntax() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return _alias;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<T> getType() {
		return _type;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getValue() {
		return _value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Operand<?>[] parseArgs( String[] aArgs, String[] aOptions, CliContext aCliContext ) throws ArgsSyntaxException {
		int first = -1;
		for ( int i = aArgs.length - 1; i >= 0; i-- ) {
			if ( !aCliContext.isOption( aArgs[i] ) ) {
				if ( first == -1 || first + -1 == i ) {
					first = i;
				}
			}
		}
		if ( first != -1 ) {
			setValue( toType( aArgs[first] ) );
			setParsedArgs( new String[] { aArgs[first] } );
			_matchCount = 1;
			return new Operand<?>[] { this };
		}

		throw _exception = new UnknownArgsException( "Unable to parse any args (neither being prefixed with \"" + aCliContext.getSyntaxMetrics().getShortOptionPrefix() + "\" nor being prefixed with \"" + aCliContext.getSyntaxMetrics().getLongOptionPrefix() + "\")!", aArgs, this );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMatchCount() {
		return _matchCount;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		super.reset();
		_matchCount = 0;
		_args = null;
		_value = null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getParsedArgs() {
		return _args;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CliSchema toSchema() {
		final CliSchema theSchema = new CliSchema( _alias, getClass(), _value, _description );
		theSchema.put( CliSchema.MATCH_COUNT, _matchCount );
		theSchema.put( CliSchema.HAS_VALUE_KEY, hasValue() );
		if ( _exception != null ) {
			theSchema.put( Schema.EXCEPTION, _exception );
		}
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [alias=" + _alias + ", args=" + Arrays.toString( _args ) + ", type=" + _type + ", value=" + _value + ", matchCount=" + _matchCount + "]";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSyntax( CliContext aCliCtx ) {
		return aCliCtx.toArgumentSpec( this );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * In case this {@link Term} participated in successful parsing command line
	 * arguments, then this {@link Term} gets approved by the
	 * {@link ArgsParser}. An approval my trigger actions such as notifying
	 * registered lambda expressions upon approval,
	 */
	protected void approve() {
		if ( _consumer != null ) {
			_consumer.accept( this );
		}
	}

	/**
	 * Sets the command line argument(s) representing the {@link Operand} and
	 * its value as parsed by the
	 * {@link #parseArgs(String[], String[], CliContext)} method.
	 * 
	 * @param aArgs The command line arguments representing this {@link Operand}
	 *        with its value.
	 */
	protected void setParsedArgs( String[] aArgs ) {
		_args = aArgs;
	}

	/**
	 * Sets the value for the {@link Operand} as parsed by the
	 * {@link #parseArgs(String[], String[], CliContext)} method.
	 * 
	 * @param aValue The value to be set for this {@link Operand}.
	 */
	protected void setValue( T aValue ) {
		_value = aValue;
	}

	/**
	 * Double dispatch hook to be implemented by subclasses of the
	 * {@link AbstractOperand} for converting a command line argument to the
	 * required {@link Operand}'s type. In case conversion failed, then an
	 * according exception is to be thrown.
	 * 
	 * @param aArg The command line argument to be converted to an instance of
	 *        the given type T.
	 * 
	 * @return An instance of type T from the provided command line argument.
	 * 
	 * @throws ParseArgsException Thrown in case the provided command line
	 *         arguments do not respect the required syntax or cannot be
	 *         converted to the required type.
	 */
	protected abstract T toType( String aArg ) throws ParseArgsException;

}
