// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import org.refcodes.struct.Relation;
import org.refcodes.struct.RelationImpl;

/**
 * The {@link AbstractOption} is an abstract implementation of an {@link Option}
 * providing the boiler plate when implementing the {@link Option} interface.
 *
 * @param <T> The generic type of the {@link AbstractOption}'s value.
 */
public abstract class AbstractOption<T> extends AbstractOperand<T> implements Option<T> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String LONG_OPTION_KEY = "LONG_OPTION";
	private static final String SHORT_OPTION_KEY = "SHORT_OPTION";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _longOption;
	private Character _shortOption;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link AbstractOption} with the given arguments.
	 * 
	 * @param aShortOption The short-option being a single character with the
	 *        additional single hyphen-minus "-" prefix.
	 * @param aLongOption The long-option being a multi-character sequence with
	 *        at least two characters with the additional double hyphen-minus
	 *        "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aAlias The option argument's name to be used when constructing the
	 *        syntax.
	 * @param aDescription The description to be used (without any line breaks).
	 */
	public AbstractOption( Character aShortOption, String aLongOption, Class<T> aType, String aAlias, String aDescription ) {
		this( aShortOption, aLongOption, aType, aAlias, aDescription, null );
	}

	/**
	 * Constructs an {@link AbstractOption} with the given arguments.
	 * 
	 * @param aShortOption The short-option being a single character with the
	 *        additional single hyphen-minus "-" prefix.
	 * @param aLongOption The long-option being a multi-character sequence with
	 *        at least two characters with the additional double hyphen-minus
	 *        "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aAlias The option argument's name to be used when constructing the
	 *        syntax.
	 * @param aDescription The description to be used (without any line breaks).
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Option} participated in successfully parsing the command
	 *        line arguments.
	 */
	public AbstractOption( Character aShortOption, String aLongOption, Class<T> aType, String aAlias, String aDescription, Consumer<? extends Operand<T>> aConsumer ) {
		super( aType, aAlias != null ? aAlias : ( aLongOption != null ? aLongOption : ( aShortOption != null ? aShortOption.toString() : null ) ), aDescription, aConsumer );
		if ( aShortOption == null && ( aLongOption == null || aLongOption.isEmpty() ) ) {
			throw new IllegalArgumentException( "At least the short option <" + ( aShortOption != null ? "'" + aShortOption + "'" : aShortOption ) + "> must not be null or the long option <" + ( aLongOption != null ? "\"" + aLongOption + "\"" : aLongOption ) + "> must not be empty!" );
		}
		_shortOption = aShortOption;
		_longOption = aLongOption;
	}

	/**
	 * Constructs an {@link AbstractOption} with the given arguments.
	 *
	 * @param aProperty The key (= alias) and the value for the operand.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 */
	public AbstractOption( Relation<String, T> aProperty, Class<T> aType ) {
		this( aProperty, aType, null );
	}

	/**
	 * Constructs an {@link AbstractOption} with the given arguments.
	 *
	 * @param aProperty The key (= alias) and the value for the operand.
	 * 
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * 
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Option} participated in successfully parsing the command
	 *        line arguments.
	 */
	public AbstractOption( Relation<String, T> aProperty, Class<T> aType, Consumer<? extends Operand<T>> aConsumer ) {
		super( aProperty, aType, aConsumer );
		_shortOption = aProperty.getKey().length() == 1 ? aProperty.getKey().charAt( 0 ) : null;
		_longOption = aProperty.getKey().length() > 1 ? aProperty.getKey() : null;
	}

	/**
	 * Constructs an {@link AbstractOption} with the given arguments.
	 *
	 * @param aLongOption The long-option being a multi-character sequence with
	 *        at least two characters with the additional double hyphen-minus
	 *        "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aDescription The description to be used (without any line breaks).
	 */
	public AbstractOption( String aLongOption, Class<T> aType, String aDescription ) {
		this( null, aLongOption, aType, null, aDescription );
	}

	/**
	 * Constructs an {@link AbstractOption} with the given arguments.
	 *
	 * @param aLongOption The long-option being a multi-character sequence with
	 *        at least two characters with the additional double hyphen-minus
	 *        "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aDescription The description to be used (without any line breaks).
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Option} participated in successfully parsing the command
	 *        line arguments.
	 */
	public AbstractOption( String aLongOption, Class<T> aType, String aDescription, Consumer<? extends Operand<T>> aConsumer ) {
		this( null, aLongOption, aType, null, aDescription, aConsumer );
	}

	/**
	 * Constructs an {@link AbstractOption} with the given arguments.
	 *
	 * @param aLongOption The long-option being a multi-character sequence with
	 *        at least two characters with the additional double hyphen-minus
	 *        "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aAlias The option argument's name to be used when constructing the
	 *        syntax.
	 * @param aDescription The description to be used (without any line breaks).
	 */
	public AbstractOption( String aLongOption, Class<T> aType, String aAlias, String aDescription ) {
		this( null, aLongOption, aType, aAlias, aDescription );
	}

	/**
	 * Constructs an {@link AbstractOption} with the given arguments.
	 *
	 * @param aLongOption The long-option being a multi-character sequence with
	 *        at least two characters with the additional double hyphen-minus
	 *        "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aAlias The option argument's name to be used when constructing the
	 *        syntax.
	 * @param aDescription The description to be used (without any line breaks).
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Option} participated in successfully parsing the command
	 *        line arguments.
	 */
	public AbstractOption( String aLongOption, Class<T> aType, String aAlias, String aDescription, Consumer<? extends Operand<T>> aConsumer ) {
		this( null, aLongOption, aType, aAlias, aDescription, aConsumer );
	}

	/**
	 * Constructs an {@link AbstractOption} with the given arguments.
	 * 
	 * @param aShortOption The short-option being a single character with the
	 *        additional single hyphen-minus "-" prefix.
	 * @param aLongOption The long-option being a multi-character sequence with
	 *        at least two characters with the additional double hyphen-minus
	 *        "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aDescription The description to be used (without any line breaks).
	 */
	protected AbstractOption( Character aShortOption, String aLongOption, Class<T> aType, String aDescription ) {
		this( aShortOption, aLongOption, aType, null, aDescription );
	}

	/**
	 * Constructs an {@link AbstractOption} with the given arguments.
	 * 
	 * @param aShortOption The short-option being a single character with the
	 *        additional single hyphen-minus "-" prefix.
	 * @param aLongOption The long-option being a multi-character sequence with
	 *        at least two characters with the additional double hyphen-minus
	 *        "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aDescription The description to be used (without any line breaks).
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Option} participated in successfully parsing the command
	 *        line arguments.
	 */
	protected AbstractOption( Character aShortOption, String aLongOption, Class<T> aType, String aDescription, Consumer<? extends Operand<T>> aConsumer ) {
		this( aShortOption, aLongOption, aType, null, aDescription, aConsumer );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLongOption() {
		return _longOption;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getShortOption() {
		return _shortOption;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Operand<?>[] parseArgs( String[] aArgs, String[] aOptions, CliContext aCliCtx ) throws ArgsSyntaxException {
		final Relation<String, String> theOptionArgument = toOptionArgument( aArgs, aOptions, aCliCtx );
		if ( theOptionArgument != null ) {
			setParsedArgs( new String[] { theOptionArgument.getKey(), theOptionArgument.getValue() } );
			setValue( toType( theOptionArgument.getValue() ) );
			_matchCount = 1;
			return new Operand<?>[] { this };
		}
		if ( contains( aArgs, aCliCtx.toShortOption( this ) ) ) {
			throw _exception = new ParseOptionArgsException( "Missing value for option \"{0}\" requiring a value.", aArgs, aCliCtx.toShortOption( this ), aCliCtx.toLongOption( this ), this );
		}
		if ( contains( aArgs, aCliCtx.toLongOption( this ) ) ) {
			throw _exception = new ParseOptionArgsException( "Missing value for option \"{1}\" requiring a value.", aArgs, aCliCtx.toShortOption( this ), aCliCtx.toLongOption( this ), this );
		}
		throw _exception = toException( aArgs, aCliCtx );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CliSchema toSchema() {
		final CliSchema theSchema = super.toSchema();
		theSchema.put( SHORT_OPTION_KEY, _shortOption );
		theSchema.put( LONG_OPTION_KEY, _longOption );
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSyntax( CliContext aCliCtx ) {
		return aCliCtx.toOptionEscapeCode() + aCliCtx.toOptionUsage( this ) + aCliCtx.toResetEscapeCode() + ( getAlias() != null ? ( " " + aCliCtx.toArgumentSpec( this ) ) : "" ) + aCliCtx.toResetEscapeCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [alias=" + getAlias() + ", args=" + Arrays.toString( getParsedArgs() ) + ", longOption=" + _longOption + ", shortOption=" + _shortOption + ", type=" + getType() + ", value=" + getValue() + ", matchCount=" + getMatchCount() + ", description=" + getDescription() + "]";
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Convenience method to create an {@link UnknownArgsException}.
	 * 
	 * @param aArgs The arguments participating tin the exception.
	 * @param aCliCtx The context to use when creating the exception's details.
	 * 
	 * @return The accordingly created {@link ArgsSyntaxException} instance.
	 */
	protected UnknownArgsException toException( String[] aArgs, CliContext aCliCtx ) {
		if ( getShortOption() != null && getLongOption() != null ) {
			return new UnknownOptionArgsException( "Neither the short-option \"{0}\" nor the long-option \"{1}\" were found in the command line arguments, provide at least one of them to match the according syntax branch!", aArgs, aCliCtx.toShortOption( this ), aCliCtx.toLongOption( this ), this );
		}
		if ( getShortOption() != null && getLongOption() == null ) {
			return new UnknownOptionArgsException( "No option \"{0}\" was found in the command line arguments, provide it to match the according syntax branch!", aArgs, aCliCtx.toShortOption( this ), aCliCtx.toLongOption( this ), this );
		}
		if ( getShortOption() == null && getLongOption() != null ) {
			return new UnknownOptionArgsException( "No option \"{1}\" was found in the command line arguments, provide it to match the according syntax branch!", aArgs, aCliCtx.toShortOption( this ), aCliCtx.toLongOption( this ), this );
		}
		throw new IllegalStateException( "At least a short-option or a long-option must be defined, but none has been configured!" );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void setParsedArgs( String[] aArgs ) {
		super.setParsedArgs( aArgs );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void setValue( T aValue ) {
		super.setValue( aValue );
	}

	/**
	 * Determines whether a given {@link String} is contained in the given
	 * {@link String} array.
	 * 
	 * @param aArgs The {@link String} array to be tested whether it contains
	 *        the given {@link String}.
	 * @param aArg The {@link String} to be tested whether it is contained in
	 *        the provided {@link String} array.
	 * 
	 * @return True in case the {@link String} is contained in the array, else
	 *         false.
	 */
	protected static boolean contains( String[] aArgs, String aArg ) {
		if ( aArg != null ) {
			final List<String> theList = new ArrayList<>( Arrays.asList( aArgs ) );
			return theList.contains( aArg );
		}
		return false;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Takes the {@link Option}'s short-option and long-option and tries to
	 * determine that {@link Option}'s value in the provided command line
	 * arguments. Depending on whether the short-option or the long-option was
	 * detected with a value, the result contains the according option as the
	 * key with the detected value in the {@link Relation} instance. Null is
	 * returned when either no option was found or no value for one of the
	 * options.
	 *
	 * @param aArgs The command line arguments from which to determine the
	 *        {@link Option}'s value.
	 * @param aOption The option for which to get the value
	 * @param aOptions The list of options (short as well as long) which are
	 *        reserved and cannot be used as value.
	 * 
	 * @return A key/value-pair containing the detected (short / long) option
	 *         alongside the detected value.
	 */
	private Relation<String, String> toOptionArgument( String[] aArgs, String aOption, String[] aOptions ) {
		String eArg;
		final String eOptArg;
		for ( int i = 0; i < aArgs.length - 1; i++ ) {
			eArg = aArgs[i];
			if ( eArg.equals( aOption ) ) {
				eOptArg = aArgs[i + 1];
				for ( String eOption : aOptions ) {
					if ( eOptArg.equalsIgnoreCase( eOption ) ) {
						return null;
					}
				}
				return new RelationImpl<>( aOption, eOptArg );
			}
		}
		return null;
	}

	/**
	 * Takes the {@link Option}'s short-option and long-option and tries to
	 * determine that {@link Option}'s value in the provided command line
	 * arguments. Depending on whether the short-option or the long-option was
	 * detected with a value, the result contains the according option as the
	 * key with the detected value in the {@link Relation} instance. Null is
	 * returned when either no option was found or no value for one of the
	 * options.
	 * 
	 * @param aArgs The command line arguments from which to determine the
	 *        {@link Option}'s value.
	 * @param aOptions The list of options (short as well as long) which are
	 *        reserved and cannot be used as value.
	 * @param aCliCtx The {@link CliContext} to run this method with.
	 * 
	 * @return A key/value-pair containing the detected (short / long) option
	 *         alongside the detected value.
	 */
	private Relation<String, String> toOptionArgument( String[] aArgs, String[] aOptions, CliContext aCliCtx ) {
		if ( aArgs.length > 1 ) {
			Relation<String, String> theAttribute = toOptionArgument( aArgs, aCliCtx.toShortOption( this ), aOptions );
			if ( theAttribute != null ) {
				return theAttribute;
			}
			theAttribute = toOptionArgument( aArgs, aCliCtx.toLongOption( this ), aOptions );
			if ( theAttribute != null ) {
				return theAttribute;
			}
		}
		return null;
	}
}
