// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.function.Consumer;

import org.refcodes.struct.Relation;
import org.refcodes.struct.RelationImpl;

/**
 * The {@link AbstractPropertyOption} is an abstract implementation of a
 * {@link PropertyOption} providing the boiler plate when implementing the
 * {@link PropertyOption} interface. A {@link PropertyOption} is a key=value
 * pair passed as a single argument (e.g. "--file=someFile").
 *
 * @param <T> The generic type of the {@link AbstractPropertyOption}'s value.
 */
public abstract class AbstractPropertyOption<T> extends AbstractOption<T> implements PropertyOption<T> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final char PROPERTY_SEPARATOR = '=';

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link AbstractPropertyOption} with the given arguments.
	 * 
	 * @param aShortProperty The short-option being a single character with the
	 *        additional single hyphen-minus "-" prefix.
	 * @param aLongProperty The long-option being a multi-character sequence
	 *        with at least two characters with the additional double
	 *        hyphen-minus "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aAlias The option argument's name to be used when constructing the
	 *        syntax.
	 * @param aDescription The description to be used (without any line breaks).
	 */
	public AbstractPropertyOption( Character aShortProperty, String aLongProperty, Class<T> aType, String aAlias, String aDescription ) {
		super( aShortProperty, aLongProperty, aType, aAlias, aDescription );
		if ( aShortProperty == null && ( aLongProperty == null || aLongProperty.isEmpty() ) ) {
			throw new IllegalArgumentException( "At least the short option <" + ( aShortProperty != null ? "'" + aShortProperty + "'" : aShortProperty ) + "> must not be null or the long option <" + ( aLongProperty != null ? "\"" + aLongProperty + "\"" : aLongProperty ) + "> must not be empty!" );
		}
	}

	/**
	 * Constructs an {@link AbstractPropertyOption} with the given arguments.
	 * 
	 * @param aShortProperty The short-option being a single character with the
	 *        additional single hyphen-minus "-" prefix.
	 * @param aLongProperty The long-option being a multi-character sequence
	 *        with at least two characters with the additional double
	 *        hyphen-minus "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aAlias The option argument's name to be used when constructing the
	 *        syntax.
	 * @param aDescription The description to be used (without any line breaks).
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link PropertyOption} participated in successfully parsing the
	 *        command line arguments.
	 */
	public AbstractPropertyOption( Character aShortProperty, String aLongProperty, Class<T> aType, String aAlias, String aDescription, Consumer<? extends Operand<T>> aConsumer ) {
		super( aShortProperty, aLongProperty, aType, aAlias, aDescription, aConsumer );
		if ( aShortProperty == null && ( aLongProperty == null || aLongProperty.isEmpty() ) ) {
			throw new IllegalArgumentException( "At least the short option <" + ( aShortProperty != null ? "'" + aShortProperty + "'" : aShortProperty ) + "> must not be null or the long option <" + ( aLongProperty != null ? "\"" + aLongProperty + "\"" : aLongProperty ) + "> must not be empty!" );
		}
	}

	/**
	 * Constructs an {@link AbstractPropertyOption} with the given arguments.
	 *
	 * @param aProperty The key (= alias) and the value for the operand.
	 * @param aType the type
	 */
	public AbstractPropertyOption( Relation<String, T> aProperty, Class<T> aType ) {
		super( aProperty, aType );
	}

	/**
	 * Constructs an {@link AbstractPropertyOption} with the given arguments.
	 *
	 * @param aProperty The key (= alias) and the value for the operand.
	 * @param aType the type
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link PropertyOption} participated in successfully parsing the
	 *        command line arguments.
	 */
	public AbstractPropertyOption( Relation<String, T> aProperty, Class<T> aType, Consumer<? extends Operand<T>> aConsumer ) {
		super( aProperty, aType, aConsumer );
	}

	/**
	 * Constructs an {@link AbstractPropertyOption} with the given arguments.
	 *
	 * @param aLongProperty The long-option being a multi-character sequence
	 *        with at least two characters with the additional double
	 *        hyphen-minus "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aDescription The description to be used (without any line breaks).
	 */
	public AbstractPropertyOption( String aLongProperty, Class<T> aType, String aDescription ) {
		super( aLongProperty, aType, aDescription );
	}

	/**
	 * Constructs an {@link AbstractPropertyOption} with the given arguments.
	 *
	 * @param aLongProperty The long-option being a multi-character sequence
	 *        with at least two characters with the additional double
	 *        hyphen-minus "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aDescription The description to be used (without any line breaks).
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link PropertyOption} participated in successfully parsing the
	 *        command line arguments.
	 */
	public AbstractPropertyOption( String aLongProperty, Class<T> aType, String aDescription, Consumer<? extends Operand<T>> aConsumer ) {
		super( aLongProperty, aType, aDescription, aConsumer );
	}

	/**
	 * Constructs an {@link AbstractPropertyOption} with the given arguments.
	 *
	 * @param aLongProperty The long-option being a multi-character sequence
	 *        with at least two characters with the additional double
	 *        hyphen-minus "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aAlias The option argument's name to be used when constructing the
	 *        syntax.
	 * @param aDescription The description to be used (without any line breaks).
	 */
	public AbstractPropertyOption( String aLongProperty, Class<T> aType, String aAlias, String aDescription ) {
		super( aLongProperty, aType, aAlias, aDescription );
	}

	/**
	 * Constructs an {@link AbstractPropertyOption} with the given arguments.
	 *
	 * @param aLongProperty The long-option being a multi-character sequence
	 *        with at least two characters with the additional double
	 *        hyphen-minus "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aAlias The option argument's name to be used when constructing the
	 *        syntax.
	 * @param aDescription The description to be used (without any line breaks).
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link PropertyOption} participated in successfully parsing the
	 *        command line arguments.
	 */
	public AbstractPropertyOption( String aLongProperty, Class<T> aType, String aAlias, String aDescription, Consumer<? extends Operand<T>> aConsumer ) {
		super( aLongProperty, aType, aAlias, aDescription, aConsumer );
	}

	/**
	 * Constructs an {@link AbstractPropertyOption} with the given arguments.
	 * 
	 * @param aShortProperty The short-option being a single character with the
	 *        additional single hyphen-minus "-" prefix.
	 * @param aLongProperty The long-option being a multi-character sequence
	 *        with at least two characters with the additional double
	 *        hyphen-minus "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aDescription The description to be used (without any line breaks).
	 */
	protected AbstractPropertyOption( Character aShortProperty, String aLongProperty, Class<T> aType, String aDescription ) {
		super( aShortProperty, aLongProperty, aType, aDescription );
	}

	/**
	 * Constructs an {@link AbstractPropertyOption} with the given arguments.
	 * 
	 * @param aShortProperty The short-option being a single character with the
	 *        additional single hyphen-minus "-" prefix.
	 * @param aLongProperty The long-option being a multi-character sequence
	 *        with at least two characters with the additional double
	 *        hyphen-minus "--" prefix.
	 * @param aType The type of the value returned by the {@link #getValue()}
	 *        method.
	 * @param aDescription The description to be used (without any line breaks).
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link PropertyOption} participated in successfully parsing the
	 *        command line arguments.
	 */
	protected AbstractPropertyOption( Character aShortProperty, String aLongProperty, Class<T> aType, String aDescription, Consumer<? extends Operand<T>> aConsumer ) {
		super( aShortProperty, aLongProperty, aType, aDescription, aConsumer );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Operand<?>[] parseArgs( String[] aArgs, String[] aOptions, CliContext aCliCtx ) throws ArgsSyntaxException {
		final Relation<String, String> thePropertyArgument = toPropertyArgument( aArgs, aCliCtx );
		if ( thePropertyArgument != null ) {
			setParsedArgs( new String[] { thePropertyArgument.getKey() + PROPERTY_SEPARATOR + thePropertyArgument.getValue() } );
			setValue( toType( thePropertyArgument.getValue() ) );
			_matchCount = 1;
			return new Operand<?>[] { this };
		}
		if ( contains( aArgs, aCliCtx.toShortOption( this ) ) ) {
			throw _exception = new ParseArgsException( "Missing value for option \"" + aCliCtx.toShortOption( this ) + "\" requiring a value.", aArgs, this );
		}
		if ( contains( aArgs, aCliCtx.toLongOption( this ) ) ) {
			throw _exception = new ParseArgsException( "Missing value for option \"" + aCliCtx.toLongOption( this ) + "\" requiring a value.", aArgs, this );
		}
		throw _exception = toException( aArgs, aCliCtx );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toUsage( CliContext aCliContext ) {
		final StringBuilder theBuilder = new StringBuilder();
		theBuilder.append( aCliContext.toOptionEscapeCode() + aCliContext.toOptionUsage( this ) + aCliContext.toResetEscapeCode() );
		final String theArgumentSpec = aCliContext.toArgumentSpec( this );
		if ( theArgumentSpec != null && theArgumentSpec.length() != 0 && theBuilder.length() > 0 ) {
			theBuilder.append( PROPERTY_SEPARATOR );
		}
		theBuilder.append( theArgumentSpec );
		return theBuilder.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSyntax( CliContext aCliCtx ) {
		return aCliCtx.toOptionEscapeCode() + aCliCtx.toOptionUsage( this ) + aCliCtx.toResetEscapeCode() + ( getAlias() != null ? ( "=" + aCliCtx.toArgumentSpec( this ) ) : "" ) + aCliCtx.toResetEscapeCode();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Takes the {@link Option}'s short-option and long-option and tries to
	 * determine that {@link Option}'s value in the provided command line
	 * arguments. Depending on whether the short-option or the long-option was
	 * detected with a value, the result contains the according option as the
	 * key with the detected value in the {@link Relation} instance. Null is
	 * returned when either no option was found or no value for one of the
	 * options.
	 *
	 * @param aArgs The command line arguments from which to determine the
	 *        {@link Option}'s value.
	 * @param aCliCtx The {@link CliContext} to run this method with.
	 * 
	 * @return A key/value-pair containing the detected (short / long) option
	 *         alongside the detected value.
	 */
	protected Relation<String, String> toPropertyArgument( String[] aArgs, CliContext aCliCtx ) {
		if ( aArgs.length > 0 ) {
			Relation<String, String> theAttribute = toPropertyArgument( aArgs, aCliCtx.toShortOption( this ) );
			if ( theAttribute != null ) {
				return theAttribute;
			}
			theAttribute = toPropertyArgument( aArgs, aCliCtx.toLongOption( this ) );
			if ( theAttribute != null ) {
				return theAttribute;
			}
		}
		return null;
	}

	/**
	 * Takes the {@link Option}'s short-option and long-option and tries to
	 * determine that {@link Option}'s value in the provided command line
	 * arguments. Depending on whether the short-option or the long-option was
	 * detected with a value, the result contains the according option as the
	 * key with the detected value in the {@link Relation} instance. Null is
	 * returned when either no option was found or no value for one of the
	 * options.
	 *
	 * @param aArgs The command line arguments from which to determine the
	 *        {@link Option}'s value.
	 * @param aOption The option for which to get the value reserved and cannot
	 *        be used as value.
	 * 
	 * @return A key/value-pair containing the detected (short / long) option
	 *         alongside the detected value.
	 */
	protected static Relation<String, String> toPropertyArgument( String[] aArgs, String aOption ) {
		if ( aOption != null ) {
			String eArg;
			String eKey;
			String eValue;
			for ( String aArg : aArgs ) {
				eArg = aArg;
				final int eIndex = eArg.indexOf( PROPERTY_SEPARATOR );
				if ( eIndex != -1 ) {
					eKey = eArg.substring( 0, eIndex );
					eValue = eArg.substring( eIndex + 1 );
					if ( aOption.equals( eKey ) ) {
						return new RelationImpl<>( eKey, eValue );
					}
				}
			}
		}
		return null;
	}
}
