// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The {@link AbstractTerm} just implements the
 * {@link #toSynopsis(SyntaxNotation)} method which delegates to the
 * {@link #toSyntax(CliContext)} method and constructs the syntax {@link String}
 * representation.
 */
abstract class AbstractTerm implements Term {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected String _description = null;
	protected ArgsSyntaxException _exception = null;
	protected boolean _isVisible = true;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link AbstractTerm}.
	 */
	public AbstractTerm() {}

	/**
	 * Constructs an {@link AbstractTerm}.
	 * 
	 * @param aDescription The description to be used (without any line breaks).
	 */
	public AbstractTerm( String aDescription ) {
		_description = aDescription;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDescription() {
		return _description;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsSyntaxException getException() {
		return _exception;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		_exception = null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setVisible( boolean isVisible ) {
		_isVisible = isVisible;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isVisible() {
		return _isVisible;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates the difference between the provided set and the provided
	 * {@link List}s therein found argument arrays subset (as of
	 * {@link Operand#getParsedArgs()}).
	 * 
	 * @param aArgs The set to be used for the diff operation.
	 * @param aArgsSubset The subset to be used for the diff operation being the
	 *        lists containing the {@link Operand} instances whose command line
	 *        arguments are to be diffed.
	 * 
	 * @return The difference between the set and the subset.
	 */
	protected static String[] toArgsDiff( String[] aArgs, List<? extends Operand<?>> aArgsSubset ) {
		return toArgsDiff( aArgs, aArgsSubset == null ? null : aArgsSubset.toArray( new Operand<?>[aArgsSubset.size()] ) );
	}

	/**
	 * Creates the difference between the provided set and the provided
	 * {@link List}s therein found argument arrays subset (as of
	 * {@link Operand#getParsedArgs()}).
	 * 
	 * @param aArgs The set to be used for the diff operation.
	 * @param aArgsSubset The subset to be used for the diff operation being the
	 *        lists containing the {@link Operand} instances whose command line
	 *        arguments are to be diffed.
	 * 
	 * @return The difference between the set and the subset.
	 */
	protected static String[] toArgsDiff( String[] aArgs, Operand<?>[] aArgsSubset ) {
		return toArgsDiff( aArgs, toParsedArgs( aArgsSubset ) );
	}

	/**
	 * Creates the difference between the provided set and the provided subset.
	 * 
	 * @param aArgs The set to be used for the diff operation.
	 * @param aArgsSubset The subset to be used for the diff operation.
	 * 
	 * @return The difference between the set and the subset.
	 */
	protected static String[] toArgsDiff( String[] aArgs, String[] aArgsSubset ) {
		final List<String> theDiff = new ArrayList<>( Arrays.asList( aArgs ) );
		int index = 0;
		String eeElement;
		for ( String eElement : aArgsSubset ) {
			out: {
				// Try to get subsequent elements |-->
				for ( int l = index; l < theDiff.size(); l++ ) {
					eeElement = theDiff.get( l );
					if ( eElement.equals( eeElement ) ) {
						theDiff.remove( l );
						index = l;
						break out;
					}
				}
				// Try to get subsequent elements <--|

				// No subsequent elelemt found, start from beginning |-->
				for ( int l = 0; l < theDiff.size(); l++ ) {
					eeElement = theDiff.get( l );
					if ( eElement.equals( eeElement ) ) {
						theDiff.remove( l );
						index = l;
						break out;
					}
				}
				// No subsequent elelemt found, start from beginning <--|
			}
		}
		return theDiff.toArray( new String[theDiff.size()] );
	}

	/**
	 * Takes all {@link Operand} instances found in the provided {@link List}s
	 * and adds all therein found argument arrays (as of
	 * {@link Operand#getParsedArgs()}) to the result.
	 * 
	 * @param aOperands The lists containing the {@link Operand} instances whose
	 *        command line arguments are to be added to the result.
	 * 
	 * @return All the command line arguments detected in the provided
	 *         {@link Operand}s {@link List}s.
	 */
	protected static String[] toParsedArgs( Operand<?>[]... aOperands ) {
		if ( aOperands != null ) {
			final List<String> theArgs = new ArrayList<>();
			for ( Operand<?>[] eOperands : aOperands ) {
				if ( eOperands != null ) {
					for ( Operand<?> eOperand : eOperands ) {
						if ( eOperand.getParsedArgs() != null ) {
							theArgs.addAll( Arrays.asList( eOperand.getParsedArgs() ) );
						}
					}
				}
			}
			return theArgs.toArray( new String[theArgs.size()] );
		}
		return null;
	}
}
