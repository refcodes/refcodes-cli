// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import org.refcodes.exception.MessageDetails;

/**
 * The {@link AllCondition} enforces that the encapsulated {@link Term} consumes
 * all arguments passed via invoking its
 * {@link Term#parseArgs(String[], CliContext)}, else a
 * {@link SuperfluousArgsException} is thrown. This enables the construction of
 * a syntax which otherwise would not be unambiguous when encapsulated within an
 * {@link XorCondition}.
 */
public class AllCondition extends AbstractCondition {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link AllCondition} with the {@link Term}
	 * ({@link Condition}) instance to be nested.
	 *
	 * @param aArg The {@link Term} ({@link Condition}) instance to be nested
	 */
	public AllCondition( Term aArg ) {
		super( "All (ALL) command line arguments passed are to be consumed by the according syntax branches.", aArg );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Operand<?>[] parseArgs( String[] aArgs, String[] aOptions, CliContext aCliCtx ) throws ArgsSyntaxException {
		final Operand<?>[] theResult;
		try {
			theResult = _children[0].parseArgs( aArgs, aOptions, aCliCtx );
		}
		catch ( ArgsSyntaxException e ) {
			throw _exception = e;
		}
		final String[] theRemainderArgs = toArgsDiff( aArgs, theResult );
		if ( theRemainderArgs != null && theRemainderArgs.length != 0 ) {
			String thePossibleCause = toPossibleCause( theRemainderArgs );
			thePossibleCause = thePossibleCause.length() > 0 ? " " + thePossibleCause : thePossibleCause;
			throw _exception = new SuperfluousArgsException( "Failed as of " + ( theRemainderArgs.length > 1 ? "(mutual) " : "" ) + "superfluous " + toString( theRemainderArgs ) + ( theRemainderArgs.length == 1 ? " argument" : " arguments" ) + ", though the arguments must be matched (ALL) without any surplus by the syntax branches!" + thePossibleCause, theRemainderArgs, this );
		}
		return theResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSynopsis( CliContext aCliCtx ) {
		if ( _children != null && _children.length > 0 ) {
			if ( _children.length == 1 ) {
				return _children[0].toSynopsis( aCliCtx );
			}
			String theSynopsis = "";
			for ( Term eChild : _children ) {
				if ( eChild.isVisible() ) {
					if ( theSynopsis.length() != 0 ) {
						theSynopsis += " ";
					}
					theSynopsis += eChild.toSyntax( aCliCtx );
				}
			}
			return theSynopsis;
		}
		return "";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSyntax( CliContext aCliCtx ) {
		if ( _children != null && _children.length > 0 ) {
			String theSyntax = "";
			for ( Term eChild : _children ) {
				if ( theSyntax.length() != 0 ) {
					theSyntax += " ";
				}
				if ( aCliCtx.getSyntaxMetrics().getAllSymbol() != null && aCliCtx.getSyntaxMetrics().getAllSymbol().length() != 0 ) {
					theSyntax += aCliCtx.getSyntaxMetrics().getAllSymbol() + aCliCtx.getSyntaxMetrics().getBeginListSymbol() + " ";
				}
				theSyntax += eChild.toSyntax( aCliCtx );
				if ( aCliCtx.getSyntaxMetrics().getAllSymbol() != null && aCliCtx.getSyntaxMetrics().getAllSymbol().length() != 0 ) {
					theSyntax += " " + aCliCtx.getSyntaxMetrics().getEndListSymbol();
				}
			}
			return theSyntax;
		}
		return "";
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public AllCondition withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the one and only child of this {@link Term}.
	 * 
	 * @return The according child.
	 */
	protected Term getChild() {
		return _children[0];
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private String toPossibleCause( final String[] aRemainderArgs ) {
		String theMessage = "";
		final Condition[] theParents = toConditions( aRemainderArgs );
		if ( theParents != null && theParents.length != 0 ) {
			ArgsSyntaxException eException;
			for ( Condition eCondition : theParents ) {
				eException = eCondition.getException();
				if ( eException != null ) {
					eException = eException.toRootMatchCause();
					theMessage += ( theMessage.length() != 0 ? " " : "" ) + MessageDetails.POSSIBLE_CAUSE + ": " + eException.toShortMessage();
					if ( eException.getSuppressed() != null && eException.getSuppressed().length != 0 ) {
						for ( var eSuppressed : eException.getSuppressed() ) {
							if ( eSuppressed instanceof ArgsSyntaxException eSyntaxException ) {
								theMessage += " " + eSyntaxException.toRootMatchCause().toShortMessage();
							}
						}
					}
				}
			}
		}
		return theMessage;
	}

	private String toString( String[] aArgs ) {
		final StringBuilder theBuilder = new StringBuilder();
		for ( int i = 0; i < aArgs.length; i++ ) {
			theBuilder.append( "\"" + aArgs[i] + "\"" );
			if ( i < aArgs.length - 1 ) {
				theBuilder.append( ", " );
			}
		}
		return theBuilder.toString();
	}
}
