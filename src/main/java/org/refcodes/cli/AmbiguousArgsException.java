// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Thrown in case the command line arguments do not match the required syntax.
 */
public class AmbiguousArgsException extends ArgsSyntaxException {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public AmbiguousArgsException( String aMessage, String[] aArgs, Term aSource ) {
		super( aMessage, aArgs, aSource );
	}

	/**
	 * {@inheritDoc}
	 */
	public AmbiguousArgsException( String aMessage, String[] aArgs, Term aSource, String aErrorCode ) {
		super( aMessage, aArgs, aSource, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public AmbiguousArgsException( String aMessage, String[] aArgs, Term aSource, Throwable aCause ) {
		super( aMessage, aArgs, aSource, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public AmbiguousArgsException( String aMessage, String[] aArgs, Term aSource, Throwable aCause, String aErrorCode ) {
		super( aMessage, aArgs, aSource, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public AmbiguousArgsException( String[] aArgs, Term aSource, Throwable aCause ) {
		super( aArgs, aSource, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public AmbiguousArgsException( String[] aArgs, Term aSource, Throwable aCause, String aErrorCode ) {
		super( aArgs, aSource, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public AmbiguousArgsException( String aMessage, String[] aArgs, String aErrorCode ) {
		super( aMessage, aArgs, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public AmbiguousArgsException( String aMessage, String[] aArgs, Throwable aCause ) {
		super( aMessage, aArgs, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public AmbiguousArgsException( String aMessage, String[] aArgs, Throwable aCause, String aErrorCode ) {
		super( aMessage, aArgs, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public AmbiguousArgsException( String[] aArgs, Throwable aCause ) {
		super( aArgs, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public AmbiguousArgsException( String[] aArgs, Throwable aCause, String aErrorCode ) {
		super( aArgs, aCause, aErrorCode );
	}
}
