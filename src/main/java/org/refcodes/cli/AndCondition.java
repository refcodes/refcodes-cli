// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.ArrayList;
import java.util.List;

/**
 * An {@link AndCondition} contains (represents) a list of {@link Condition}
 * ({@link Term}) instances (nested by the {@link AndCondition}) of which all
 * are to be parsed successfully as of invoking the
 * {@link Condition#parseArgs(String[], String[], CliContext)} methods. The
 * command line arguments syntax <code>-a &amp; -b &amp; -c</code> specifies
 * that all "-a", "-b" and "-c" must be set. In case at least one is not set,
 * then the {@link AndCondition} will terminate the
 * {@link #parseArgs(String[], String[], CliContext)} method with an exception.
 * If one argument is optional, then consider nesting it in an
 * {@link AnyCondition}.
 */
public class AndCondition extends AbstractCondition {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link AndCondition} with the {@link Term}
	 * ({@link Condition}) instances to be nested.
	 *
	 * @param aArgs The {@link Term} ({@link Condition}) instances to be nested.
	 */
	public AndCondition( Term... aArgs ) {
		super( "All (AND) syntax branches must match from the provided command line arguments.", aArgs );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Operand<?>[] parseArgs( String[] aArgs, String[] aOptions, CliContext aCliCtx ) throws ArgsSyntaxException {
		final List<Operand<?>> theResult = new ArrayList<>();
		Operand<?>[] eOperands;
		ArgsSyntaxException theCause = null;
		for ( Term eChild : _children ) {
			try {
				eOperands = eChild.parseArgs( aArgs, aOptions, aCliCtx );
				if ( eOperands != null && eOperands.length != 0 ) {
					for ( var eOperand : eOperands ) {
						theResult.add( eOperand );
					}
				}
				aArgs = toArgsDiff( aArgs, eOperands );
			}
			// catch ( UnknownArgsException | AmbiguousArgsException e ) {
			catch ( ArgsSyntaxException e ) {
				if ( theCause == null ) {
					theCause = e;
				}
				else {
					theCause.addSuppressed( e );
				}
			}
		}
		if ( theCause != null ) {
			throw _exception = new ArgsSyntaxException( "At least one syntax branch did not match the command line arguments, though all (AND) syntax branches must be matched by the command line arguments!", aArgs, this, theCause );
		}
		return theResult.toArray( new Operand<?>[theResult.size()] );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSynopsis( CliContext aCliCtx ) {
		String theSynopsis = "";
		for ( Term eChild : _children ) {
			if ( eChild.isVisible() ) {
				if ( theSynopsis.length() != 0 ) {
					theSynopsis += aCliCtx.getSyntaxMetrics().getAndSymbol() != null && aCliCtx.getSyntaxMetrics().getAndSymbol().length() != 0 ? " " + aCliCtx.getSyntaxMetrics().getAndSymbol() + " " : " ";
				}
				theSynopsis += eChild.toSyntax( aCliCtx );
			}
		}
		return theSynopsis;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSyntax( CliContext aCliCtx ) {
		return toSynopsis( aCliCtx );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public AndCondition withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}
}
