// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.ArrayList;
import java.util.List;

/**
 * Any of the nested {@link Condition} conditions may match for the
 * {@link AnyCondition} to match, e.g. all of the nested conditions are
 * optional. The {@link AnyCondition} can nested by an {@link AndCondition} and
 * encapsulates a {@link Condition} which may be provided but does not
 * necessarily needs to be provided in order for the {@link AndCondition} to
 * successfully parse the nested {@link Condition} items including the
 * {@link AnyCondition}.
 */
public class AnyCondition extends AbstractCondition {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link AnyCondition} with the {@link Term}
	 * ({@link Condition}) instances to be nested.
	 *
	 * @param aArgs The {@link Term} ({@link Condition}) instances to be nested
	 */
	public AnyCondition( Term... aArgs ) {
		super( "Any (OPTIONAL) syntax branches may optionally match from the command line arguments, though not all command line arguments are required to match.", aArgs );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Operand<?>[] parseArgs( String[] aArgs, String[] aOptions, CliContext aCliCtx ) throws ArgsSyntaxException {
		final List<Operand<?>> theResult = new ArrayList<>();
		Operand<?>[] eOperands = null;
		ArgsSyntaxException theCause = null;
		for ( Term eChild : _children ) {
			try {
				eOperands = eChild.parseArgs( aArgs, aOptions, aCliCtx );
				if ( eOperands != null ) {
					for ( var eOperand : eOperands ) {
						theResult.add( eOperand );
					}
				}
				aArgs = toArgsDiff( aArgs, eOperands );
			}
			catch ( ArgsSyntaxException e ) {
				if ( theCause == null ) {
					theCause = e;
				}
				else {
					theCause.addSuppressed( e );
				}
			}
		}
		if ( theCause != null ) {
			_exception = new ArgsSyntaxException( "At least one syntax branch did not match the command line arguments, anyhow the syntax branches may just optionally (ANY) be matched by the command line arguments!", aArgs, this, theCause );
		}
		return theResult.toArray( new Operand<?>[theResult.size()] );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		super.reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSyntax( CliContext aCliCtx ) {
		if ( _children != null && _children.length > 0 ) {
			String theSyntax = "";
			for ( Term eChild : _children ) {
				if ( eChild.isVisible() ) {
					if ( theSyntax.length() != 0 ) {
						theSyntax += " ";
					}
					theSyntax += aCliCtx.getSyntaxMetrics().getAnySymbol() != null && aCliCtx.getSyntaxMetrics().getAnySymbol().length() != 0 ? aCliCtx.getSyntaxMetrics().getAnySymbol() : "";
					theSyntax += aCliCtx.getSyntaxMetrics().getBeginOptionalSymbol() != null && aCliCtx.getSyntaxMetrics().getBeginOptionalSymbol().length() != 0 ? aCliCtx.getSyntaxMetrics().getBeginOptionalSymbol() + " " : "";
					theSyntax += eChild.toSyntax( aCliCtx );
					theSyntax += aCliCtx.getSyntaxMetrics().getEndOptionalSymbol() != null && aCliCtx.getSyntaxMetrics().getEndOptionalSymbol().length() != 0 ? " " + aCliCtx.getSyntaxMetrics().getEndOptionalSymbol() : "";
				}
			}
			return theSyntax;
		}
		return "";
	}

	/**
	 * Retrieves the {@link ArgsSyntaxException} exception caught while invoking
	 * the {@link #parseArgs(String[], CliContext)} method. The exception (and
	 * the according suppressed ones alongside the causes) is not thrown by the
	 * {@link #parseArgs(String[], CliContext)} method as of the semantics of
	 * this {@link AnyCondition}! Nevertheless, the exception can be used to
	 * analyze argument parsing failure (e.g. for better error messages).
	 * 
	 * @return The {@link ArgsSyntaxException} exception occurring while parsing
	 *         the arguments.
	 */
	@Override
	public ArgsSyntaxException getException() {
		return super.getException();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public AnyCondition withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}
}
