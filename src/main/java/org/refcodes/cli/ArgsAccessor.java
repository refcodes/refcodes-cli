// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a command line arguments (short "args") array.
 */
public interface ArgsAccessor {

	/**
	 * Retrieves the command line arguments from the args property.
	 * 
	 * @return The command line arguments stored by the args property.
	 */
	String[] getArgs();

	/**
	 * Provides a mutator for a args property.
	 */
	public interface ArgsMutator {

		/**
		 * Sets the command line arguments for the args property.
		 * 
		 * @param aArgs The command line arguments to be stored by the args
		 *        property.
		 */
		void setArgs( String[] aArgs );
	}

	/**
	 * Provides a args property.
	 */
	public interface ArgsProperty extends ArgsAccessor, ArgsMutator {

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs: Sets the given {@link String} array
		 * (setter) as of {@link #setArgs(String[])} and returns the very same
		 * value (getter).
		 * 
		 * @param aArgs The arguments to set (via {@link #setArgs(String[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String[] letArgs( String[] aArgs ) {
			setArgs( aArgs );
			return aArgs;
		}
	}
}
