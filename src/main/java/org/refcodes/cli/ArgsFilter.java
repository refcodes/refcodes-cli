// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Enumeration declaring command line argument filter.
 */
public enum ArgsFilter {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Java system properties are to be filtered.
	 */
	D(Pattern.compile( "^-D[\\S\\s]*" )),

	/**
	 * Matches {@link #D} as well as {@link #XX}.
	 */
	D_XX(Pattern.compile( "(^-D[\\S\\s]*)|(^-XX:[\\S\\s]*)" )),

	/**
	 * No arguments are filtered.
	 */
	NONE(null),

	/**
	 * JVM command-line options that are specified with <code>-XX:</code> are
	 * not checked for validity.
	 */
	XX(Pattern.compile( "^-XX:[\\S\\s]*" ));

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	/** The pattern. */
	private Pattern _pattern;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new args filter.
	 *
	 * @param aArgsFilter the pattern
	 */
	private ArgsFilter( Pattern aArgsFilter ) {
		_pattern = aArgsFilter;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Filters the provided args by including the args matching this
	 * enumerations criteria. (opposite of {@link #toFiltered(List)}.
	 * 
	 * @param aArgs The args to be filtered.
	 * 
	 * @return The filtered args.
	 */
	public List<String> fromFilter( List<String> aArgs ) {
		return fromFilter( aArgs, _pattern );
	}

	/**
	 * Filters the provided args by including the args matching this
	 * enumerations criteria. (opposite of {@link #toFiltered(String[])}
	 * 
	 * @param aArgs The args to be filtered.
	 * 
	 * @return The filtered args.
	 */
	public String[] fromFilter( String[] aArgs ) {
		return fromFilter( aArgs, _pattern );
	}

	/**
	 * Returns the args {@link Pattern} used to filter (exclude) the according
	 * args.
	 * 
	 * @return The according filter {@link Pattern}.
	 */
	public Pattern getFilter() {
		return _pattern;
	}

	/**
	 * Filters the provided args by excluding (filtering) the args matching this
	 * enumerations criteria.
	 * 
	 * @param aArgs The args to be filtered.
	 * 
	 * @return The filtered args.
	 */
	public List<String> toFiltered( List<String> aArgs ) {
		return toFiltered( aArgs, _pattern );
	}

	/**
	 * Filters the provided args by excluding (filtering) the args matching this
	 * enumerations criteria.
	 * 
	 * @param aArgs The args to be filtered.
	 * 
	 * @return The filtered args.
	 */
	public String[] toFiltered( String[] aArgs ) {
		return toFiltered( aArgs, _pattern );
	}

	/**
	 * Filters the provided args by including the args matching the provided
	 * {@link Pattern} (opposite of {@link #toFiltered(List, Pattern)}.
	 * 
	 * @param aArgs The args to be filtered.
	 * @param aArgsFilter The {@link Pattern} to be used for filtering.
	 * 
	 * @return The filtered args.
	 */
	public static List<String> fromFilter( List<String> aArgs, Pattern aArgsFilter ) {
		final List<String> theArgs = new ArrayList<>();
		if ( aArgsFilter == null ) {
			return theArgs;
		}
		for ( String eArg : aArgs ) {
			if ( aArgsFilter.matcher( eArg ).matches() ) {
				theArgs.add( eArg );
			}
		}
		return theArgs;
	}

	/**
	 * Filters the provided args by including the args matching the provided
	 * {@link Pattern} (opposite of {@link #toFiltered(String[], Pattern)}.
	 * 
	 * @param aArgs The args to be filtered.
	 * @param aArgsFilter The {@link Pattern} to be used for filtering.
	 * 
	 * @return The filtered args.
	 */
	public static String[] fromFilter( String[] aArgs, Pattern aArgsFilter ) {
		if ( aArgsFilter == null ) {
			return new String[0];
		}
		final List<String> theArgs = new ArrayList<>();
		for ( String eArg : aArgs ) {
			if ( aArgsFilter.matcher( eArg ).matches() ) {
				theArgs.add( eArg );
			}
		}
		return theArgs.toArray( new String[theArgs.size()] );
	}

	/**
	 * Retrieves a {@link ArgsFilter} depending on the given string, ignoring
	 * the case as well as being graceful regarding "-" and "_",.
	 *
	 * @param aValue The name of the {@link ArgsFilter} to be interpreted
	 *        graceful.
	 * 
	 * @return The {@link ArgsFilter} being determined or null if none was
	 *         found.
	 */
	public static ArgsFilter toArgsFilter( String aValue ) {
		aValue = aValue != null ? aValue.replaceAll( "-", "" ).replaceAll( "_", "" ) : aValue;
		for ( ArgsFilter eValue : values() ) {
			if ( eValue.name().replaceAll( "-", "" ).replaceAll( "_", "" ).equalsIgnoreCase( aValue ) ) {
				return eValue;
			}
		}
		return null;
	}

	/**
	 * Filters the provided args by excluding (filtering) the args matching the
	 * provided {@link Pattern}.
	 * 
	 * @param aArgs The args to be filtered.
	 * @param aArgsFilter The {@link Pattern} to be used for filtering.
	 * 
	 * @return The filtered args.
	 */
	public static List<String> toFiltered( List<String> aArgs, Pattern aArgsFilter ) {
		if ( aArgsFilter == null ) {
			return aArgs;
		}
		final List<String> theArgs = new ArrayList<>();
		for ( String eArg : aArgs ) {
			if ( !aArgsFilter.matcher( eArg ).matches() ) {
				theArgs.add( eArg );
			}
		}
		return theArgs;
	}

	/**
	 * Filters the provided args by excluding (filtering) the args matching the
	 * provided {@link Pattern}.
	 * 
	 * @param aArgs The args to be filtered.
	 * @param aArgsFilter The {@link Pattern} to be used for filtering.
	 * 
	 * @return The filtered args.
	 */
	public static String[] toFiltered( String[] aArgs, Pattern aArgsFilter ) {
		if ( aArgsFilter == null ) {
			return aArgs;
		}
		final List<String> theArgs = new ArrayList<>();
		for ( String eArg : aArgs ) {
			if ( !aArgsFilter.matcher( eArg ).matches() ) {
				theArgs.add( eArg );
			}
		}
		return theArgs.toArray( new String[theArgs.size()] );
	}
}
