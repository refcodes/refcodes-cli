// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.refcodes.data.AnsiEscapeCode;
import org.refcodes.data.ArgsPrefix;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.data.ConsoleDimension;
import org.refcodes.data.Delimiter;
import org.refcodes.data.License;
import org.refcodes.data.Literal;
import org.refcodes.graphical.BoxBorderMode;
import org.refcodes.mixin.Approvable;
import org.refcodes.runtime.Arguments;
import org.refcodes.runtime.Execution;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.runtime.Terminal;
import org.refcodes.struct.PropertyImpl;
import org.refcodes.struct.RelationImpl;
import org.refcodes.textual.AsciiArtBuilder;
import org.refcodes.textual.AsciiArtMode;
import org.refcodes.textual.ColumnWidthType;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.HorizAlignTextBuilder;
import org.refcodes.textual.HorizAlignTextMode;
import org.refcodes.textual.PixmapRatioMode;
import org.refcodes.textual.SplitTextMode;
import org.refcodes.textual.TableBuilder;
import org.refcodes.textual.TableStyle;
import org.refcodes.textual.TextBlockBuilder;
import org.refcodes.textual.TextBorderBuilder;
import org.refcodes.textual.TextBoxGrid;
import org.refcodes.textual.TextBoxStyle;
import org.refcodes.textual.TextLineBuilder;
import org.refcodes.textual.VerboseTextBuilder;

/**
 * A straightforward implementation of the {@link ParseArgs} interface. The
 * constructor only provides means to set the required attributes as the
 * attributes to be adjusted optionally are already sufficiently preconfigured.
 * For adjusting them, a flavor of the Builder-Pattern is provided with which
 * you can easily chain the configuration of this instance; as them methods
 * return the instance of this class being configured. This helps to prevent the
 * telescoping constructor anti-pattern.
 * <p>
 * The {@link SyntaxNotation} is pre-set with the {@link SyntaxNotation#LOGICAL}
 * notation.
 * <p>
 * The console width id preconfigured with the console's width as determined by
 * the <code>SystemUtility.getTerminalWidth()</code>.
 * <p>
 * The standard out {@link PrintStream} is preconfigured with the
 * {@link System#out} {@link PrintStream}.
 * <p>
 * The newline characters to be used for line breaks is "\r\n" on Windows
 * machines and "\"n" on all other machines as of the
 * <code>SystemUtility.getLineBreak()</code>.
 *
 * @see "http://en.wikipedia.org/wiki/Builder_pattern"
 */
public class ArgsParser implements ParseArgs {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( ArgsParser.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NULL_NAME = "<null>";
	private static final String DEFAULT_NAME = "foobar";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Term _argsSyntax;
	private String _argumentEscapeCode = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.DEFAULT_FOREGROUND_COLOR, AnsiEscapeCode.FAINT, AnsiEscapeCode.ITALIC );
	private String _bannerBorderEscapeCode = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.DEFAULT_FOREGROUND_COLOR, AnsiEscapeCode.FAINT );
	private String _bannerEscapeCode = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.DEFAULT_FOREGROUND_COLOR, AnsiEscapeCode.BOLD );
	private Font _bannerFont = new Font( FontFamily.DIALOG, FontStyle.PLAIN, 12 );
	private char[] _bannerFontPalette = AsciiColorPalette.HALFTONE_GRAY.getPalette();
	private String _commandEscapeCode = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.DEFAULT_FOREGROUND_COLOR, AnsiEscapeCode.ITALIC );
	private int _consoleWidth = Terminal.toHeuristicWidth();
	private String _copyrightNote = License.COPYRIGHT_NOTE.getText();
	private String _description = "See the syntax declaration for usage, see the descriptions for the short- and the long-options. Option arguments are noted in angle brackets.";
	private String _descriptionEscapeCode = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.DEFAULT_FOREGROUND_COLOR );
	private List<Example> _examples = null;
	private boolean _hasOverrideSeparatorLnChar = false;
	private boolean _isEscapeCodesEnabled = Terminal.isAnsiTerminalEnabled();
	private String _licenseNote = License.LICENSE_NOTE.getText();;
	private String _lineBreak = Terminal.getLineBreak();
	private String _lineSeparatorEscapeCode = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.DEFAULT_FOREGROUND_COLOR, AnsiEscapeCode.FAINT );
	private String _longOptionPrefix = SyntaxNotation.DEFAULT.getLongOptionPrefix();
	private int _maxConsoleWidth = -1;
	private String _name = DEFAULT_NAME;
	private String _optionEscapeCode = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.DEFAULT_FOREGROUND_COLOR, AnsiEscapeCode.BOLD );
	private String _resetEscapeCode = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.RESET );
	private Character _shortOptionPrefix = SyntaxNotation.DEFAULT.getShortOptionPrefix();
	private SyntaxMetrics _syntaxMetrics = SyntaxNotation.DEFAULT;
	private TextBoxGrid _textBoxGrid = TextBoxStyle.toExecutionTextBoxGrid();
	private char _separatorLnChar = _textBoxGrid.getInnerLine();
	private String _title = null;
	protected PrintStream _errStream = Execution.toSystemErr();
	protected PrintStream _stdStream = Execution.toSystemOut();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link ParseArgs} instance without any restrictions to the
	 * parsed arguments. The constructor only provides means to set the required
	 * attributes as the attributes to be adjusted optionally are already
	 * sufficiently preconfigured. For adjusting them, a flavor of the
	 * Builder-Pattern is provided with which you can easily chain the
	 * configuration of this instance; as them methods return the instance of
	 * this class being configured.
	 */
	public ArgsParser() {
		this( (Term) null );
	}

	/**
	 * Constructs the {@link ParseArgs} instance without any restrictions to the
	 * parsed arguments. The constructor only provides means to set the required
	 * attributes as the attributes to be adjusted optionally are already
	 * sufficiently preconfigured. For adjusting them, a flavor of the
	 * Builder-Pattern is provided with which you can easily chain the
	 * configuration of this instance; as them methods return the instance of
	 * this class being configured.
	 *
	 * @param aCliCtx The {@link CliContext} to be used for initializing.
	 */
	public ArgsParser( CliContext aCliCtx ) {
		this( null, aCliCtx );
	}

	/**
	 * Constructs the {@link ParseArgs} instance with the given root
	 * {@link Condition} and the default {@link SyntaxNotation#LOGICAL}. The
	 * constructor only provides means to set the required attributes as the
	 * attributes to be adjusted optionally are already sufficiently
	 * preconfigured. For adjusting them, a flavor of the Builder-Pattern is
	 * provided with which you can easily chain the configuration of this
	 * instance; as them methods return the instance of this class being
	 * configured.
	 * 
	 * @param aArgsSyntax The args syntax root {@link Term} node being the node
	 *        from which parsing the command line arguments starts.
	 */
	public ArgsParser( Term aArgsSyntax ) {
		_argsSyntax = aArgsSyntax;
	}

	/**
	 * Constructs the {@link ParseArgs} instance with the given root
	 * {@link Condition} and the default {@link SyntaxNotation#LOGICAL}. The
	 * constructor only provides means to set the required attributes as the
	 * attributes to be adjusted optionally are already sufficiently
	 * preconfigured. For adjusting them, a flavor of the Builder-Pattern is
	 * provided with which you can easily chain the configuration of this
	 * instance; as them methods return the instance of this class being
	 * configured.
	 *
	 * @param aArgsSyntax The args syntax root {@link Term} node being the node
	 *        from which parsing the command line arguments starts.
	 * @param aCliCtx The {@link CliContext} to be used for initializing.
	 */
	public ArgsParser( Term aArgsSyntax, CliContext aCliCtx ) {
		_argsSyntax = aArgsSyntax;
		_syntaxMetrics = aCliCtx.getSyntaxMetrics();
		_argumentEscapeCode = aCliCtx.getArgumentEscapeCode();
		_optionEscapeCode = aCliCtx.getOptionEscapeCode();
		_resetEscapeCode = aCliCtx.getResetEscapeCode();
		_isEscapeCodesEnabled = aCliCtx.isEscapeCodesEnabled();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addExample( Example aExampleUsage ) {
		if ( _examples == null ) {
			synchronized ( this ) {
				if ( _examples == null ) {
					_examples = new ArrayList<>();
				}
			}
		}
		_examples.add( aExampleUsage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void errorLn( String aLine ) {
		if ( aLine == null ) {
			aLine = "null";
		}
		final String[] theLines = new TextBlockBuilder().withText( aLine ).withColumnWidth( _consoleWidth ).withSplitTextMode( SplitTextMode.AT_SPACE ).toStrings();
		_errStream.print( fromTextBlock( theLines, toLineBreak() ) );
		_errStream.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Operand<?>[] evalArgs( String[] aArgs ) throws ArgsSyntaxException {
		// Any command line filtering system property set? |-->
		final String theFilterProperty = SystemProperty.ARGS_FILTER.getValue();
		if ( theFilterProperty != null && theFilterProperty.length() != 0 ) {
			final ArgsFilter theArgsFilter = ArgsFilter.toArgsFilter( theFilterProperty );
			if ( theArgsFilter == null ) {
				LOGGER.log( Level.WARNING, "The provided system property <" + SystemProperty.ARGS_FILTER.getKey() + "> with value <" + SystemProperty.ARGS_FILTER.getValue() + "> does not resolve to a valid args filter! Valid args filter values are: " + VerboseTextBuilder.asString( ArgsFilter.values() ) );
			}
			else {
				aArgs = theArgsFilter.toFiltered( aArgs );
			}
		}
		// Any command line filtering system property set? <--|
		final CliContext theCliCtx = toCliContext( _isEscapeCodesEnabled );
		if ( _argsSyntax == null ) {
			final var theOperands = fromArgs( aArgs, getDelimiter() );
			approve( theOperands, theCliCtx );
			return theOperands;
		}
		else {
			aArgs = theCliCtx.toExpandOptions( aArgs );
			try {
				final var theOperands = _argsSyntax.parseArgs( aArgs, theCliCtx );
				final String[] theSuperflousArgs = AbstractTerm.toArgsDiff( aArgs, theOperands );
				if ( theSuperflousArgs != null && theSuperflousArgs.length > 0 ) {
					throw new SuperfluousArgsException( "Superfluous command arguments " + new VerboseTextBuilder().withElements( theSuperflousArgs ).toString() + " were provided but cannot be evaluated or are not supported as of the given combination of arguments.", theSuperflousArgs, _argsSyntax );
				}
				approve( theOperands, theCliCtx );
				return theOperands;
			}
			catch ( ArgsSyntaxException e ) {
				//	ArgsSyntaxException theException = toHeuristicCause();
				//	throw (theException != null ? theException : e);
				throw e;
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Term getArgsSyntax() {
		return _argsSyntax;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getArgumentEscapeCode() {
		return _argumentEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getBannerBorderEscapeCode() {
		return _bannerBorderEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getBannerEscapeCode() {
		return _bannerEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Font getBannerFont() {
		return _bannerFont;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char[] getBannerFontPalette() {
		return _bannerFontPalette;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCommandEscapeCode() {
		return _commandEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getConsoleWidth() {
		return _consoleWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCopyright() {
		return _copyrightNote;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDescription() {
		return _description;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDescriptionEscapeCode() {
		return _descriptionEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Example[] getExamples() {
		return _examples != null ? _examples.toArray( new Example[_examples.size()] ) : null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLicense() {
		return _licenseNote;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLineBreak() {
		return _lineBreak;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLineSeparatorEscapeCode() {
		return _lineSeparatorEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLongOptionPrefix() {
		return _longOptionPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMaxConsoleWidth() {
		return _maxConsoleWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return _name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getOptionEscapeCode() {
		return _optionEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getResetEscapeCode() {
		return _resetEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getSeparatorLnChar() {
		return _separatorLnChar;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getShortOptionPrefix() {
		return _shortOptionPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SyntaxMetrics getSyntaxMetrics() {
		return _syntaxMetrics;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TextBoxGrid getTextBoxGrid() {
		return _textBoxGrid;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getTitle() {
		return _title;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEscapeCodesEnabled() {
		return _isEscapeCodesEnabled;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printBanner() {
		printBanner( _stdStream );
	}

	/**
	 * Hook for intercepting the {@link PrintStream} to evaluate the result.
	 * 
	 * @param aStdStream The (intercepting) {@link PrintStream}.
	 */
	void printBanner( PrintStream aStdStream ) {
		int theBannerWidth = _consoleWidth - 4;
		String[] theCanvas = new AsciiArtBuilder().withText( _title != null ? _title : ( _name != null ? _name : NULL_NAME ) ).withFont( _bannerFont ).withAsciiColors( _bannerFontPalette ).withColumnWidth( theBannerWidth ).withAsciiArtMode( AsciiArtMode.NORMAL ).withPixmapRatioMode( PixmapRatioMode.ONE_HALF ).toStrings();
		final boolean hasBorder = hasBorder( theCanvas );
		if ( hasBorder ) {
			theBannerWidth = _consoleWidth - 2;
		}
		theCanvas = new AsciiArtBuilder().withText( _title != null ? _title : _name ).withFont( _bannerFont ).withAsciiColors( _bannerFontPalette ).withColumnWidth( theBannerWidth ).withAsciiArtMode( AsciiArtMode.NORMAL ).withPixmapRatioMode( PixmapRatioMode.ONE_HALF ).toStrings();
		theCanvas = new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.CENTER ).withText( theCanvas ).withColumnWidth( theBannerWidth ).withFillChar( ' ' ).toStrings();
		if ( !hasBorder ) {
			theCanvas = new TextBorderBuilder().withBoxBorderMode( BoxBorderMode.ALL ).withText( theCanvas ).withBorderWidth( 1 ).withBorderChar( ' ' ).toStrings();
		}
		theCanvas = new TextBorderBuilder().withTextBoxGrid( _textBoxGrid ).withText( theCanvas ).withBoxBorderMode( BoxBorderMode.ALL ).toStrings();

		if ( _isEscapeCodesEnabled ) {
			theCanvas[0] = _bannerBorderEscapeCode + theCanvas[0] + _resetEscapeCode;
			if ( theCanvas.length > 1 ) {
				for ( int i = 1; i < theCanvas.length - 1; i++ ) {
					theCanvas[i] = _bannerBorderEscapeCode + theCanvas[i].substring( 0, 1 ) + _resetEscapeCode + _bannerEscapeCode + theCanvas[i].substring( 1, theCanvas[i].length() - 1 ) + _resetEscapeCode + _bannerBorderEscapeCode + theCanvas[i].substring( theCanvas[i].length() - 1 ) + _resetEscapeCode;
				}
			}
			theCanvas[theCanvas.length - 1] = _bannerBorderEscapeCode + theCanvas[theCanvas.length - 1] + _resetEscapeCode;
		}
		aStdStream.print( fromTextBlock( theCanvas, toLineBreak() ) );
		aStdStream.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printCopyright() {
		printCopyright( _stdStream );
	}

	/**
	 * Hook for intercepting the {@link PrintStream} to evaluate the result.
	 * 
	 * @param aStdStream The (intercepting) {@link PrintStream}.
	 */
	void printCopyright( PrintStream aStdStream ) {
		aStdStream.println( _copyrightNote );
		aStdStream.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printDescription() {
		printDescription( _stdStream );
	}

	/**
	 * Hook for intercepting the {@link PrintStream} to evaluate the result.
	 * 
	 * @param aStdStream The (intercepting) {@link PrintStream}.
	 */
	void printDescription( PrintStream aStdStream ) {
		aStdStream.println( ( _isEscapeCodesEnabled ? _descriptionEscapeCode : "" ) + _description + ( _isEscapeCodesEnabled ? _resetEscapeCode : "" ) );
		aStdStream.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printExamples() {
		printExamples( _stdStream );
	}

	/**
	 * Hook for intercepting the {@link PrintStream} to evaluate the result.
	 * 
	 * @param aStdStream The (intercepting) {@link PrintStream}.
	 */
	void printExamples( PrintStream aStdStream ) {
		if ( _examples != null && _examples.size() != 0 ) {
			int theMaxLength = 0;
			String eDescription;
			for ( Example eUsage : _examples ) {
				eDescription = eUsage._description;
				if ( eDescription.length() > theMaxLength ) {
					theMaxLength = eDescription.length();
				}
			}
			if ( theMaxLength > _consoleWidth / 2 ) {
				theMaxLength = _consoleWidth / 2;
				if ( theMaxLength < ConsoleDimension.MIN_WIDTH.getValue() / 2 ) {
					theMaxLength = ConsoleDimension.MIN_WIDTH.getValue() / 2;
				}
			}

			final TableBuilder theTable = new TableBuilder().withTableStyle( TableStyle.BLANK_HEADER_BLANK_BODY ).withRowWidth( _consoleWidth ).withPrintStream( aStdStream ).withEscapeCodesEnabled( _isEscapeCodesEnabled ).withLeftBorder( false ).withRightBorder( false ).withDividerLine( false );
			if ( _isEscapeCodesEnabled ) {
				theTable.setResetEscapeCode( _resetEscapeCode );
			}
			theTable.addColumn().withColumnHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withColumnWidth( theMaxLength, ColumnWidthType.ABSOLUTE );
			if ( _isEscapeCodesEnabled ) {
				theTable.withRowColumnEscapeCode( _descriptionEscapeCode );
			}
			theTable.addColumn().withColumnWidth( 2, ColumnWidthType.ABSOLUTE );
			theTable.addColumn().withColumnWidth( _name.length(), ColumnWidthType.ABSOLUTE );
			theTable.addColumn().withColumnWidth( 1, ColumnWidthType.ABSOLUTE );
			theTable.addColumn().withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT ).withColumnSplitTextMode( SplitTextMode.AT_SPACE );
			theTable.withLineBreak( toLineBreak() );

			final String theCommand = ( _isEscapeCodesEnabled ? _commandEscapeCode : "" ) + _name + ( _isEscapeCodesEnabled ? _resetEscapeCode : "" );
			if ( _examples != null && _examples.size() != 0 ) {
				final CliContext theCliContext = toCliContext( _isEscapeCodesEnabled );
				for ( Example eUsage : _examples ) {
					String eArgs = "";
					String eOpt;
					for ( Operand<?> eOperand : eUsage.getOperands() ) {
						if ( eArgs.length() != 0 ) {
							eArgs += " ";
						}
						eOpt = eOperand.toUsage( theCliContext );
						eArgs += eOpt;
					}
					theTable.printRowContinue( eUsage.getDescription(), ": ", theCommand, " ", eArgs );
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printHeader() {
		printHeader( _stdStream );
	}

	/**
	 * Hook for intercepting the {@link PrintStream} to evaluate the result.
	 * 
	 * @param aStdStream The (intercepting) {@link PrintStream}.
	 */
	void printHeader( PrintStream aStdStream ) {
		int theBannerWidth = _consoleWidth - 4;
		String[] theCanvas = AsciiArtBuilder.asSimpleBanner( _title != null ? _title : _name, theBannerWidth, AsciiArtMode.NORMAL, _bannerFontPalette );
		final boolean hasBorder = hasBorder( theCanvas );
		if ( hasBorder ) {
			theBannerWidth = _consoleWidth - 2;
		}
		theCanvas = AsciiArtBuilder.asSimpleBanner( _title != null ? _title : _name, theBannerWidth, AsciiArtMode.NORMAL, _bannerFontPalette );
		theCanvas = new HorizAlignTextBuilder().withHorizAlignTextMode( HorizAlignTextMode.CENTER ).withText( theCanvas ).withColumnWidth( theBannerWidth ).withFillChar( ' ' ).toStrings();
		if ( !hasBorder ) {
			theCanvas = new TextBorderBuilder().withBoxBorderMode( BoxBorderMode.ALL ).withText( theCanvas ).withBorderWidth( 1 ).withBorderChar( ' ' ).toStrings();
		}
		theCanvas = new TextBorderBuilder().withTextBoxGrid( _textBoxGrid ).withText( theCanvas ).withBoxBorderMode( BoxBorderMode.ALL ).toStrings();

		if ( _isEscapeCodesEnabled ) {
			theCanvas[0] = _bannerBorderEscapeCode + theCanvas[0] + _resetEscapeCode;
			if ( theCanvas.length > 1 ) {
				for ( int i = 1; i < theCanvas.length - 1; i++ ) {
					theCanvas[i] = _bannerBorderEscapeCode + theCanvas[i].substring( 0, 1 ) + _resetEscapeCode + _bannerEscapeCode + theCanvas[i].substring( 1, theCanvas[i].length() - 1 ) + _resetEscapeCode + _bannerBorderEscapeCode + theCanvas[i].substring( theCanvas[i].length() - 1 ) + _resetEscapeCode;
				}
			}
			theCanvas[theCanvas.length - 1] = _bannerBorderEscapeCode + theCanvas[theCanvas.length - 1] + _resetEscapeCode;
		}
		aStdStream.print( fromTextBlock( theCanvas, toLineBreak() ) );
		aStdStream.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printLicense() {
		printLicense( _stdStream );
	}

	/**
	 * Hook for intercepting the {@link PrintStream} to evaluate the result.
	 * 
	 * @param aStdStream The (intercepting) {@link PrintStream}.
	 */
	void printLicense( PrintStream aStdStream ) {
		aStdStream.println( _licenseNote );
		aStdStream.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printLn() {
		printLn( _stdStream );
	}

	/**
	 * Hook for intercepting the {@link PrintStream} to evaluate the result.
	 * 
	 * @param aStdStream The (intercepting) {@link PrintStream}.
	 */
	void printLn( PrintStream aStdStream ) {
		aStdStream.println();
		aStdStream.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printLn( String aLine ) {
		printLn( aLine, _stdStream );
	}

	/**
	 * Hook for intercepting the {@link PrintStream} to evaluate the result.
	 *
	 * @param aLine The line to be printed.
	 * @param aStdStream The (intercepting) {@link PrintStream}.
	 */
	void printLn( String aLine, PrintStream aStdStream ) {
		final String[] theLines = new TextBlockBuilder().withText( aLine ).withColumnWidth( _consoleWidth ).withSplitTextMode( SplitTextMode.AT_SPACE ).toStrings();
		aStdStream.print( fromTextBlock( theLines, toLineBreak() ) );
		aStdStream.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printOptions() {
		printOptions( _stdStream );
	}

	/**
	 * Hook for intercepting the {@link PrintStream} to evaluate the result.
	 * 
	 * @param aStdStream The (intercepting) {@link PrintStream}.
	 */
	void printOptions( PrintStream aStdStream ) {
		if ( _argsSyntax != null ) {
			final Operand<?>[] theOperands;
			if ( _argsSyntax instanceof Condition theCondition ) {
				theOperands = theCondition.toOperands();
			}
			else if ( _argsSyntax instanceof Operand<?> theOperand ) {
				theOperands = new Operand<?>[] { theOperand };
			}
			else {
				throw new IllegalStateException( "The args syntax of type <" + _argsSyntax.getClass().getName() + "> is neither of type <" + Condition.class.getName() + "> nor <" + Operand.class.getName() + "> and therefor no operands are available for processing!" );
			}
			final Map<String, String[]> theOptArgs = new HashMap<>();
			int theMaxLength = 0;
			String eSpec;
			int eEffectiveLength;
			if ( theOperands != null && theOperands.length != 0 ) {
				final CliContext theCliContext = toCliContext( _isEscapeCodesEnabled );
				for ( Operand<?> eOperand : theOperands ) {
					if ( eOperand.isVisible() ) {
						eSpec = theCliContext.toSpec( eOperand );
						eEffectiveLength = AnsiEscapeCode.toLength( eSpec, _isEscapeCodesEnabled );
						if ( eEffectiveLength > theMaxLength ) {
							theMaxLength = eEffectiveLength;
						}
						theOptArgs.put( eSpec, new String[] { eSpec, ":", " ", eOperand.getDescription() } );
					}
				}
			}
			final TableBuilder theTable = new TableBuilder().withTableStyle( TableStyle.BLANK_HEADER_BLANK_BODY ).withRowWidth( _consoleWidth ).withPrintStream( aStdStream ).withEscapeCodesEnabled( _isEscapeCodesEnabled ).withLeftBorder( false ).withRightBorder( false ).withDividerLine( false );
			theTable.addColumn().withColumnHorizAlignTextMode( HorizAlignTextMode.RIGHT ).withColumnWidth( theMaxLength, ColumnWidthType.ABSOLUTE );
			theTable.addColumn().withColumnWidth( 1, ColumnWidthType.ABSOLUTE );
			theTable.addColumn().withColumnWidth( 1, ColumnWidthType.ABSOLUTE );
			theTable.addColumn().withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT ).withColumnSplitTextMode( SplitTextMode.AT_SPACE );
			if ( _isEscapeCodesEnabled ) {
				theTable.withRowColumnEscapeCode( _descriptionEscapeCode );
			}
			theTable.withLineBreak( toLineBreak() );
			final List<String> theKeys = new ArrayList<>( theOptArgs.keySet() );
			theKeys.sort( new Comparator<String>() {
				@Override
				public int compare( String a, String b ) {
					if ( a != null ) {
						while ( a.startsWith( "-" ) ) {
							a = a.substring( 1 );
						}
					}
					if ( b != null ) {
						while ( b.startsWith( "-" ) ) {
							b = b.substring( 1 );
						}
					}
					return a.compareToIgnoreCase( b );
				}
			} );
			for ( String eKey : theKeys ) {
				theTable.printRowContinue( theOptArgs.get( eKey ) );
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printSeparatorLn() {
		printSeparatorLn( _stdStream );
	}

	/**
	 * Hook for intercepting the {@link PrintStream} to evaluate the result.
	 * 
	 * @param aStdStream The (intercepting) {@link PrintStream}.
	 */
	void printSeparatorLn( PrintStream aStdStream ) {
		String theLine = new TextLineBuilder().withColumnWidth( _consoleWidth ).withLineChar( _separatorLnChar ).toString() + toLineBreak();
		if ( _isEscapeCodesEnabled ) {
			theLine = _lineSeparatorEscapeCode + theLine + _resetEscapeCode;
		}
		aStdStream.print( theLine );
		aStdStream.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printSynopsis() {
		printSynopsis( _stdStream );
	}

	/**
	 * Hook for intercepting the {@link PrintStream} to evaluate the result.
	 * 
	 * @param aStdStream The (intercepting) {@link PrintStream}.
	 */
	void printSynopsis( PrintStream aStdStream ) {
		final Term[] theChildren;
		if ( _argsSyntax instanceof XorCondition theCases ) {
			theChildren = theCases._children;
		}
		else {
			theChildren = new Term[] { _argsSyntax };
		}
		final TableBuilder theTable = new TableBuilder().withTableStyle( TableStyle.BLANK_HEADER_BLANK_BODY ).withRowWidth( _consoleWidth ).withPrintStream( aStdStream ).withEscapeCodesEnabled( _isEscapeCodesEnabled ).withLeftBorder( false ).withRightBorder( false ).withDividerLine( true );
		if ( _isEscapeCodesEnabled ) {
			theTable.setResetEscapeCode( _resetEscapeCode );
		}
		theTable.addColumn().withColumnWidth( _name != null ? _name.length() : 0, ColumnWidthType.ABSOLUTE ).withColumnHorizAlignTextMode( HorizAlignTextMode.RIGHT );
		theTable.addColumn().withColumnHorizAlignTextMode( HorizAlignTextMode.LEFT ).withColumnSplitTextMode( SplitTextMode.AT_SPACE );
		theTable.withLineBreak( toLineBreak() );

		final String theCommand = ( _isEscapeCodesEnabled ? _commandEscapeCode : "" ) + ( _name != null ? _name : "" ) + ( _isEscapeCodesEnabled ? _resetEscapeCode : "" );
		String eUsage;
		for ( Term eChild : theChildren ) {
			eUsage = ( eChild != null ) ? eChild.toSynopsis( toCliContext( _isEscapeCodesEnabled ) ) : "<?>";
			theTable.printRowContinue( theCommand, eUsage );
		}
		aStdStream.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		if ( _argsSyntax != null ) {
			_argsSyntax.reset();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setArgsSyntax( Term aArgsSyntax ) {
		_argsSyntax = aArgsSyntax;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setArgumentEscapeCode( String aParamEscapeCode ) {
		_argumentEscapeCode = aParamEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBannerBorderEscapeCode( String aBannerBorderEscapeCode ) {
		_bannerBorderEscapeCode = aBannerBorderEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBannerEscapeCode( String aBannerEscapeCode ) {
		_bannerEscapeCode = aBannerEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBannerFont( Font aBannerFont ) {
		_bannerFont = aBannerFont;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBannerFontPalette( AsciiColorPalette aBannerFontPalette ) {
		_bannerFontPalette = aBannerFontPalette.getPalette();

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBannerFontPalette( char[] aColorPalette ) {
		_bannerFontPalette = aColorPalette;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCommandEscapeCode( String aCommandEscapeCode ) {
		_commandEscapeCode = aCommandEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setConsoleWidth( int aConsoleWidth ) {
		_consoleWidth = _maxConsoleWidth != -1 ? ( _maxConsoleWidth < aConsoleWidth ? _maxConsoleWidth : aConsoleWidth ) : aConsoleWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCopyright( String aCopyrightNote ) {
		_copyrightNote = aCopyrightNote;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDescription( String aDescription ) {
		_description = aDescription;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDescriptionEscapeCode( String aDescriptionEscapeCode ) {
		_descriptionEscapeCode = aDescriptionEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setErrorOut( PrintStream aErrorOut ) {
		_errStream = aErrorOut;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEscapeCodesEnabled( boolean isEscapeCodesEnabled ) {
		_isEscapeCodesEnabled = isEscapeCodesEnabled;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setExamples( Example[] aExamples ) {
		_examples = new ArrayList<>( Arrays.asList( aExamples ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLicense( String aLicenseNote ) {
		_licenseNote = aLicenseNote;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLineBreak( String aLineBreak ) {
		if ( aLineBreak == null ) {
			aLineBreak = Terminal.getLineBreak();
		}
		_lineBreak = aLineBreak;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLineSeparatorEscapeCode( String aLineSeparatorEscapeCode ) {
		_lineSeparatorEscapeCode = aLineSeparatorEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLongOptionPrefix( String aLongOptionPrefix ) {
		_longOptionPrefix = aLongOptionPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMaxConsoleWidth( int aMaxConsoleWidth ) {
		_maxConsoleWidth = aMaxConsoleWidth;
		_consoleWidth = _maxConsoleWidth != -1 ? ( _maxConsoleWidth < _consoleWidth ? _maxConsoleWidth : _consoleWidth ) : _consoleWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setName( String aName ) {
		_name = aName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setOptionEscapeCode( String aOptEscapeCode ) {
		_optionEscapeCode = aOptEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setResetEscapeCode( String aResetEscapeCode ) {
		_resetEscapeCode = aResetEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSeparatorLnChar( char aSeparatorLnChar ) {
		_separatorLnChar = aSeparatorLnChar;
		_hasOverrideSeparatorLnChar = true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setShortOptionPrefix( Character aShortOptionPrefix ) {
		_shortOptionPrefix = aShortOptionPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStandardOut( PrintStream aStandardOut ) {
		_stdStream = aStandardOut;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSyntaxMetrics( SyntaxMetrics aSyntaxMetrics ) {
		_syntaxMetrics = aSyntaxMetrics;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTextBoxGrid( TextBoxGrid aTextBoxGrid ) {
		_textBoxGrid = aTextBoxGrid;
		if ( !_hasOverrideSeparatorLnChar ) {
			_separatorLnChar = aTextBoxGrid.getInnerLine();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTitle( String aTitle ) {
		_title = aTitle;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withAddExample( Example aExamples ) {
		addExample( aExamples );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withAddExample( String aDescription, Operand<?>... aOperands ) {
		addExample( aDescription, aOperands );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withArgsSyntax( Term aArgsSyntax ) {
		setArgsSyntax( aArgsSyntax );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withArgumentEscapeCode( String aParamEscCode ) {
		setArgumentEscapeCode( aParamEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withBannerBorderEscapeCode( String aBannerBorderEscCode ) {
		setBannerBorderEscapeCode( aBannerBorderEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withBannerEscapeCode( String aBannerEscCode ) {
		setBannerEscapeCode( aBannerEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withBannerFont( Font aBannerFont ) {
		setBannerFont( aBannerFont );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withBannerFontPalette( AsciiColorPalette aBannerFontPalette ) {
		setBannerFontPalette( aBannerFontPalette );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withBannerFontPalette( char[] aColorPalette ) {
		setBannerFontPalette( aColorPalette );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withCommandEscapeCode( String aCommandEscCode ) {
		setCommandEscapeCode( aCommandEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withConsoleWidth( int aConsoleWidth ) {
		setConsoleWidth( aConsoleWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withCopyright( String aCopyright ) {
		setCopyright( aCopyright );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withDescription( String aDescription ) {
		setDescription( aDescription );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withDescriptionEscapeCode( String aDescriptionEscCode ) {
		setDescriptionEscapeCode( aDescriptionEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withErrorOut( PrintStream aErrorOut ) {
		setErrorOut( aErrorOut );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withEscapeCodesEnabled( boolean aIsEscCodeEnabled ) {
		setEscapeCodesEnabled( aIsEscCodeEnabled );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withEvalArgs( List<String> aArgs ) throws ArgsSyntaxException {
		evalArgs( aArgs );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withEvalArgs( List<String> aArgs, ArgsFilter aArgsFilter ) throws ArgsSyntaxException {
		evalArgs( aArgs, aArgsFilter );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withEvalArgs( List<String> aArgs, Pattern aFilterExp ) throws ArgsSyntaxException {
		evalArgs( aArgs, aFilterExp );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withEvalArgs( String[] aArgs ) throws ArgsSyntaxException {
		evalArgs( aArgs );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withEvalArgs( String[] aArgs, ArgsFilter aArgsFilter ) throws ArgsSyntaxException {
		evalArgs( aArgs, aArgsFilter );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withEvalArgs( String[] aArgs, Pattern aFilterExp ) throws ArgsSyntaxException {
		evalArgs( aArgs, aFilterExp );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withExamples( Collection<Example> aExamples ) {
		setExamples( aExamples );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withExamples( Example[] aExamples ) {
		setExamples( aExamples );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withLicense( String aLicense ) {
		setLicense( aLicense );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withLineBreak( String aLineBreak ) {
		setLineBreak( aLineBreak );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withLineSeparatorEscapeCode( String aLineSeparatorEscCode ) {
		setLineSeparatorEscapeCode( aLineSeparatorEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withLongOptionPrefix( String aLongOptionPrefix ) {
		setLongOptionPrefix( aLongOptionPrefix );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withMaxConsoleWidth( int aMaxConsoleWidth ) {
		setMaxConsoleWidth( aMaxConsoleWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withName( String aName ) {
		setName( aName );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withOptionEscapeCode( String aOptEscCode ) {
		setOptionEscapeCode( aOptEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withResetEscapeCode( String aResetEscCode ) {
		setResetEscapeCode( aResetEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withSeparatorLnChar( char aSeparatorLnChar ) {
		setSeparatorLnChar( aSeparatorLnChar );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withShortOptionPrefix( Character aShortOptionPrefix ) {
		setShortOptionPrefix( aShortOptionPrefix );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withStandardOut( PrintStream aStandardOut ) {
		setStandardOut( aStandardOut );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withSyntaxMetrics( SyntaxMetrics aSyntaxMetrics ) {
		setSyntaxMetrics( aSyntaxMetrics );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withSyntaxMetrics( SyntaxNotation aSyntaxNotation ) {
		setSyntaxMetrics( aSyntaxNotation );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withTextBoxGrid( TextBoxGrid aTextBoxGrid ) {
		setTextBoxGrid( aTextBoxGrid );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withTextBoxGrid( TextBoxStyle aTextBoxStyle ) {
		setTextBoxGrid( aTextBoxStyle );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArgsParser withTitle( String aTitle ) {
		setTitle( aTitle );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * From text block.
	 *
	 * @param aTextBlock the text block
	 * @param aDelimeter the deimeter
	 * 
	 * @return the string
	 */
	private String fromTextBlock( String[] aTextBlock, String aDelimeter ) {
		final StringBuilder theBuilder = new StringBuilder();
		for ( String eString : aTextBlock ) {
			if ( aDelimeter != null && aDelimeter.length() != 0 ) {
				if ( theBuilder.length() > 0 ) {
					theBuilder.append( aDelimeter );
				}
			}
			theBuilder.append( eString );
		}
		return theBuilder.toString() + toLineBreak();
	}

	private boolean hasBorder( String[] aCanvas ) {
		if ( aCanvas != null && aCanvas.length != 0 ) {
			char ePrevChar = aCanvas[0].length() != 0 ? aCanvas[0].charAt( 0 ) : ' ';
			// Top |-->
			for ( int i = 0; i < aCanvas[0].length(); i++ ) {
				if ( ePrevChar != aCanvas[0].charAt( i ) ) {
					return false;
				}
				ePrevChar = aCanvas[0].charAt( i );
			}
			// Bottom |-->
			for ( int i = 0; i < aCanvas[aCanvas.length - 1].length(); i++ ) {
				if ( ePrevChar != aCanvas[aCanvas.length - 1].charAt( i ) ) {
					return false;
				}
				ePrevChar = aCanvas[aCanvas.length - 1].charAt( i );
			}
			// Left |-->
			for ( String aCanva : aCanvas ) {
				if ( ePrevChar != aCanva.charAt( 0 ) ) {
					return false;
				}
				ePrevChar = aCanva.charAt( 0 );
			}
			// Right |-->
			for ( String aCanva : aCanvas ) {
				if ( ePrevChar != aCanva.charAt( aCanva.length() - 1 ) ) {
					return false;
				}
				ePrevChar = aCanva.charAt( aCanva.length() - 1 );
			}
		}
		return true;
	}

	/**
	 * The delimiter to be used for colliding command line args when creating
	 * non colliding arg's aliases (keys for key/value-pairs). Used when parsing
	 * args without any syntax definition.
	 * 
	 * @return The according delimiter for solving colliding command line args.
	 */
	protected char getDelimiter() {
		return Delimiter.INDEX.getChar();
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private CliContext toCliContext( boolean isEscapeCodesEnabled ) {
		final String theNotationProperty = SystemProperty.ARGS_NOTATION.getValue();
		final SyntaxNotation theSyntaxNotation = SyntaxNotation.toSyntaxNotation( theNotationProperty );
		final CliContext theCliCtx = new CliContext( this, theSyntaxNotation, isEscapeCodesEnabled );
		return theCliCtx;
	}

	private String toLineBreak() {
		if ( Terminal.isLineBreakRequired( _consoleWidth ) ) {
			return _lineBreak;
		}
		return "";
	}

	private static void approve( Operand<?>[] aOperands, CliContext aCliCtx ) throws ArgsSyntaxException {
		for ( Operand<?> eOperand : aOperands ) {
			if ( eOperand instanceof AbstractOperand<?> e ) { // Using an AbstractOperand, we need not unveil the approve() method to API users
				try {
					e.approve();
				}
				catch ( RuntimeException exc ) {
					final String theSpec = eOperand.toSpec( aCliCtx );
					throw new ParseArgsException( "Unable to parse provided argument(s) {0} for operand \"" + theSpec + "\"!", eOperand.getParsedArgs(), eOperand, exc );
				}
			}
			else if ( eOperand instanceof Approvable e ) { // Using an Approvable, we enable third party Operand implementations to be used
				try {
					e.approve();
				}
				catch ( RuntimeException exc ) {
					final String theSpec = eOperand.toSpec( aCliCtx );
					throw new ParseArgsException( "Unable to parse provided argument(s) {0} for operand \"" + theSpec + "\"!", eOperand.getParsedArgs(), eOperand, exc );
				}
			}
		}
	}

	/**
	 * Heuristically loads the arguments without any syntax required, e.g.
	 * without any root {@link Condition} to be set.
	 * 
	 * @param aArgs The arguments to be loaded.
	 * @param aDelimiter The delimiter to resolve name clashes.
	 * 
	 * @return A list of heuristically determined {@link Flag} and
	 *         {@link StringOperand} instances.
	 */
	private static Operand<?>[] fromArgs( String[] aArgs, char aDelimiter ) {
		final List<Operand<?>> theResult = new ArrayList<>();
		final Map<String, String> theArgs = Arguments.toProperties( aArgs, ArgsPrefix.toPrefixes(), aDelimiter );
		for ( String eKey : theArgs.keySet() ) {
			if ( Literal.TRUE.getValue().equalsIgnoreCase( theArgs.get( eKey ) ) ) {
				theResult.add( new Flag( new RelationImpl<>( eKey, Boolean.TRUE ) ) );
			}
			else {
				theResult.add( new StringOperand( new PropertyImpl( eKey, theArgs.get( eKey ) ) ) );
			}
		}
		return theResult.toArray( new Operand[theResult.size()] );
	}
}
