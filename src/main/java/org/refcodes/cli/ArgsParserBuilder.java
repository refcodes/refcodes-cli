// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.List;
import java.util.regex.Pattern;

/**
 * This mixin provides builder additions (as of the builder pattern for chained
 * configuring method calls) for parsing command line arguments.
 *
 * @param <B> the generic type
 */
public interface ArgsParserBuilder<B extends ArgsParserBuilder<B>> {

	/**
	 * Evaluates the provided command line arguments .
	 * 
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @return This instance as of the builder pattern to chain further method
	 *         calls.
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	default B withEvalArgs( List<String> aArgs ) throws ArgsSyntaxException {
		return withEvalArgs( aArgs.toArray( new String[aArgs.size()] ) );
	}

	/**
	 * Evaluates the provided command line arguments, filtering (excluding) the
	 * arguments matching the provided {@link ArgsFilter}.
	 * 
	 * @param aArgs The command line arguments to be evaluated.
	 * @param aArgsFilter The {@link ArgsFilter} used to filter (exclude) any
	 *        "unwanted" or otherwise evaluated arguments.
	 * 
	 * @return This instance as of the builder pattern to chain further method
	 *         calls.
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	default B withEvalArgs( List<String> aArgs, ArgsFilter aArgsFilter ) throws ArgsSyntaxException {
		return withEvalArgs( aArgsFilter.toFiltered( aArgs ) );
	}

	/**
	 * Evaluates the provided command line arguments, filtering (excluding) the
	 * arguments matching the provided {@link Pattern}.
	 * 
	 * @param aArgs The command line arguments to be evaluated.
	 * @param aFilterExp The {@link Pattern} used to filter (exclude) any
	 *        "unwanted" or otherwise evaluated arguments.
	 * 
	 * @return This instance as of the builder pattern to chain further method
	 *         calls.
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	default B withEvalArgs( List<String> aArgs, Pattern aFilterExp ) throws ArgsSyntaxException {
		return withEvalArgs( ArgsFilter.toFiltered( aArgs, aFilterExp ) );
	}

	/**
	 * Evaluates the provided command line arguments.
	 * 
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @return This instance as of the builder pattern to chain further method
	 *         calls.
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	B withEvalArgs( String[] aArgs ) throws ArgsSyntaxException;

	/**
	 * Evaluates the provided command line arguments, filtering (excluding) the
	 * arguments matching the provided {@link ArgsFilter}.
	 * 
	 * @param aArgs The command line arguments to be evaluated.
	 * @param aArgsFilter The {@link ArgsFilter} used to filter (exclude) any
	 *        "unwanted" or otherwise evaluated arguments.
	 * 
	 * @return This instance as of the builder pattern to chain further method
	 *         calls.
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	default B withEvalArgs( String[] aArgs, ArgsFilter aArgsFilter ) throws ArgsSyntaxException {
		return withEvalArgs( aArgsFilter.toFiltered( aArgs ) );
	}

	/**
	 * Evaluates the provided command line arguments, filtering (excluding) the
	 * arguments matching the provided {@link Pattern}.
	 * 
	 * @param aArgs The command line arguments to be evaluated.
	 * @param aFilterExp The {@link Pattern} used to filter (exclude) any
	 *        "unwanted" or otherwise evaluated arguments.
	 * 
	 * @return This instance as of the builder pattern to chain further method
	 *         calls.
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	default B withEvalArgs( String[] aArgs, Pattern aFilterExp ) throws ArgsSyntaxException {
		return withEvalArgs( ArgsFilter.toFiltered( aArgs, aFilterExp ) );
	}
}
