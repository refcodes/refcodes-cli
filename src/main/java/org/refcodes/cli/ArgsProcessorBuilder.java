// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.io.PrintStream;

import org.refcodes.cli.ArgsSyntaxAccessor.ArgsSyntaxBuilder;
import org.refcodes.cli.ArgumentEscapeCodeAccessor.ArgumentEscapeCodeBuilder;
import org.refcodes.cli.BannerBorderEscapeCodeAccessor.BannerBorderEscapeCodeBuilder;
import org.refcodes.cli.BannerEscapeCodeAccessor.BannerEscapeCodeBuilder;
import org.refcodes.cli.BannerFontAccessor.BannerFontBuilder;
import org.refcodes.cli.BannerFontPaletteAccessor.BannerFontPaletteBuilder;
import org.refcodes.cli.CommandEscapeCodeAccessor.CommandEscapeCodeBuilder;
import org.refcodes.cli.CopyrightAccessor.CopyrightBuilder;
import org.refcodes.cli.DescriptionEscapeCodeAccessor.DescriptionEscapeCodeBuilder;
import org.refcodes.cli.ExamplesAccessor.ExamplesBuilder;
import org.refcodes.cli.LicenseAccessor.LicenseBuilder;
import org.refcodes.cli.LineSeparatorEscapeCodeAccessor.LineSeparatorEscapeCodeBuilder;
import org.refcodes.cli.LongOptionPrefixAccessor.LongOptionPrefixBuilder;
import org.refcodes.cli.OptionEscapeCodeAccessor.OptionEscapeCodeBuilder;
import org.refcodes.cli.ShortOptionPrefixAccessor.ShortOptionPrefixBuilder;
import org.refcodes.cli.SyntaxMetricsAccessor.SyntaxMetricsBuilder;
import org.refcodes.mixin.ConsoleWidthAccessor.ConsoleWidthBuilder;
import org.refcodes.mixin.DescriptionAccessor.DescriptionBuilder;
import org.refcodes.mixin.EscapeCodesStatusAccessor.EscapeCodeStatusBuilder;
import org.refcodes.mixin.LineBreakAccessor.LineBreakBuilder;
import org.refcodes.mixin.MaxConsoleWidthAccessor.MaxConsoleWidthBuilder;
import org.refcodes.mixin.NameAccessor.NameBuilder;
import org.refcodes.mixin.ResetEscapeCodeAccessor.ResetEscapeCodeBuilder;
import org.refcodes.mixin.TitleAccessor.TitleBuilder;
import org.refcodes.textual.TextBoxGridAccessor.TextBoxGridBuilder;

/**
 * The {@link ArgsProcessorBuilder} provides common minimum builder
 * functionality required by to build an argument processing instance.
 * 
 * @param <B> The type of the builder to be returned for chaining of operations.
 */
public interface ArgsProcessorBuilder<B extends ArgsProcessorBuilder<B>> extends LineBreakBuilder<B>, LineSeparatorEscapeCodeBuilder<B>, DescriptionEscapeCodeBuilder<B>, LicenseBuilder<B>, CopyrightBuilder<B>, ConsoleWidthBuilder<B>, MaxConsoleWidthBuilder<B>, BannerFontPaletteBuilder<B>, BannerFontBuilder<B>, CommandEscapeCodeBuilder<B>, BannerEscapeCodeBuilder<B>, BannerBorderEscapeCodeBuilder<B>, ExamplesBuilder<B>, TitleBuilder<B>, NameBuilder<B>, DescriptionBuilder<B>, EscapeCodeStatusBuilder<B>, ResetEscapeCodeBuilder<B>, OptionEscapeCodeBuilder<B>, ArgumentEscapeCodeBuilder<B>, SyntaxMetricsBuilder<B>, ShortOptionPrefixBuilder<B>, LongOptionPrefixBuilder<B>, TextBoxGridBuilder<B>, ArgsSyntaxBuilder<B> {

	/**
	 * Builder method adding an application's {@link Example} element.
	 * 
	 * @param aExamples The application's {@link Example} element to be added.
	 * 
	 * @return This builder as of the builder pattern.
	 */
	B withAddExample( Example aExamples );

	/**
	 * Builder method adding an application's {@link Example} element.
	 * 
	 * @param aDescription The example's description.
	 * @param aOperands The command line arguments required by the example.
	 * 
	 * @return This builder as of the builder pattern.
	 */
	B withAddExample( String aDescription, Operand<?>... aOperands );

	/**
	 * Set the error out {@link PrintStream} and make other adjustments with the
	 * result (with regards to the Builder-Pattern).
	 * 
	 * @param aErrorOut The error out {@link PrintStream} to set.
	 * 
	 * @return This instance with regard to the builder pattern.
	 */
	B withErrorOut( PrintStream aErrorOut );

	/**
	 * Builder method adding an separator line char.
	 * 
	 * @param aSeparatorLnChar THe according separator line char to use.
	 * 
	 * @return This builder as of the builder pattern.
	 */
	B withSeparatorLnChar( char aSeparatorLnChar );

	/**
	 * Set the standard out {@link PrintStream} and make other adjustments with
	 * the result (with regards to the Builder-Pattern).
	 * 
	 * @param aStandardOut The standard out {@link PrintStream} to set.
	 * 
	 * @return This instance with regard to the builder pattern.
	 */
	B withStandardOut( PrintStream aStandardOut );
}
