// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.List;
import java.util.regex.Pattern;

/**
 * The {@link ArgsProvidierBuilder} interface provides builder methods for
 * feeding command line arguments.
 *
 * @param <B> The builder type for chaining builder methods.
 */
public interface ArgsProvidierBuilder<B extends ArgsProvidierBuilder<B>> {

	/**
	 * Builder method providing the application's command line arguments.
	 * 
	 * @param aArgs The application's command line arguments.
	 * 
	 * @return This builder as of the builder pattern.
	 */
	B withArgs( List<String> aArgs );

	/**
	 * Builder method providing the application's command line arguments,
	 * filtering (excluding) the arguments matching the provided
	 * {@link ArgsFilter}.
	 * 
	 * @param aArgs The application's command line arguments.
	 * @param aArgsFilter The {@link ArgsFilter} used to filter (exclude) any
	 *        "unwanted" or otherwise evaluated arguments.
	 * 
	 * @return This instance as of the builder pattern to chain further method
	 *         calls.
	 */
	default B withArgs( List<String> aArgs, ArgsFilter aArgsFilter ) {
		return withArgs( aArgsFilter.toFiltered( aArgs.toArray( new String[aArgs.size()] ) ) );
	}

	/**
	 * Builder method providing the application's command line arguments,
	 * filtering (excluding) the arguments matching the provided
	 * {@link Pattern}.
	 * 
	 * @param aArgs The application's command line arguments.
	 * @param aFilterExp The {@link Pattern} used to filter (exclude) any
	 *        "unwanted" or otherwise evaluated arguments.
	 * 
	 * @return This instance as of the builder pattern to chain further method
	 *         calls.
	 */
	default B withArgs( List<String> aArgs, Pattern aFilterExp ) {
		return withArgs( ArgsFilter.toFiltered( aArgs.toArray( new String[aArgs.size()] ), aFilterExp ) );
	}

	/**
	 * Builder method providing the application's command line arguments.
	 * 
	 * @param aArgs The application's command line arguments.
	 * 
	 * @return This builder as of the builder pattern.
	 */
	B withArgs( String[] aArgs );

	/**
	 * Builder method providing the application's command line arguments,
	 * filtering (excluding) the arguments matching the provided
	 * {@link ArgsFilter}.
	 * 
	 * @param aArgs The application's command line arguments.
	 * @param aArgsFilter The {@link ArgsFilter} used to filter (exclude) any
	 *        "unwanted" or otherwise evaluated arguments.
	 * 
	 * @return This instance as of the builder pattern to chain further method
	 *         calls.
	 */
	default B withArgs( String[] aArgs, ArgsFilter aArgsFilter ) {
		return withArgs( aArgsFilter.toFiltered( aArgs ) );
	}

	/**
	 * Builder method providing the application's command line arguments,
	 * filtering (excluding) the arguments matching the provided
	 * {@link Pattern}.
	 * 
	 * @param aArgs The application's command line arguments.
	 * @param aFilterExp The {@link Pattern} used to filter (exclude) any
	 *        "unwanted" or otherwise evaluated arguments.
	 * 
	 * @return This instance as of the builder pattern to chain further method
	 *         calls.
	 */
	default B withArgs( String[] aArgs, Pattern aFilterExp ) {
		return withArgs( ArgsFilter.toFiltered( aArgs, aFilterExp ) );
	}
}
