// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a (root) {@link Term} property.
 */
public interface ArgsSyntaxAccessor {

	/**
	 * Provides a builder method for a (root) {@link Term} property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ArgsSyntaxBuilder<B extends ArgsSyntaxBuilder<B>> {

		/**
		 * Sets the (root) {@link Term} for the (root) {@link Term} property.
		 * 
		 * @param aArgsSyntax The (root) {@link Term} to be stored by the (root)
		 *        {@link Term} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withArgsSyntax( Term aArgsSyntax );
	}

	/**
	 * Provides a mutator for a (root) {@link Term} property.
	 */
	public interface ArgsSyntaxMutator {

		/**
		 * Sets the (root) {@link Term} for the (root) {@link Term} property.
		 * 
		 * @param aArgsSyntax The (root) {@link Term} to be stored by the (root)
		 *        {@link Term} property.
		 */
		void setArgsSyntax( Term aArgsSyntax );
	}

	/**
	 * Provides a (root) {@link Term} property.
	 */
	public interface ArgsSyntaxProperty extends ArgsSyntaxAccessor, ArgsSyntaxMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Condition}
		 * (setter) as of {@link #setArgsSyntax(Term)} and returns the very same
		 * value (getter).
		 * 
		 * @param aArgsSyntax The {@link Condition} to set (via
		 *        {@link #setArgsSyntax(Term)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Term letArgsSyntax( Term aArgsSyntax ) {
			setArgsSyntax( aArgsSyntax );
			return aArgsSyntax;
		}
	}

	/**
	 * Retrieves the (root) {@link Term} from the (root) {@link Term} property.
	 * 
	 * @return The (root) {@link Term} stored by the (root) {@link Term}
	 *         property.
	 */
	Term getArgsSyntax();
}
