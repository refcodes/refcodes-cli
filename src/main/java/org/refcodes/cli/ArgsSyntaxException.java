// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.ArrayList;
import java.util.List;

import org.refcodes.cli.CliException.CliArgsException;
import org.refcodes.exception.MessageDetails;
import org.refcodes.mixin.SourceAccessor;

/**
 * Thrown in case of a command line arguments mismatch regarding provided and
 * expected args.
 */
public class ArgsSyntaxException extends CliArgsException implements SourceAccessor<Term>, MatchCountAccessor {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected Term _source;
	protected int _matchCount;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSource The {@link Term} responsible for this exception (source).
	 */
	public ArgsSyntaxException( String aMessage, String[] aArgs, Term aSource ) {
		super( aMessage, aArgs );
		_source = aSource;
		_matchCount = aSource.getMatchCount();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSource The {@link Term} responsible for this exception (source).
	 */
	public ArgsSyntaxException( String aMessage, String[] aArgs, Term aSource, String aErrorCode ) {
		super( aMessage, aArgs, aErrorCode );
		_source = aSource;
		_matchCount = aSource.getMatchCount();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSource The {@link Term} responsible for this exception (source).
	 */
	public ArgsSyntaxException( String aMessage, String[] aArgs, Term aSource, Throwable aCause ) {
		super( aMessage, aArgs, aCause );
		_source = aSource;
		_matchCount = aSource.getMatchCount();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSource The {@link Term} responsible for this exception (source).
	 */
	public ArgsSyntaxException( String aMessage, String[] aArgs, Term aSource, Throwable aCause, String aErrorCode ) {
		super( aMessage, aArgs, aCause, aErrorCode );
		_source = aSource;
		_matchCount = aSource.getMatchCount();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSource The {@link Term} responsible for this exception (source).
	 */
	public ArgsSyntaxException( String[] aArgs, Term aSource, Throwable aCause ) {
		super( aArgs, aCause );
		_source = aSource;
		_matchCount = aSource.getMatchCount();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aSource The {@link Term} responsible for this exception (source).
	 */
	public ArgsSyntaxException( String[] aArgs, Term aSource, Throwable aCause, String aErrorCode ) {
		super( aArgs, aCause, aErrorCode );
		_source = aSource;
		_matchCount = aSource.getMatchCount();
	}

	/**
	 * {@inheritDoc}
	 */
	public ArgsSyntaxException( String aMessage, String[] aArgs ) {
		super( aMessage, aArgs );
	}

	/**
	 * {@inheritDoc}
	 */
	public ArgsSyntaxException( String aMessage, String[] aArgs, String aErrorCode ) {
		super( aMessage, aArgs, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ArgsSyntaxException( String aMessage, String[] aArgs, Throwable aCause ) {
		super( aMessage, aArgs, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public ArgsSyntaxException( String aMessage, String[] aArgs, Throwable aCause, String aErrorCode ) {
		super( aMessage, aArgs, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public ArgsSyntaxException( String[] aArgs, Throwable aCause ) {
		super( aArgs, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public ArgsSyntaxException( String[] aArgs, Throwable aCause, String aErrorCode ) {
		super( aArgs, aCause, aErrorCode );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines the number of args being matched upon encountering this
	 * exception.
	 * 
	 * @return The number of args matching till this exception occurred.
	 */
	@Override
	public int getMatchCount() {
		return _matchCount; // Don't talk to strangers, directly provide the match count.
	}

	/**
	 * Returns the {@link Term} responsible for this exception.
	 * 
	 * @return The {@link Term} where this exception occurred.
	 */
	@Override
	public Term getSource() {
		return _source;
	}

	/**
	 * Tries to determine the most verbose description of this exception by
	 * crawling the exception tree down the causes and suppressed exceptions and
	 * evaluating their {@link Term} source's {@link Term#getMatchCount()}
	 * argument match counts.
	 * 
	 * @return The heuristic and verbose cause of this exception.
	 */
	public String toHeuristicMessage() {
		if ( getMatchCount() > 0 ) {
			final List<ArgsSyntaxException> theExceptions = toMatchAnyExceptions( this );
			if ( !theExceptions.isEmpty() ) {
				final StringBuilder theBuilder = new StringBuilder();
				for ( ArgsSyntaxException eArgsSyntaxException : theExceptions ) {
					if ( theBuilder.length() != 0 ) {
						theBuilder.append( " " + MessageDetails.POSSIBLE_CAUSE + ": " );
					}
					if ( toArgsSyntaxExceptionDepth( eArgsSyntaxException ) <= 1 ) {
						theBuilder.append( eArgsSyntaxException.toMessage() );
					}
					else {
						theBuilder.append( eArgsSyntaxException.toShortMessage() );
					}
				}
				return theBuilder.toString();
			}
		}
		return "None of the provided arguments clearly matched any syntax branch requirements.";
	}

	/**
	 * Tries to determine the most likely cause of this exception by crawling
	 * the exception tree down the causes and suppressed exceptions and
	 * evaluating their {@link Term} source's {@link Term#getMatchCount()}
	 * argument match counts.
	 * 
	 * @return The heuristic cause of this exception.
	 */
	//	public String toVerboseHeuristicMessage() {
	//		final ArgsSyntaxException theRootCause = toRootMatchCause();
	//		int theMatchCount = theRootCause.getMatchCount();
	//		final List<ArgsSyntaxException> theCauses = new ArrayList<>();
	//		theCauses.add( theRootCause );
	//		if ( getSuppressed() != null ) {
	//			for ( Throwable eSuppressed : getSuppressed() ) {
	//				if ( eSuppressed instanceof ArgsSyntaxException theException ) {
	//					theException = theException.toRootMatchCause();
	//					if ( theException.getMatchCount() == theMatchCount ) {
	//						theCauses.add( theException );
	//					}
	//					else if ( theException.getMatchCount() > theMatchCount ) {
	//						theMatchCount = theException.getMatchCount();
	//						theCauses.clear();
	//						theCauses.add( theException );
	//					}
	//				}
	//			}
	//		}
	//		if ( !theCauses.isEmpty() ) {
	//			final StringBuilder theMessage = new StringBuilder();
	//			final Iterator<ArgsSyntaxException> e = theCauses.iterator();
	//			ArgsSyntaxException eException;
	//			while ( e.hasNext() ) {
	//				eException = e.next();
	//				theMessage.append( eException.toShortMessage() );
	//				if ( e.hasNext() ) {
	//					theMessage.append( ' ' );
	//				}
	//			}
	//			return theMessage.toString();
	//		}
	//		return toShortMessage();
	//	}

	/**
	 * Determines the root cause of this exception with the same match count
	 * ({@link #getMatchCount()}) as this exception.
	 * 
	 * @return The actual root cause of this exception or this exception if no
	 *         (more specific) root cause was determined.
	 */
	public ArgsSyntaxException toRootMatchCause() {
		return toRootMatchCause( this );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getPatternArguments() {
		return new Object[] { _args, _source, _matchCount };
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static int toArgsSyntaxExceptionDepth( Throwable aException ) {
		int theDepth = 0;
		while ( aException.getCause() != null && aException instanceof ArgsSyntaxException && aException.getCause() != aException ) {
			aException = aException.getCause();
			theDepth++;
		}
		return theDepth;
	}

	private static List<ArgsSyntaxException> toMatchAnyExceptions( ArgsSyntaxException aArgsSyntaxException ) {
		final List<ArgsSyntaxException> theExceptions = new ArrayList<>();
		final List<ArgsSyntaxException> eExceptions = toRootLevelExceptions( aArgsSyntaxException );
		for ( ArgsSyntaxException eException : eExceptions ) {
			if ( eException.getMatchCount() > 0 ) {
				theExceptions.add( eException );
			}
		}
		return theExceptions;
	}

	private static List<ArgsSyntaxException> toRootLevelExceptions( ArgsSyntaxException aArgsSyntaxException ) {
		List<ArgsSyntaxException> eExceptions = toLevelExceptions( aArgsSyntaxException );
		ArgsSyntaxException eException = aArgsSyntaxException;
		while ( eExceptions.size() == 1 ) {
			eException = eExceptions.get( 0 );
			if ( eException.getCause() instanceof ArgsSyntaxException theArgsSyntaxException && theArgsSyntaxException.getSource() instanceof Condition ) {
				eExceptions = toLevelExceptions( theArgsSyntaxException );
			}
			else {
				return eExceptions;
			}
		}
		return eExceptions;
	}

	private static List<ArgsSyntaxException> toLevelExceptions( ArgsSyntaxException aArgsSyntaxException ) {
		final List<ArgsSyntaxException> theLevelExceptions = new ArrayList<>();
		theLevelExceptions.add( aArgsSyntaxException );
		if ( aArgsSyntaxException.getSuppressed() != null ) {
			for ( Throwable eThrowable : aArgsSyntaxException.getSuppressed() ) {
				if ( eThrowable instanceof ArgsSyntaxException theArgsSyntaxException ) {
					theLevelExceptions.add( theArgsSyntaxException );
				}
			}
		}
		return theLevelExceptions;
	}

	private static ArgsSyntaxException toRootMatchCause( ArgsSyntaxException aException ) {
		ArgsSyntaxException theException = aException;
		while ( theException.getCause() instanceof ArgsSyntaxException eCause && eCause != theException && eCause.getMatchCount() >= aException.getMatchCount() ) {
			theException = eCause;
			if ( theException != null && theException != aException ) {
				return theException;
			}
		}
		if ( aException.getSuppressed() != null && aException.getSuppressed().length != 0 ) {
			for ( Throwable eSuppressed : aException.getSuppressed() ) {
				if ( eSuppressed instanceof ArgsSyntaxException eException ) {
					eException = toRootMatchCause( eException );
					if ( eException != null && eException != aException ) {
						return eException;
					}
				}
			}
		}
		return aException;
	}
}
