// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a argument Escape-Code property.
 */
public interface ArgumentEscapeCodeAccessor {

	/**
	 * Provides a builder method for a argument Escape-Code property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ArgumentEscapeCodeBuilder<B extends ArgumentEscapeCodeBuilder<B>> {

		/**
		 * Sets the argument Escape-Code for the argument Escape-Code property.
		 * 
		 * @param aArgumentEscCode The argument Escape-Code to be stored by the
		 *        argument Escape-Code property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withArgumentEscapeCode( String aArgumentEscCode );
	}

	/**
	 * Provides a mutator for a argument Escape-Code property.
	 */
	public interface ArgumentEscapeCodeMutator {

		/**
		 * Sets the argument Escape-Code for the argument Escape-Code property.
		 * 
		 * @param aArgumentEscCode The argument Escape-Code to be stored by the
		 *        argument Escape-Code property.
		 */
		void setArgumentEscapeCode( String aArgumentEscCode );
	}

	/**
	 * Provides a argument Escape-Code property.
	 */
	public interface ArgumentEscapeCodeProperty extends ArgumentEscapeCodeAccessor, ArgumentEscapeCodeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setArgumentEscapeCode(String)} and returns the very
		 * same value (getter).
		 * 
		 * @param aArgumentEscCode The {@link String} to set (via
		 *        {@link #setArgumentEscapeCode(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letArgumentEscapeCode( String aArgumentEscCode ) {
			setArgumentEscapeCode( aArgumentEscCode );
			return aArgumentEscCode;
		}
	}

	/**
	 * Retrieves the argument Escape-Code from the argument Escape-Code
	 * property.
	 * 
	 * @return The argument Escape-Code stored by the argument Escape-Code
	 *         property.
	 */
	String getArgumentEscapeCode();
}
