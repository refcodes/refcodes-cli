// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for an argument prefix property.
 */
public interface ArgumentPrefixAccessor {

	/**
	 * Provides a builder method for an argument prefix property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ArgumentPrefixBuilder<B extends ArgumentPrefixBuilder<B>> {

		/**
		 * Sets the argument prefix for the argument prefix property.
		 * 
		 * @param aArgumentPrefix The argument prefix to be stored by the
		 *        argument prefix property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withArgumentPrefix( String aArgumentPrefix );
	}

	/**
	 * Provides a mutator for an argument prefix property.
	 */
	public interface ArgumentPrefixMutator {

		/**
		 * Sets the argument prefix for the argument prefix property.
		 * 
		 * @param aArgumentPrefix The argument prefix to be stored by the
		 *        argument prefix property.
		 */
		void setArgumentPrefix( String aArgumentPrefix );
	}

	/**
	 * Provides an argument prefix property.
	 */
	public interface ArgumentPrefixProperty extends ArgumentPrefixAccessor, ArgumentPrefixMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setArgumentPrefix(String)} and returns the very same
		 * value (getter).
		 * 
		 * @param aArgumentPrefix The {@link String} to set (via
		 *        {@link #setArgumentPrefix(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letArgumentPrefix( String aArgumentPrefix ) {
			setArgumentPrefix( aArgumentPrefix );
			return aArgumentPrefix;
		}
	}

	/**
	 * Retrieves the argument prefix from the argument prefix property.
	 * 
	 * @return The argument prefix stored by the argument prefix property.
	 */
	String getArgumentPrefix();
}
