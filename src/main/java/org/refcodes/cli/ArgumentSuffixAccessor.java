// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for an argument suffix property.
 */
public interface ArgumentSuffixAccessor {

	/**
	 * Provides a builder method for an argument suffix property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ArgumentSuffixBuilder<B extends ArgumentSuffixBuilder<B>> {

		/**
		 * Sets the argument suffix for the argument suffix property.
		 * 
		 * @param aArgumentSuffix The argument suffix to be stored by the
		 *        argument suffix property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withArgumentSuffix( String aArgumentSuffix );
	}

	/**
	 * Provides a mutator for an argument suffix property.
	 */
	public interface ArgumentSuffixMutator {

		/**
		 * Sets the argument suffix for the argument suffix property.
		 * 
		 * @param aArgumentSuffix The argument suffix to be stored by the
		 *        argument suffix property.
		 */
		void setArgumentSuffix( String aArgumentSuffix );
	}

	/**
	 * Provides an argument suffix property.
	 */
	public interface ArgumentSuffixProperty extends ArgumentSuffixAccessor, ArgumentSuffixMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setArgumentSuffix(String)} and returns the very same
		 * value (getter).
		 * 
		 * @param aArgumentSuffix The {@link String} to set (via
		 *        {@link #setArgumentSuffix(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letArgumentSuffix( String aArgumentSuffix ) {
			setArgumentSuffix( aArgumentSuffix );
			return aArgumentSuffix;
		}
	}

	/**
	 * Retrieves the argument suffix from the argument suffix property.
	 * 
	 * @return The argument suffix stored by the argument suffix property.
	 */
	String getArgumentSuffix();
}
