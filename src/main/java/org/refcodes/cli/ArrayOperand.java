// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Creates an array representation facade for the encapsulated {@link Operand}.
 * This way any {@link Operand} can also be used as an array {@link Operand},
 * e.g. it can be provided multiple times in the command line arguments.
 *
 * @param <T> the generic type
 */
public class ArrayOperand<T> extends AbstractTerm implements Operand<T[]> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String MAX_LENGTH = "MAX_LENGTH";
	private static final String MIN_LENGTH = "MIN_LENGTH";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _maxLength;
	private int _minLength;
	private Operand<T> _operand;
	private Class<T[]> _type;
	private T[] _values = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link ArrayOperand} by encapsulating the given
	 * {@link Operand} and providing its definition as array definition to the
	 * CLI.
	 * 
	 * @param aOperand The {@link Operand} which's array counterpart is to be
	 *        defined.
	 */
	public ArrayOperand( Operand<T> aOperand ) {
		this( aOperand, -1, -1 );
	}

	/**
	 * Constructs the {@link ArrayOperand} by encapsulating the given
	 * {@link Operand} and providing its definition as array definition to the
	 * CLI.
	 * 
	 * @param aOperand The {@link Operand} which's array counterpart is to be
	 *        defined.
	 * @param aLength The number of array elements, or -1 if there is no limit.
	 */
	public ArrayOperand( Operand<T> aOperand, int aLength ) {
		this( aOperand, aLength, aLength );
	}

	/**
	 * Constructs the {@link ArrayOperand} by encapsulating the given
	 * {@link Operand} and providing its definition as array definition to the
	 * CLI.
	 * 
	 * @param aOperand The {@link Operand} which's array counterpart is to be
	 *        defined.
	 * @param aMinLength The minimum number of array elements, or -1 if there is
	 *        no limit.
	 * @param aMaxLength The maximum number of array elements, or -1 if there is
	 *        no limit.
	 */
	@SuppressWarnings("unchecked")
	public ArrayOperand( Operand<T> aOperand, int aMinLength, int aMaxLength ) {
		if ( aMaxLength == 0 || ( aMinLength != -1 && aMaxLength != -1 && aMinLength > aMaxLength ) || aMaxLength < -1 || aMaxLength < -1 ) {
			throw new IllegalArgumentException( "The minimum length <" + aMinLength + "> must be less or equal than the maximum length <" + aMaxLength + ">, both values must be greater (minimum and maximum) or equal (minimum) than <0> (or <-1> when to be ignored)!" );
		}
		_operand = aOperand;
		_minLength = aMinLength;
		_maxLength = aMaxLength;
		_type = (Class<T[]>) Array.newInstance( aOperand.getType(), 0 ).getClass();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMatchCount() {
		return _values != null ? _values.length : 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compareTo( Operand<?> o ) {
		return _operand.compareTo( o );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return _operand.getAlias();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDescription() {
		return _operand.getDescription();
	}

	/**
	 * Retrieves the maximum number of array elements to be present, e.g.
	 * maximum number of options provided in the command line arguments.
	 * 
	 * @return The maximum number of array elements, or -1 if there is no limit.
	 */
	public int getMaxLength() {
		return _maxLength;
	}

	/**
	 * Retrieves the minimum number of array elements to be present, e.g.
	 * minimum number of options provided in the command line arguments.
	 * 
	 * @return The minimum number of array elements, or -1 if there is no limit.
	 */
	public int getMinLength() {
		return _minLength;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<T[]> getType() {
		return _type;
		// return (Class<T[]>) _operand.getType().arrayType();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T[] getValue() {
		return _values;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Operand<?>[] parseArgs( String[] aArgs, String[] aOperands, CliContext aCliCtx ) throws ArgsSyntaxException {
		final List<T> theValues = new ArrayList<>();
		final List<Operand<?>> theResult = new ArrayList<>();
		int index = 0;
		Operand<?>[] eOperands;
		Object eObj;
		try {
			while ( _maxLength == -1 || index < _maxLength ) {
				eOperands = _operand.parseArgs( aArgs, aOperands, aCliCtx );
				for ( Operand<?> eOperand : eOperands ) {
					try {
						eObj = eOperand.clone();
					}
					catch ( CloneNotSupportedException e ) {
						throw _exception = new ParseArgsException( "The type <" + _operand.getType().getSimpleName() + "> does not support cloning!", aArgs, this, e );
					}
					theResult.add( (Operand<?>) eObj );
				}
				theValues.add( _operand.getValue() );
				aArgs = AbstractTerm.toArgsDiff( aArgs, eOperands );
				index++;
			}
		}
		catch ( UnknownArgsException | AmbiguousArgsException | ParseArgsException e ) {
			if ( _maxLength != -1 && index > _maxLength ) {
				throw _exception = e;
			}
		}
		if ( theValues.size() == 0 || ( _minLength != -1 && index < _minLength ) ) {
			throw _exception = new UnknownArgsException( "Not one argument matched (as of \"" + toUsage( aCliCtx ) + "\")", aArgs, this );
		}
		_values = theValues.toArray( (T[]) Array.newInstance( _operand.getType(), theValues.size() ) );
		return theResult.toArray( new Operand<?>[theResult.size()] );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		super.reset();
		_operand.reset();
		_values = null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getParsedArgs() {
		return _operand.getParsedArgs();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CliSchema toSchema() {
		final CliSchema theSchema = new CliSchema( _operand.getDescription(), getClass(), getValue(), _operand.getAlias() );
		theSchema.put( MAX_LENGTH, _maxLength );
		theSchema.put( MIN_LENGTH, _minLength );
		if ( _exception != null ) {
			theSchema.put( CliSchema.EXCEPTION, _exception );
		}
		return theSchema;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSyntax( CliContext aCliCtx ) {
		return aCliCtx.toArgumentSpec( this, _minLength, _maxLength );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public ArrayOperand<T> withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Hook for accessing the encapsulated {@link Operand}.
	 * 
	 * @return The according {@link Operand}.
	 */
	protected Operand<T> getOperand() {
		return _operand;
	}
}
