// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Creates an array representation facade for the encapsulated {@link Option}.
 * This way any {@link Option} can also be used as an array {@link Option}, e.g.
 * it can be provided multiple times in the command line arguments.
 *
 * @param <T> the generic type
 */
public class ArrayOption<T> extends ArrayOperand<T> implements Option<T[]> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link ArrayOption} by encapsulating the given
	 * {@link Option} and providing its definition as array definition to the
	 * CLI.
	 * 
	 * @param aOption The {@link Option} which's array counterpart is to be
	 *        defined.
	 */
	public ArrayOption( Option<T> aOption ) {
		this( aOption, -1, -1 );
	}

	/**
	 * Constructs the {@link ArrayOption} by encapsulating the given
	 * {@link Option} and providing its definition as array definition to the
	 * CLI.
	 * 
	 * @param aOption The {@link Option} which's array counterpart is to be
	 *        defined.
	 * @param aLength The number of array elements, or -1 if there is no limit.
	 */
	public ArrayOption( Option<T> aOption, int aLength ) {
		this( aOption, aLength, aLength );
	}

	/**
	 * Constructs the {@link ArrayOption} by encapsulating the given
	 * {@link Option} and providing its definition as array definition to the
	 * CLI.
	 * 
	 * @param aOption The {@link Option} which's array counterpart is to be
	 *        defined.
	 * @param aMinLength The minimum number of array elements, or -1 if there is
	 *        no limit.
	 * @param aMaxLength The maximum number of array elements, or -1 if there is
	 *        no limit.
	 */
	public ArrayOption( Option<T> aOption, int aMinLength, int aMaxLength ) {
		super( aOption, aMinLength, aMaxLength );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLongOption() {
		return ( (Option<T>) getOperand() ).getLongOption();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getShortOption() {
		return ( (Option<T>) getOperand() ).getShortOption();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSyntax( CliContext aCliCtx ) {
		return aCliCtx.toOptionEscapeCode() + aCliCtx.toOptionUsage( this ) + ( getAlias() != null ? ( " " + aCliCtx.toArgumentSpec( this, getMinLength(), getMaxLength() ) ) : "" ) + aCliCtx.toResetEscapeCode();
	}
}
