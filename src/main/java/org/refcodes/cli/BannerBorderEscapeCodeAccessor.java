// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a banner border Escape-Code property.
 */
public interface BannerBorderEscapeCodeAccessor {

	/**
	 * Provides a builder method for a banner border Escape-Code property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BannerBorderEscapeCodeBuilder<B extends BannerBorderEscapeCodeBuilder<B>> {

		/**
		 * Sets the banner border Escape-Code for the banner border Escape-Code
		 * property.
		 * 
		 * @param aBannerBorderEscCode The banner border Escape-Code to be
		 *        stored by the banner border Escape-Code property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBannerBorderEscapeCode( String aBannerBorderEscCode );
	}

	/**
	 * Provides a mutator for a banner border Escape-Code property.
	 */
	public interface BannerBorderEscapeCodeMutator {

		/**
		 * Sets the banner border Escape-Code for the banner border Escape-Code
		 * property.
		 * 
		 * @param aBannerBorderEscCode The banner border Escape-Code to be
		 *        stored by the banner border Escape-Code property.
		 */
		void setBannerBorderEscapeCode( String aBannerBorderEscCode );
	}

	/**
	 * Provides a banner border Escape-Code property.
	 */
	public interface BannerBorderEscapeCodeProperty extends BannerBorderEscapeCodeAccessor, BannerBorderEscapeCodeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setBannerBorderEscapeCode(String)} and returns the very
		 * same value (getter).
		 * 
		 * @param aBannerBorderEscCode The {@link String} to set (via
		 *        {@link #setBannerBorderEscapeCode(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letBannerBorderEscapeCode( String aBannerBorderEscCode ) {
			setBannerBorderEscapeCode( aBannerBorderEscCode );
			return aBannerBorderEscCode;
		}
	}

	/**
	 * Retrieves the banner border Escape-Code from the banner border
	 * Escape-Code property.
	 * 
	 * @return The banner border Escape-Code stored by the banner border
	 *         Escape-Code property.
	 */
	String getBannerBorderEscapeCode();
}
