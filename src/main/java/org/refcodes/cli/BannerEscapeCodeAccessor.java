// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a banner Escape-Code property.
 */
public interface BannerEscapeCodeAccessor {

	/**
	 * Provides a builder method for a banner Escape-Code property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BannerEscapeCodeBuilder<B extends BannerEscapeCodeBuilder<B>> {

		/**
		 * Sets the banner Escape-Code for the banner Escape-Code property.
		 * 
		 * @param aBannerEscCode The banner Escape-Code to be stored by the
		 *        banner Escape-Code property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBannerEscapeCode( String aBannerEscCode );
	}

	/**
	 * Provides a mutator for a banner Escape-Code property.
	 */
	public interface BannerEscapeCodeMutator {

		/**
		 * Sets the banner Escape-Code for the banner Escape-Code property.
		 * 
		 * @param aBannerEscCode The banner Escape-Code to be stored by the
		 *        banner Escape-Code property.
		 */
		void setBannerEscapeCode( String aBannerEscCode );
	}

	/**
	 * Provides a banner Escape-Code property.
	 */
	public interface BannerEscapeCodeProperty extends BannerEscapeCodeAccessor, BannerEscapeCodeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setBannerEscapeCode(String)} and returns the very same
		 * value (getter).
		 * 
		 * @param aBannerEscCode The {@link String} to set (via
		 *        {@link #setBannerEscapeCode(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letBannerEscapeCode( String aBannerEscCode ) {
			setBannerEscapeCode( aBannerEscCode );
			return aBannerEscCode;
		}
	}

	/**
	 * Retrieves the banner Escape-Code from the banner Escape-Code property.
	 * 
	 * @return The banner Escape-Code stored by the banner Escape-Code property.
	 */
	String getBannerEscapeCode();
}
