// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import org.refcodes.textual.Font;

/**
 * Provides an accessor for a banner {@link Font} property.
 */
public interface BannerFontAccessor {

	/**
	 * Provides a builder method for a banner {@link Font} property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BannerFontBuilder<B extends BannerFontBuilder<B>> {

		/**
		 * Sets the banner {@link Font} for the banner {@link Font} property.
		 * 
		 * @param aBannerFont The banner {@link Font} to be stored by the banner
		 *        {@link Font} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBannerFont( Font aBannerFont );
	}

	/**
	 * Provides a mutator for a banner {@link Font} property.
	 */
	public interface BannerFontMutator {

		/**
		 * Sets the banner {@link Font} for the banner {@link Font} property.
		 * 
		 * @param aBannerFont The banner {@link Font} to be stored by the banner
		 *        {@link Font} property.
		 */
		void setBannerFont( Font aBannerFont );
	}

	/**
	 * Provides a banner {@link Font} property.
	 */
	public interface BannerFontProperty extends BannerFontAccessor, BannerFontMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Font} (setter) as
		 * of {@link #setBannerFont(Font)} and returns the very same value
		 * (getter).
		 * 
		 * @param aBannerFont The {@link Font} to set (via
		 *        {@link #setBannerFont(Font)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Font letBannerFont( Font aBannerFont ) {
			setBannerFont( aBannerFont );
			return aBannerFont;
		}
	}

	/**
	 * Retrieves the banner {@link Font} from the banner {@link Font} property.
	 * 
	 * @return The banner {@link Font} stored by the banner {@link Font}
	 *         property.
	 */
	Font getBannerFont();
}
