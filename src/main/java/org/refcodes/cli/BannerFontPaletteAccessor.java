// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import org.refcodes.data.AsciiColorPalette;

/**
 * Provides an accessor for a banner font palette property.
 */
public interface BannerFontPaletteAccessor {

	/**
	 * Provides a builder method for a banner font palette property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface BannerFontPaletteBuilder<B extends BannerFontPaletteBuilder<B>> {

		/**
		 * Sets the banner font palette for the banner font palette property.
		 * 
		 * @param aBannerFontPalette The banner font palette to be stored by the
		 *        banner palette property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBannerFontPalette( AsciiColorPalette aBannerFontPalette );

		/**
		 * Sets the banner font palette for the banner font palette property.
		 * 
		 * @param aBannerFontPalette The banner font palette to be stored by the
		 *        banner palette property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withBannerFontPalette( char[] aBannerFontPalette );
	}

	/**
	 * Provides a mutator for a banner font palette property.
	 */
	public interface BannerFontPaletteMutator {

		/**
		 * Sets the banner font palette for the banner font palette property.
		 * 
		 * @param aBannerFontPalette The banner font palette to be stored by the
		 *        banner palette property.
		 */
		void setBannerFontPalette( AsciiColorPalette aBannerFontPalette );

		/**
		 * Sets the banner font palette for the banner font palette property.
		 * 
		 * @param aBannerFontPalette The banner font palette to be stored by the
		 *        banner palette property.
		 */
		void setBannerFontPalette( char[] aBannerFontPalette );

	}

	/**
	 * Provides a banner font palette property.
	 */
	public interface BannerFontPaletteProperty extends BannerFontPaletteAccessor, BannerFontPaletteMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given palette (setter) as of
		 * {@link #setBannerFontPalette(char[])} and returns the very same value
		 * (getter).
		 * 
		 * @param aBannerFontPalette The palette to set (via
		 *        {@link #setBannerFontPalette(char[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default char[] letBannerFontPalette( char[] aBannerFontPalette ) {
			setBannerFontPalette( aBannerFontPalette );
			return aBannerFontPalette;
		}
	}

	/**
	 * Retrieves the banner font palette from the banner font palette property.
	 * 
	 * @return The banner font palette stored by the banner font palette
	 *         property.
	 */
	char[] getBannerFontPalette();
}
