// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.function.Consumer;

import org.refcodes.data.BooleanLiterals;
import org.refcodes.struct.Relation;

/**
 * The {@link BooleanOption} represents an {@link Option} holding
 * <code>boolean</code> values.
 */
public class BooleanOption extends AbstractOption<Boolean> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link BooleanOption}
	 */
	public BooleanOption( Character aShortOption, String aLongOption, String aDescription ) {
		super( aShortOption, aLongOption, Boolean.class, aDescription );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link BooleanOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link BooleanOption} participated in successfully parsing the
	 *        command line arguments.
	 */
	public BooleanOption( Character aShortOption, String aLongOption, String aDescription, Consumer<BooleanOption> aConsumer ) {
		super( aShortOption, aLongOption, Boolean.class, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link BooleanOption}
	 */
	public BooleanOption( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		super( aShortOption, aLongOption, Boolean.class, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link BooleanOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link BooleanOption} participated in successfully parsing the
	 *        command line arguments.
	 */
	public BooleanOption( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<BooleanOption> aConsumer ) {
		super( aShortOption, aLongOption, Boolean.class, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the alias being the
	 * proerty's key and the value being the property's value. Depending on the
	 * provided property's key, the key is either used for the short option or
	 * the long option.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 */
	public BooleanOption( Relation<String, Boolean> aProperty ) {
		super( aProperty, Boolean.class );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the alias being the
	 * proerty's key and the value being the property's value. Depending on the
	 * provided property's key, the key is either used for the short option or
	 * the long option.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link BooleanOption} participated in successfully parsing the
	 *        command line arguments.
	 */
	public BooleanOption( Relation<String, Boolean> aProperty, Consumer<BooleanOption> aConsumer ) {
		super( aProperty, Boolean.class, aConsumer );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link BooleanOption}
	 */
	public BooleanOption( String aLongOption, String aDescription ) {
		super( aLongOption, Boolean.class, aDescription );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link BooleanOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link BooleanOption} participated in successfully parsing the
	 *        command line arguments.
	 */
	public BooleanOption( String aLongOption, String aDescription, Consumer<BooleanOption> aConsumer ) {
		super( aLongOption, Boolean.class, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link BooleanOption}
	 */
	public BooleanOption( String aLongOption, String aAlias, String aDescription ) {
		super( aLongOption, Boolean.class, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link BooleanOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link BooleanOption} participated in successfully parsing the
	 *        command line arguments.
	 */
	public BooleanOption( String aLongOption, String aAlias, String aDescription, Consumer<BooleanOption> aConsumer ) {
		super( aLongOption, Boolean.class, aAlias, aDescription, aConsumer );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean getValue() {
		return super.getValue() != null ? super.getValue() : Boolean.FALSE;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public BooleanOption withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Boolean toType( String aArg ) throws ParseArgsException {
		try {
			if ( BooleanLiterals.isTrue( aArg ) ) {
				_matchCount = 1;
				return true;
			}
			if ( BooleanLiterals.isFalse( aArg ) ) {
				_matchCount = 1;
				return false;
			}
			final Boolean theValue = Boolean.valueOf( aArg );
			_matchCount = 1;
			return theValue;
		}
		catch ( NumberFormatException e ) {
			final ParseArgsException theException = new ParseArgsException( "Unable to parse the argument <" + aArg + "> of option \"" + getAlias() + "\" to a <boolean> value.", new String[] { aArg }, this, e );
			_exception = theException;
			throw theException;
		}
	}
}
