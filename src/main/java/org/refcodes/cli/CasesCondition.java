// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * The {@link CasesCondition} is an {@link XorCondition} making sure that all
 * possible cases inside the {@link XorCondition} must(!) consume all provided
 * args in order to match! This makes the {@link CasesCondition} most useful as
 * a root condition with exotic (possibly ambiguous) cases encapsulated.
 */
public class CasesCondition extends XorCondition {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link CasesCondition} with the {@link Term}
	 * ({@link Condition}) instances to be nested.
	 *
	 * @param aArgs The {@link Term} ({@link Condition}) instances to be nested
	 */
	public CasesCondition( Term... aArgs ) {
		super( "Switches (CASE) exactly one matching case and makes sure that a match must(!) consume all provided args.", toAllConditions( aArgs ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static Term[] toAllConditions( Term[] aArgs ) {
		for ( int i = 0; i < aArgs.length; i++ ) {
			if ( !( aArgs[i] instanceof AllCondition ) ) {
				aArgs[i] = new AllCondition( aArgs[i] );
			}
		}
		return aArgs;
	}
}
