// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.function.Consumer;

import org.refcodes.struct.Relation;

/**
 * The {@link CharOption} represents an {@link Option} holding
 * <code>String</code> values.
 */
public class CharOption extends AbstractOption<Character> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link CharOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link CharOption}
	 */
	public CharOption( Character aShortOption, String aLongOption, String aDescription ) {
		super( aShortOption, aLongOption, Character.class, aDescription );
	}

	/**
	 * Instantiates a new {@link CharOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link CharOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link CharOption} participated in successfully parsing the
	 *        command line arguments.
	 */
	public CharOption( Character aShortOption, String aLongOption, String aDescription, Consumer<CharOption> aConsumer ) {
		super( aShortOption, aLongOption, Character.class, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link CharOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link CharOption}
	 */
	public CharOption( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		super( aShortOption, aLongOption, Character.class, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link CharOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link CharOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link CharOption} participated in successfully parsing the
	 *        command line arguments.
	 */
	public CharOption( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<CharOption> aConsumer ) {
		super( aShortOption, aLongOption, Character.class, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link CharOption} with the alias being the proerty's
	 * key and the value being the property's value. Depending on the provided
	 * property's key, the key is either used for the short option or the long
	 * option.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 */
	public CharOption( Relation<String, Character> aProperty ) {
		super( aProperty, Character.class );
	}

	/**
	 * Instantiates a new {@link CharOption} with the alias being the proerty's
	 * key and the value being the property's value. Depending on the provided
	 * property's key, the key is either used for the short option or the long
	 * option.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link CharOption} participated in successfully parsing the
	 *        command line arguments.
	 */
	public CharOption( Relation<String, Character> aProperty, Consumer<CharOption> aConsumer ) {
		super( aProperty, Character.class, aConsumer );
	}

	/**
	 * Instantiates a new {@link CharOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link CharOption}
	 */
	public CharOption( String aLongOption, String aDescription ) {
		super( aLongOption, Character.class, aDescription );
	}

	/**
	 * Instantiates a new {@link CharOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link CharOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link CharOption} participated in successfully parsing the
	 *        command line arguments.
	 */
	public CharOption( String aLongOption, String aDescription, Consumer<CharOption> aConsumer ) {
		super( aLongOption, Character.class, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link CharOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link CharOption}
	 */
	public CharOption( String aLongOption, String aAlias, String aDescription ) {
		super( aLongOption, Character.class, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link CharOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link CharOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link CharOption} participated in successfully parsing the
	 *        command line arguments.
	 */
	public CharOption( String aLongOption, String aAlias, String aDescription, Consumer<CharOption> aConsumer ) {
		super( aLongOption, Character.class, aAlias, aDescription, aConsumer );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public CharOption withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Character toType( String aArg ) throws ParseArgsException {
		if ( aArg.length() == 1 ) {
			_matchCount = 1;
			return aArg.charAt( 0 );
		}
		final ParseArgsException theException = new ParseArgsException( "Unable to convert <" + aArg + "> to a character!", new String[] { aArg }, this );
		_exception = theException;
		throw theException;
	}
}
