// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.refcodes.data.AnsiEscapeCode;
import org.refcodes.runtime.Terminal;

/**
 * The {@link CliContext} describes the context in which an {@link ParseArgs} is
 * running. The {@link CliContext} may depend on the operating system or user
 * settings applied, when running the {@link ParseArgs} implementations.
 */
public class CliContext implements CliMetrics, Optionable {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private enum UsageMode {
		USAGE, SPEC
	}

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _argumentEscapeCode = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.DEFAULT_FOREGROUND_COLOR, AnsiEscapeCode.FAINT, AnsiEscapeCode.ITALIC );
	private boolean _isEscapeCodesEnabled = Terminal.isAnsiTerminalEnabled();
	private String _optionEscapeCode = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.DEFAULT_FOREGROUND_COLOR, AnsiEscapeCode.BOLD );
	private String _resetEscapeCode = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.RESET );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private SyntaxMetrics _syntaxMetrics = SyntaxNotation.LOGICAL;

	/**
	 * Constructs an accordingly preconfigured {@link CliContext} instance.
	 */
	public CliContext() {}

	/**
	 * Constructs an accordingly configured {@link CliContext} instance.
	 *
	 * @param isEscapeCodesEnabled Determines whether using escape codes when
	 *        rendering a {@link Term} node's syntax.
	 */
	public CliContext( boolean isEscapeCodesEnabled ) {
		_isEscapeCodesEnabled = isEscapeCodesEnabled;
	}

	/**
	 * Constructs an accordingly configured {@link CliContext} instance.
	 *
	 * @param aCliMetrics The {@link CliMetrics} describing the
	 *        {@link CliContext}.
	 */
	public CliContext( CliMetrics aCliMetrics ) {
		_syntaxMetrics = aCliMetrics.getSyntaxMetrics();
		_isEscapeCodesEnabled = aCliMetrics.isEscapeCodesEnabled();
		_argumentEscapeCode = aCliMetrics.getArgumentEscapeCode();
		_optionEscapeCode = aCliMetrics.getOptionEscapeCode();
		_resetEscapeCode = aCliMetrics.getResetEscapeCode();
	}

	/**
	 * Constructs an accordingly configured {@link CliContext} instance.
	 *
	 * @param aCliMetrics The {@link CliMetrics} describing the
	 *        {@link CliContext}.
	 * 
	 * @param aSyntaxMetrics When being provided, then it overrides the
	 *        {@link SyntaxMetrics} provided by the {@link CliMetrics}.
	 */
	public CliContext( CliMetrics aCliMetrics, SyntaxMetrics aSyntaxMetrics ) {
		_syntaxMetrics = aSyntaxMetrics != null ? aSyntaxMetrics : aCliMetrics.getSyntaxMetrics();
		_isEscapeCodesEnabled = aCliMetrics.isEscapeCodesEnabled();
		_argumentEscapeCode = aCliMetrics.getArgumentEscapeCode();
		_optionEscapeCode = aCliMetrics.getOptionEscapeCode();
		_resetEscapeCode = aCliMetrics.getResetEscapeCode();
	}

	/**
	 * Constructs an accordingly configured {@link CliContext} instance.
	 *
	 * @param aCliMetrics The {@link CliMetrics} describing the
	 *        {@link CliContext}.
	 * 
	 * @param aSyntaxMetrics When being provided, then it overrides the
	 *        {@link SyntaxMetrics} provided by the {@link CliMetrics}.
	 * 
	 * @param isEscapeCodesEnabled Overrides the according
	 *        {@link CliMetrics#isEscapeCodesEnabled()} attribute. This is
	 *        useful when using the {@link CliContext} instance for generating
	 *        exception massages which must not have ANSI escape codes.
	 */
	public CliContext( CliMetrics aCliMetrics, SyntaxMetrics aSyntaxMetrics, boolean isEscapeCodesEnabled ) {
		_syntaxMetrics = aSyntaxMetrics != null ? aSyntaxMetrics : aCliMetrics.getSyntaxMetrics();
		_isEscapeCodesEnabled = isEscapeCodesEnabled;
		_argumentEscapeCode = aCliMetrics.getArgumentEscapeCode();
		_optionEscapeCode = aCliMetrics.getOptionEscapeCode();
		_resetEscapeCode = aCliMetrics.getResetEscapeCode();
	}

	/**
	 * Constructs an accordingly configured {@link CliContext} instance.
	 *
	 * @param aArgumentEscapeCode The escape code to use when rendering
	 *        (option's) arguments
	 * @param aOptionEscapeCode The escape code to be used when rendering
	 *        options.
	 * @param aResetEscapeCode The escape code resetting any previously applied
	 *        escape codes.
	 */
	public CliContext( String aArgumentEscapeCode, String aOptionEscapeCode, String aResetEscapeCode ) {
		_argumentEscapeCode = aArgumentEscapeCode;
		_optionEscapeCode = aOptionEscapeCode;
		_resetEscapeCode = aResetEscapeCode;
	}

	/**
	 * Constructs an accordingly configured {@link CliContext} instance.
	 *
	 * @param aSyntaxMetrics The {@link SyntaxMetrics} describing the syntax
	 *        notation to be used such as the {@link SyntaxNotation}
	 *        definitions.
	 */
	public CliContext( SyntaxMetrics aSyntaxMetrics ) {
		_syntaxMetrics = aSyntaxMetrics;
	}

	/**
	 * Constructs an accordingly configured {@link CliContext} instance.
	 *
	 * @param aSyntaxMetrics The {@link SyntaxMetrics} describing the syntax
	 *        notation to be used such as the {@link SyntaxNotation}
	 *        definitions.
	 * @param isEscapeCodesEnabled Determines whether using escape codes when
	 *        rendering a {@link Term} node's syntax.
	 */
	public CliContext( SyntaxMetrics aSyntaxMetrics, boolean isEscapeCodesEnabled ) {
		_syntaxMetrics = aSyntaxMetrics;
		_isEscapeCodesEnabled = isEscapeCodesEnabled;
	}

	/**
	 * Constructs an accordingly configured {@link CliContext} instance.
	 * 
	 * @param aSyntaxMetrics The {@link SyntaxMetrics} describing the syntax
	 *        notation to be used such as the {@link SyntaxNotation}
	 *        definitions.
	 * @param aShortOptionPrefix The short option prefix to be used when parsing
	 *        {@link Option} elements.
	 * @param aLongOptionPrefix The long option prefix to be used when parsing
	 *        {@link Option} elements.
	 * @param isEscapeCodesEnabled Determines whether using escape codes when
	 *        rendering a {@link Term} node's syntax.
	 * @param aArgumentEscapeCode The escape code to use when rendering
	 *        (option's) arguments
	 * @param aOptionEscapeCode The escape code to be used when rendering
	 *        options.
	 * @param aResetEscapeCode The escape code resetting any previously applied
	 *        escape codes.
	 */
	public CliContext( SyntaxMetrics aSyntaxMetrics, Character aShortOptionPrefix, String aLongOptionPrefix, boolean isEscapeCodesEnabled, String aArgumentEscapeCode, String aOptionEscapeCode, String aResetEscapeCode ) {
		_syntaxMetrics = aSyntaxMetrics;
		_isEscapeCodesEnabled = isEscapeCodesEnabled;
		_argumentEscapeCode = aArgumentEscapeCode;
		_optionEscapeCode = aOptionEscapeCode;
		_resetEscapeCode = aResetEscapeCode;
	}

	/**
	 * Constructs an accordingly configured {@link CliContext} instance.
	 *
	 * @param aSyntaxMetrics The {@link SyntaxMetrics} describing the syntax
	 *        notation to be used such as the {@link SyntaxNotation}
	 *        definitions.
	 * @param aArgumentEscapeCode The escape code to use when rendering
	 *        (option's) arguments
	 * @param aOptionEscapeCode The escape code to be used when rendering
	 *        options.
	 * @param aResetEscapeCode The escape code resetting any previously applied
	 *        escape codes.
	 */
	public CliContext( SyntaxMetrics aSyntaxMetrics, String aArgumentEscapeCode, String aOptionEscapeCode, String aResetEscapeCode ) {
		_syntaxMetrics = aSyntaxMetrics;
		_argumentEscapeCode = aArgumentEscapeCode;
		_optionEscapeCode = aOptionEscapeCode;
		_resetEscapeCode = aResetEscapeCode;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	private CliContext( Builder builder ) {
		_syntaxMetrics = builder.syntaxMetrics;
		_argumentEscapeCode = builder.argumentEscapeCode;
		_optionEscapeCode = builder.optionEscapeCode;
		_resetEscapeCode = builder.resetEscapeCode;
		_isEscapeCodesEnabled = builder.isEscapeCodesEnabled;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getArgumentEscapeCode() {
		return _argumentEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getOptionEscapeCode() {
		return _optionEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getResetEscapeCode() {
		return _resetEscapeCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SyntaxMetrics getSyntaxMetrics() {
		return _syntaxMetrics;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEscapeCodesEnabled() {
		return _isEscapeCodesEnabled;
	}

	/**
	 * Determines whether the given argument is an option in terms of this
	 * context, e.g. whether it starts either with a short option prefix (as of
	 * {@link SyntaxMetrics#getShortOptionPrefix()} or a long option prefix (as
	 * of {@link SyntaxMetrics#getLongOptionPrefix()}).
	 * 
	 * @param aArg The argument to be tested whether it matches the option
	 *        pattern as of this context.
	 * 
	 * @return True in case the argument may be a short option or a long option,
	 *         e.g. it is prefixed accordingly.
	 */
	public boolean isOption( String aArg ) {
		if ( aArg != null && aArg.length() != 0 ) {
			if ( _syntaxMetrics.getLongOptionPrefix() != null && aArg.startsWith( _syntaxMetrics.getLongOptionPrefix() ) ) {
				return true;
			}
			if ( _syntaxMetrics.getShortOptionPrefix() != null && _syntaxMetrics.getShortOptionPrefix().equals( aArg.charAt( 0 ) ) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Determines all options (short and long) found in the provided
	 * {@link Term} node and its children.
	 * 
	 * @param aConstituent The element (with its children if applicable) from
	 *        which to determine the options.
	 * 
	 * @return An array containing all found options.
	 */
	public String[] toAllOptions( Term aConstituent ) {
		final Set<String> theOptions = new HashSet<>();
		String[] eOptions;
		if ( aConstituent instanceof Condition theCondition ) {
			for ( Operand<?> eOperand : theCondition.toOperands() ) {
				eOptions = toAllOptions( eOperand );
				if ( eOptions != null && eOptions.length != 0 ) {
					theOptions.addAll( Arrays.asList( eOptions ) );
				}
			}
		}
		if ( aConstituent instanceof Option<?> theOption ) {
			final String theShortOption = toShortOption( theOption );
			final String theLongOption = toLongOption( theOption );
			if ( theShortOption != null ) {
				theOptions.add( theShortOption );
			}
			if ( theLongOption != null ) {
				theOptions.add( theLongOption );
			}
		}
		return theOptions.toArray( new String[theOptions.size()] );
	}

	/**
	 * Constructs the argument escape code depending on the escape code status
	 * (as of {@link #isEscapeCodesEnabled()}) and whether the argument escape
	 * code is not null or null (as of {@link #getArgumentEscapeCode()}). In
	 * case escape codes are enabled and the argument escape code is set, then
	 * the argument escape code is returned, else an empty {@link String}.
	 * 
	 * @return The argument escape code or an empty {@link String} depending on
	 *         the escape code status and the argument escape code's value.
	 */
	public String toArgumentEscapeCode() {
		return _isEscapeCodesEnabled ? ( _argumentEscapeCode != null ? _argumentEscapeCode : "" ) : "";
	}

	/**
	 * Creates the argument specification from the provided {@link Operand}. In
	 * case we got a {@link Flag}, then there will be an empty argument
	 * specification as the {@link Flag} implies a argument. If the
	 * {@link Operand} is an option, then the argument will be prefixed and
	 * suffixed different to an {@link Operand} type.
	 * 
	 * @param aOperand The operand from which to get the argument specification.
	 * 
	 * @return The argument specification.
	 */
	public String toArgumentSpec( Operand<?> aOperand ) {
		if ( aOperand instanceof ArrayOperand<?> ) {
			final ArrayOperand<?> theArrayOperand = (ArrayOperand<?>) aOperand;
			return toArgumentSpec( aOperand, theArrayOperand.getMinLength(), theArrayOperand.getMaxLength() );
		}
		return toArgumentSpec( aOperand, -1, -1 );
	}

	/**
	 * Creates the argument specification from the provided {@link Operand}. In
	 * case we got a {@link Flag}, then there will be an empty argument
	 * specification as the {@link Flag} implies an argument. If the
	 * {@link Operand} is an option, then the argument will be prefixed and
	 * suffixed different to an {@link Operand} type.
	 *
	 * @param aOperand The operand from which to get the argument specification.
	 * @param aMin The minimum value for the operand.
	 * @param aMax The maximum value for the operand.
	 * 
	 * @return The argument specification.
	 */
	public String toArgumentSpec( Operand<?> aOperand, int aMin, int aMax ) {
		if ( aOperand.getAlias() == null ) {
			return "";
		}
		final StringBuilder theBuilder = new StringBuilder();
		if ( !( aOperand instanceof Flag ) ) {
			if ( aOperand.getAlias() != null ) {
				if ( aOperand instanceof Option<?> ) {
					theBuilder.append( getSyntaxMetrics().getArgumentPrefix() );
				}
				theBuilder.append( ( aOperand instanceof NoneOperand ? aOperand.toSyntax( this ) : toArgumentEscapeCode() + aOperand.getAlias() ) + toResetEscapeCode() );
				if ( aOperand.getType().isArray() ) {
					theBuilder.append( _syntaxMetrics.getBeginArraySymbol() );
				}
				else if ( aMin != -1 || aMax != -1 ) {
					theBuilder.append( _syntaxMetrics.getBeginRangeSymbol() );
				}
				if ( aMin != -1 && aMin == aMax ) {
					theBuilder.append( aMin );
				}
				else {
					if ( aMin != -1 ) {
						theBuilder.append( aMin );
					}
					if ( aOperand.getType().isArray() || aMin != -1 || aMax != -1 ) {
						theBuilder.append( _syntaxMetrics.getIntervalSymbol() );
					}
					if ( aMax != -1 ) {
						theBuilder.append( aMax );
					}
				}
				if ( aOperand.getType().isArray() ) {
					theBuilder.append( _syntaxMetrics.getEndArraySymbol() );
				}
				else if ( aMin != -1 || aMax != -1 ) {
					theBuilder.append( _syntaxMetrics.getEndRangeSymbol() );
				}
				if ( aOperand instanceof Option<?> ) {
					theBuilder.append( _syntaxMetrics.getArgumentSuffix() );
				}
			}
		}
		return theBuilder.toString();
	}

	/**
	 * Expands the provided args' short options and returns the resulting args.
	 * A short option prefix may be used to denote multiple short options, given
	 * a short option prefix being used is "-", then the argument "-xyz" is
	 * expanded to be "-x", "-y" and "-z".
	 * 
	 * @param aArgs The args to be expanded.
	 * 
	 * @return The expanded args.
	 */
	public String[] toExpandOptions( String[] aArgs ) {
		final List<String> theArgs = new ArrayList<>();
		if ( getSyntaxMetrics().getShortOptionPrefix() != null ) {
			for ( String eArg : aArgs ) {
				if ( eArg.length() > 2 && eArg.charAt( 2 ) != '=' && ( getSyntaxMetrics().getLongOptionPrefix() == null || getSyntaxMetrics().getLongOptionPrefix().isEmpty() || !eArg.startsWith( getSyntaxMetrics().getLongOptionPrefix() ) ) && eArg.charAt( 0 ) == getSyntaxMetrics().getShortOptionPrefix() ) {
					for ( int i = 1; i < eArg.length(); i++ ) {
						theArgs.add( getSyntaxMetrics().getShortOptionPrefix().toString() + eArg.charAt( i ) );
					}
				}
				else {
					theArgs.add( eArg );
				}
			}
		}
		return theArgs.toArray( new String[theArgs.size()] );
	}

	/**
	 * Creates the {@link CliContext} instance specific prefixed long option
	 * {@link String} from the provided {@link Option} by prefixing the
	 * {@link Option}'s long option (as of {@link Option#getLongOption()}) with
	 * the long option prefix (as of
	 * {@link SyntaxMetrics#getLongOptionPrefix()}).
	 * 
	 * @param aOption The {@link Option} from which to create the according
	 *        prefixed long option {@link String}.
	 * 
	 * @return The accordingly prefixed long option {@link String} or
	 *         <code>null</code> if there is no long option set for the provided
	 *         {@link Option} or the {@link Option} is null.
	 */
	public String toLongOption( Option<?> aOption ) {
		return aOption != null && aOption.getLongOption() != null ? _syntaxMetrics.getLongOptionPrefix() + aOption.getLongOption() : null;
	}

	/**
	 * Creates the {@link CliContext} instance specific preferred prefixed
	 * option {@link String}, being the short option (as of
	 * {@link #toShortOption(Option)}) or, in case of the short option being
	 * <code>null</code>, the long option (as of {@link #toLongOption(Option)}).
	 * 
	 * @param aOption The {@link Option} from which to create the accordingly
	 *        prefixed option {@link String}.
	 * 
	 * @return The accordingly prefixed option {@link String} or
	 *         <code>null</code> if there is neither a short option nor a long
	 *         option set for the provided {@link Option} or the {@link Option}
	 *         is null.
	 */
	public String toOptionUsage( Option<?> aOption ) {
		final String theOption = toShortOption( aOption );
		return theOption != null ? theOption : toLongOption( aOption );
	}

	/**
	 * Creates the {@link CliContext} instance specific option notation
	 * {@link String} (without any arguments), consisting of the short option
	 * (as of {@link #toShortOption(Option)}) and(!) the long option (as of
	 * {@link #toLongOption(Option)}) as of them not being <code>null</code>.
	 * 
	 * @param aOption The {@link Option} from which to create the according
	 *        option notation {@link String}.
	 * 
	 * @return The according notation of the {@link Option} (without any
	 *         arguments).
	 */
	public String toOptionSpec( Option<?> aOption ) {
		final String theShortOption = toShortOption( aOption );
		final String theLongOption = toLongOption( aOption );
		if ( theShortOption != null && theLongOption != null ) {
			return theShortOption + " " + theLongOption;
		}
		return theShortOption != null ? theShortOption : theLongOption;
	}

	/**
	 * Constructs the option escape code depending on the escape code status (as
	 * of {@link #isEscapeCodesEnabled()}) and whether the option escape code is
	 * not null or null (as of {@link #getOptionEscapeCode()}). In case escape
	 * codes are enabled and the option escape code is set, then the option
	 * escape code is returned, else an empty {@link String}.
	 * 
	 * @return The option escape code or an empty {@link String} depending on
	 *         the escape code status and the option escape code's value.
	 */
	public String toOptionEscapeCode() {
		return _isEscapeCodesEnabled ? ( _optionEscapeCode != null ? _optionEscapeCode : "" ) : "";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toOptions( Option<?> aOption ) {
		return toOptions( aOption, _syntaxMetrics.getShortOptionPrefix(), _syntaxMetrics.getLongOptionPrefix() );
	}

	/**
	 * Constructs the reset escape code depending on the escape code status (as
	 * of {@link #isEscapeCodesEnabled()}) and whether the reset escape code is
	 * not null or null (as of {@link #getResetEscapeCode()}). In case escape
	 * codes are enabled and the reset escape code is set, then the reset escape
	 * code is returned, else an empty {@link String}.
	 * 
	 * @return The reset escape code or an empty {@link String} depending on the
	 *         escape code status and the reset escape code's value.
	 */
	public String toResetEscapeCode() {
		return _isEscapeCodesEnabled ? ( _resetEscapeCode != null ? _resetEscapeCode : "" ) : "";
	}

	/**
	 * Creates the {@link CliContext} instance specific prefixed short option
	 * {@link String} from the provided {@link Option} by prefixing the
	 * {@link Option}'s short option (as of {@link Option#getShortOption()})
	 * with the short option prefix (as of
	 * {@link SyntaxMetrics#getShortOptionPrefix()}).
	 * 
	 * @param aOption The {@link Option} from which to create the according
	 *        prefixed short option {@link String}.
	 * 
	 * @return The accordingly prefixed short option {@link String} or
	 *         <code>null</code> if there is no short option set for the
	 *         provided {@link Option} or the {@link Option} is null.
	 */
	public String toShortOption( Option<?> aOption ) {
		return aOption != null && aOption.getShortOption() != null ? _syntaxMetrics.getShortOptionPrefix().toString() + aOption.getShortOption() : null;
	}

	/**
	 * Creates a specification for the given {@link Operand} and its arguments.
	 * 
	 * @param aOperand The {@link Operand} for which to generate the
	 *        specification.
	 * 
	 * @return The according specification text.
	 */
	public String toSpec( Operand<?> aOperand ) {
		return toNotation( aOperand, UsageMode.SPEC );
	}

	/**
	 * Creates a specification for the given {@link Operand} and its arguments.
	 * In case of having an {@link Option} provided, then the short option and
	 * the long option are declared by the notation (as of
	 * {@link #toOptionSpec(Option)}).
	 * 
	 * @param aOperand The {@link Operand} for which to generate the
	 *        specification.
	 * 
	 * @return The according usage text.
	 */
	public String toUsage( Operand<?> aOperand ) {
		return toNotation( aOperand, UsageMode.USAGE );
	}

	/**
	 * Creates a {@link Builder} used to fluently construct {@link CliContext}
	 * instances.
	 * 
	 * @return The {@link Builder} for constructing {@link CliContext}
	 *         instances.
	 */
	public static Builder builder() {
		return new Builder();
	}
	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Generates an array with the available options.
	 *
	 * @param aOption the option
	 * @param aShortOptionPrefix the short option prefix
	 * @param aLongOptionPrefix the long option prefix
	 * 
	 * @return the string[]
	 */
	static String[] toOptions( Option<?> aOption, Character aShortOptionPrefix, String aLongOptionPrefix ) {
		int i = 0;
		final String theShortOption = aOption != null && aOption.getShortOption() != null ? ( aShortOptionPrefix != null ? aShortOptionPrefix.toString() : "" ) + aOption.getShortOption() : null;
		final String theLongOption = aOption != null && aOption.getLongOption() != null ? ( aLongOptionPrefix != null ? aLongOptionPrefix : "" ) + aOption.getLongOption() : null;
		if ( theShortOption != null ) {
			i++;
		}
		if ( theLongOption != null ) {
			i++;
		}
		final String[] theResult = new String[i];
		i = 0;
		if ( theShortOption != null ) {
			theResult[i] = theShortOption;
			i++;
		}
		if ( theLongOption != null ) {
			theResult[i] = theLongOption;
		}
		return theResult;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private String toNotation( Operand<?> aOperand, UsageMode aUsageMode ) {
		final StringBuilder theBuilder = new StringBuilder();
		if ( aOperand instanceof Option<?> theOption ) {
			final String theUsage = switch ( aUsageMode ) {
			case SPEC -> toOptionSpec( theOption );
			case USAGE -> toOptionUsage( theOption );
			};
			theBuilder.append( toOptionEscapeCode() + theUsage + toResetEscapeCode() );
		}
		final String theArgumentSpec = toArgumentSpec( aOperand );
		if ( theArgumentSpec != null && theArgumentSpec.length() != 0 && theBuilder.length() > 0 ) {
			theBuilder.append( ' ' );
		}
		theBuilder.append( theArgumentSpec );
		return theBuilder.toString();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Builder to build {@link SyntaxMetricsBuilder} instances.
	 */
	public static final class Builder implements SyntaxMetricsBuilder<Builder>, ArgumentEscapeCodeBuilder<Builder>, OptionEscapeCodeBuilder<Builder>, ResetEscapeCodeBuilder<Builder>, EscapeCodeStatusBuilder<Builder> {

		private String argumentEscapeCode = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.DEFAULT_FOREGROUND_COLOR, AnsiEscapeCode.FAINT, AnsiEscapeCode.ITALIC );
		private boolean isEscapeCodesEnabled = Terminal.isAnsiTerminalEnabled();
		private String optionEscapeCode = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.DEFAULT_FOREGROUND_COLOR, AnsiEscapeCode.BOLD );
		private String resetEscapeCode = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.RESET );
		private SyntaxMetrics syntaxMetrics = SyntaxNotation.LOGICAL;

		private Builder() {}

		/**
		 * {@link Builder} for fluently building a {@link CliContext} instances.
		 * 
		 * @return The according constructed {@link CliContext} instance.
		 */
		public CliContext build() {
			return new CliContext( this );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withArgumentEscapeCode( String aArgumentEscapeCode ) {
			argumentEscapeCode = aArgumentEscapeCode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withEscapeCodesEnabled( boolean isEscapeCodesEnabled ) {
			this.isEscapeCodesEnabled = isEscapeCodesEnabled;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withOptionEscapeCode( String aOptionEscapeCode ) {
			optionEscapeCode = aOptionEscapeCode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withResetEscapeCode( String aResetEscapeCode ) {
			resetEscapeCode = aResetEscapeCode;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSyntaxMetrics( SyntaxMetrics aSyntaxMetrics ) {
			syntaxMetrics = aSyntaxMetrics;
			return this;
		}
	}
}
