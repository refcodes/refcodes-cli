// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a {@link CliContext} property.
 */
public interface CliContextAccessor {

	/**
	 * Provides a builder method for a {@link CliContext} property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface CliContextBuilder<B extends CliContextBuilder<B>> {

		/**
		 * Sets the {@link CliContext} for the {@link CliContext} property.
		 * 
		 * @param aCliContext The {@link CliContext} to be stored by the root
		 *        condition property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withCliContext( CliContext aCliContext );

	}

	/**
	 * Provides a mutator for a {@link CliContext} property.
	 */
	public interface CliContextMutator {

		/**
		 * Sets the {@link CliContext} for the {@link CliContext} property.
		 * 
		 * @param aCliContext The {@link CliContext} to be stored by the root
		 *        condition property.
		 */
		void setCliContext( CliContext aCliContext );

	}

	/**
	 * Provides a {@link CliContext} property.
	 */
	public interface CliContextProperty extends CliContextAccessor, CliContextMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link CliContext}
		 * (setter) as of {@link #setCliContext(CliContext)} and returns the
		 * very same value (getter).
		 * 
		 * @param aCliContext The {@link CliContext} to set (via
		 *        {@link #setCliContext(CliContext)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default CliContext letCliContext( CliContext aCliContext ) {
			setCliContext( aCliContext );
			return aCliContext;
		}
	}

	/**
	 * Retrieves the {@link CliContext} from the {@link CliContext} property.
	 * 
	 * @return The {@link CliContext} stored by the {@link CliContext} property.
	 */
	CliContext getCliContext();
}
