// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import org.refcodes.exception.AbstractException;

/**
 * Base exception for the CLI artifact.
 */
public abstract class CliException extends AbstractException {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	public CliException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public CliException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public CliException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public CliException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public CliException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public CliException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * This abstract exception is the base exception for all command line
	 * argument related exceptions.
	 */
	public abstract static class CliArgsException extends CliException implements ArgsAccessor {

		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		protected String[] _args;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 * 
		 * @param aArgs The command line arguments involved in this exception.
		 */
		public CliArgsException( String aMessage, String[] aArgs ) {
			super( aMessage );
			_args = aArgs;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aArgs The command line arguments involved in this exception.
		 */
		public CliArgsException( String aMessage, String[] aArgs, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_args = aArgs;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aArgs The command line arguments involved in this exception.
		 */
		public CliArgsException( String aMessage, String[] aArgs, Throwable aCause ) {
			super( aMessage, aCause );
			_args = aArgs;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aArgs The command line arguments involved in this exception.
		 */
		public CliArgsException( String aMessage, String[] aArgs, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_args = aArgs;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aArgs The command line arguments involved in this exception.
		 */
		public CliArgsException( String[] aArgs, Throwable aCause ) {
			super( aCause );
			_args = aArgs;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aArgs The command line arguments involved in this exception.
		 */
		public CliArgsException( String[] aArgs, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_args = aArgs;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String[] getArgs() {
			return _args;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getPatternArguments() {
			return new Object[] { _args };
		}
	}
}
