// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import org.refcodes.mixin.EscapeCodesStatusAccessor;
import org.refcodes.mixin.ResetEscapeCodeAccessor;

/**
 * The {@link CliMetrics} define the attributes required to parse a {@link Term}
 * tree's syntax by an {@link ParseArgs}.
 */
public interface CliMetrics extends ResetEscapeCodeAccessor, OptionEscapeCodeAccessor, ArgumentEscapeCodeAccessor, EscapeCodesStatusAccessor, SyntaxMetricsAccessor {

}
