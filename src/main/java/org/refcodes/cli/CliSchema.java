// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import org.refcodes.mixin.ValueAccessor;
import org.refcodes.schema.Schema;

/**
 * The purpose of a {@link CliSchema} is automatically generate documentation of
 * {@link Term} structures.
 */
public class CliSchema extends Schema implements ValueAccessor<Object> {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String MATCH_COUNT = "MATCH_COUNT";
	public static final String HAS_VALUE_KEY = "HAS_VALUE";

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link CliSchema}.
	 *
	 * @param aType The type providing the this {@link Schema} instance.
	 * @param aDescription The description of the schema described by the
	 *        {@link Schema} providing type.
	 * @param aValue The a value of the according element.
	 */
	public CliSchema( Class<?> aType, String aDescription, Object aValue ) {
		super( aType, aDescription );
		put( VALUE, aValue );
	}

	/**
	 * Instantiates a new {@link CliSchema}.
	 *
	 * @param aType The type providing the this {@link Schema} instance.
	 * @param aDescription The description of the schema described by the
	 *        {@link Schema} providing type.
	 * @param aChildren The children's {@link Schema} descriptions representing
	 *        part of the {@link Schema} providing type.
	 */
	public CliSchema( Class<?> aType, String aDescription, Schema... aChildren ) {
		super( aType, aDescription, aChildren );
	}

	/**
	 * Instantiates a new {@link CliSchema}.
	 *
	 * @param aAlias The alias (name) of the schema described by the
	 *        {@link Schema} providing type.
	 * @param aType The type providing the this {@link Schema} instance.
	 * @param aValue The a value of the according element.
	 * @param aDescription The description of the schema described by the
	 *        {@link Schema} providing type.
	 * @param aChildren The children's {@link Schema} descriptions representing
	 *        part of the {@link Schema} providing type.
	 */
	public CliSchema( String aAlias, Class<?> aType, Object aValue, String aDescription, Schema... aChildren ) {
		super( aAlias, aType, aDescription, aChildren );
		put( VALUE, aValue );
	}

	/**
	 * Instantiates a new {@link CliSchema}.
	 *
	 * @param aAlias The alias (name) of the schema described by the
	 *        {@link Schema} providing type.
	 * @param aType The type providing the this {@link Schema} instance.
	 * @param aDescription The description of the schema described by the
	 *        {@link Schema} providing type.
	 * @param aChildren The children's {@link Schema} descriptions representing
	 *        part of the {@link Schema} providing type.
	 */
	public CliSchema( String aAlias, Class<?> aType, String aDescription, Schema... aChildren ) {
		super( aAlias, aType, aDescription, aChildren );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getValue() {
		return get( VALUE );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

}