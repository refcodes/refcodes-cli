// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.function.Consumer;

import org.refcodes.struct.Relation;

/**
 * Declarative syntactic sugar which may be statically imported in order to
 * allow declarative definitions for the command line {@link Flag},
 * {@link Condition}, {@link Option} and {@link Operand} elements.
 */
public class CliSugar {

	private CliSugar() {
		throw new IllegalStateException( "Utility class" );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link AllCondition} with the {@link Term}
	 * ({@link Condition}) instance to be nested.
	 *
	 * @param aArg The {@link Term} ({@link Condition}) instance to be nested.
	 * 
	 * @return The according ALL condition.
	 * 
	 * @see AllCondition
	 */
	public static AllCondition all( Term aArg ) {
		return new AllCondition( aArg );
	}

	/**
	 * Instantiates a new {@link AndCondition} with the {@link Term}
	 * ({@link Condition}) instances to be nested.
	 *
	 * @param aArgs The {@link Term} ({@link Condition}) instances to be nested.
	 * 
	 * @return The according AND condition.
	 * 
	 * @see AndCondition
	 */
	public static AndCondition and( Term... aArgs ) {
		return new AndCondition( aArgs );
	}

	/**
	 * Instantiates a new {@link AnyCondition} with the {@link Term}
	 * ({@link Condition}) instances to be nested. Any of the nested
	 * {@link Condition} conditions may match for the {@link AnyCondition} to
	 * match, e.g. all of the nested conditions are optional.
	 *
	 * @param aArgs The {@link Term} ({@link Condition}) instances to be nested
	 * 
	 * @return The according {@link AbstractCondition}.
	 * 
	 * @see AnyCondition
	 */
	public static AnyCondition any( Term... aArgs ) {
		return new AnyCondition( aArgs );
	}

	/**
	 * Creates an array representation facade for the encapsulated
	 * {@link Operand}. This way any {@link Operand} can also be used as an
	 * array {@link Operand}, e.g. it can be provided multiple times in the
	 * command line arguments.
	 * 
	 * @param <T> The type of the {@link Operand} for which to create an
	 *        {@link ArrayOperand}.
	 * @param aOperand The {@link Operand} which's array counterpart is to be
	 *        defined.
	 * 
	 * @return The according {@link ArrayOperand}.
	 */
	public static <T> ArrayOperand<T> asArray( Operand<T> aOperand ) {
		return new ArrayOperand<>( aOperand );
	}

	/**
	 * Creates an array representation facade for the encapsulated
	 * {@link Operand}. This way any {@link Operand} can also be used as an
	 * array {@link Operand}, e.g. it can by provided multiple times in the
	 * command line arguments.
	 * 
	 * @param <T> The type of the {@link Operand} for which to create an
	 *        {@link ArrayOperand}.
	 * @param aOperand The {@link Operand} which's array counterpart is to be
	 *        defined.
	 * @param aLength The number of array elements, or -1 if there is no limit.
	 * 
	 * @return The according {@link ArrayOperand}.
	 */
	public static <T> ArrayOperand<T> asArray( Operand<T> aOperand, int aLength ) {
		return new ArrayOperand<>( aOperand, aLength );
	}

	/**
	 * Creates an array representation facade for the encapsulated
	 * {@link Operand}. This way any {@link Operand} can also be used as an
	 * array {@link Operand}, e.g. it can by provided multiple times in the
	 * command line arguments.
	 * 
	 * @param <T> The type of the {@link Operand} for which to create an
	 *        {@link ArrayOperand}.
	 * @param aOperand The {@link Operand} which's array counterpart is to be
	 *        defined.
	 * @param aMinLength The minimum number of array elements, or -1 if there is
	 *        no limit.
	 * @param aMaxLength The maximum number of array elements, or -1 if there is
	 *        no limit.
	 * 
	 * @return The according {@link ArrayOperand}.
	 */
	public static <T> ArrayOperand<T> asArray( Operand<T> aOperand, int aMinLength, int aMaxLength ) {
		return new ArrayOperand<>( aOperand, aMinLength, aMaxLength );
	}

	/**
	 * Creates an array representation facade for the encapsulated
	 * {@link Option}. This way any {@link Option} can also be used as an array
	 * {@link Option}, e.g. it can be provided multiple times in the command
	 * line arguments.
	 * 
	 * @param <T> The type of the {@link Option} for which to create an
	 *        {@link ArrayOption}.
	 * @param aOption The {@link Option} which's array counterpart is to be
	 *        defined.
	 * 
	 * @return The according {@link ArrayOption}.
	 */
	public static <T> ArrayOption<T> asArray( Option<T> aOption ) {
		return new ArrayOption<>( aOption );
	}

	/**
	 * Creates an array representation facade for the encapsulated
	 * {@link Option}. This way any {@link Option} can also be used as an array
	 * {@link Option}, e.g. it can by provided multiple times in the command
	 * line arguments.
	 * 
	 * @param <T> The type of the {@link Option} for which to create an
	 *        {@link ArrayOption}.
	 * @param aOption The {@link Option} which's array counterpart is to be
	 *        defined.
	 * @param aLength The number of array elements, or -1 if there is no limit.
	 * 
	 * @return The according {@link ArrayOption}.
	 */
	public static <T> ArrayOption<T> asArray( Option<T> aOption, int aLength ) {
		return new ArrayOption<>( aOption, aLength );
	}

	/**
	 * Creates an array representation facade for the encapsulated
	 * {@link Option}. This way any {@link Option} can also be used as an array
	 * {@link Option}, e.g. it can by provided multiple times in the command
	 * line arguments.
	 * 
	 * @param <T> The type of the {@link Option} for which to create an
	 *        {@link ArrayOption}.
	 * @param aOption The {@link Option} which's array counterpart is to be
	 *        defined.
	 * @param aMinLength The minimum number of array elements, or -1 if there is
	 *        no limit.
	 * @param aMaxLength The maximum number of array elements, or -1 if there is
	 *        no limit.
	 * 
	 * @return The according {@link ArrayOption}.
	 */
	public static <T> ArrayOption<T> asArray( Option<T> aOption, int aMinLength, int aMaxLength ) {
		return new ArrayOption<>( aOption, aMinLength, aMaxLength );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link BooleanOption}
	 * 
	 * @return The accordingly created {@link BooleanOption} instance.
	 * 
	 * @see BooleanOption
	 */
	public static BooleanOption booleanOption( Character aShortOption, String aLongOption, String aDescription ) {
		return new BooleanOption( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link BooleanOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link BooleanOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link BooleanOption} instance.
	 * 
	 * @see BooleanOption
	 */
	public static BooleanOption booleanOption( Character aShortOption, String aLongOption, String aDescription, Consumer<BooleanOption> aConsumer ) {
		return new BooleanOption( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link BooleanOption}
	 * 
	 * @return The accordingly created {@link BooleanOption} instance.
	 * 
	 * @see BooleanOption
	 */
	public static BooleanOption booleanOption( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new BooleanOption( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link BooleanOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link BooleanOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link BooleanOption} instance.
	 * 
	 * @see BooleanOption
	 */
	public static BooleanOption booleanOption( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<BooleanOption> aConsumer ) {
		return new BooleanOption( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link BooleanOption}
	 * 
	 * @return The accordingly created {@link BooleanOption} instance.
	 * 
	 * @see BooleanOption
	 */
	public static BooleanOption booleanOption( String aLongOption, String aDescription ) {
		return new BooleanOption( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link BooleanOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link BooleanOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link BooleanOption} instance.
	 * 
	 * @see BooleanOption
	 */
	public static BooleanOption booleanOption( String aLongOption, String aDescription, Consumer<BooleanOption> aConsumer ) {
		return new BooleanOption( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link BooleanOption}
	 * 
	 * @return The accordingly created {@link BooleanOption} instance.
	 * 
	 * @see BooleanOption
	 */
	public static BooleanOption booleanOption( String aLongOption, String aAlias, String aDescription ) {
		return new BooleanOption( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link BooleanOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link BooleanOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link BooleanOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link BooleanOption} instance.
	 * 
	 * @see BooleanOption
	 */
	public static BooleanOption booleanOption( String aLongOption, String aAlias, String aDescription, Consumer<BooleanOption> aConsumer ) {
		return new BooleanOption( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link BooleanProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption the boolean property
	 * @param aDescription The description of the {@link BooleanProperty}
	 * 
	 * @return The accordingly created {@link BooleanProperty} instance.
	 * 
	 * @see BooleanProperty
	 */
	public static BooleanProperty booleanProperty( Character aShortOption, String aLongOption, String aDescription ) {
		return new BooleanProperty( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link BooleanProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption the boolean property
	 * @param aDescription The description of the {@link BooleanProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link BooleanProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link BooleanProperty} instance.
	 * 
	 * @see BooleanProperty
	 */
	public static BooleanProperty booleanProperty( Character aShortOption, String aLongOption, String aDescription, Consumer<BooleanProperty> aConsumer ) {
		return new BooleanProperty( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link BooleanProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link BooleanProperty}
	 * 
	 * @return The accordingly created {@link BooleanProperty} instance.
	 * 
	 * @see BooleanProperty
	 */
	public static BooleanProperty booleanProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new BooleanProperty( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link BooleanProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link BooleanProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link BooleanProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link BooleanProperty} instance.
	 * 
	 * @see BooleanProperty
	 */
	public static BooleanProperty booleanProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<BooleanProperty> aConsumer ) {
		return new BooleanProperty( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link BooleanProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link BooleanProperty}
	 * 
	 * @return The accordingly created {@link BooleanProperty} instance.
	 * 
	 * @see BooleanProperty
	 */
	public static BooleanProperty booleanProperty( String aLongOption, String aDescription ) {
		return new BooleanProperty( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link BooleanProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link BooleanProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link BooleanProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link BooleanProperty} instance.
	 * 
	 * @see BooleanProperty
	 */
	public static BooleanProperty booleanProperty( String aLongOption, String aDescription, Consumer<BooleanProperty> aConsumer ) {
		return new BooleanProperty( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link BooleanProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link BooleanProperty}
	 * 
	 * @return The accordingly created {@link BooleanProperty} instance.
	 * 
	 * @see BooleanProperty
	 */
	public static BooleanProperty booleanProperty( String aLongOption, String aAlias, String aDescription ) {
		return new BooleanProperty( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link BooleanProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link BooleanProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link BooleanProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link BooleanProperty} instance.
	 * 
	 * @see BooleanProperty
	 */
	public static BooleanProperty booleanProperty( String aLongOption, String aAlias, String aDescription, Consumer<BooleanProperty> aConsumer ) {
		return new BooleanProperty( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link CasesCondition} with the {@link Term}
	 * ({@link Condition}) instances to be nested.
	 *
	 * @param aArgs The {@link Term} ({@link Condition}) instances to be nested.
	 * 
	 * @return The according CasesCondition (https://www.metacodes.pro XOR)
	 *         condition.
	 * 
	 * @see CasesCondition
	 */
	public static CasesCondition cases( Term... aArgs ) {
		return new CasesCondition( aArgs );
	}

	/**
	 * Instantiates a new {@link CharOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link CharOption}
	 * 
	 * @return The accordingly created {@link EnumOption}instance.
	 * 
	 * @see CharOption
	 */
	public static CharOption charOption( Character aShortOption, String aLongOption, String aDescription ) {
		return new CharOption( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link CharOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link CharOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link CharOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link CharOption} instance.
	 * 
	 * @see CharOption
	 */
	public static CharOption charOption( Character aShortOption, String aLongOption, String aDescription, Consumer<CharOption> aConsumer ) {
		return new CharOption( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link CharOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link CharOption}
	 * 
	 * @return The accordingly created {@link EnumOption}instance.
	 * 
	 * @see CharOption
	 */
	public static CharOption charOption( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new CharOption( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link CharOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link CharOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link CharOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link CharOption} instance.
	 * 
	 * @see CharOption
	 */
	public static CharOption charOption( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<CharOption> aConsumer ) {
		return new CharOption( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link CharOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link CharOption}
	 * 
	 * @return The accordingly created {@link CharOption} instance.
	 * 
	 * @see CharOption
	 */
	public static CharOption charOption( String aLongOption, String aDescription ) {
		return new CharOption( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link CharOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link CharOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link CharOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link CharOption} instance.
	 * 
	 * @see CharOption
	 */
	public static CharOption charOption( String aLongOption, String aDescription, Consumer<CharOption> aConsumer ) {
		return new CharOption( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link CharOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link CharOption}
	 * 
	 * @return The accordingly created {@link CharOption} instance.
	 * 
	 * @see CharOption
	 */
	public static CharOption charOption( String aLongOption, String aAlias, String aDescription ) {
		return new CharOption( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link CharOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link CharOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link CharOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link CharOption} instance.
	 * 
	 * @see CharOption
	 */
	public static CharOption charOption( String aLongOption, String aAlias, String aDescription, Consumer<CharOption> aConsumer ) {
		return new CharOption( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link CharProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link CharProperty}
	 * 
	 * @return The accordingly created {@link EnumProperty}instance.
	 * 
	 * @see CharProperty
	 */
	public static CharProperty charProperty( Character aShortOption, String aLongOption, String aDescription ) {
		return new CharProperty( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link CharProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link CharProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link CharProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link EnumProperty}instance.
	 * 
	 * @see CharProperty
	 */
	public static CharProperty charProperty( Character aShortOption, String aLongOption, String aDescription, Consumer<CharProperty> aConsumer ) {
		return new CharProperty( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link CharProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link CharProperty}
	 * 
	 * @return The accordingly created {@link EnumProperty}instance.
	 * 
	 * @see CharProperty
	 */
	public static CharProperty charProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new CharProperty( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link CharProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link CharProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link CharProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link EnumProperty}instance.
	 * 
	 * @see CharProperty
	 */
	public static CharProperty charProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<CharProperty> aConsumer ) {
		return new CharProperty( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link CharProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link CharProperty}
	 * 
	 * @return The accordingly created {@link CharProperty} instance.
	 * 
	 * @see CharProperty
	 */
	public static CharProperty charProperty( String aLongOption, String aDescription ) {
		return new CharProperty( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link CharProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link CharProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link CharProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link CharProperty} instance.
	 * 
	 * @see CharProperty
	 */
	public static CharProperty charProperty( String aLongOption, String aDescription, Consumer<CharProperty> aConsumer ) {
		return new CharProperty( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link CharProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link CharProperty}
	 * 
	 * @return The accordingly created {@link CharProperty} instance.
	 * 
	 * @see CharProperty
	 */
	public static CharProperty charProperty( String aLongOption, String aAlias, String aDescription ) {
		return new CharProperty( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link CharProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link CharProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link CharProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link CharProperty} instance.
	 * 
	 * @see CharProperty
	 */
	public static CharProperty charProperty( String aLongOption, String aAlias, String aDescription, Consumer<CharProperty> aConsumer ) {
		return new CharProperty( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Constructs the predefined clean {@link Flag}.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see CleanFlag
	 */
	public static CleanFlag cleanFlag() {
		return new CleanFlag();
	}

	/**
	 * Constructs the predefined clean {@link Flag}.
	 * 
	 * @param hasShortOption True in case to also enable the short option, else
	 *        only the long option takes effect.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see CleanFlag
	 */
	public static CleanFlag cleanFlag( boolean hasShortOption ) {
		return new CleanFlag( hasShortOption );
	}

	/**
	 * Constructs the predefined clean {@link Flag}.
	 *
	 * @param aDescription The description to use when printing out the help
	 *        text.
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see CleanFlag
	 */
	public static CleanFlag cleanFlag( String aDescription ) {
		return new CleanFlag( aDescription );
	}

	/**
	 * Creates a {@link ConfigOption} representing value specifying a
	 * configuration resource (file).
	 *
	 * @return the according {@link ConfigOption}.
	 * 
	 * @see ConfigOption
	 */
	public static ConfigOption configOption() {
		return new ConfigOption();
	}

	/**
	 * Instantiates a new config (file) {@link Option}.
	 *
	 * @param aDescription The description to use.
	 *
	 * @return the according {@link ConfigOption}.
	 * 
	 * @see ConfigOption
	 */
	public static ConfigOption configOption( String aDescription ) {
		return new ConfigOption( aDescription );
	}

	/**
	 * Constructs the predefined daemon {@link Flag}.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see DaemonFlag
	 */
	public static DaemonFlag daemonFlag() {
		return new DaemonFlag();
	}

	/**
	 * Constructs the predefined daemon {@link Flag}.
	 * 
	 * @param hasShortOption True in case to also enable the short option, else
	 *        only the long option takes effect.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see DaemonFlag
	 */
	public static DaemonFlag daemonFlag( boolean hasShortOption ) {
		return new DaemonFlag( hasShortOption );
	}

	/**
	 * Constructs the predefined daemon {@link Flag}.
	 * 
	 * @param aDescription The description to be used (without any line breaks).
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see DaemonFlag
	 */
	public static DaemonFlag daemonFlag( String aDescription ) {
		return new DaemonFlag( aDescription );
	}

	/**
	 * Constructs the predefined debug {@link Flag}.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see DebugFlag
	 */
	public static DebugFlag debugFlag() {
		return new DebugFlag();
	}

	/**
	 * Constructs the predefined debug {@link Flag}.
	 * 
	 * @param hasShortOption True in case to also enable the short option, else
	 *        only the long option takes effect.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see DebugFlag
	 */
	public static DebugFlag debugFlag( boolean hasShortOption ) {
		return new DebugFlag( hasShortOption );
	}

	/**
	 * Constructs the predefined debug {@link Flag}.
	 *
	 * @param aDescription The description to use when printing out the help
	 *        text.
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see DebugFlag
	 */
	public static DebugFlag debugFlag( String aDescription ) {
		return new DebugFlag( aDescription );
	}

	/**
	 * Instantiates a new {@link DoubleOption} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link DoubleOption}
	 * 
	 * @return The accordingly created {@link DoubleOption} instance.
	 * 
	 * @see DoubleOption
	 */
	public static DoubleOption doubleOption( Character aShortOption, String aLongOption, String aDescription ) {
		return new DoubleOption( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link DoubleOption} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link DoubleOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link DoubleOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link DoubleOption} instance.
	 * 
	 * @see DoubleOption
	 */
	public static DoubleOption doubleOption( Character aShortOption, String aLongOption, String aDescription, Consumer<DoubleOption> aConsumer ) {
		return new DoubleOption( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link DoubleOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link DoubleOption}
	 * 
	 * @return The accordingly created {@link DoubleOption} instance.
	 * 
	 * @see DoubleOption
	 */
	public static DoubleOption doubleOption( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new DoubleOption( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link DoubleOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link DoubleOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link DoubleOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link DoubleOption} instance.
	 * 
	 * @see DoubleOption
	 */
	public static DoubleOption doubleOption( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<DoubleOption> aConsumer ) {
		return new DoubleOption( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link DoubleOption} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link DoubleOption}
	 * 
	 * @return The accordingly created {@link DoubleOption} instance.
	 * 
	 * @see DoubleOption
	 */
	public static DoubleOption doubleOption( String aLongOption, String aDescription ) {
		return new DoubleOption( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link DoubleOption} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link DoubleOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link DoubleOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link DoubleOption} instance.
	 * 
	 * @see DoubleOption
	 */
	public static DoubleOption doubleOption( String aLongOption, String aDescription, Consumer<DoubleOption> aConsumer ) {
		return new DoubleOption( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link DoubleOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link DoubleOption}
	 * 
	 * @return The accordingly created {@link DoubleOption} instance.
	 * 
	 * @see DoubleOption
	 */
	public static DoubleOption doubleOption( String aLongOption, String aAlias, String aDescription ) {
		return new DoubleOption( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link DoubleOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link DoubleOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link DoubleOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link DoubleOption} instance.
	 * 
	 * @see DoubleOption
	 */
	public static DoubleOption doubleOption( String aLongOption, String aAlias, String aDescription, Consumer<DoubleOption> aConsumer ) {
		return new DoubleOption( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link DoubleProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption the double property
	 * @param aDescription The description of the {@link DoubleProperty}
	 * 
	 * @return The accordingly created {@link DoubleProperty} instance.
	 * 
	 * @see DoubleProperty
	 */
	public static DoubleProperty doubleProperty( Character aShortOption, String aLongOption, String aDescription ) {
		return new DoubleProperty( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link DoubleProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption the double property
	 * @param aDescription The description of the {@link DoubleProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link DoubleProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link DoubleProperty} instance.
	 * 
	 * @see DoubleProperty
	 */
	public static DoubleProperty doubleProperty( Character aShortOption, String aLongOption, String aDescription, Consumer<DoubleProperty> aConsumer ) {
		return new DoubleProperty( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link DoubleProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link DoubleProperty}
	 * 
	 * @return The accordingly created {@link DoubleProperty} instance.
	 * 
	 * @see DoubleProperty
	 */
	public static DoubleProperty doubleProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new DoubleProperty( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link DoubleProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link DoubleProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link DoubleProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link DoubleProperty} instance.
	 * 
	 * @see DoubleProperty
	 */
	public static DoubleProperty doubleProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<DoubleProperty> aConsumer ) {
		return new DoubleProperty( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link DoubleProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link DoubleProperty}
	 * 
	 * @return The accordingly created {@link DoubleProperty} instance.
	 * 
	 * @see DoubleProperty
	 */
	public static DoubleProperty doubleProperty( String aLongOption, String aDescription ) {
		return new DoubleProperty( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link DoubleProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link DoubleProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link DoubleProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link DoubleProperty} instance.
	 * 
	 * @see DoubleProperty
	 */
	public static DoubleProperty doubleProperty( String aLongOption, String aDescription, Consumer<DoubleProperty> aConsumer ) {
		return new DoubleProperty( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link DoubleProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link DoubleProperty}
	 * 
	 * @return The accordingly created {@link DoubleProperty} instance.
	 * 
	 * @see DoubleProperty
	 */
	public static DoubleProperty doubleProperty( String aLongOption, String aAlias, String aDescription ) {
		return new DoubleProperty( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link DoubleProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link DoubleProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link DoubleProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link DoubleProperty} instance.
	 * 
	 * @see DoubleProperty
	 */
	public static DoubleProperty doubleProperty( String aLongOption, String aAlias, String aDescription, Consumer<DoubleProperty> aConsumer ) {
		return new DoubleProperty( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link EnumOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param <T> The generic type of the enumeration.
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aDescription The description of the {@link EnumOption}
	 * 
	 * @return The accordingly created {@link EnumOption} instance.
	 * 
	 * @see EnumOption
	 */
	public static <T extends Enum<T>> EnumOption<T> enumOption( Character aShortOption, String aLongOption, Class<T> aType, String aDescription ) {
		return new EnumOption<>( aShortOption, aLongOption, aType, aDescription );
	}

	/**
	 * Instantiates a new {@link EnumOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param <T> The generic type of the enumeration.
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aDescription The description of the {@link EnumOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link EnumOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link EnumOption} instance.
	 * 
	 * @see EnumOption
	 */
	public static <T extends Enum<T>> EnumOption<T> enumOption( Character aShortOption, String aLongOption, Class<T> aType, String aDescription, Consumer<EnumOption<T>> aConsumer ) {
		return new EnumOption<>( aShortOption, aLongOption, aType, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link EnumOption} with the given arguments.
	 *
	 * @param <T> The generic type of the enumeration.
	 * 
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link EnumOption}
	 * 
	 * @return The accordingly created {@link EnumOption} instance.
	 * 
	 * @see EnumOption
	 */
	public static <T extends Enum<T>> EnumOption<T> enumOption( Character aShortOption, String aLongOption, Class<T> aType, String aAlias, String aDescription ) {
		return new EnumOption<>( aShortOption, aLongOption, aType, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link EnumOption} with the given arguments.
	 *
	 * @param <T> The generic type of the enumeration.
	 * 
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link EnumOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link EnumOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link EnumOption} instance.
	 * 
	 * @see EnumOption
	 */
	public static <T extends Enum<T>> EnumOption<T> enumOption( Character aShortOption, String aLongOption, Class<T> aType, String aAlias, String aDescription, Consumer<EnumOption<T>> aConsumer ) {
		return new EnumOption<>( aShortOption, aLongOption, aType, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link EnumOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param <T> The generic type of the enumeration.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aDescription The description of the {@link EnumOption}
	 * 
	 * @return The accordingly created {@link EnumOption} instance.
	 * 
	 * @see EnumOption
	 */
	public static <T extends Enum<T>> EnumOption<T> enumOption( String aLongOption, Class<T> aType, String aDescription ) {
		return new EnumOption<>( aLongOption, aType, aDescription );
	}

	/**
	 * Instantiates a new {@link EnumOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param <T> The generic type of the enumeration.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aDescription The description of the {@link EnumOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link EnumOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link EnumOption} instance.
	 * 
	 * @see EnumOption
	 */
	public static <T extends Enum<T>> EnumOption<T> enumOption( String aLongOption, Class<T> aType, String aDescription, Consumer<EnumOption<T>> aConsumer ) {
		return new EnumOption<>( aLongOption, aType, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link EnumOption} with the given arguments.
	 *
	 * @param <T> The generic type of the enumeration.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link EnumOption}
	 * 
	 * @return The accordingly created {@link EnumOption} instance.
	 * 
	 * @see EnumOption
	 */
	public static <T extends Enum<T>> EnumOption<T> enumOption( String aLongOption, Class<T> aType, String aAlias, String aDescription ) {
		return new EnumOption<>( aLongOption, aType, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link EnumOption} with the given arguments.
	 *
	 * @param <T> The generic type of the enumeration.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link EnumOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link EnumOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link EnumOption} instance.
	 * 
	 * @see EnumOption
	 */
	public static <T extends Enum<T>> EnumOption<T> enumOption( String aLongOption, Class<T> aType, String aAlias, String aDescription, Consumer<EnumOption<T>> aConsumer ) {
		return new EnumOption<>( aLongOption, aType, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param <T> The generic type of the enumeration.
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aDescription The description of the {@link EnumProperty}
	 * 
	 * @return The accordingly created {@link EnumProperty} instance.
	 * 
	 * @see EnumProperty
	 */
	public static <T extends Enum<T>> EnumProperty<T> enumProperty( Character aShortOption, String aLongOption, Class<T> aType, String aDescription ) {
		return new EnumProperty<>( aShortOption, aLongOption, aType, aDescription );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param <T> The generic type of the enumeration.
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aDescription The description of the {@link EnumProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link EnumProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link EnumProperty} instance.
	 * 
	 * @see EnumProperty
	 */
	public static <T extends Enum<T>> EnumProperty<T> enumProperty( Character aShortOption, String aLongOption, Class<T> aType, String aDescription, Consumer<EnumProperty<T>> aConsumer ) {
		return new EnumProperty<>( aShortOption, aLongOption, aType, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments.
	 *
	 * @param <T> The generic type of the enumeration.
	 * 
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link EnumProperty}
	 * 
	 * @return The accordingly created {@link EnumProperty} instance.
	 * 
	 * @see EnumProperty
	 */
	public static <T extends Enum<T>> EnumProperty<T> enumProperty( Character aShortOption, String aLongOption, Class<T> aType, String aAlias, String aDescription ) {
		return new EnumProperty<>( aShortOption, aLongOption, aType, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments.
	 *
	 * @param <T> The generic type of the enumeration.
	 * 
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link EnumProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link EnumProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link EnumProperty} instance.
	 * 
	 * @see EnumProperty
	 */
	public static <T extends Enum<T>> EnumProperty<T> enumProperty( Character aShortOption, String aLongOption, Class<T> aType, String aAlias, String aDescription, Consumer<EnumProperty<T>> aConsumer ) {
		return new EnumProperty<>( aShortOption, aLongOption, aType, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param <T> The generic type of the enumeration.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aDescription The description of the {@link EnumProperty}
	 * 
	 * @return The accordingly created {@link EnumProperty} instance.
	 * 
	 * @see EnumProperty
	 */
	public static <T extends Enum<T>> EnumProperty<T> enumProperty( String aLongOption, Class<T> aType, String aDescription ) {
		return new EnumProperty<>( aLongOption, aType, aDescription );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param <T> The generic type of the enumeration.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aDescription The description of the {@link EnumProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link EnumProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link EnumProperty} instance.
	 * 
	 * @see EnumProperty
	 */
	public static <T extends Enum<T>> EnumProperty<T> enumProperty( String aLongOption, Class<T> aType, String aDescription, Consumer<EnumProperty<T>> aConsumer ) {
		return new EnumProperty<>( aLongOption, aType, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments.
	 *
	 * @param <T> The generic type of the enumeration.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link EnumProperty}
	 * 
	 * @return The accordingly created {@link EnumProperty} instance.
	 * 
	 * @see EnumProperty
	 */
	public static <T extends Enum<T>> EnumProperty<T> enumProperty( String aLongOption, Class<T> aType, String aAlias, String aDescription ) {
		return new EnumProperty<>( aLongOption, aType, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments.
	 *
	 * @param <T> The generic type of the enumeration.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link EnumProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link EnumProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link EnumProperty} instance.
	 * 
	 * @see EnumProperty
	 */
	public static <T extends Enum<T>> EnumProperty<T> enumProperty( String aLongOption, Class<T> aType, String aAlias, String aDescription, Consumer<EnumProperty<T>> aConsumer ) {
		return new EnumProperty<>( aLongOption, aType, aAlias, aDescription, aConsumer );
	}

	/**
	 * Sugar for creating an {@link Example} from a description and the
	 * according {@link Operand} elements.
	 * 
	 * @param aDescription The explanatory text of the example.
	 * @param aOperands The operands required for the {@link Example}.
	 * 
	 * @return The according {@link Example} instance.
	 */
	public static Example example( String aDescription, Operand<?>... aOperands ) {
		return new Example( aDescription, aOperands );
	}

	/**
	 * Sugar for creating an {@link Example} array from a varargs argument.
	 * 
	 * @param aExamples the {@link Example} varargs argument.
	 * 
	 * @return The according array representation.
	 */
	public static Example[] examples( Example... aExamples ) {
		return aExamples;
	}

	/**
	 * Instantiates a new {@link FileOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link FileOption}
	 * 
	 * @return The according {@link FileOption}.
	 * 
	 * @see FileOption
	 */
	public static FileOption fileOption( Character aShortOption, String aLongOption, String aDescription ) {
		return new FileOption( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link FileOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link FileOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FileOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The according {@link FileOption}.
	 * 
	 * @see FileOption
	 */
	public static FileOption fileOption( Character aShortOption, String aLongOption, String aDescription, Consumer<FileOption> aConsumer ) {
		return new FileOption( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FileOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FileOption}
	 * 
	 * @return The according {@link FileOption}.
	 * 
	 * @see FileOption
	 */
	public static FileOption fileOption( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new FileOption( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link FileOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FileOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FileOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The according {@link FileOption}.
	 * 
	 * @see FileOption
	 */
	public static FileOption fileOption( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<FileOption> aConsumer ) {
		return new FileOption( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FileOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription the description
	 * 
	 * @return The according {@link FileOption}.
	 * 
	 * @see FileOption
	 */
	public static FileOption fileOption( String aLongOption, String aDescription ) {
		return new FileOption( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link FileOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription the description
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FileOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The according {@link FileOption}.
	 * 
	 * @see FileOption
	 */
	public static FileOption fileOption( String aLongOption, String aDescription, Consumer<FileOption> aConsumer ) {
		return new FileOption( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FileOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FileOption}
	 * 
	 * @return The according {@link FileOption}.
	 * 
	 * @see FileOption
	 */
	public static FileOption fileOption( String aLongOption, String aAlias, String aDescription ) {
		return new FileOption( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link FileOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FileOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FileOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The according {@link FileOption}.
	 * 
	 * @see FileOption
	 */
	public static FileOption fileOption( String aLongOption, String aAlias, String aDescription, Consumer<FileOption> aConsumer ) {
		return new FileOption( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link FileProperty}
	 * 
	 * @return The according {@link FileProperty}.
	 * 
	 * @see FileProperty
	 */
	public static FileProperty fileProperty( Character aShortOption, String aLongOption, String aDescription ) {
		return new FileProperty( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link FileProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FileProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The according {@link FileProperty}.
	 * 
	 * @see FileProperty
	 */
	public static FileProperty fileProperty( Character aShortOption, String aLongOption, String aDescription, Consumer<FileProperty> aConsumer ) {
		return new FileProperty( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FileProperty}
	 * 
	 * @return The according {@link FileProperty}.
	 * 
	 * @see FileProperty
	 */
	public static FileProperty fileProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new FileProperty( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FileProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FileProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The according {@link FileProperty}.
	 * 
	 * @see FileProperty
	 */
	public static FileProperty fileProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<FileProperty> aConsumer ) {
		return new FileProperty( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription the description
	 * 
	 * @return The according {@link FileProperty}.
	 * 
	 * @see FileProperty
	 */
	public static FileProperty fileProperty( String aLongOption, String aDescription ) {
		return new FileProperty( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription the description
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FileProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The according {@link FileProperty}.
	 * 
	 * @see FileProperty
	 */
	public static FileProperty fileProperty( String aLongOption, String aDescription, Consumer<FileProperty> aConsumer ) {
		return new FileProperty( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FileProperty}
	 * 
	 * @return The according {@link FileProperty}.
	 * 
	 * @see FileProperty
	 */
	public static FileProperty fileProperty( String aLongOption, String aAlias, String aDescription ) {
		return new FileProperty( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FileProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FileProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The according {@link FileProperty}.
	 * 
	 * @see FileProperty
	 */
	public static FileProperty fileProperty( String aLongOption, String aAlias, String aDescription, Consumer<FileProperty> aConsumer ) {
		return new FileProperty( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link Flag} with the given arguments. In case a long
	 * option is provided, the intance's alias will automatically be set with
	 * the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link Flag}
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see Flag
	 */
	public static Flag flag( Character aShortOption, String aLongOption, String aDescription ) {
		return new Flag( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link Flag} with the given arguments. In case a long
	 * option is provided, the intance's alias will automatically be set with
	 * the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link Flag}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Flag} participated in successfully parsing the command line
	 *        arguments.
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see Flag
	 */
	public static Flag flag( Character aShortOption, String aLongOption, String aDescription, Consumer<Flag> aConsumer ) {
		return new Flag( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link Flag} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link Flag}
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see Flag
	 */
	public static Flag flag( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new Flag( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link Flag} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link Flag}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Flag} participated in successfully parsing the command line
	 *        arguments.
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see Flag
	 */
	public static Flag flag( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<Flag> aConsumer ) {
		return new Flag( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link Flag} with the given arguments. In case a long
	 * option is provided, the intance's alias will automatically be set with
	 * the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link Flag}
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see Flag
	 */
	public static Flag flag( String aLongOption, String aDescription ) {
		return new Flag( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link Flag} with the given arguments. In case a long
	 * option is provided, the intance's alias will automatically be set with
	 * the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link Flag}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Flag} participated in successfully parsing the command line
	 *        arguments.
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see Flag
	 */
	public static Flag flag( String aLongOption, String aDescription, Consumer<Flag> aConsumer ) {
		return new Flag( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link Flag} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link Flag}
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see Flag
	 */
	public static Flag flag( String aLongOption, String aAlias, String aDescription ) {
		return new Flag( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link Flag} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link Flag}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Flag} participated in successfully parsing the command line
	 *        arguments.
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see Flag
	 */
	public static Flag flag( String aLongOption, String aAlias, String aDescription, Consumer<Flag> aConsumer ) {
		return new Flag( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FloatOption} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link FloatOption}
	 * 
	 * @return The accordingly created {@link FloatOption} instance.
	 * 
	 * @see FloatOption
	 */
	public static FloatOption floatOption( Character aShortOption, String aLongOption, String aDescription ) {
		return new FloatOption( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link FloatOption} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link FloatOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FloatOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link FloatOption} instance.
	 * 
	 * @see FloatOption
	 */
	public static FloatOption floatOption( Character aShortOption, String aLongOption, String aDescription, Consumer<FloatOption> aConsumer ) {
		return new FloatOption( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FloatOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FloatOption}
	 * 
	 * @return The accordingly created {@link FloatOption} instance.
	 * 
	 * @see FloatOption
	 */
	public static FloatOption floatOption( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new FloatOption( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link FloatOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FloatOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FloatOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link FloatOption} instance.
	 * 
	 * @see FloatOption
	 */
	public static FloatOption floatOption( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<FloatOption> aConsumer ) {
		return new FloatOption( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FloatOption} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link FloatOption}
	 * 
	 * @return The accordingly created {@link FloatOption} instance.
	 * 
	 * @see FloatOption
	 */
	public static FloatOption floatOption( String aLongOption, String aDescription ) {
		return new FloatOption( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link FloatOption} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link FloatOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FloatOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link FloatOption} instance.
	 * 
	 * @see FloatOption
	 */
	public static FloatOption floatOption( String aLongOption, String aDescription, Consumer<FloatOption> aConsumer ) {
		return new FloatOption( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FloatOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FloatOption}
	 * 
	 * @return The accordingly created {@link FloatOption} instance.
	 * 
	 * @see FloatOption
	 */
	public static FloatOption floatOption( String aLongOption, String aAlias, String aDescription ) {
		return new FloatOption( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link FloatOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FloatOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FloatOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link FloatOption} instance.
	 * 
	 * @see FloatOption
	 */
	public static FloatOption floatOption( String aLongOption, String aAlias, String aDescription, Consumer<FloatOption> aConsumer ) {
		return new FloatOption( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FloatProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption the float property
	 * @param aDescription The description of the {@link FloatProperty}
	 * 
	 * @return The accordingly created {@link FloatProperty} instance.
	 * 
	 * @see FloatProperty
	 */
	public static FloatProperty floatProperty( Character aShortOption, String aLongOption, String aDescription ) {
		return new FloatProperty( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link FloatProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption the float property
	 * @param aDescription The description of the {@link FloatProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FloatProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link FloatProperty} instance.
	 * 
	 * @see FloatProperty
	 */
	public static FloatProperty floatProperty( Character aShortOption, String aLongOption, String aDescription, Consumer<FloatProperty> aConsumer ) {
		return new FloatProperty( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FloatProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FloatProperty}
	 * 
	 * @return The accordingly created {@link FloatProperty} instance.
	 * 
	 * @see FloatProperty
	 */
	public static FloatProperty floatProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new FloatProperty( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link FloatProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FloatProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FloatProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link FloatProperty} instance.
	 * 
	 * @see FloatProperty
	 */
	public static FloatProperty floatProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<FloatProperty> aConsumer ) {
		return new FloatProperty( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FloatProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link FloatProperty}
	 * 
	 * @return The accordingly created {@link FloatProperty} instance.
	 * 
	 * @see FloatProperty
	 */
	public static FloatProperty floatProperty( String aLongOption, String aDescription ) {
		return new FloatProperty( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link FloatProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link FloatProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FloatProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link FloatProperty} instance.
	 * 
	 * @see FloatProperty
	 */
	public static FloatProperty floatProperty( String aLongOption, String aDescription, Consumer<FloatProperty> aConsumer ) {
		return new FloatProperty( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FloatProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FloatProperty}
	 * 
	 * @return The accordingly created {@link FloatProperty} instance.
	 * 
	 * @see FloatProperty
	 */
	public static FloatProperty floatProperty( String aLongOption, String aAlias, String aDescription ) {
		return new FloatProperty( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link FloatProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FloatProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FloatProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link FloatProperty} instance.
	 * 
	 * @see FloatProperty
	 */
	public static FloatProperty floatProperty( String aLongOption, String aAlias, String aDescription, Consumer<FloatProperty> aConsumer ) {
		return new FloatProperty( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Constructs the predefined force {@link Flag}.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see ForceFlag
	 */
	public static ForceFlag forceFlag() {
		return new ForceFlag();
	}

	/**
	 * Constructs the predefined force {@link Flag}.
	 * 
	 * @param hasShortOption True in case to also enable the short option, else
	 *        only the long option takes effect.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see ForceFlag
	 */
	public static ForceFlag forceFlag( boolean hasShortOption ) {
		return new ForceFlag( hasShortOption );
	}

	/**
	 * Constructs the predefined force {@link Flag}.
	 *
	 * @param aDescription The description to use when printing out the help
	 *        text.
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see ForceFlag
	 */
	public static ForceFlag forceFlag( String aDescription ) {
		return new ForceFlag( aDescription );
	}

	/**
	 * Constructs the predefined help {@link Flag}.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see HelpFlag
	 */
	public static HelpFlag helpFlag() {
		return new HelpFlag();
	}

	/**
	 * Constructs the predefined help {@link Flag}.
	 * 
	 * @param hasShortOption True in case to also enable the short option, else
	 *        only the long option takes effect.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see HelpFlag
	 */
	public static HelpFlag helpFlag( boolean hasShortOption ) {
		return new HelpFlag( hasShortOption );
	}

	/**
	 * Constructs the predefined help {@link Flag}.
	 *
	 * @param aDescription The description to use when printing out the help
	 *        text.
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see HelpFlag
	 */
	public static HelpFlag helpFlag( String aDescription ) {
		return new HelpFlag( aDescription );
	}

	/**
	 * Constructs the predefined init {@link Flag}.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see InitFlag
	 */
	public static InitFlag initFlag() {
		return new InitFlag();
	}

	/**
	 * Constructs the predefined init {@link Flag}.
	 * 
	 * @param hasShortOption True in case to also enable the short option, else
	 *        only the long option takes effect.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see InitFlag
	 */
	public static InitFlag initFlag( boolean hasShortOption ) {
		return new InitFlag( hasShortOption );
	}

	/**
	 * Constructs the predefined init {@link Flag}.
	 *
	 * @param aDescription The description to use when printing out the help
	 *        text.
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see InitFlag
	 */
	public static InitFlag initFlag( String aDescription ) {
		return new InitFlag( aDescription );
	}

	/**
	 * Instantiates a new {@link IntOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link IntOption}
	 * 
	 * @return The accordingly created {@link IntOption} instance.
	 * 
	 * @see IntOption
	 */
	public static IntOption intOption( Character aShortOption, String aLongOption, String aDescription ) {
		return new IntOption( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link IntOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link IntOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link IntOption} participated in successfully parsing the command
	 *        line arguments.
	 * 
	 * @return The accordingly created {@link IntOption} instance.
	 * 
	 * @see IntOption
	 */
	public static IntOption intOption( Character aShortOption, String aLongOption, String aDescription, Consumer<IntOption> aConsumer ) {
		return new IntOption( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link IntOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link IntOption}
	 * 
	 * @return The accordingly created {@link IntOption} instance.
	 * 
	 * @see IntOption
	 */
	public static IntOption intOption( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new IntOption( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link IntOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link IntOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link IntOption} participated in successfully parsing the command
	 *        line arguments.
	 * 
	 * @return The accordingly created {@link IntOption} instance.
	 * 
	 * @see IntOption
	 */
	public static IntOption intOption( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<IntOption> aConsumer ) {
		return new IntOption( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link IntOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link IntOption}
	 * 
	 * @return The accordingly created {@link IntOption} instance.
	 * 
	 * @see IntOption
	 */
	public static IntOption intOption( String aLongOption, String aDescription ) {
		return new IntOption( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link IntOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link IntOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link IntOption} participated in successfully parsing the command
	 *        line arguments.
	 * 
	 * @return The accordingly created {@link IntOption} instance.
	 * 
	 * @see IntOption
	 */
	public static IntOption intOption( String aLongOption, String aDescription, Consumer<IntOption> aConsumer ) {
		return new IntOption( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link IntOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link IntOption}
	 * 
	 * @return The accordingly created {@link IntOption} instance.
	 * 
	 * @see IntOption
	 */
	public static IntOption intOption( String aLongOption, String aAlias, String aDescription ) {
		return new IntOption( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link IntOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link IntOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link IntOption} participated in successfully parsing the command
	 *        line arguments.
	 * 
	 * @return The accordingly created {@link IntOption} instance.
	 * 
	 * @see IntOption
	 */
	public static IntOption intOption( String aLongOption, String aAlias, String aDescription, Consumer<IntOption> aConsumer ) {
		return new IntOption( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link IntProperty}
	 * 
	 * @return The accordingly created {@link IntProperty} instance.
	 * 
	 * @see IntProperty
	 */
	public static IntProperty intProperty( Character aShortOption, String aLongOption, String aDescription ) {
		return new IntProperty( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link IntProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link IntProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link IntProperty} instance.
	 * 
	 * @see IntProperty
	 */
	public static IntProperty intProperty( Character aShortOption, String aLongOption, String aDescription, Consumer<IntProperty> aConsumer ) {
		return new IntProperty( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link IntProperty}
	 * 
	 * @return The accordingly created {@link IntProperty} instance.
	 * 
	 * @see IntProperty
	 */
	public static IntProperty intProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new IntProperty( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link IntProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link IntProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link IntProperty} instance.
	 * 
	 * @see IntProperty
	 */
	public static IntProperty intProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<IntProperty> aConsumer ) {
		return new IntProperty( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link IntProperty}
	 * 
	 * @return The accordingly created {@link IntProperty} instance.
	 * 
	 * @see IntProperty
	 */
	public static IntProperty intProperty( String aLongOption, String aDescription ) {
		return new IntProperty( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link IntProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link IntProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link IntProperty} instance.
	 * 
	 * @see IntProperty
	 */
	public static IntProperty intProperty( String aLongOption, String aDescription, Consumer<IntProperty> aConsumer ) {
		return new IntProperty( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link IntProperty}
	 * 
	 * @return The accordingly created {@link IntProperty} instance.
	 * 
	 * @see IntProperty
	 */
	public static IntProperty intProperty( String aLongOption, String aAlias, String aDescription ) {
		return new IntProperty( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link IntProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link IntProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link IntProperty} instance.
	 * 
	 * @see IntProperty
	 */
	public static IntProperty intProperty( String aLongOption, String aAlias, String aDescription, Consumer<IntProperty> aConsumer ) {
		return new IntProperty( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link LongOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link LongOption}
	 * 
	 * @return The accordingly created {@link LongOption} instance.
	 * 
	 * @see LongOption
	 */
	public static LongOption longOption( Character aShortOption, String aLongOption, String aDescription ) {
		return new LongOption( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link LongOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link LongOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link LongOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link LongOption} instance.
	 * 
	 * @see LongOption
	 */
	public static LongOption longOption( Character aShortOption, String aLongOption, String aDescription, Consumer<LongOption> aConsumer ) {
		return new LongOption( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link LongOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link LongOption}
	 * 
	 * @return The accordingly created {@link LongOption} instance.
	 * 
	 * @see LongOption
	 */
	public static LongOption longOption( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new LongOption( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link LongOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link LongOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link LongOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link LongOption} instance.
	 * 
	 * @see LongOption
	 */
	public static LongOption longOption( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<LongOption> aConsumer ) {
		return new LongOption( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link LongOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link LongOption}
	 * 
	 * @return The accordingly created {@link LongOption} instance.
	 * 
	 * @see LongOption
	 */
	public static LongOption longOption( String aLongOption, String aDescription ) {
		return new LongOption( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link LongOption} with the given arguments. In case a
	 * long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link LongOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link LongOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link LongOption} instance.
	 * 
	 * @see LongOption
	 */
	public static LongOption longOption( String aLongOption, String aDescription, Consumer<LongOption> aConsumer ) {
		return new LongOption( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link LongOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link LongOption}
	 * 
	 * @return The accordingly created {@link LongOption} instance.
	 * 
	 * @see LongOption
	 */
	public static LongOption longOption( String aLongOption, String aAlias, String aDescription ) {
		return new LongOption( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link LongOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link LongOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link LongOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link LongOption} instance.
	 * 
	 * @see LongOption
	 */
	public static LongOption longOption( String aLongOption, String aAlias, String aDescription, Consumer<LongOption> aConsumer ) {
		return new LongOption( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link LongProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link LongProperty}
	 * 
	 * @return The accordingly created {@link LongProperty} instance.
	 * 
	 * @see LongProperty
	 */
	public static LongProperty longProperty( Character aShortOption, String aLongOption, String aDescription ) {
		return new LongProperty( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link LongProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link LongProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link LongProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link LongProperty} instance.
	 * 
	 * @see LongProperty
	 */
	public static LongProperty longProperty( Character aShortOption, String aLongOption, String aDescription, Consumer<LongProperty> aConsumer ) {
		return new LongProperty( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link LongProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link LongProperty}
	 * 
	 * @return The accordingly created {@link LongProperty} instance.
	 * 
	 * @see LongProperty
	 */
	public static LongProperty longProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new LongProperty( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link LongProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link LongProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link LongProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link LongProperty} instance.
	 * 
	 * @see LongProperty
	 */
	public static LongProperty longProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<LongProperty> aConsumer ) {
		return new LongProperty( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link LongProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link LongProperty}
	 * 
	 * @return The accordingly created {@link LongProperty} instance.
	 * 
	 * @see LongProperty
	 */
	public static LongProperty longProperty( String aLongOption, String aDescription ) {
		return new LongProperty( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link LongProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link LongProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link LongProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link LongProperty} instance.
	 * 
	 * @see LongProperty
	 */
	public static LongProperty longProperty( String aLongOption, String aDescription, Consumer<LongProperty> aConsumer ) {
		return new LongProperty( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link LongProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link LongProperty}
	 * 
	 * @return The accordingly created {@link LongProperty} instance.
	 * 
	 * @see LongProperty
	 */
	public static LongProperty longProperty( String aLongOption, String aAlias, String aDescription ) {
		return new LongProperty( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link LongProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link LongProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link LongProperty} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link LongProperty} instance.
	 * 
	 * @see LongProperty
	 */
	public static LongProperty longProperty( String aLongOption, String aAlias, String aDescription, Consumer<LongProperty> aConsumer ) {
		return new LongProperty( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link NoneOperand}.
	 *
	 * @param aProperty The key (= alias) and the value for the operand.
	 * 
	 * @return The accordingly created {@link NoneOperand}.
	 * 
	 * @see NoneOperand
	 */
	public static NoneOperand none( Relation<String, Boolean> aProperty ) {
		return new NoneOperand( aProperty );
	}

	/**
	 * Instantiates a new {@link NoneOperand}.
	 *
	 * @param aDescription The description to be used (without any line breaks).
	 * 
	 * @return The accordingly created {@link NoneOperand}.
	 * 
	 * @see NoneOperand
	 */
	public static NoneOperand none( String aDescription ) {
		return new NoneOperand( aDescription );
	}

	/**
	 * Instantiates a new {@link NoneOperand}.
	 *
	 * @param aAlias The identifier to be used when printing the syntax via the
	 *        {@link Term#toSyntax(CliContext)} method.
	 * @param aDescription The description to be used (without any line breaks).
	 * 
	 * @return The accordingly created {@link NoneOperand}.
	 * 
	 * @see NoneOperand
	 */
	public static NoneOperand none( String aAlias, String aDescription ) {
		return new NoneOperand( aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link NoneOperand}.
	 *
	 * @param aProperty The key (= alias) and the value for the operand.
	 * 
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link NoneOperand} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link NoneOperand}.
	 * 
	 * @see NoneOperand
	 */
	public static NoneOperand none( Relation<String, Boolean> aProperty, Consumer<NoneOperand> aConsumer ) {
		return new NoneOperand( aProperty, aConsumer );
	}

	/**
	 * Instantiates a new {@link NoneOperand}.
	 *
	 * @param aDescription The description to be used (without any line breaks).
	 * 
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link NoneOperand} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link NoneOperand}.
	 * 
	 * @see NoneOperand
	 */
	public static NoneOperand none( String aDescription, Consumer<NoneOperand> aConsumer ) {
		return new NoneOperand( aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link NoneOperand}.
	 *
	 * @param aAlias The identifier to be used when printing the syntax via the
	 *        {@link Term#toSyntax(CliContext)} method.
	 *
	 * @param aDescription The description to be used (without any line breaks).
	 *
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link NoneOperand} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link NoneOperand}.
	 * 
	 * @see NoneOperand
	 */
	public static NoneOperand none( String aAlias, String aDescription, Consumer<NoneOperand> aConsumer ) {
		return new NoneOperand( aAlias, aDescription, aConsumer );
	}

	/**
	 * Constructs a {@link Operation} with the given arguments.
	 * 
	 * @param aOperation The operation to declare.
	 * @param aDescription The description to be used (without any line breaks).
	 * 
	 * @return The accordingly created {@link Operation}.
	 */
	public static Operation operation( String aOperation, String aDescription ) {
		return new Operation( aOperation, aDescription );
	}

	/**
	 * Constructs a {@link Operation} with the given arguments.
	 * 
	 * @param aOperation The operation to declare.
	 * @param aDescription The description to be used (without any line breaks).
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Operation} participated in successfully parsing the command
	 *        line arguments.
	 * 
	 * @return The accordingly created {@link Operation}.
	 */
	public static Operation operation( String aOperation, String aDescription, Consumer<Operation> aConsumer ) {
		return new Operation( aOperation, aDescription, aConsumer );
	}

	/**
	 * Constructs a {@link Operation} with the given arguments.
	 * 
	 * @param aOperation The operation to declare.
	 * @param aAlias The operation's name to be used when constructing the
	 *        syntax.
	 * @param aDescription The description to be used (without any line breaks).
	 * 
	 * @return The accordingly created {@link Operation}.
	 */
	public static Operation operation( String aOperation, String aAlias, String aDescription ) {
		return new Operation( aOperation, aAlias, aDescription );
	}

	/**
	 * Constructs a {@link Operation} with the given arguments.
	 * 
	 * @param aOperation The operation to declare.
	 * @param aAlias The operation's name to be used when constructing the
	 *        syntax.
	 * @param aDescription The description to be used (without any line breaks).
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Operation} participated in successfully parsing the command
	 *        line arguments.
	 * 
	 * @return The accordingly created {@link Operation}.
	 */
	public static Operation operation( String aOperation, String aAlias, String aDescription, Consumer<Operation> aConsumer ) {
		return new Operation( aOperation, aAlias, aDescription, aConsumer );
	}

	/**
	 * Semantically identical synonym for the {@link #optional(Term...)}
	 * declaration.
	 *
	 * @param aArgs The {@link Term} ({@link Condition}) instances to be nested
	 * 
	 * @return The according {@link AbstractCondition}.
	 * 
	 * @see AnyCondition
	 */
	public static AnyCondition optional( Term... aArgs ) {
		return new AnyCondition( aArgs );
	}

	/**
	 * Instantiates a new {@link OrCondition} with the {@link Term}
	 * ({@link Condition}) instances to be nested.
	 *
	 * @param aArgs The {@link Term} ({@link Condition}) instances to be nested.
	 * 
	 * @return The according OR condition.
	 * 
	 * @see OrCondition
	 */
	public static OrCondition or( Term... aArgs ) {
		return new OrCondition( aArgs );
	}

	/**
	 * Constructs the predefined quiet {@link Flag}.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see QuietFlag
	 */
	public static QuietFlag quietFlag() {
		return new QuietFlag();
	}

	/**
	 * Constructs the predefined quiet {@link Flag}.
	 * 
	 * @param hasShortOption True in case to also enable the short option, else
	 *        only the long option takes effect.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see QuietFlag
	 */
	public static QuietFlag quietFlag( boolean hasShortOption ) {
		return new QuietFlag( hasShortOption );
	}

	/**
	 * Constructs the predefined quiet {@link Flag}.
	 *
	 * @param aDescription The description to use when printing out the help
	 *        text.
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see QuietFlag
	 */
	public static QuietFlag quietFlag( String aDescription ) {
		return new QuietFlag( aDescription );
	}

	/**
	 * Instantiates a new {@link StringOperand}.
	 *
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description to be used (without any line breaks).
	 * 
	 * @return The according {@link StringOperand}.
	 * 
	 * @see StringOperand
	 */
	public static StringOperand stringOperand( String aAlias, String aDescription ) {
		return new StringOperand( aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link StringOperand}.
	 *
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description to be used (without any line breaks).
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringOperand} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The according {@link StringOperand}.
	 * 
	 * @see StringOperand
	 */
	public static StringOperand stringOperand( String aAlias, String aDescription, Consumer<StringOperand> aConsumer ) {
		return new StringOperand( aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link StringOption} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption the string option
	 * @param aDescription The description of the {@link StringOption}
	 * 
	 * @return The accordingly created {@link StringOption} instance.
	 * 
	 * @see StringOption
	 */
	public static StringOption stringOption( Character aShortOption, String aLongOption, String aDescription ) {
		return new StringOption( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link StringOption} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption the string option
	 * @param aDescription The description of the {@link StringOption}
	 * 
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link StringOption} instance.
	 * 
	 * @see StringOption
	 */
	public static StringOption stringOption( Character aShortOption, String aLongOption, String aDescription, Consumer<StringOption> aConsumer ) {
		return new StringOption( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link StringOption} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link StringOption}
	 * 
	 * @return The accordingly created {@link StringOption} instance.
	 * 
	 * @see StringOption
	 */
	public static StringOption stringOption( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new StringOption( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link StringOption} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption the string option
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link StringOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link StringOption} instance.
	 * 
	 * @see StringOption
	 */
	public static StringOption stringOption( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<StringOption> aConsumer ) {
		return new StringOption( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link StringOption} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link StringOption}
	 * 
	 * @return The accordingly created {@link StringOption} instance.
	 * 
	 * @see StringOption
	 */
	public static StringOption stringOption( String aLongOption, String aDescription ) {
		return new StringOption( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link StringOption} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link StringOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link StringOption} instance.
	 * 
	 * @see StringOption
	 */
	public static StringOption stringOption( String aLongOption, String aDescription, Consumer<StringOption> aConsumer ) {
		return new StringOption( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link StringOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link StringOption}
	 * 
	 * @return The accordingly created {@link StringOption} instance.
	 * 
	 * @see StringOption
	 */
	public static StringOption stringOption( String aLongOption, String aAlias, String aDescription ) {
		return new StringOption( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link StringOption} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link StringOption}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringOption} participated in successfully parsing the
	 *        command line arguments.
	 * 
	 * @return The accordingly created {@link StringOption} instance.
	 * 
	 * @see StringOption
	 */
	public static StringOption stringOption( String aLongOption, String aAlias, String aDescription, Consumer<StringOption> aConsumer ) {
		return new StringOption( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link StringProperty}
	 * 
	 * @return The accordingly created {@link StringProperty} instance.
	 * 
	 * @see StringProperty
	 */
	public static StringProperty stringProperty( Character aShortOption, String aLongOption, String aDescription ) {
		return new StringProperty( aShortOption, aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link StringProperty}
	 * 
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringProperty} participated in successfully parsing the
	 *        command line arguments.
	 *
	 * @return The accordingly created {@link StringProperty} instance.
	 * 
	 * @see StringProperty
	 */
	public static StringProperty stringProperty( Character aShortOption, String aLongOption, String aDescription, Consumer<StringProperty> aConsumer ) {
		return new StringProperty( aShortOption, aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link StringProperty}
	 * 
	 * @return The accordingly created {@link StringProperty} instance.
	 * 
	 * @see StringProperty
	 */
	public static StringProperty stringProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		return new StringProperty( aShortOption, aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link StringProperty}
	 * 
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringProperty} participated in successfully parsing the
	 *        command line arguments.
	 *
	 * @return The accordingly created {@link StringProperty} instance.
	 * 
	 * @see StringProperty
	 */
	public static StringProperty stringProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<StringProperty> aConsumer ) {
		return new StringProperty( aShortOption, aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link StringProperty}
	 * 
	 * @return The accordingly created {@link StringProperty} instance.
	 * 
	 * @see StringProperty
	 */
	public static StringProperty stringProperty( String aLongOption, String aDescription ) {
		return new StringProperty( aLongOption, aDescription );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link StringProperty}
	 * 
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringProperty} participated in successfully parsing the
	 *        command line arguments.
	 *
	 * @return The accordingly created {@link StringProperty} instance.
	 * 
	 * @see StringProperty
	 */
	public static StringProperty stringProperty( String aLongOption, String aDescription, Consumer<StringProperty> aConsumer ) {
		return new StringProperty( aLongOption, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link StringProperty}
	 * 
	 * @return The accordingly created {@link StringProperty} instance.
	 * 
	 * @see StringProperty
	 */
	public static StringProperty stringProperty( String aLongOption, String aAlias, String aDescription ) {
		return new StringProperty( aLongOption, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link StringProperty}
	 * 
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringProperty} participated in successfully parsing the
	 *        command line arguments.
	 *
	 * @return The accordingly created {@link StringProperty} instance.
	 * 
	 * @see StringProperty
	 */
	public static StringProperty stringProperty( String aLongOption, String aAlias, String aDescription, Consumer<StringProperty> aConsumer ) {
		return new StringProperty( aLongOption, aAlias, aDescription, aConsumer );
	}

	/**
	 * Constructs the predefined system info {@link Flag}.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see SysInfoFlag
	 */
	public static SysInfoFlag sysInfoFlag() {
		return new SysInfoFlag();
	}

	/**
	 * Constructs the predefined system info {@link Flag}.
	 * 
	 * @param hasShortOption True in case to also enable the short option, else
	 *        only the long option takes effect.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see SysInfoFlag
	 */
	public static SysInfoFlag sysInfoFlag( boolean hasShortOption ) {
		return new SysInfoFlag( hasShortOption );
	}

	/**
	 * Constructs the predefined system info {@link Flag}.
	 *
	 * @param aDescription The description to use when printing out the help
	 *        text.
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see SysInfoFlag
	 */
	public static SysInfoFlag sysInfoFlag( String aDescription ) {
		return new SysInfoFlag( aDescription );
	}

	/**
	 * Constructs the predefined verbose {@link Flag}.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see VerboseFlag
	 */
	public static VerboseFlag verboseFlag() {
		return new VerboseFlag();
	}

	/**
	 * Constructs the predefined verbose {@link Flag}.
	 * 
	 * @param hasShortOption True in case to also enable the short option, else
	 *        only the long option takes effect.
	 *
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see VerboseFlag
	 */
	public static VerboseFlag verboseFlag( boolean hasShortOption ) {
		return new VerboseFlag( hasShortOption );
	}

	/**
	 * Constructs the predefined verbose {@link Flag}.
	 *
	 * @param aDescription The description to use when printing out the help
	 *        text.
	 * 
	 * @return The accordingly created {@link Flag} instance.
	 * 
	 * @see VerboseFlag
	 */
	public static VerboseFlag verboseFlag( String aDescription ) {
		return new VerboseFlag( aDescription );
	}

	/**
	 * Instantiates a new {@link XorCondition} with the {@link Term}
	 * ({@link Condition}) instances to be nested.
	 *
	 * @param aArgs The {@link Term} ({@link Condition}) instances to be nested.
	 * 
	 * @return The according XOR condition.
	 * 
	 * @see XorCondition
	 */
	public static XorCondition xor( Term... aArgs ) {
		return new XorCondition( aArgs );
	}
}
