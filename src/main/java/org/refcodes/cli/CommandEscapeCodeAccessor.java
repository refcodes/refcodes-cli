// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a command Escape-Code property.
 */
public interface CommandEscapeCodeAccessor {

	/**
	 * Provides a builder method for a command Escape-Code property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface CommandEscapeCodeBuilder<B extends CommandEscapeCodeBuilder<B>> {

		/**
		 * Sets the command Escape-Code for the command Escape-Code property.
		 * 
		 * @param aCommandEscCode The command Escape-Code to be stored by the
		 *        command Escape-Code property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withCommandEscapeCode( String aCommandEscCode );
	}

	/**
	 * Provides a mutator for a command Escape-Code property.
	 */
	public interface CommandEscapeCodeMutator {

		/**
		 * Sets the command Escape-Code for the command Escape-Code property.
		 * 
		 * @param aCommandEscCode The command Escape-Code to be stored by the
		 *        command Escape-Code property.
		 */
		void setCommandEscapeCode( String aCommandEscCode );
	}

	/**
	 * Provides a command Escape-Code property.
	 */
	public interface CommandEscapeCodeProperty extends CommandEscapeCodeAccessor, CommandEscapeCodeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setCommandEscapeCode(String)} and returns the very same
		 * value (getter).
		 * 
		 * @param aCommandEscCode The {@link String} to set (via
		 *        {@link #setCommandEscapeCode(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letCommandEscapeCode( String aCommandEscCode ) {
			setCommandEscapeCode( aCommandEscCode );
			return aCommandEscCode;
		}
	}

	/**
	 * Retrieves the command Escape-Code from the command Escape-Code property.
	 * 
	 * @return The command Escape-Code stored by the command Escape-Code
	 *         property.
	 */
	String getCommandEscapeCode();
}
