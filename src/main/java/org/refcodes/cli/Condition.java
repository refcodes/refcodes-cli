// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.refcodes.mixin.ChildrenAccessor;

/**
 * The {@link Condition} interface represents a node (and therewith the syntax
 * for the arguments) in the command line arguments syntax tree by simply
 * extending the {@link Term} interface and adding the functionality of
 * providing access to the added {@link Operand}s (leafs).
 */
public interface Condition extends Term, ChildrenAccessor<Term[]> {

	/**
	 * Traverses the hierarchy of {@link Condition} tree and returns the (first)
	 * operand matching the given type.
	 *
	 * @param <T> the generic type
	 * @param aType The type for which to seek for.
	 * 
	 * @return The according {@link Operand} element or null if none matching
	 *         was found
	 */
	default <T extends Operand<?>> T toOperand( Class<T> aType ) {
		return toOperand( null, aType );
	}

	/**
	 * Traverses the hierarchy of {@link Condition} tree and returns the (first)
	 * operand matching the given alias.
	 * 
	 * @param aAlias The alias for which to seek for.
	 * 
	 * @return The according {@link Operand} element or null if none matching
	 *         was found.
	 */
	default Operand<?> toOperand( String aAlias ) {
		return toOperand( aAlias, null );
	}

	/**
	 * Traverses the hierarchy of {@link Condition} tree and returns the (first)
	 * operand matching the given alias and type.
	 *
	 * @param <T> the generic type
	 * @param aAlias The alias for which to seek for.
	 * @param aType The type for which to seek for.
	 * 
	 * @return The according {@link Operand} element or null if none matching
	 *         was found
	 */
	<T extends Operand<?>> T toOperand( String aAlias, Class<T> aType );

	/**
	 * Traverses the hierarchy of {@link Condition}s and collects all therein
	 * found {@link Operand}s and sub-types such as the {@link Flag} or the
	 * {@link Option}. This is most useful when creating a list of
	 * {@link Option}s, {@link Flag}es and {@link Operand}s when printing out
	 * detailed help.
	 * 
	 * @return The according {@link Operand} elements.
	 */
	Operand<?>[] toOperands();

	/**
	 * Determines the potential {@link Operand} instances matching the given
	 * args (intersection) which might be useful to get more details on failing
	 * to parse arguments (in case we have superfluous arguments and want to see
	 * which {@link Operand} addressed them).
	 * 
	 * @param aArgs The arguments for which to determine the {@link Operand}
	 *        parameters.
	 * 
	 * @return The according {@link Operand} parameters or null if none was
	 *         found.
	 */
	default Operand<?>[] toOperands( String[] aArgs ) {
		final Set<Operand<?>> theOperands = new HashSet<>();
		Operand<?>[] eOperands;
		if ( getChildren() != null && getChildren().length != 0 ) {
			for ( Term eTerm : getChildren() ) {
				if ( eTerm instanceof Operand<?> eOperand ) {
					if ( hasIntersection( aArgs, eOperand.getParsedArgs() ) ) {
						theOperands.add( eOperand );
					}
				}
				else if ( eTerm instanceof Condition eCondition ) {
					eOperands = eCondition.toOperands( aArgs );
					if ( eOperands != null && eOperands.length != 0 ) {
						for ( Operand<?> eParent : eOperands ) {
							theOperands.add( eParent );
						}
					}
				}
			}
		}
		if ( !theOperands.isEmpty() ) {
			return theOperands.toArray( new Operand<?>[theOperands.size()] );
		}
		return null;
	}

	/**
	 * Determines the potential {@link Condition} instances matching the given
	 * args (intersection) which might be useful to get more details on failing
	 * to parse arguments (in case we have superfluous arguments and want to see
	 * which {@link Condition} parent's {@link Operand} child addressed them).
	 * 
	 * @param aArgs The arguments for which to determine the {@link Condition}
	 *        parents.
	 * 
	 * @return The according {@link Condition} parents or null if none was
	 *         found.
	 */
	default Condition[] toConditions( String[] aArgs ) {
		final Set<Condition> theParents = new HashSet<>();
		Condition[] eParents;
		if ( getChildren() != null && getChildren().length != 0 ) {
			for ( Term eTerm : getChildren() ) {
				if ( eTerm instanceof Operand<?> eOperand ) {
					if ( hasIntersection( aArgs, eOperand.getParsedArgs() ) ) {
						theParents.add( this );
					}
				}
				else if ( eTerm instanceof Condition eCondition ) {
					eParents = eCondition.toConditions( aArgs );
					if ( eParents != null && eParents.length != 0 ) {
						for ( Condition eParent : eParents ) {
							theParents.add( eParent );
						}
					}
				}
			}
		}
		if ( !theParents.isEmpty() ) {
			final Iterator<Condition> e = theParents.iterator();
			Condition eCondition;
			while ( e.hasNext() ) {
				eCondition = e.next();
				for ( Condition eParent : new HashSet<>( theParents ) ) {
					if ( eCondition != eParent && eCondition.hasChild( eParent ) ) {
						e.remove();
					}
				}
			}
			return theParents.toArray( new Condition[theParents.size()] );
		}
		return null;
	}

	/**
	 * Determines whether the provided {@link Term} is a direct child of this
	 * {@link Condition}.
	 * 
	 * @param aTerm The {@link Term} for which to determine if it is a direct
	 *        child.
	 * 
	 * @return True in case it is a direct child, else false,
	 */
	default boolean isChild( Term aTerm ) {
		if ( getChildren() != null && getChildren().length != 0 ) {
			for ( Term eTerm : getChildren() ) {
				if ( eTerm == aTerm ) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Determines whether the provided {@link Term} is a child (or a child's
	 * child) of this {@link Condition}.
	 * 
	 * @param aTerm The {@link Term} for which to determine if it is a child (or
	 *        a child's child).
	 * 
	 * @return True in case it is a child (or a child's child), else false,
	 */
	default boolean hasChild( Term aTerm ) {
		if ( getChildren() != null && getChildren().length != 0 ) {
			for ( Term eTerm : getChildren() ) {
				if ( eTerm == aTerm ) {
					return true;
				}
				if ( eTerm instanceof Condition eCondition ) {
					return eCondition.hasChild( aTerm );
				}
			}
		}
		return false;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER: 
	// /////////////////////////////////////////////////////////////////////////

	private static boolean hasIntersection( String[] aArgs1, String[] aArgs2 ) {
		if ( aArgs1 != null && aArgs2 != null ) {
			for ( String eArg1 : aArgs1 ) {
				for ( String eArg2 : aArgs2 ) {
					if ( eArg1 != null && eArg1.equals( eArg2 ) ) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
