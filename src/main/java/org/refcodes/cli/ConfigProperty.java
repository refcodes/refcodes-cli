// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * The {@link ConfigProperty} represents an {@link PropertyOption} holding a
 * value specifying a configuration resource (file).
 */
public class ConfigProperty extends StringProperty {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ALIAS = "config";
	public static final String LONG_OPTION = "config";
	public static final Character SHORT_OPTION = 'c';

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the predefined config (file) {@link PropertyOption}.
	 */
	public ConfigProperty() {
		this( false );
	}

	/**
	 * Constructs the predefined config (file) {@link PropertyOption}.
	 * 
	 * @param hasShortProperty True in case to also enable the short option,
	 *        else only the long option takes effect.
	 */
	public ConfigProperty( boolean hasShortProperty ) {
		super( hasShortProperty ? SHORT_OPTION : null, LONG_OPTION, ALIAS, "Specifies the resource (file) to use when loading the configuration." );
	}

	/**
	 * Instantiates a new config (file) {@link PropertyOption}.
	 *
	 * @param aDescription The description to use.
	 */
	public ConfigProperty( String aDescription ) {
		this( aDescription, false );
	}

	/**
	 * Constructs the predefined config (file) {@link PropertyOption}.
	 *
	 * @param aDescription The description to be used (without any line breaks).
	 * @param hasShortProperty True in case to also enable the short option,
	 *        else only the long option takes effect.
	 */
	public ConfigProperty( String aDescription, boolean hasShortProperty ) {
		super( hasShortProperty ? SHORT_OPTION : null, LONG_OPTION, ALIAS, aDescription );
	}
}
