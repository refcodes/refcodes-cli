// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a copyright property.
 */
public interface CopyrightAccessor {

	/**
	 * Provides a builder method for a copyright property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface CopyrightBuilder<B extends CopyrightBuilder<B>> {

		/**
		 * Sets the copyright for the copyright property.
		 * 
		 * @param aCopyright The copyright to be stored by the console width
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withCopyright( String aCopyright );
	}

	/**
	 * Provides a mutator for a copyright property.
	 */
	public interface CopyrightMutator {

		/**
		 * Sets the copyright for the copyright property.
		 * 
		 * @param aCopyright The copyright to be stored by the console width
		 *        property.
		 */
		void setCopyright( String aCopyright );
	}

	/**
	 * Provides a copyright property.
	 */
	public interface CopyrightProperty extends CopyrightAccessor, CopyrightMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given width (setter) as of
		 * {@link #setCopyright(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aCopyright The width to set (via
		 *        {@link #setCopyright(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letCopyright( String aCopyright ) {
			setCopyright( aCopyright );
			return aCopyright;
		}
	}

	/**
	 * Retrieves the copyright from the copyright property.
	 * 
	 * @return The copyright stored by the copyright property.
	 */
	String getCopyright();
}
