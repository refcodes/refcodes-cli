// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * A predefined daemon {@link Flag}: A predefined {@link Flag} gives its
 * {@link #SHORT_OPTION}, its {@link #LONG_OPTION} as well as its {@link #ALIAS}
 * an according semantics regarded by other subsystems.
 */
public class DaemonFlag extends Flag {

	public static final String ALIAS = "daemon";
	public static final String LONG_OPTION = "daemon";
	public static final Character SHORT_OPTION = 'd';

	/**
	 * Constructs the predefined daemon {@link Flag}.
	 */
	public DaemonFlag() {
		this( false );
	}

	/**
	 * Constructs the predefined daemon {@link Flag}.
	 * 
	 * @param hasShortOption True in case to also enable the short option, else
	 *        only the long option takes effect.
	 */
	public DaemonFlag( boolean hasShortOption ) {
		super( hasShortOption ? SHORT_OPTION : null, LONG_OPTION, ALIAS, "Execute in daemon (thread) mode." );
	}

	/**
	 * Constructs the predefined daemon {@link Flag}.
	 * 
	 * @param aDescription The description to be used (without any line breaks).
	 */
	public DaemonFlag( String aDescription ) {
		this( aDescription, false );
	}

	/**
	 * Constructs the predefined daemon {@link Flag}.
	 *
	 * @param aDescription The description to be used (without any line breaks).
	 * @param hasShortOption True in case to also enable the short option, else
	 *        only the long option takes effect.
	 */
	public DaemonFlag( String aDescription, boolean hasShortOption ) {
		super( hasShortOption ? SHORT_OPTION : null, LONG_OPTION, ALIAS, aDescription );
	}
}
