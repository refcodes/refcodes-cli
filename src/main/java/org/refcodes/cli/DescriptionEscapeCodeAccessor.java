// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a description Escape-Code property.
 */
public interface DescriptionEscapeCodeAccessor {

	/**
	 * Provides a builder method for a description Escape-Code property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface DescriptionEscapeCodeBuilder<B extends DescriptionEscapeCodeBuilder<B>> {

		/**
		 * Sets the description Escape-Code for the description Escape-Code
		 * property.
		 * 
		 * @param aDescriptionEscCode The description Escape-Code to be stored
		 *        by the description Escape-Code property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withDescriptionEscapeCode( String aDescriptionEscCode );
	}

	/**
	 * Provides a mutator for a description Escape-Code property.
	 */
	public interface DescriptionEscapeCodeMutator {

		/**
		 * Sets the description Escape-Code for the description Escape-Code
		 * property.
		 * 
		 * @param aDescriptionEscCode The description Escape-Code to be stored
		 *        by the description Escape-Code property.
		 */
		void setDescriptionEscapeCode( String aDescriptionEscCode );
	}

	/**
	 * Provides a description Escape-Code property.
	 */
	public interface DescriptionEscapeCodeProperty extends DescriptionEscapeCodeAccessor, DescriptionEscapeCodeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setDescriptionEscapeCode(String)} and returns the very
		 * same value (getter).
		 * 
		 * @param aDescriptionEscCode The {@link String} to set (via
		 *        {@link #setDescriptionEscapeCode(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letDescriptionEscapeCode( String aDescriptionEscCode ) {
			setDescriptionEscapeCode( aDescriptionEscCode );
			return aDescriptionEscCode;
		}
	}

	/**
	 * Retrieves the description Escape-Code from the description Escape-Code
	 * property.
	 * 
	 * @return The description Escape-Code stored by the description Escape-Code
	 *         property.
	 */
	String getDescriptionEscapeCode();
}
