// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.function.Consumer;

import org.refcodes.struct.Relation;

/**
 * The {@link EnumProperty} represents an {@link PropertyOption} of enumeration
 * types.
 *
 * @param <T> The generic type of the enumeration.
 */
public class EnumProperty<T extends Enum<T>> extends AbstractPropertyOption<T> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortProperty The short option to use.
	 * @param aLongProperty The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aDescription The description of the {@link EnumProperty}
	 */
	public EnumProperty( Character aShortProperty, String aLongProperty, Class<T> aType, String aDescription ) {
		super( aShortProperty, aLongProperty, aType, aDescription );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aDescription The description of the {@link EnumProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link EnumProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public EnumProperty( Character aShortOption, String aLongOption, Class<T> aType, String aDescription, Consumer<EnumProperty<T>> aConsumer ) {
		super( aShortOption, aLongOption, aType, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments.
	 *
	 * @param aShortProperty The short option to use.
	 * @param aLongProperty The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link EnumProperty}
	 */
	public EnumProperty( Character aShortProperty, String aLongProperty, Class<T> aType, String aAlias, String aDescription ) {
		super( aShortProperty, aLongProperty, aType, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link EnumProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link EnumProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public EnumProperty( Character aShortOption, String aLongOption, Class<T> aType, String aAlias, String aDescription, Consumer<EnumProperty<T>> aConsumer ) {
		super( aShortOption, aLongOption, aType, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the alias being the
	 * proerty's key and the value being the property's value. Depending on the
	 * provided property's key, the key is either used for the short option or
	 * the long option.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 * @param aType The type of the enumeration to be used.
	 */
	public EnumProperty( Relation<String, T> aProperty, Class<T> aType ) {
		super( aProperty, aType );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the alias being the
	 * proerty's key and the value being the property's value. Depending on the
	 * provided property's key, the key is either used for the short option or
	 * the long option.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 * @param aType The type of the enumeration to be used.
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link EnumProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public EnumProperty( Relation<String, T> aProperty, Class<T> aType, Consumer<EnumProperty<T>> aConsumer ) {
		super( aProperty, aType, aConsumer );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongProperty The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aDescription The description of the {@link EnumProperty}
	 */
	public EnumProperty( String aLongProperty, Class<T> aType, String aDescription ) {
		super( aLongProperty, aType, aDescription );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aDescription The description of the {@link EnumProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link EnumProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public EnumProperty( String aLongOption, Class<T> aType, String aDescription, Consumer<EnumProperty<T>> aConsumer ) {
		super( aLongOption, aType, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments.
	 *
	 * @param aLongProperty The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link EnumProperty}
	 */
	public EnumProperty( String aLongProperty, Class<T> aType, String aAlias, String aDescription ) {
		super( aLongProperty, aType, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link EnumProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aType The type of the enumeration to be used.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link EnumProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link EnumProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public EnumProperty( String aLongOption, Class<T> aType, String aAlias, String aDescription, Consumer<EnumProperty<T>> aConsumer ) {
		super( aLongOption, aType, aAlias, aDescription, aConsumer );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public EnumProperty<T> withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected T toType( String aArg ) throws ParseArgsException {
		IllegalArgumentException theFirstCause = null;
		try {
			final T theValue = Enum.valueOf( getType(), aArg );
			_matchCount = 1;
			return theValue;
		}
		catch ( IllegalArgumentException e1 ) {
			theFirstCause = e1;
			try {
				final T theValue = Enum.valueOf( getType(), aArg.toUpperCase() );
				_matchCount = 1;
				return theValue;
			}
			catch ( IllegalArgumentException e2 ) {
				try {
					final T theValue = Enum.valueOf( getType(), aArg.toLowerCase() );
					_matchCount = 1;
					return theValue;
				}
				catch ( IllegalArgumentException e3 ) {
					final ParseArgsException theException = new ParseArgsException( "Invalid argument \"" + aArg + "\" for option <" + getAlias() + ">.", new String[] { aArg }, this, theFirstCause );
					_exception = theException;
					throw theException;
				}
			}
		}
	}
}
