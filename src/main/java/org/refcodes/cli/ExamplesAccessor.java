// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.Collection;

/**
 * Provides an accessor for an {@link Example} instances property.
 */
public interface ExamplesAccessor {

	/**
	 * Provides a builder method for an {@link Example} instances property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ExamplesBuilder<B extends ExamplesBuilder<B>> {

		/**
		 * Builder method providing the application's {@link Example} elements.
		 * 
		 * @param aExamples The application's {@link Example} elements to use.
		 * 
		 * @return This builder as of the builder pattern.
		 */
		default B withExamples( Collection<Example> aExamples ) {
			return withExamples( aExamples.toArray( new Example[aExamples.size()] ) );
		}

		/**
		 * Sets the {@link Example} instances for the {@link Example} instances
		 * property.
		 * 
		 * @param aExamples The {@link Example} instances to be stored.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withExamples( Example[] aExamples );
	}

	/**
	 * Provides a mutator for an {@link Example} instances property.
	 */
	public interface ExamplesMutator {

		/**
		 * Sets the {@link Example} instances for the {@link Example} instances
		 * property.
		 * 
		 * @param aExamples The {@link Example} instances to be stored.
		 */
		default void setExamples( Collection<Example> aExamples ) {
			setExamples( aExamples.toArray( new Example[aExamples.size()] ) );
		}

		/**
		 * Sets the {@link Example} instances for the {@link Example} instances
		 * property.
		 * 
		 * @param aExamples The {@link Example} instances to be stored.
		 */
		void setExamples( Example[] aExamples );
	}

	/**
	 * Provides an {@link Example} instances property.
	 */
	public interface ExamplesProperty extends ExamplesAccessor, ExamplesMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Condition}
		 * (setter) as of {@link #setExamples(Example[])} and returns the very
		 * same value (getter).
		 * 
		 * @param aExamples The {@link Condition} to set (via
		 *        {@link #setExamples(Example[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Example[] letExamples( Example[] aExamples ) {
			setExamples( aExamples );
			return aExamples;
		}
	}

	/**
	 * Retrieves the {@link Example} instances from the {@link Example}
	 * instances property.
	 * 
	 * @return The {@link Example} instances stored by the {@link Example}
	 *         instances property.
	 */
	Example[] getExamples();
}
