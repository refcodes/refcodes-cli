// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.io.File;
import java.util.function.Consumer;

import org.refcodes.struct.Relation;

/**
 * The {@link FileProperty} represents an {@link PropertyOption} holding
 * <code>File</code> values.
 */
public class FileProperty extends AbstractPropertyOption<File> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortProperty The short option to use.
	 * @param aLongProperty The long option to use.
	 * @param aDescription The description of the {@link FileProperty}
	 */
	public FileProperty( Character aShortProperty, String aLongProperty, String aDescription ) {
		super( aShortProperty, aLongProperty, File.class, aDescription );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link FileProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FileProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public FileProperty( Character aShortOption, String aLongOption, String aDescription, Consumer<FileProperty> aConsumer ) {
		super( aShortOption, aLongOption, File.class, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments.
	 *
	 * @param aShortProperty The short option to use.
	 * @param aLongProperty The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FileProperty}
	 */
	public FileProperty( Character aShortProperty, String aLongProperty, String aAlias, String aDescription ) {
		super( aShortProperty, aLongProperty, File.class, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FileProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FileProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public FileProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<FileProperty> aConsumer ) {
		super( aShortOption, aLongOption, File.class, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the alias being the
	 * proerty's key and the value being the property's value. Depending on the
	 * provided property's key, the key is either used for the short option or
	 * the long option.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 */
	public FileProperty( Relation<String, File> aProperty ) {
		super( aProperty, File.class );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the alias being the
	 * proerty's key and the value being the property's value. Depending on the
	 * provided property's key, the key is either used for the short option or
	 * the long option.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FileProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public FileProperty( Relation<String, File> aProperty, Consumer<FileProperty> aConsumer ) {
		super( aProperty, File.class, aConsumer );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongProperty The long option to use.
	 * @param aDescription The description of the {@link FileProperty}
	 */
	public FileProperty( String aLongProperty, String aDescription ) {
		super( aLongProperty, File.class, aDescription );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link FileProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FileProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public FileProperty( String aLongOption, String aDescription, Consumer<FileProperty> aConsumer ) {
		super( aLongOption, File.class, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments.
	 *
	 * @param aLongProperty The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FileProperty}
	 */
	public FileProperty( String aLongProperty, String aAlias, String aDescription ) {
		super( aLongProperty, File.class, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link FileProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link FileProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link FileProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public FileProperty( String aLongOption, String aAlias, String aDescription, Consumer<FileProperty> aConsumer ) {
		super( aLongOption, File.class, aAlias, aDescription, aConsumer );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public FileProperty withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected File toType( String aArg ) throws ParseArgsException {
		return new File( aArg );
	}
}
