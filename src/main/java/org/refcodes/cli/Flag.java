// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.function.Consumer;

import org.refcodes.mixin.EnabledAccessor;
import org.refcodes.struct.Relation;

/**
 * The {@link Flag} class implements the {@link Option} interface for
 * representing either a <code>true</code> or a <code>false</code> state: When a
 * flag is provided to your command line arguments, then it is considered to be
 * <code>true</code>, when it is omitted, then it is considered to be
 * <code>false</code> as of {@link #isEnabled()}.
 */
public class Flag extends AbstractOption<Boolean> implements EnabledAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link Flag} with the given arguments. In case a long
	 * option is provided, the intance's alias will automatically be set with
	 * the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link Flag}
	 */
	public Flag( Character aShortOption, String aLongOption, String aDescription ) {
		super( aShortOption, aLongOption, Boolean.class, aDescription );
		setValue( false );
	}

	/**
	 * Instantiates a new {@link Flag} with the given arguments. In case a long
	 * option is provided, the intance's alias will automatically be set with
	 * the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link Flag}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Flag} participated in successfully parsing the command line
	 *        arguments.
	 */
	public Flag( Character aShortOption, String aLongOption, String aDescription, Consumer<Flag> aConsumer ) {
		super( aShortOption, aLongOption, Boolean.class, aDescription, aConsumer );
		setValue( false );
	}

	/**
	 * Instantiates a new {@link Flag} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link Flag}
	 */
	public Flag( Character aShortOption, String aLongOption, String aAlias, String aDescription ) {
		super( aShortOption, aLongOption, Boolean.class, aAlias, aDescription );
		setValue( false );
	}

	/**
	 * Instantiates a new {@link Flag} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link Flag}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Flag} participated in successfully parsing the command line
	 *        arguments.
	 */
	public Flag( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<Flag> aConsumer ) {
		super( aShortOption, aLongOption, Boolean.class, aAlias, aDescription, aConsumer );
		setValue( false );
	}

	/**
	 * Instantiates a new {@link Flag} with the alias being the proerty's key
	 * and the value being the property's value. Depending on the provided
	 * property's key, the key is either used for the short option or the long
	 * option.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 */
	public Flag( Relation<String, Boolean> aProperty ) {
		super( aProperty, Boolean.class );
	}

	/**
	 * Instantiates a new {@link Flag} with the alias being the proerty's key
	 * and the value being the property's value. Depending on the provided
	 * property's key, the key is either used for the short option or the long
	 * option.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Flag} participated in successfully parsing the command line
	 *        arguments.
	 */
	public Flag( Relation<String, Boolean> aProperty, Consumer<Flag> aConsumer ) {
		super( aProperty, Boolean.class, aConsumer );
	}

	/**
	 * Instantiates a new {@link Flag} with the given arguments. In case a long
	 * option is provided, the intance's alias will automatically be set with
	 * the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link Flag}
	 */
	public Flag( String aLongOption, String aDescription ) {
		super( aLongOption, Boolean.class, aDescription );
		setValue( false );
	}

	/**
	 * Instantiates a new {@link Flag} with the given arguments. In case a long
	 * option is provided, the intance's alias will automatically be set with
	 * the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link Flag}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Flag} participated in successfully parsing the command line
	 *        arguments.
	 */
	public Flag( String aLongOption, String aDescription, Consumer<Flag> aConsumer ) {
		super( aLongOption, Boolean.class, aDescription, aConsumer );
		setValue( false );
	}

	/**
	 * Instantiates a new {@link Flag} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link Flag}
	 */
	public Flag( String aLongOption, String aAlias, String aDescription ) {
		super( aLongOption, Boolean.class, aAlias, aDescription );
		setValue( false );
	}

	/**
	 * Instantiates a new {@link Flag} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link Flag}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Flag} participated in successfully parsing the command line
	 *        arguments.
	 */
	public Flag( String aLongOption, String aAlias, String aDescription, Consumer<Flag> aConsumer ) {
		super( aLongOption, Boolean.class, aAlias, aDescription, aConsumer );
		setValue( false );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns true if the switch has been set (enabled).
	 * 
	 * @return True in case the {@link Flag} has been provided (set), else
	 *         false.
	 */
	@Override
	public boolean isEnabled() {
		final Boolean theValue = getValue();
		if ( theValue != null ) {
			return theValue;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Operand<Boolean>[] parseArgs( String[] aArgs, String[] aOptions, CliContext aCliCtx ) throws ArgsSyntaxException {
		for ( String eArg : aArgs ) {
			String theOption = aCliCtx.toShortOption( this );
			if ( eArg.equals( theOption ) ) {
				setValue( true );
				setParsedArgs( new String[] { theOption } );
				_matchCount = 1;
				return (Operand<Boolean>[]) new Operand<?>[] { this };
			}
			theOption = aCliCtx.toLongOption( this );
			if ( eArg.equals( theOption ) ) {
				setValue( true );
				setParsedArgs( new String[] { theOption } );
				_matchCount = 1;
				return (Operand<Boolean>[]) new Operand<?>[] { this };
			}
		}
		throw _exception = toException( aArgs, aCliCtx );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		super.reset();
		super.setValue( false );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSyntax( CliContext aCliCtx ) {
		return aCliCtx.toOptionEscapeCode() + aCliCtx.toOptionUsage( this ) + aCliCtx.toResetEscapeCode();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public Flag withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Boolean toType( String aArg ) throws ParseArgsException {
		throw new UnsupportedOperationException( "*** NOT REQUIRED BY THIS IMPLEMENTATION ***" );
	}
}
