// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.function.Consumer;

import org.refcodes.struct.Relation;

/**
 * The {@link IntProperty} represents an {@link PropertyOption} holding
 * <code>int</code> values.
 */
public class IntProperty extends AbstractPropertyOption<Integer> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortProperty The short option to use.
	 * @param aLongProperty The long option to use.
	 * @param aDescription The description of the {@link IntProperty}
	 */
	public IntProperty( Character aShortProperty, String aLongProperty, String aDescription ) {
		super( aShortProperty, aLongProperty, Integer.class, aDescription );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option, else the short option is used ass alias.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link IntProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link IntProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public IntProperty( Character aShortOption, String aLongOption, String aDescription, Consumer<IntProperty> aConsumer ) {
		super( aShortOption, aLongOption, Integer.class, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments.
	 *
	 * @param aShortProperty The short option to use.
	 * @param aLongProperty The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link IntProperty}
	 */
	public IntProperty( Character aShortProperty, String aLongProperty, String aAlias, String aDescription ) {
		super( aShortProperty, aLongProperty, Integer.class, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments.
	 *
	 * @param aShortOption The short option to use.
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link IntProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link IntProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public IntProperty( Character aShortOption, String aLongOption, String aAlias, String aDescription, Consumer<IntProperty> aConsumer ) {
		super( aShortOption, aLongOption, Integer.class, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the alias being the proerty's
	 * key and the value being the property's value. Depending on the provided
	 * property's key, the key is either used for the short option or the long
	 * option.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 */
	public IntProperty( Relation<String, Integer> aProperty ) {
		super( aProperty, Integer.class );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the alias being the proerty's
	 * key and the value being the property's value. Depending on the provided
	 * property's key, the key is either used for the short option or the long
	 * option.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link IntProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public IntProperty( Relation<String, Integer> aProperty, Consumer<IntProperty> aConsumer ) {
		super( aProperty, Integer.class, aConsumer );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongProperty The long option to use.
	 * @param aDescription The description of the {@link IntProperty}
	 */
	public IntProperty( String aLongProperty, String aDescription ) {
		super( aLongProperty, Integer.class, aDescription );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments. In case
	 * a long option is provided, the intance's alias will automatically be set
	 * with the long option.
	 *
	 * @param aLongOption The long option to use.
	 * @param aDescription The description of the {@link IntProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link IntProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public IntProperty( String aLongOption, String aDescription, Consumer<IntProperty> aConsumer ) {
		super( aLongOption, Integer.class, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments.
	 *
	 * @param aLongProperty The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link IntProperty}
	 */
	public IntProperty( String aLongProperty, String aAlias, String aDescription ) {
		super( aLongProperty, Integer.class, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link IntProperty} with the given arguments.
	 *
	 * @param aLongOption The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link IntProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link IntProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public IntProperty( String aLongOption, String aAlias, String aDescription, Consumer<IntProperty> aConsumer ) {
		super( aLongOption, Integer.class, aAlias, aDescription, aConsumer );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public IntProperty withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Integer toType( String aArg ) throws ParseArgsException {
		try {
			final Integer theValue = Integer.valueOf( aArg );
			_matchCount = 1;
			return theValue;
		}
		catch ( NumberFormatException e ) {
			final ParseArgsException theException = new ParseArgsException( "Unable to parse the argument <" + aArg + "> of option \"" + getAlias() + "\" to an <int> value.", new String[] { aArg }, this, e );
			_exception = theException;
			throw theException;
		}
	}
}
