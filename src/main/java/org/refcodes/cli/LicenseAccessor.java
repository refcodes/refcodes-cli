// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a license property.
 */
public interface LicenseAccessor {

	/**
	 * Provides a builder method for a license property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface LicenseBuilder<B extends LicenseBuilder<B>> {

		/**
		 * Sets the license for the license property.
		 * 
		 * @param aLicense The license to be stored by the console width
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withLicense( String aLicense );
	}

	/**
	 * Provides a mutator for a license property.
	 */
	public interface LicenseMutator {

		/**
		 * Sets the license for the license property.
		 * 
		 * @param aLicense The license to be stored by the console width
		 *        property.
		 */
		void setLicense( String aLicense );
	}

	/**
	 * Provides a license property.
	 */
	public interface LicenseProperty extends LicenseAccessor, LicenseMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given width (setter) as of
		 * {@link #setLicense(String)} and returns the very same value (getter).
		 * 
		 * @param aLicense The width to set (via {@link #setLicense(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letLicense( String aLicense ) {
			setLicense( aLicense );
			return aLicense;
		}
	}

	/**
	 * Retrieves the license from the license property.
	 * 
	 * @return The license stored by the license property.
	 */
	String getLicense();
}
