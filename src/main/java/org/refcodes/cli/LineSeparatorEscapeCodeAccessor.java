// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a line separator Escape-Code property.
 */
public interface LineSeparatorEscapeCodeAccessor {

	/**
	 * Provides a builder method for a line separator Escape-Code property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface LineSeparatorEscapeCodeBuilder<B extends LineSeparatorEscapeCodeBuilder<B>> {

		/**
		 * Sets the line separator Escape-Code for the line separator
		 * Escape-Code property.
		 * 
		 * @param aLineSeparatorEscCode The line separator Escape-Code to be
		 *        stored by the line separator Escape-Code property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withLineSeparatorEscapeCode( String aLineSeparatorEscCode );
	}

	/**
	 * Provides a mutator for a line separator Escape-Code property.
	 */
	public interface LineSeparatorEscapeCodeMutator {

		/**
		 * Sets the line separator Escape-Code for the line separator
		 * Escape-Code property.
		 * 
		 * @param aLineSeparatorEscCode The line separator Escape-Code to be
		 *        stored by the line separator Escape-Code property.
		 */
		void setLineSeparatorEscapeCode( String aLineSeparatorEscCode );
	}

	/**
	 * Provides a line separator Escape-Code property.
	 */
	public interface LineSeparatorEscapeCodeProperty extends LineSeparatorEscapeCodeAccessor, LineSeparatorEscapeCodeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setLineSeparatorEscapeCode(String)} and returns the
		 * very same value (getter).
		 * 
		 * @param aLineSeparatorEscCode The {@link String} to set (via
		 *        {@link #setLineSeparatorEscapeCode(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letLineSeparatorEscapeCode( String aLineSeparatorEscCode ) {
			setLineSeparatorEscapeCode( aLineSeparatorEscCode );
			return aLineSeparatorEscCode;
		}
	}

	/**
	 * Retrieves the line separator Escape-Code from the line separator
	 * Escape-Code property.
	 * 
	 * @return The line separator Escape-Code stored by the line separator
	 *         Escape-Code property.
	 */
	String getLineSeparatorEscapeCode();
}
