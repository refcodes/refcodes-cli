// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a long option property.
 */
public interface LongOptionAccessor {

	/**
	 * Retrieves the long option from the long option property.
	 * 
	 * @return The long option stored by the long option property.
	 */
	String getLongOption();

	/**
	 * Provides a mutator for a long option property.
	 */
	public interface LongOptionMutator {

		/**
		 * Sets the long option for the long option property.
		 * 
		 * @param aLongOption The long option to be stored by the long option
		 *        property.
		 */
		void setLongOption( String aLongOption );
	}

	/**
	 * Provides a builder method for a long option property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface LongOptionBuilder<B extends LongOptionBuilder<B>> {

		/**
		 * Sets the long option for the long option property.
		 * 
		 * @param aLongOption The long option to be stored by the long option
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withLongOption( String aLongOption );
	}

	/**
	 * Provides a long option property.
	 */
	public interface LongOptionProperty extends LongOptionAccessor, LongOptionMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setLongOption(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aLongOption The {@link String} to set (via
		 *        {@link #setLongOption(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letLongOption( String aLongOption ) {
			setLongOption( aLongOption );
			return aLongOption;
		}
	}
}
