// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a long option prefix property.
 */
public interface LongPropertyPrefixAccessor {

	/**
	 * Provides a builder method for a long option prefix property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface LongPropertyPrefixBuilder<B extends LongPropertyPrefixBuilder<B>> {

		/**
		 * Sets the long option prefix for the long option prefix property.
		 * 
		 * @param aLongPropertyPrefix The long option prefix to be stored by the
		 *        long option prefix property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withLongPropertyPrefix( String aLongPropertyPrefix );
	}

	/**
	 * Provides a mutator for a long option prefix property.
	 */
	public interface LongPropertyPrefixMutator {

		/**
		 * Sets the long option prefix for the long option prefix property.
		 * 
		 * @param aLongPropertyPrefix The long option prefix to be stored by the
		 *        long option prefix property.
		 */
		void setLongPropertyPrefix( String aLongPropertyPrefix );
	}

	/**
	 * Provides a long option prefix property.
	 */
	public interface LongPropertyPrefixProperty extends LongPropertyPrefixAccessor, LongPropertyPrefixMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setLongPropertyPrefix(String)} and returns the very
		 * same value (getter).
		 * 
		 * @param aLongPropertyPrefix The {@link String} to set (via
		 *        {@link #setLongPropertyPrefix(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letLongPropertyPrefix( String aLongPropertyPrefix ) {
			setLongPropertyPrefix( aLongPropertyPrefix );
			return aLongPropertyPrefix;
		}
	}

	/**
	 * Retrieves the long option prefix from the long option prefix property.
	 * 
	 * @return The long option prefix stored by the long option prefix property.
	 */
	String getLongPropertyPrefix();
}
