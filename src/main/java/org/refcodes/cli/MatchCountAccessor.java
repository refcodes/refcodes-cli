// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a match-count property.
 */
public interface MatchCountAccessor {

	/**
	 * Retrieves the match-count from the match-count property.
	 * 
	 * @return The matchCount stored by the match-count property.
	 */
	int getMatchCount();

	/**
	 * Provides a mutator for a match-count property.
	 */
	public interface MatchCountMutator {

		/**
		 * Sets the match-count for the match-count property.
		 * 
		 * @param aMatchCount The matchCount to be stored by the match-count
		 *        property.
		 */
		void setMatchCount( int aMatchCount );
	}

	/**
	 * Provides a builder method for a match-count property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface MatchCountBuilder<B extends MatchCountBuilder<B>> {

		/**
		 * Sets the match-count for the match-count property.
		 * 
		 * @param aMatchCount The matchCount to be stored by the match-count
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withMatchCount( int aMatchCount );
	}

	/**
	 * Provides a match-count property.
	 */
	public interface MatchCountProperty extends MatchCountAccessor, MatchCountMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setMatchCount(int)} and returns the very same value (getter).
		 * 
		 * @param aMatchCount The value to set (via
		 *        {@link #setMatchCount(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letMatchCount( int aMatchCount ) {
			setMatchCount( aMatchCount );
			return aMatchCount;
		}
	}
}
