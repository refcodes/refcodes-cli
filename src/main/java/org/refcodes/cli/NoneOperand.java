// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.function.Consumer;

import org.refcodes.mixin.EnabledAccessor;
import org.refcodes.struct.Relation;

/**
 * The {@link NoneOperand} represents an empty set of arguments, e.g. no command
 * line argument is being passed. Usually only makes sense near the root of a
 * {@link Term} tree (e.g. inside the main {@link XorCondition}) to test ALL
 * arguments passed to an application and one invocation may have neither
 * optional (as of {@link AnyCondition}) nor mandatory arguments allowed.
 */
public class NoneOperand extends AbstractOperand<Boolean> implements EnabledAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ALIAS = "none";

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link NoneOperand}.
	 *
	 * @param aProperty The key (= alias) and the value for the operand.
	 */
	public NoneOperand( Relation<String, Boolean> aProperty ) {
		super( aProperty, Boolean.class );
	}

	/**
	 * Instantiates a new {@link NoneOperand}.
	 *
	 * @param aDescription The description to be used (without any line breaks).
	 */
	public NoneOperand( String aDescription ) {
		super( Boolean.class, ALIAS, aDescription );
	}

	/**
	 * Instantiates a new {@link NoneOperand}.
	 *
	 * @param aAlias The identifier to be used when printing the syntax via the
	 *        {@link #toSyntax(CliContext)} method.
	 * @param aDescription The description to be used (without any line breaks).
	 */
	public NoneOperand( String aAlias, String aDescription ) {
		super( Boolean.class, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link NoneOperand}.
	 *
	 * @param aProperty The key (= alias) and the value for the operand.
	 * 
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link NoneOperand} participated in successfully parsing the
	 *        command line arguments.
	 */
	public NoneOperand( Relation<String, Boolean> aProperty, Consumer<NoneOperand> aConsumer ) {
		super( aProperty, Boolean.class, aConsumer );
	}

	/**
	 * Instantiates a new {@link NoneOperand}.
	 *
	 * @param aDescription The description to be used (without any line breaks).
	 * 
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link NoneOperand} participated in successfully parsing the
	 *        command line arguments.
	 */
	public NoneOperand( String aDescription, Consumer<NoneOperand> aConsumer ) {
		super( Boolean.class, ALIAS, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link NoneOperand}.
	 *
	 * @param aAlias The identifier to be used when printing the syntax via the
	 *        {@link #toSyntax(CliContext)} method.
	 *
	 * @param aDescription The description to be used (without any line breaks).
	 *
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link NoneOperand} participated in successfully parsing the
	 *        command line arguments.
	 */
	public NoneOperand( String aAlias, String aDescription, Consumer<NoneOperand> aConsumer ) {
		super( Boolean.class, aAlias, aDescription, aConsumer );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines whether none arguments have been passed.
	 * 
	 * @return True in case none arguments have been passed.
	 */
	@Override
	public boolean isEnabled() {
		final Boolean theValue = getValue();
		if ( theValue != null ) {
			return theValue;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Operand<?>[] parseArgs( String[] aArgs, String[] aOptions, CliContext aCliContext ) throws ArgsSyntaxException {
		final boolean isEmpty = aArgs == null || aArgs.length == 0;
		setValue( isEmpty );
		if ( !isEmpty ) {
			throw _exception = new UnknownArgsException( "No arguments are expected but <" + aArgs.length + "> arguments have been provided!", aArgs, this );
		}
		return new Operand<?>[] { this };
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSyntax( CliContext aCliCtx ) {
		return aCliCtx.toArgumentEscapeCode() + aCliCtx.getSyntaxMetrics().getEmptySymbol() + aCliCtx.toResetEscapeCode();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public NoneOperand withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To value.
	 *
	 * @param aArg the arg
	 * 
	 * @return the string
	 */
	@Override
	protected Boolean toType( String aArg ) {
		return ( aArg == null || aArg.isEmpty() );
	}
}
