// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import org.refcodes.mixin.AliasAccessor;
import org.refcodes.mixin.Clonable;
import org.refcodes.mixin.TypeAccessor;
import org.refcodes.mixin.ValueAccessor;

/**
 * An {@link Operand} represents a value parsed from command line arguments. In
 * comparison to an {@link Option} an {@link Operand} just represents a plain
 * value without an identifier in front of it. An {@link Operand} has a state
 * which changes with each invocation of the
 * {@link #parseArgs(String[], String[], CliContext)} method.
 * <p>
 * It is recommended to put your {@link Operand} instance(s) at the end of your
 * top {@link Condition} to enforce it to be the last {@link Term}(s) when
 * parsing the command line arguments - this makes sure that any {@link Option}s
 * pick their option arguments so that the {@link Operand}(s) will correctly be
 * left over for parsing command line argument(s); the {@link Operand} will not
 * pick by mistake an option argument.
 *
 * @param <T> The generic type of the {@link Operand}'s value.
 */
public interface Operand<T> extends Term, ValueAccessor<T>, TypeAccessor<T>, ParsedArgsAccessor, Comparable<Operand<?>>, AliasAccessor, Cloneable, Clonable {
	/**
	 * Retrieves the name (alias) of the parameter value which can be the name
	 * of the operand ({@link Operand}) or the option argument ({@link Option}),
	 * depending on the sub-type inheriting from this interface. The parameter
	 * name is merely used for constructing the command line arguments syntax
	 * {@link String} via {@link #toSyntax(CliContext)} and the command line
	 * arguments detail description when creating a command line tool's help
	 * output. Attention: The alias can also be used as a key when putting an
	 * {@link Operand}'s key/value-pair into a dictionary such as is done by the
	 * <code>ApplicationProperties</code> type provided by the
	 * <code>refcodes-properties-ext-runime</code> artifact!
	 * 
	 * @return The name of the parameter, e.g the name of the operand or the
	 *         name of the option argument.
	 */
	@Override
	String getAlias();

	/**
	 * {@inheritDoc}
	 */
	@Override
	T getValue();

	/**
	 * When being parsed via the
	 * {@link #parseArgs(String[], String[], CliContext)} method, use this
	 * method to determine whether the {@link #getValue()} method contains
	 * parsed value.
	 * 
	 * @return The true in case there a value has been parsed via the
	 *         {@link #parseArgs(String[], String[], CliContext)} method.
	 */
	default boolean hasValue() {
		return getValue() != null;
	}

	/**
	 * Retrieves the (command line) args which represent this {@link Operand}
	 * after being "parsed" (provided with command line args).
	 * 
	 * @return The according args from the command line args.
	 */
	@Override
	String[] getParsedArgs();

	/**
	 * Creates a specification for this {@link Operand}, helpful e.g. in human
	 * readable messages. The specification is created using a plain (default)
	 * {@link CliContext} instance, for fine grained control on the
	 * specification, use a dedicated {@link CliContext} instance and invoke
	 * {@link #toUsage(CliContext)}.
	 * 
	 * @return The according usage text.
	 */
	default String toUsage() {
		return toUsage( new CliContext( false ) );
	}

	/**
	 * Creates a specification for this {@link Operand}, helpful e.g. in human
	 * readable messages.
	 * 
	 * @param aCliContext The {@link CliContext} to use when creating a
	 *        beautiful usage text.
	 * 
	 * @return The according usage text.
	 */
	default String toUsage( CliContext aCliContext ) {
		return aCliContext.toUsage( this );
	}

	/**
	 * Creates a specification for this {@link Operand} and its arguments.
	 * 
	 * @return The according specification text.
	 */
	default String toSpec() {
		return toSpec( new CliContext( false ) );
	}

	/**
	 * Creates a specification for this {@link Operand} and its arguments.
	 * 
	 * @param aCliContext The {@link CliContext} to use when creating a
	 *        beautiful specification text.
	 * 
	 * @return The according specification text.
	 */
	default String toSpec( CliContext aCliContext ) {
		return aCliContext.toSpec( this );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	default <V> V toValue( String aAlias ) {
		if ( aAlias.equals( getAlias() ) && hasValue() ) {
			return (V) getValue();
		}
		return null;
	}
}
