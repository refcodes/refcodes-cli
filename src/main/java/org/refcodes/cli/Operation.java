// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.function.Consumer;

import org.refcodes.mixin.EnabledAccessor;

/**
 * The {@link Operation} is an argument representing a function or a method
 * ("command") and is either provided or not provided as of
 * {@link #isEnabled()}. It must neither be prefixed with "-" nor with "--" in
 * contrast to the {@link Option} or the {@link Flag} type.
 */
public class Operation extends AbstractOperand<Boolean> implements EnabledAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _operation;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link Operation} with the given arguments.
	 * 
	 * @param aOperation The operation to declare.
	 * @param aDescription The description to be used (without any line breaks).
	 */
	public Operation( String aOperation, String aDescription ) {
		this( aOperation, aOperation, aDescription, null );
	}

	/**
	 * Constructs a {@link Operation} with the given arguments.
	 * 
	 * @param aOperation The operation to declare.
	 * @param aDescription The description to be used (without any line breaks).
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Operation} participated in successfully parsing the command
	 *        line arguments.
	 */
	public Operation( String aOperation, String aDescription, Consumer<Operation> aConsumer ) {
		this( aOperation, aOperation, aDescription, aConsumer );
	}

	/**
	 * Constructs a {@link Operation} with the given arguments.
	 * 
	 * @param aOperation The operation to declare.
	 * @param aAlias The operation's name to be used when constructing the
	 *        syntax.
	 * @param aDescription The description to be used (without any line breaks).
	 */
	public Operation( String aOperation, String aAlias, String aDescription ) {
		this( aOperation, aAlias, aDescription, null );
	}

	/**
	 * Constructs a {@link Operation} with the given arguments.
	 * 
	 * @param aOperation The operation to declare.
	 * @param aAlias The operation's name to be used when constructing the
	 *        syntax.
	 * @param aDescription The description to be used (without any line breaks).
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link Operation} participated in successfully parsing the command
	 *        line arguments.
	 */
	public Operation( String aOperation, String aAlias, String aDescription, Consumer<Operation> aConsumer ) {
		super( Boolean.class, aAlias, aDescription, aConsumer );
		if ( aOperation == null ) {
			throw new IllegalArgumentException( "The operation argument must not be null!" );
		}
		_operation = aOperation;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	/**
	 * Returns the operation's name.
	 * 
	 * @return The according operation's name.
	 */
	public String getOperation() {
		return _operation;
	}

	/**
	 * Returns true if the switch has been set (enabled).
	 * 
	 * @return True in case the {@link Flag} has been provided (set), else
	 *         false.
	 */
	@Override
	public boolean isEnabled() {
		final Boolean theValue = getValue();
		if ( theValue != null ) {
			return theValue;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Operand<Boolean>[] parseArgs( String[] aArgs, String[] aOptions, CliContext aCliContext ) throws ArgsSyntaxException {
		final boolean hasOperation;
		out: {
			if ( aArgs != null ) {
				for ( String aArg : aArgs ) {
					if ( getOperation().equals( aArg ) ) {
						hasOperation = true;
						break out;
					}
				}
			}
			hasOperation = false;
		}
		if ( hasOperation ) {
			setParsedArgs( new String[] { getOperation() } );
			setValue( true );
			_matchCount = 1;
			return (Operand<Boolean>[]) new Operand<?>[] { this };
		}
		throw _exception = new UnknownArgsException( "The operation \"" + getOperation() + "\" was found in the command line arguments.", aArgs, this );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		super.reset();
		super.setValue( false );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSyntax( CliContext aCliCtx ) {
		return aCliCtx.toOptionEscapeCode() + getOperation() + ( getAlias() != null ? ( " " + aCliCtx.toArgumentSpec( this ) ) : "" ) + aCliCtx.toResetEscapeCode();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public Operation withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Boolean toType( String aArg ) throws ParseArgsException {
		throw new UnsupportedOperationException( "*** NOT REQUIRED BY THIS IMPLEMENTATION ***" );
	}
}
