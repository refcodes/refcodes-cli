// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import org.refcodes.data.ArgsPrefix;

/**
 * An {@link Option} represents a command line option with the according
 * option's value. An {@link Option} can be seen as a key/value(s) pair usually
 * represented by two succeeding arguments (e.g. "--file simeFile". An
 * {@link Option} has a state which changes with each invocation of the
 * {@link #parseArgs(String[], String[], CliContext)} method.
 *
 * @param <T> The generic type of the {@link Option}'s value.
 */
public interface Option<T> extends Operand<T> {

	/**
	 * Returns the long-option representing an {@link Option} instance. A long
	 * option usually is being prefixed with a double hyphen-minus "--" as
	 * defined in {@link ArgsPrefix#POSIX_LONG_OPTION}.
	 * 
	 * @return The long-option {@link String}.
	 */
	String getLongOption();

	/**
	 * Returns the short-option representing an {@link Option} instance. A short
	 * option usually is being prefixed with a single hyphen-minus "-" as
	 * defined in {@link ArgsPrefix#POSIX_SHORT_OPTION}.
	 * 
	 * @return The short-option {@link String}.
	 */
	Character getShortOption();
}
