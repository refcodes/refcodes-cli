// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a option Escape-Code property.
 */
public interface OptionEscapeCodeAccessor {

	/**
	 * Provides a builder method for a option Escape-Code property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface OptionEscapeCodeBuilder<B extends OptionEscapeCodeBuilder<B>> {

		/**
		 * Sets the option Escape-Code for the option Escape-Code property.
		 * 
		 * @param aOptionEscCode The option Escape-Code to be stored by the
		 *        option Escape-Code property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withOptionEscapeCode( String aOptionEscCode );
	}

	/**
	 * Provides a mutator for a option Escape-Code property.
	 */
	public interface OptionEscapeCodeMutator {

		/**
		 * Sets the option Escape-Code for the option Escape-Code property.
		 * 
		 * @param aOptionEscCode The option Escape-Code to be stored by the
		 *        option Escape-Code property.
		 */
		void setOptionEscapeCode( String aOptionEscCode );
	}

	/**
	 * Provides a option Escape-Code property.
	 */
	public interface OptionEscapeCodeProperty extends OptionEscapeCodeAccessor, OptionEscapeCodeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setOptionEscapeCode(String)} and returns the very same
		 * value (getter).
		 * 
		 * @param aOptionEscCode The {@link String} to set (via
		 *        {@link #setOptionEscapeCode(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letOptionEscapeCode( String aOptionEscCode ) {
			setOptionEscapeCode( aOptionEscCode );
			return aOptionEscCode;
		}
	}

	/**
	 * Retrieves the option Escape-Code from the option Escape-Code property.
	 * 
	 * @return The option Escape-Code stored by the option Escape-Code property.
	 */
	String getOptionEscapeCode();
}
