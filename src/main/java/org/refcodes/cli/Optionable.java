// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides means to determine context specific information on {@link Option}
 * instances.
 */
public interface Optionable {

	/**
	 * Retrieves an array of the context specific options being configured for
	 * the given {@link Option} instance, being the long option (when set) and
	 * the short option (when set), by prepending the according runtime specific
	 * short option and long option prefix (if applicable).
	 * 
	 * @param aOption The {@link Option} instance for which to get the
	 *        configured short and long options.
	 * 
	 * @return The array of options being the long option (if not null) and the
	 *         short option (if not null) with the according context specific
	 *         prefixes.
	 */
	String[] toOptions( Option<?> aOption );

}
