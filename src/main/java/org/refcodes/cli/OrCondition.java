// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.ArrayList;
import java.util.List;

/**
 * An {@link OrCondition} represents a list of {@link Condition} ({@link Term})
 * instances of which at least one must be parsed successfully when the
 * {@link Term} s' {@link Term#parseArgs(String[], String[], CliContext)}
 * methods are invoked. The command line arguments syntax
 * <code>{ -a | -b | -c }</code> specifies that at least "-a", "-b" or "-c" must
 * be set. In case none is set , then the {@link OrCondition} will terminate the
 * {@link #parseArgs(String[], String[], CliContext)} method with an exception.
 */
public class OrCondition extends AbstractCondition {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link OrCondition} with the {@link Term}
	 * ({@link Condition}) instances to be nested.
	 *
	 * @param aArgs The {@link Term} ({@link Condition}) instances to be nested.
	 */
	public OrCondition( Term... aArgs ) {
		super( "At least one (OR) syntax branch must match from the command line arguments.", aArgs );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Operand<?>[] parseArgs( String[] aArgs, String[] aOptions, CliContext aCliCtx ) throws ArgsSyntaxException {
		List<Operand<?>> theResult = null;
		Operand<?>[] eOperands;
		ArgsSyntaxException theCause = null;
		for ( Term eChild : _children ) {
			try {
				eOperands = eChild.parseArgs( aArgs, aOptions, aCliCtx );
				if ( eOperands != null ) {
					if ( theResult == null ) {
						theResult = new ArrayList<>();
					}
					for ( var eOperand : eOperands ) {
						theResult.add( eOperand );
					}
				}
				aArgs = toArgsDiff( aArgs, eOperands );
			}
			// catch ( UnknownArgsException | AmbiguousArgsException e ) {
			catch ( ArgsSyntaxException e ) {
				if ( theCause == null ) {
					theCause = e;
				}
				else {
					theCause.addSuppressed( e );
				}
			}
		}
		if ( theResult != null ) {
			return theResult.toArray( new Operand<?>[theResult.size()] );
		}
		throw _exception = new ArgsSyntaxException( "Not any syntax branch matched the command line arguments, though at least one (OR) syntax branch must be matched by the command line arguments!", aArgs, this, theCause );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSynopsis( CliContext aCliCtx ) {
		String theSynopsis = "";
		for ( Term eChild : _children ) {
			if ( eChild.isVisible() ) {
				if ( theSynopsis.length() != 0 ) {
					theSynopsis += aCliCtx.getSyntaxMetrics().getOrSymbol() != null && aCliCtx.getSyntaxMetrics().getOrSymbol().length() != 0 ? " " + aCliCtx.getSyntaxMetrics().getOrSymbol() + " " : " ";
				}
				theSynopsis += eChild.toSyntax( aCliCtx );
			}
		}
		return theSynopsis;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public OrCondition withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}
}
