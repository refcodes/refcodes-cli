// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.io.PrintStream;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.refcodes.cli.ArgsSyntaxAccessor.ArgsSyntaxProperty;
import org.refcodes.cli.ArgumentEscapeCodeAccessor.ArgumentEscapeCodeProperty;
import org.refcodes.cli.BannerBorderEscapeCodeAccessor.BannerBorderEscapeCodeProperty;
import org.refcodes.cli.BannerEscapeCodeAccessor.BannerEscapeCodeProperty;
import org.refcodes.cli.BannerFontAccessor.BannerFontProperty;
import org.refcodes.cli.BannerFontPaletteAccessor.BannerFontPaletteProperty;
import org.refcodes.cli.CommandEscapeCodeAccessor.CommandEscapeCodeProperty;
import org.refcodes.cli.CopyrightAccessor.CopyrightProperty;
import org.refcodes.cli.DescriptionEscapeCodeAccessor.DescriptionEscapeCodeProperty;
import org.refcodes.cli.ExamplesAccessor.ExamplesProperty;
import org.refcodes.cli.LicenseAccessor.LicenseProperty;
import org.refcodes.cli.LineSeparatorEscapeCodeAccessor.LineSeparatorEscapeCodeProperty;
import org.refcodes.cli.LongOptionPrefixAccessor.LongOptionPrefixProperty;
import org.refcodes.cli.OptionEscapeCodeAccessor.OptionEscapeCodeProperty;
import org.refcodes.cli.ShortOptionPrefixAccessor.ShortOptionPrefixProperty;
import org.refcodes.cli.SyntaxMetricsAccessor.SyntaxMetricsProperty;
import org.refcodes.mixin.Approvable;
import org.refcodes.mixin.ConsoleWidthAccessor.ConsoleWidthProperty;
import org.refcodes.mixin.DescriptionAccessor.DescriptionProperty;
import org.refcodes.mixin.EscapeCodesStatusAccessor.EscapeCodeStatusProperty;
import org.refcodes.mixin.LineBreakAccessor.LineBreakProperty;
import org.refcodes.mixin.MaxConsoleWidthAccessor.MaxConsoleWidthProperty;
import org.refcodes.mixin.NameAccessor.NameProperty;
import org.refcodes.mixin.ResetEscapeCodeAccessor.ResetEscapeCodeProperty;
import org.refcodes.mixin.Resetable;
import org.refcodes.mixin.TitleAccessor.TitleProperty;
import org.refcodes.schema.Schema;
import org.refcodes.schema.Schemable;
import org.refcodes.textual.Font;
import org.refcodes.textual.TextBoxGrid;
import org.refcodes.textual.TextBoxGridAccessor.TextBoxGridProperty;
import org.refcodes.textual.TextBoxStyle;

/**
 * The {@link ParseArgs} provides means for parsing command line arguments and
 * constructing a command line utility's help output.
 */
public interface ParseArgs extends Schemable, Optionable, CliMetrics, LineBreakProperty, LineSeparatorEscapeCodeProperty, DescriptionEscapeCodeProperty, LicenseProperty, CopyrightProperty, ConsoleWidthProperty, MaxConsoleWidthProperty, BannerFontPaletteProperty, BannerFontProperty, CommandEscapeCodeProperty, BannerEscapeCodeProperty, BannerBorderEscapeCodeProperty, ExamplesProperty, TitleProperty, NameProperty, DescriptionProperty, EscapeCodeStatusProperty, ResetEscapeCodeProperty, OptionEscapeCodeProperty, ArgumentEscapeCodeProperty, SyntaxMetricsProperty, ShortOptionPrefixProperty, LongOptionPrefixProperty, TextBoxGridProperty, ArgsSyntaxProperty, ArgsProcessorBuilder<ParseArgs>, ArgsParserBuilder<ParseArgs>, Resetable {

	/**
	 * Adds a usage example by providing a description of the example and the
	 * command line arguments required by the example.
	 * 
	 * @param aExample The description as well as the command line arguments
	 *        used by the example.
	 */
	void addExample( Example aExample );

	/**
	 * Adds a usage example by providing a description of the example and the
	 * command line arguments required by the example.
	 * 
	 * @param aDescription The description of the example.
	 * @param aOperands The command line arguments used by the example.
	 */
	default void addExample( String aDescription, Operand<?>... aOperands ) {
		addExample( new Example( aDescription, aOperands ) );
	}

	/**
	 * Prints the given line to standard error with regards to the console width
	 * as specified by the {@link #withConsoleWidth(int)} method.
	 * 
	 * @param aLine The line to be printed.
	 */
	void errorLn( String aLine );

	/**
	 * Same as {@link #evalArgs(String[])} with the difference that the elements
	 * representing the arguments are passed as a list instead of an array.
	 *
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @return The list of evaluated command line arguments being instances of
	 *         the {@link Operand} interfaces or its sub-types. In case an
	 *         {@link Operand} implements the {@link Approvable} interface or
	 *         inherits from the {@link AbstractOperand} type, then the
	 *         {@link Operand} is also approved by invoking
	 *         {@link Approvable#approve()} or {@link AbstractOperand#approve()}
	 *         (which may trigger a registered lambda expression).
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	default Operand<?>[] evalArgs( List<String> aArgs ) throws ArgsSyntaxException {
		return evalArgs( aArgs.toArray( new String[aArgs.size()] ) );
	}

	/**
	 * Evaluates the provided command line arguments as of
	 * {@link #evalArgs(String[])}, additionally filtering (excluding) the
	 * arguments matching the provided {@link ArgsFilter}.
	 * 
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @param aArgsFilter The {@link ArgsFilter} used to filter (exclude) any
	 *        "unwanted" or otherwise evaluated arguments.
	 * 
	 * @return The list of evaluated command line arguments being instances of
	 *         the {@link Operand} interfaces or its sub-types. In case an
	 *         {@link Operand} implements the {@link Approvable} interface or
	 *         inherits from the {@link AbstractOperand} type, then the
	 *         {@link Operand} is also approved by invoking
	 *         {@link Approvable#approve()} or {@link AbstractOperand#approve()}
	 *         (which may trigger a registered lambda expression).
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	default Operand<?>[] evalArgs( List<String> aArgs, ArgsFilter aArgsFilter ) throws ArgsSyntaxException {
		return evalArgs( aArgsFilter.toFiltered( aArgs ) );
	}

	/**
	 * Evaluates the provided command line arguments as of
	 * {@link #evalArgs(String[])}, additionally filtering (excluding) the
	 * arguments matching the provided {@link Pattern}.
	 * 
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @param aFilterExp The {@link Pattern} used to filter (exclude) any
	 *        "unwanted" or otherwise evaluated arguments.
	 * 
	 * @return The list of evaluated command line arguments being instances of
	 *         the {@link Operand} interfaces or its sub-types. In case an
	 *         {@link Operand} implements the {@link Approvable} interface or
	 *         inherits from the {@link AbstractOperand} type, then the
	 *         {@link Operand} is also approved by invoking
	 *         {@link Approvable#approve()} or {@link AbstractOperand#approve()}
	 *         (which may trigger a registered lambda expression).
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	default Operand<?>[] evalArgs( List<String> aArgs, Pattern aFilterExp ) throws ArgsSyntaxException {
		return evalArgs( ArgsFilter.toFiltered( aArgs, aFilterExp ) );
	}

	/**
	 * Evaluates the provided command line arguments and determines the
	 * according values by evaluating the root {@link Condition}. In case of
	 * parsing failure, an according exception is thrown. ATTENTION: This method
	 * tests(!) for superfluous command line arguments being passed; e.g.
	 * command line arguments not being evaluated by any of the {@link Term}
	 * instance being traversed starting at the root {@link Condition} will be
	 * reported. Business logic therefore should invoke this root node's
	 * {@link #evalArgs(String[])} method instead of a {@link Condition}'s
	 * {@link Condition#parseArgs(String[], String[], CliContext)} method; as
	 * ignoring superfluous command line arguments will aCause unexpected
	 * behavior from the point of view of the invoker.
	 * 
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @return The list of evaluated command line arguments being instances of
	 *         the {@link Operand} interfaces or its sub-types. In case an
	 *         {@link Operand} implements the {@link Approvable} interface or
	 *         inherits from the {@link AbstractOperand} type, then the
	 *         {@link Operand} is also approved by invoking
	 *         {@link Approvable#approve()} or {@link AbstractOperand#approve()}
	 *         (which may trigger a registered lambda expression).
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	Operand<?>[] evalArgs( String[] aArgs ) throws ArgsSyntaxException;

	/**
	 * Evaluates the provided command line arguments as of
	 * {@link #evalArgs(String[])}, additionally filtering (excluding) the
	 * arguments matching the provided {@link ArgsFilter}.
	 * 
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @param aArgsFilter The {@link ArgsFilter} used to filter (exclude) any
	 *        "unwanted" or otherwise evaluated arguments.
	 * 
	 * @return The list of evaluated command line arguments being instances of
	 *         the {@link Operand} interfaces or its sub-types. In case an
	 *         {@link Operand} implements the {@link Approvable} interface or
	 *         inherits from the {@link AbstractOperand} type, then the
	 *         {@link Operand} is also approved by invoking
	 *         {@link Approvable#approve()} or {@link AbstractOperand#approve()}
	 *         (which may trigger a registered lambda expression).
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	default Operand<?>[] evalArgs( String[] aArgs, ArgsFilter aArgsFilter ) throws ArgsSyntaxException {
		return evalArgs( aArgsFilter.toFiltered( aArgs ) );
	}

	/**
	 * Evaluates the provided command line arguments as of
	 * {@link #evalArgs(String[])}, additionally filtering (excluding) the
	 * arguments matching the provided {@link Pattern}.
	 * 
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @param aFilterExp The {@link Pattern} used to filter (exclude) any
	 *        "unwanted" or otherwise evaluated arguments.
	 * 
	 * @return The list of evaluated command line arguments being instances of
	 *         the {@link Operand} interfaces or its sub-types. In case an
	 *         {@link Operand} implements the {@link Approvable} interface or
	 *         inherits from the {@link AbstractOperand} type, then the
	 *         {@link Operand} is also approved by invoking
	 *         {@link Approvable#approve()} or {@link AbstractOperand#approve()}
	 *         (which may trigger a registered lambda expression).
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	default Operand<?>[] evalArgs( String[] aArgs, Pattern aFilterExp ) throws ArgsSyntaxException {
		return evalArgs( ArgsFilter.toFiltered( aArgs, aFilterExp ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	Term getArgsSyntax();

	/**
	 * {@inheritDoc}
	 */
	@Override
	String getArgumentEscapeCode();

	/**
	 * {@inheritDoc}
	 */
	@Override
	String getBannerBorderEscapeCode();

	/**
	 * {@inheritDoc}
	 */
	@Override
	String getBannerEscapeCode();

	/**
	 * {@inheritDoc}
	 */
	@Override
	String getCommandEscapeCode();

	/**
	 * {@inheritDoc}
	 */
	@Override
	String getCopyright();

	/**
	 * {@inheritDoc}
	 */
	@Override
	String getDescription();

	/**
	 * {@inheritDoc}
	 */
	@Override
	String getDescriptionEscapeCode();

	/**
	 * {@inheritDoc}
	 */
	@Override
	Example[] getExamples();

	/**
	 * {@inheritDoc}
	 */
	@Override
	String getLicense();

	/**
	 * {@inheritDoc}
	 */
	@Override
	String getLineSeparatorEscapeCode();

	/**
	 * {@inheritDoc}
	 */
	@Override
	String getOptionEscapeCode();

	/**
	 * Gets the character to be used when printing a separator line with the
	 * {@link #printSeparatorLn()} method.
	 * 
	 * @return aSeparatorChar The character used by the
	 *         {@link #printSeparatorLn()} method when printing out the line of
	 *         characters..
	 */
	char getSeparatorLnChar();

	/**
	 * Prints the banner; the banner most probably is an ASCII_HEADER_ASCII_BODY
	 * art text block which's look depends strongly on the taste of the author
	 * implementing this interface.
	 */
	void printBanner();

	/**
	 * Prints the help as of {@link #printHelp()} without the banner.
	 */
	default void printBody() {
		if ( getLicense() != null ) {
			printLicense();
			printSeparatorLn();
		}
		printSynopsis();
		printSeparatorLn();
		if ( getDescription() != null ) {
			printDescription();
			printSeparatorLn();
		}
		if ( getArgsSyntax() != null ) {
			printOptions();
			printSeparatorLn();
		}
		if ( getExamples() != null && getExamples().length != 0 ) {
			printExamples();
			printSeparatorLn();
		}
		if ( getCopyright() != null ) {
			printCopyright();
			printSeparatorLn();
		}
	}

	/**
	 * Prints the copyright note as specified by the
	 * {@link #withCopyright(String)} method; with regards to the console width
	 * as specified by the {@link #withConsoleWidth(int)} method.
	 */
	void printCopyright();

	/**
	 * Prints the description as set by the {@link #withDescription(String)}
	 * method with regards to the console width as specified by the
	 * {@link #withConsoleWidth(int)} method.
	 */
	void printDescription();

	/**
	 * Prints example usages as as added by the
	 * {@link #addExample(String, Operand...)} method with regards to the
	 * console width as specified by the {@link #withConsoleWidth(int)} method.
	 */
	void printExamples();

	/**
	 * Prints the header, which might be a simplified banner: Can be used when
	 * overriding methods such as {@link #printHelp()} or
	 * {@link #printBanner()}.
	 */
	void printHeader();

	/**
	 * Prints the help to the standard output specified by the
	 * {@link #withStandardOut(PrintStream)} method. This method can make use of
	 * the more atomic methods {@link #printBanner()}, {@link #printSynopsis()},
	 * {@link #printDescription()}, {@link #printOptions()} or
	 * {@link #printSeparatorLn()} to print a help text which's look depends
	 * strongly on the taste of the author implementing this interface. In case
	 * you dislike the implementing author's taste, feel free to overwrite this
	 * method and compose your own help text from the building blocks such as
	 * {@link #printBanner()}, {@link #printSynopsis()},
	 * {@link #printDescription()}, {@link #printOptions()} or
	 * {@link #printSeparatorLn()}
	 */
	default void printHelp() {
		printBanner();
		printBody();
	}

	/**
	 * Prints the license note as specified by the {@link #withLicense(String)}
	 * method; with regards to the console width as specified by the
	 * {@link #withConsoleWidth(int)} method.
	 */
	void printLicense();

	/**
	 * Prints an empty line / a line break.
	 */
	void printLn();

	/**
	 * Prints the given line to standard out with regards to the console width
	 * as specified by the {@link #withConsoleWidth(int)} method.
	 * 
	 * @param aLine The line to be printed.
	 */
	void printLn( String aLine );

	/**
	 * Prints the {@link Option}s (short- and the long-options), the
	 * {@link Flag}es and the {@link Operand} and their description with regards
	 * to the console width as specified by the {@link #withConsoleWidth(int)}
	 * method.
	 */
	void printOptions();

	/**
	 * Prints a separator line using the separator character as specified by the
	 * {@link #withSeparatorLnChar(char)} method; with regards to the console
	 * width as specified by the {@link #withConsoleWidth(int)} method.
	 */
	void printSeparatorLn();

	/**
	 * Prints the syntax as retrieved by the root {@link Condition} element as
	 * of {@link Condition#toSynopsis(SyntaxNotation)} with regards to the
	 * {@link SyntaxNotation} set by the
	 * {@link #withSyntaxMetrics(SyntaxMetrics)} method.
	 */
	void printSynopsis();

	/**
	 * Set the error out {@link PrintStream} and make other adjustments with the
	 * result (with regards to the Builder-Pattern).
	 * 
	 * @param aErrorOut The error out {@link PrintStream} to set.
	 */
	void setErrorOut( PrintStream aErrorOut );

	/**
	 * {@inheritDoc}
	 */
	@Override
	void setLineSeparatorEscapeCode( String aLineSeparatorEscCode );

	/**
	 * Set the character to be used when printing a separator line with the
	 * {@link #printSeparatorLn()} method.
	 * 
	 * @param aSeparatorLnChar The character used by the
	 *        {@link #printSeparatorLn()} method when printing out the line of
	 *        characters..
	 */
	void setSeparatorLnChar( char aSeparatorLnChar );

	/**
	 * Set the standard out {@link PrintStream} and make other adjustments with
	 * the result (with regards to the Builder-Pattern).
	 * 
	 * @param aStandardOut The standard out {@link PrintStream} to set.
	 */
	void setStandardOut( PrintStream aStandardOut );

	/**
	 * {@inheritDoc}
	 */
	@Override
	void setSyntaxMetrics( SyntaxMetrics aSyntaxMetrics );

	/**
	 * {@inheritDoc}
	 */
	@Override
	default String[] toOptions( Option<?> aOption ) {
		return CliContext.toOptions( aOption, getShortOptionPrefix(), getLongOptionPrefix() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default CliSchema toSchema() {
		if ( getArgsSyntax() != null ) {
			return new CliSchema( getName(), getClass(), getDescription(), new Schema[] { getArgsSyntax().toSchema() } );
		}
		return new CliSchema( getName(), getClass(), getDescription() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withAddExample( Example aExample ) {
		addExample( aExample );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withAddExample( String aDescription, Operand<?>... aOperands ) {
		addExample( aDescription, aOperands );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withArgumentEscapeCode( String aParamEscCode ) {
		setArgumentEscapeCode( aParamEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withBannerBorderEscapeCode( String aBannerBorderEscCode ) {
		setBannerEscapeCode( aBannerBorderEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withBannerEscapeCode( String aBannerEscCode ) {
		setBannerEscapeCode( aBannerEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withBannerFont( Font aBannerFont ) {
		setBannerFont( aBannerFont );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withBannerFontPalette( char[] aColorPalette ) {
		setBannerFontPalette( aColorPalette );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withCommandEscapeCode( String aCommandEscCode ) {
		setCommandEscapeCode( aCommandEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withConsoleWidth( int aConsoleWidth ) {
		setConsoleWidth( aConsoleWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withCopyright( String aCopyright ) {
		setCopyright( aCopyright );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withDescription( String aDescription ) {
		setDescription( aDescription );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withDescriptionEscapeCode( String aDescriptionEscCode ) {
		setDescriptionEscapeCode( aDescriptionEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withErrorOut( PrintStream aErrorOut ) {
		setErrorOut( aErrorOut );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withEscapeCodesEnabled( boolean isEscCodeEnabled ) {
		setEscapeCodesEnabled( isEscCodeEnabled );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withEvalArgs( List<String> aArgs ) throws ArgsSyntaxException {
		evalArgs( aArgs.toArray( new String[aArgs.size()] ) );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withEvalArgs( String[] aArgs ) throws ArgsSyntaxException {
		evalArgs( aArgs );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withExamples( Collection<Example> aExamples ) {
		for ( Example eExample : aExamples ) {
			addExample( eExample );
		}
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withExamples( Example[] aExamples ) {
		for ( Example eExample : aExamples ) {
			addExample( eExample );
		}
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withLicense( String aLicense ) {
		setLicense( aLicense );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withLineBreak( String aLineBreak ) {
		setLineBreak( aLineBreak );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withLineSeparatorEscapeCode( String aLineSeparatorEscCode ) {
		setLineSeparatorEscapeCode( aLineSeparatorEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withLongOptionPrefix( String aLongOptionPrefix ) {
		setLongOptionPrefix( aLongOptionPrefix );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withMaxConsoleWidth( int aMaxConsoleWidth ) {
		setMaxConsoleWidth( aMaxConsoleWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withName( String aName ) {
		setName( aName );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withOptionEscapeCode( String aOptEscCode ) {
		setOptionEscapeCode( aOptEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withResetEscapeCode( String aResetEscCode ) {
		setResetEscapeCode( aResetEscCode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withSeparatorLnChar( char aSeparatorLnChar ) {
		setSeparatorLnChar( aSeparatorLnChar );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withShortOptionPrefix( Character aShortOptionPrefix ) {
		setShortOptionPrefix( aShortOptionPrefix );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withStandardOut( PrintStream aStandardOut ) {
		setStandardOut( aStandardOut );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withSyntaxMetrics( SyntaxMetrics aSyntaxMetrics ) {
		setSyntaxMetrics( aSyntaxMetrics );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withTextBoxGrid( TextBoxGrid aTextBoxGrid ) {
		setTextBoxGrid( aTextBoxGrid );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withTextBoxGrid( TextBoxStyle aTextBoxStyle ) {
		setTextBoxGrid( aTextBoxStyle );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default ParseArgs withTitle( String aTitle ) {
		setTitle( aTitle );
		return this;
	}
}