// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Thrown in case the provided command line arguments do not respect the
 * required semantics or cannot be converted to the required type by an
 * {@link Option} (having a short- and/or a long-option), e.g. the arguments
 * were rejected as them did not work for the according syntax.
 */
public class ParseOptionArgsException extends ParseArgsException implements ShortOptionAccessor, LongOptionAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final String _longOption;
	private final String _shortOption;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * @param aShortOption The full short option (prefix + suffix) for which the
	 *        arguments did not apply.
	 * @param aLongOption The full long option (prefix + suffix) for which the
	 *        arguments did not apply.
	 */
	public ParseOptionArgsException( String aMessage, String[] aArgs, String aShortOption, String aLongOption, Term aSource ) {
		super( aMessage, aArgs, aSource );
		_shortOption = aShortOption;
		_longOption = aLongOption;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aShortOption The full short option (prefix + suffix) for which the
	 *        arguments did not apply.
	 * @param aLongOption The full long option (prefix + suffix) for which the
	 *        arguments did not apply.
	 */
	public ParseOptionArgsException( String aMessage, String[] aArgs, String aShortOption, String aLongOption, Term aSource, String aErrorCode ) {
		super( aMessage, aArgs, aSource, aErrorCode );
		_shortOption = aShortOption;
		_longOption = aLongOption;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aShortOption The full short option (prefix + suffix) for which the
	 *        arguments did not apply.
	 * @param aLongOption The full long option (prefix + suffix) for which the
	 *        arguments did not apply.
	 */
	public ParseOptionArgsException( String aMessage, String[] aArgs, String aShortOption, String aLongOption, Throwable aCause, Term aSource ) {
		super( aMessage, aArgs, aSource, aCause );
		_shortOption = aShortOption;
		_longOption = aLongOption;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aShortOption The full short option (prefix + suffix) for which the
	 *        arguments did not apply.
	 * @param aLongOption The full long option (prefix + suffix) for which the
	 *        arguments did not apply.
	 */
	public ParseOptionArgsException( String aMessage, String[] aArgs, String aShortOption, String aLongOption, Throwable aCause, Term aSource, String aErrorCode ) {
		super( aMessage, aArgs, aSource, aCause, aErrorCode );
		_shortOption = aShortOption;
		_longOption = aLongOption;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aShortOption The full short option (prefix + suffix) for which the
	 *        arguments did not apply.
	 * @param aLongOption The full long option (prefix + suffix) for which the
	 *        arguments did not apply.
	 */
	public ParseOptionArgsException( String[] aArgs, String aShortOption, String aLongOption, Throwable aCause, Term aSource ) {
		super( aArgs, aSource, aCause );
		_shortOption = aShortOption;
		_longOption = aLongOption;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aShortOption The full short option (prefix + suffix) for which the
	 *        arguments did not apply.
	 * @param aLongOption The full long option (prefix + suffix) for which the
	 *        arguments did not apply.
	 */
	public ParseOptionArgsException( String[] aArgs, String aShortOption, String aLongOption, Throwable aCause, Term aSource, String aErrorCode ) {
		super( aArgs, aSource, aCause, aErrorCode );
		_shortOption = aShortOption;
		_longOption = aLongOption;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aShortOption The full short option (prefix + suffix) for which the
	 *        arguments did not apply.
	 * @param aLongOption The full long option (prefix + suffix) for which the
	 *        arguments did not apply.
	 */
	public ParseOptionArgsException( String aMessage, String[] aArgs, String aShortOption, String aLongOption ) {
		super( aMessage, aArgs );
		_shortOption = aShortOption;
		_longOption = aLongOption;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aShortOption The full short option (prefix + suffix) for which the
	 *        arguments did not apply.
	 * @param aLongOption The full long option (prefix + suffix) for which the
	 *        arguments did not apply.
	 */
	public ParseOptionArgsException( String aMessage, String[] aArgs, String aShortOption, String aLongOption, String aErrorCode ) {
		super( aMessage, aArgs, aErrorCode );
		_shortOption = aShortOption;
		_longOption = aLongOption;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aShortOption The full short option (prefix + suffix) for which the
	 *        arguments did not apply.
	 * @param aLongOption The full long option (prefix + suffix) for which the
	 *        arguments did not apply.
	 */
	public ParseOptionArgsException( String aMessage, String[] aArgs, String aShortOption, String aLongOption, Throwable aCause ) {
		super( aMessage, aArgs, aCause );
		_shortOption = aShortOption;
		_longOption = aLongOption;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aShortOption The full short option (prefix + suffix) for which the
	 *        arguments did not apply.
	 * @param aLongOption The full long option (prefix + suffix) for which the
	 *        arguments did not apply.
	 */
	public ParseOptionArgsException( String aMessage, String[] aArgs, String aShortOption, String aLongOption, Throwable aCause, String aErrorCode ) {
		super( aMessage, aArgs, aCause, aErrorCode );
		_shortOption = aShortOption;
		_longOption = aLongOption;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aShortOption The full short option (prefix + suffix) for which the
	 *        arguments did not apply.
	 * @param aLongOption The full long option (prefix + suffix) for which the
	 *        arguments did not apply.
	 */
	public ParseOptionArgsException( String[] aArgs, String aShortOption, String aLongOption, Throwable aCause ) {
		super( aArgs, aCause );
		_shortOption = aShortOption;
		_longOption = aLongOption;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param aShortOption The full short option (prefix + suffix) for which the
	 *        arguments did not apply.
	 * @param aLongOption The full long option (prefix + suffix) for which the
	 *        arguments did not apply.
	 */
	public ParseOptionArgsException( String[] aArgs, String aShortOption, String aLongOption, Throwable aCause, String aErrorCode ) {
		super( aArgs, aCause, aErrorCode );
		_shortOption = aShortOption;
		_longOption = aLongOption;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLongOption() {
		return _longOption;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getShortOption() {
		return _shortOption;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getPatternArguments() {
		return new Object[] { _shortOption, _longOption, _args, _source, _matchCount };
	}
}
