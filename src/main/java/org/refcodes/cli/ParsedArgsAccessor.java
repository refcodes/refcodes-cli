// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a command line arguments (short "args") array.
 */
public interface ParsedArgsAccessor {

	/**
	 * Retrieves the command line arguments from the parsed args property.
	 * 
	 * @return The command line arguments stored by the parsed args property.
	 */
	String[] getParsedArgs();

	/**
	 * Provides a mutator for a parsed args property.
	 */
	public interface ParsedArgsMutator {

		/**
		 * Sets the command line arguments for the parsed args property.
		 * 
		 * @param aParsedArgs The command line arguments to be stored by the
		 *        args property.
		 */
		void setParsedArgs( String[] aParsedArgs );
	}

	/**
	 * Provides a parsed args property.
	 */
	public interface ParsedArgsProperty extends ParsedArgsAccessor, ParsedArgsMutator {

		/**
		 * This method stores and passes through the given arguments, which is
		 * very useful for builder APIs: Sets the given {@link String} array
		 * (setter) as of {@link #setParsedArgs(String[])} and returns the very
		 * same value (getter).
		 * 
		 * @param aParsedArgs The arguments to set (via
		 *        {@link #setParsedArgs(String[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String[] letParsedArgs( String[] aParsedArgs ) {
			setParsedArgs( aParsedArgs );
			return aParsedArgs;
		}
	}
}
