// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * An {@link PropertyOption} represents a command line property with the
 * according porperty's key and value. A {@link PropertyOption} can be seen as a
 * key=value pair and is passed in a single argument as such (e.g.
 * "--file=simeFile"). A {@link PropertyOption} has a state which changes with
 * each invocation of the {@link #parseArgs(String[], String[], CliContext)}
 * method.
 *
 * @param <T> The generic type of the {@link PropertyOption}'s value.
 */
public interface PropertyOption<T> extends Option<T> {}
