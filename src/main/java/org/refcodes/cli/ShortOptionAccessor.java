// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a short option property.
 */
public interface ShortOptionAccessor {

	/**
	 * Retrieves the short option from the short option property.
	 * 
	 * @return The short option stored by the short option property.
	 */
	String getShortOption();

	/**
	 * Provides a mutator for a short option property.
	 */
	public interface ShortOptionMutator {

		/**
		 * Sets the short option for the short option property.
		 * 
		 * @param aShortOption The short option to be stored by the short option
		 *        property.
		 */
		void setShortOption( String aShortOption );
	}

	/**
	 * Provides a builder method for a short option property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ShortOptionBuilder<B extends ShortOptionBuilder<B>> {

		/**
		 * Sets the short option for the short option property.
		 * 
		 * @param aShortOption The short option to be stored by the short option
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withShortOption( String aShortOption );
	}

	/**
	 * Provides a short option property.
	 */
	public interface ShortOptionProperty extends ShortOptionAccessor, ShortOptionMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link String} (setter)
		 * as of {@link #setShortOption(String)} and returns the very same value
		 * (getter).
		 * 
		 * @param aShortOption The {@link String} to set (via
		 *        {@link #setShortOption(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default String letShortOption( String aShortOption ) {
			setShortOption( aShortOption );
			return aShortOption;
		}
	}
}
