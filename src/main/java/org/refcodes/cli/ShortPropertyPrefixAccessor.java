// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a short option prefix property.
 */
public interface ShortPropertyPrefixAccessor {

	/**
	 * Provides a builder method for a short option prefix property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ShortPropertyPrefixBuilder<B extends ShortPropertyPrefixBuilder<B>> {

		/**
		 * Sets the short option prefix for the short option prefix property.
		 * 
		 * @param aShortPropertyPrefix The short option prefix to be stored by
		 *        the short option prefix property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withShortPropertyPrefix( Character aShortPropertyPrefix );
	}

	/**
	 * Provides a mutator for a short option prefix property.
	 */
	public interface ShortPropertyPrefixMutator {

		/**
		 * Sets the short option prefix for the short option prefix property.
		 * 
		 * @param aShortPropertyPrefix The short option prefix to be stored by
		 *        the short option prefix property.
		 */
		void setShortPropertyPrefix( Character aShortPropertyPrefix );
	}

	/**
	 * Provides a short option prefix property.
	 */
	public interface ShortPropertyPrefixProperty extends ShortPropertyPrefixAccessor, ShortPropertyPrefixMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Character}
		 * (setter) as of {@link #setShortPropertyPrefix(Character)} and returns
		 * the very same value (getter).
		 * 
		 * @param aShortPropertyPrefix The {@link Character} to set (via
		 *        {@link #setShortPropertyPrefix(Character)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Character letShortPropertyPrefix( Character aShortPropertyPrefix ) {
			setShortPropertyPrefix( aShortPropertyPrefix );
			return aShortPropertyPrefix;
		}
	}

	/**
	 * Retrieves the short option prefix from the short option prefix property.
	 * 
	 * @return The short option prefix stored by the short option prefix
	 *         property.
	 */
	Character getShortPropertyPrefix();
}
