// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.function.Consumer;

import org.refcodes.struct.Relation;

/**
 * An {@link Operand} (neither a short option nor a long option prepended nor
 * being a switch) being s {@link String}. An {@link Operand} stands by itself
 * and represents itself.
 */
public class StringOperand extends AbstractOperand<String> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {StringOperand Flag} with the alias being the
	 * proerty's key and the value being the property's value.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 */
	public StringOperand( Relation<String, String> aProperty ) {
		super( aProperty, String.class );
	}

	/**
	 * Instantiates a new {StringOperand Flag} with the alias being the
	 * proerty's key and the value being the property's value.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringOperand} participated in successfully parsing the
	 *        command line arguments.
	 */
	public StringOperand( Relation<String, String> aProperty, Consumer<StringOperand> aConsumer ) {
		super( aProperty, String.class, aConsumer );
	}

	/**
	 * Instantiates a new {@link StringOperand}.
	 *
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description to be used (without any line breaks).
	 */
	public StringOperand( String aAlias, String aDescription ) {
		super( String.class, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link StringOperand}.
	 *
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description to be used (without any line breaks).
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringOperand} participated in successfully parsing the
	 *        command line arguments.
	 */
	public StringOperand( String aAlias, String aDescription, Consumer<StringOperand> aConsumer ) {
		super( String.class, aAlias, aDescription, aConsumer );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public StringOperand withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To value.
	 *
	 * @param aArg the arg
	 * 
	 * @return the string
	 */
	@Override
	protected String toType( String aArg ) {
		return aArg;
	}
}
