// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.function.Consumer;

import org.refcodes.struct.Relation;

/**
 * The {@link StringProperty} represents an {@link PropertyOption} holding
 * <code>String</code> values.
 */
public class StringProperty extends AbstractPropertyOption<String> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option, else the short option is used ass alias.
	 *
	 * @param aShortProperty The short option to use.
	 * @param aLongProperty The long option to use.
	 * @param aDescription The description of the {@link StringProperty}
	 */
	public StringProperty( Character aShortProperty, String aLongProperty, String aDescription ) {
		super( aShortProperty, aLongProperty, String.class, aDescription );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option, else the short option is used ass alias.
	 *
	 * @param aShortProperty The short option to use.
	 * @param aLongProperty The long option to use.
	 * @param aDescription The description of the {@link StringProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public StringProperty( Character aShortProperty, String aLongProperty, String aDescription, Consumer<StringProperty> aConsumer ) {
		super( aShortProperty, aLongProperty, String.class, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments.
	 *
	 * @param aShortProperty The short option to use.
	 * @param aLongProperty The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link StringProperty}
	 */
	public StringProperty( Character aShortProperty, String aLongProperty, String aAlias, String aDescription ) {
		super( aShortProperty, aLongProperty, String.class, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments.
	 *
	 * @param aShortProperty The short option to use.
	 * @param aLongProperty The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link StringProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public StringProperty( Character aShortProperty, String aLongProperty, String aAlias, String aDescription, Consumer<StringProperty> aConsumer ) {
		super( aShortProperty, aLongProperty, String.class, aAlias, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the alias being the
	 * proerty's key and the value being the property's value. Depending on the
	 * provided property's key, the key is either used for the short option or
	 * the long option.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 */
	public StringProperty( Relation<String, String> aProperty ) {
		super( aProperty, String.class );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the alias being the
	 * proerty's key and the value being the property's value. Depending on the
	 * provided property's key, the key is either used for the short option or
	 * the long option.
	 * 
	 * @param aProperty The key (=alias) and the value for the operand.
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public StringProperty( Relation<String, String> aProperty, Consumer<StringProperty> aConsumer ) {
		super( aProperty, String.class, aConsumer );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option.
	 *
	 * @param aLongProperty The long option to use.
	 * @param aDescription The description of the {@link StringProperty}
	 */
	public StringProperty( String aLongProperty, String aDescription ) {
		super( aLongProperty, String.class, aDescription );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments. In
	 * case a long option is provided, the intance's alias will automatically be
	 * set with the long option.
	 *
	 * @param aLongProperty The long option to use.
	 * @param aDescription The description of the {@link StringProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public StringProperty( String aLongProperty, String aDescription, Consumer<StringProperty> aConsumer ) {
		super( aLongProperty, String.class, aDescription, aConsumer );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments.
	 *
	 * @param aLongProperty The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link StringProperty}
	 */
	public StringProperty( String aLongProperty, String aAlias, String aDescription ) {
		super( aLongProperty, String.class, aAlias, aDescription );
	}

	/**
	 * Instantiates a new {@link StringProperty} with the given arguments.
	 *
	 * @param aLongProperty The long option to use.
	 * @param aAlias The alias to be used for naming purposes.
	 * @param aDescription The description of the {@link StringProperty}
	 * @param aConsumer The {@link Consumer} being invoked in case this
	 *        {@link StringProperty} participated in successfully parsing the
	 *        command line arguments.
	 */
	public StringProperty( String aLongProperty, String aAlias, String aDescription, Consumer<StringProperty> aConsumer ) {
		super( aLongProperty, String.class, aAlias, aDescription, aConsumer );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public StringProperty withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String toType( String aArg ) throws ParseArgsException {
		return aArg;
	}
}
