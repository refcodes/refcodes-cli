// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * The {@link Synopsisable} interface defines those methods required to create a
 * human readable usage {@link String} similar to a manpage's synopsis from the
 * implementing instance.
 */
public interface Synopsisable {

	/**
	 * Returns the human readable (verbose) syntax of implementing class
	 * ATTENTION: As of different parenthesis settings for some notations
	 * regarding the root {@link Term} and the child {@link Term}s, the method
	 * {@link Term#toSyntax(CliContext)} is called from inside a {@link Term}
	 * hierarchy. In case the syntax is to be retrieved from the root
	 * {@link Term} or an encapsulating and different type, then the applicable
	 * method to be called is {@link #toSynopsis(SyntaxNotation)}, as for some
	 * notations it will for example not create the most outer braces.
	 * 
	 * This method uses the {@link SyntaxNotation#LOGICAL} be default!
	 * 
	 * @return The human readable (verbose) command line arguments syntax.
	 */
	default String toSynopsis() {
		return toSynopsis( new CliContext() );
	}

	/**
	 * Returns the human readable (verbose) syntax of the implementing class
	 * ATTENTION: As of different parenthesis settings for some notations
	 * regarding the root {@link Term} and the child {@link Term}s, the method
	 * {@link Term#toSyntax(CliContext)} is called from inside a {@link Term}
	 * hierarchy. In case the syntax is to be retrieved from the root
	 * {@link Term} or an encapsulating and different type, then the applicable
	 * method to be called is {@link #toSynopsis(SyntaxNotation)}, as for some
	 * notations it will for example not create the most outer braces.
	 * 
	 * @param aCliCtx The {@link CliContext} for which the syntax is being
	 *        generated.
	 *
	 * @return The human readable (verbose) command line arguments syntax.
	 */
	String toSynopsis( CliContext aCliCtx );

	/**
	 * Returns the human readable (verbose) syntax of implementing class
	 * ATTENTION: As of different parenthesis settings for some notations
	 * regarding the root {@link Term} and the child {@link Term}s, the method
	 * {@link Term#toSyntax(CliContext)} is called from inside a {@link Term}
	 * hierarchy. In case the syntax is to be retrieved from the root
	 * {@link Term} or an encapsulating and different type, then the applicable
	 * method to be called is {@link #toSynopsis(SyntaxNotation)}, as for some
	 * notations it will for example not create the most outer braces.
	 * 
	 * This method uses the {@link SyntaxNotation#LOGICAL} be default!
	 *
	 * @param aOptEscCode The escape code to be used when processing an option,
	 *        e.g. this can be an ANSI Escape-Code to highlight the option.
	 * @param aParamEscCode The escape code to be used when processing am
	 *        argument, e.g. this can be an ANSI Escape-Code to highlight the
	 *        option.
	 * @param aResetEscCode The escape code to close (reset) any Escape-Code
	 *        being set before.
	 * 
	 * @return The human readable (verbose) command line arguments syntax.
	 */
	default String toSynopsis( String aOptEscCode, String aParamEscCode, String aResetEscCode ) {
		return toSynopsis( new CliContext( aOptEscCode, aParamEscCode, aResetEscCode ) );
	}

	/**
	 * Returns the human readable (verbose) syntax of implementing class
	 * ATTENTION: As of different parenthesis settings for some notations
	 * regarding the root {@link Term} and the child {@link Term}s, the method
	 * {@link Term#toSyntax(CliContext)} is called from inside a {@link Term}
	 * hierarchy. In case the syntax is to be retrieved from the root
	 * {@link Term} or an encapsulating and different type, then the applicable
	 * method to be called is {@link #toSynopsis(SyntaxNotation)}, as for some
	 * notations it will for example not create the most outer braces.
	 * 
	 * @param aSyntaxNotation The syntax notation used for generating the
	 *        command line arguments syntax.
	 * 
	 * @return The human readable (verbose) command line arguments syntax.
	 */
	default String toSynopsis( SyntaxNotation aSyntaxNotation ) {
		return toSynopsis( new CliContext( aSyntaxNotation ) );
	}

	/**
	 * Returns the human readable (verbose) syntax of implementing class
	 * ATTENTION: As of different parenthesis settings for some notations
	 * regarding the root {@link Term} and the child {@link Term}s, the method
	 * {@link Term#toSyntax(CliContext)} is called from inside a {@link Term}
	 * hierarchy. In case the syntax is to be retrieved from the root
	 * {@link Term} or an encapsulating and different type, then the applicable
	 * method to be called is {@link #toSynopsis(SyntaxNotation)}, as for some
	 * notations it will for example not create the most outer braces.
	 *
	 * @param aSyntaxNotation The syntax notation used for generating the
	 *        command line arguments syntax.
	 * @param aOptEscCode The escape code to be used when processing an option,
	 *        e.g. this can be an ANSI Escape-Code to highlight the option.
	 * @param aParamEscCode The escape code to be used when processing am
	 *        argument, e.g. this can be an ANSI Escape-Code to highlight the
	 *        option.
	 * @param aResetEscCode The escape code to close (reset) any Escape-Code
	 *        being set before.
	 * 
	 * @return The human readable (verbose) command line arguments syntax.
	 */
	default String toSynopsis( SyntaxNotation aSyntaxNotation, String aOptEscCode, String aParamEscCode, String aResetEscCode ) {
		return toSynopsis( new CliContext( aSyntaxNotation, aOptEscCode, aParamEscCode, aResetEscCode ) );
	}
}