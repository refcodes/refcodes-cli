// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.HashSet;
import java.util.Set;

/**
 * The {@link SyntaxMetrics} provides properties specific to a specific command
 * line syntax notation.
 */
public interface SyntaxMetrics extends ShortOptionPrefixAccessor, LongOptionPrefixAccessor, ArgumentPrefixAccessor, ArgumentSuffixAccessor {

	/**
	 * Retrieves the symbol for representing an ALL condition.
	 * 
	 * @return The according ALL symbol.
	 */
	String getAllSymbol();

	/**
	 * Retrieves the symbol for representing an AND condition.
	 * 
	 * @return The according AND symbol.
	 */
	String getAndSymbol();

	/**
	 * Retrieves the symbol for representing an OPTIONAL condition.
	 * 
	 * @return The according OPTIONAL symbol.
	 */
	String getAnySymbol();

	/**
	 * Retrieves the symbol representing the beginning of an array, e.g. an
	 * opening square brace.
	 * 
	 * @return The symbol representing the beginning of an array.
	 */
	String getBeginArraySymbol();

	/**
	 * Retrieves the symbol representing the beginning of a list, e.g. an
	 * opening brace.
	 * 
	 * @return The symbol representing the beginning of a list.
	 */
	String getBeginListSymbol();

	/**
	 * Retrieves the symbol representing the beginning of optional elements,
	 * e.g. an opening square brace.
	 * 
	 * @return The symbol representing the beginning of optional elements.
	 */
	String getBeginOptionalSymbol();

	/**
	 * Retrieves the symbol representing the beginning of a range, e.g. an
	 * opening curly brace.
	 * 
	 * @return The symbol representing the beginning of a range.
	 */
	String getBeginRangeSymbol();

	/**
	 * Retrieves the symbol for representing an EMPTY condition (no args).
	 * 
	 * @return The according EMPTY symbol.
	 */
	String getEmptySymbol();

	/**
	 * Retrieves the symbol representing the end of an array, e.g. a closing
	 * square brace.
	 * 
	 * @return The symbol representing the end of an array.
	 */
	String getEndArraySymbol();

	/**
	 * Retrieves the symbol representing the end of a list, e.g. a closing
	 * brace.
	 * 
	 * @return The symbol representing the end of a list.
	 */
	String getEndListSymbol();

	/**
	 * Retrieves the symbol representing the end of optional elements, e.g. a
	 * closing square brace.
	 * 
	 * @return The symbol representing the end of a optional elements.
	 */
	String getEndOptionalSymbol();

	/**
	 * Retrieves the symbol representing the end of a range, e.g. a closing
	 * curly brace.
	 * 
	 * @return The symbol representing the end of a range.
	 */
	String getEndRangeSymbol();

	/**
	 * Retrieves the symbol representing an interval, e.g. "..." or "-".
	 * 
	 * @return The symbol representing an interval.
	 */
	String getIntervalSymbol();

	/**
	 * Retrieves the symbol for representing an OR condition.
	 * 
	 * @return The according OR symbol.
	 */
	String getOrSymbol();

	/**
	 * Retrieves the symbol for representing an XOR condition.
	 * 
	 * @return The according XOR symbol.
	 */
	String getXorSymbol();

	/**
	 * Returns an array of the prefixes configured by this {@link SyntaxMetrics}
	 * instance, omitting any null or empty prefixes as well as any duplicates.
	 * 
	 * @return The array containing the configured options' prefixes.
	 */
	default String[] toOptionPrefixes() {
		final Set<String> thePrefixes = new HashSet<>();
		if ( getShortOptionPrefix() != null ) {
			thePrefixes.add( getShortOptionPrefix().toString() );
		}
		if ( getLongOptionPrefix() != null && getLongOptionPrefix().length() != 0 ) {
			thePrefixes.add( getLongOptionPrefix() );
		}
		return thePrefixes.toArray( new String[thePrefixes.size()] );
	}
}
