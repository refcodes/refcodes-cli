// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * Provides an accessor for a {@link SyntaxMetrics} property.
 */
public interface SyntaxMetricsAccessor {

	/**
	 * Provides a builder method for a {@link SyntaxMetrics} property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface SyntaxMetricsBuilder<B extends SyntaxMetricsBuilder<B>> {

		/**
		 * Sets the {@link SyntaxMetrics} for the {@link SyntaxMetrics}
		 * property.
		 * 
		 * @param aSyntaxMetrics The {@link SyntaxMetrics} to be stored by the
		 *        {@link SyntaxMetrics} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withSyntaxMetrics( SyntaxMetrics aSyntaxMetrics );

		/**
		 * Sets the {@link SyntaxNotation} for the {@link SyntaxNotation}
		 * property.
		 * 
		 * @param aSyntaxNotation The {@link SyntaxNotation} to be stored by the
		 *        {@link SyntaxNotation} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		default B withSyntaxMetrics( SyntaxNotation aSyntaxNotation ) {
			return withSyntaxMetrics( (SyntaxMetrics) aSyntaxNotation );
		}
	}

	/**
	 * Provides a mutator for a {@link SyntaxMetrics} property.
	 */
	public interface SyntaxMetricsMutator {

		/**
		 * Sets the {@link SyntaxMetrics} for the {@link SyntaxMetrics}
		 * property.
		 * 
		 * @param aSyntaxMetrics The {@link SyntaxMetrics} to be stored by the
		 *        {@link SyntaxMetrics} property.
		 */
		void setSyntaxMetrics( SyntaxMetrics aSyntaxMetrics );

		/**
		 * Sets the {@link SyntaxNotation} for the {@link SyntaxNotation}
		 * property.
		 * 
		 * @param aSyntaxNotation The {@link SyntaxNotation} to be stored by the
		 *        {@link SyntaxNotation} property.
		 */
		default void setSyntaxMetrics( SyntaxNotation aSyntaxNotation ) {
			setSyntaxMetrics( (SyntaxMetrics) aSyntaxNotation );
		}
	}

	/**
	 * Provides a {@link SyntaxMetrics} property.
	 */
	public interface SyntaxMetricsProperty extends SyntaxMetricsAccessor, SyntaxMetricsMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link SyntaxMetrics}
		 * (setter) as of {@link #setSyntaxMetrics(SyntaxMetrics)} and returns
		 * the very same value (getter).
		 * 
		 * @param aSyntaxMetrics The {@link SyntaxMetrics} to set (via
		 *        {@link #setSyntaxMetrics(SyntaxMetrics)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default SyntaxMetrics letSyntaxMetrics( SyntaxMetrics aSyntaxMetrics ) {
			setSyntaxMetrics( aSyntaxMetrics );
			return aSyntaxMetrics;
		}

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link SyntaxNotation}
		 * (setter) as of {@link #setSyntaxMetrics(SyntaxNotation)} and returns
		 * the very same value (getter).
		 * 
		 * @param aSyntaxNotation The {@link SyntaxNotation} to set (via
		 *        {@link #setSyntaxMetrics(SyntaxNotation)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default SyntaxNotation letSyntaxMetrics( SyntaxNotation aSyntaxNotation ) {
			setSyntaxMetrics( aSyntaxNotation );
			return aSyntaxNotation;
		}
	}

	/**
	 * Retrieves the {@link SyntaxMetrics} from the {@link SyntaxMetrics}
	 * property.
	 * 
	 * @return The {@link SyntaxMetrics} stored by the decorator (enabled) tags
	 *         property.
	 */
	SyntaxMetrics getSyntaxMetrics();
}
