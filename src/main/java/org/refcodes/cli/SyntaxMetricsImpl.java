// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

/**
 * The {@link SyntaxMetricsImpl} class implements the {@link SyntaxMetrics}
 * interface for building custom {@link SyntaxMetrics} instances.
 */
public class SyntaxMetricsImpl implements SyntaxMetrics {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Builder to build {@link SyntaxMetrics} instances.
	 */
	public static final class Builder implements ShortOptionPrefixBuilder<Builder>, LongOptionPrefixBuilder<Builder>, ArgumentPrefixBuilder<Builder>, ArgumentSuffixBuilder<Builder> {

		private String allSymbol = SyntaxNotation.DEFAULT.getAllSymbol();
		private String andSymbol = SyntaxNotation.DEFAULT.getAndSymbol();
		private String anySymbol = SyntaxNotation.DEFAULT.getAnySymbol();
		private String argNotationPrefix = SyntaxNotation.DEFAULT.getArgumentPrefix();
		private String argNotationSuffix = SyntaxNotation.DEFAULT.getArgumentSuffix();
		private String beginArraySymbol = SyntaxNotation.DEFAULT.getBeginArraySymbol();
		private String beginListSymbol = SyntaxNotation.DEFAULT.getBeginListSymbol();
		private String beginOptionalSymbol = SyntaxNotation.DEFAULT.getBeginOptionalSymbol();
		private String beginRangeSymbol = SyntaxNotation.DEFAULT.getBeginRangeSymbol();
		private String emptySymbol = SyntaxNotation.DEFAULT.getEmptySymbol();
		private String endArraySymbol = SyntaxNotation.DEFAULT.getEndArraySymbol();
		private String endListSymbol = SyntaxNotation.DEFAULT.getEndListSymbol();
		private String endOptionalSymbol = SyntaxNotation.DEFAULT.getEndOptionalSymbol();
		private String endRangeSymbol = SyntaxNotation.DEFAULT.getEndRangeSymbol();
		private String intervalSymbol = SyntaxNotation.DEFAULT.getIntervalSymbol();
		private String longOptionPrefix = SyntaxNotation.DEFAULT.getLongOptionPrefix();
		private String orSymbol = SyntaxNotation.DEFAULT.getOrSymbol();
		private Character shortOptionPrefix = SyntaxNotation.DEFAULT.getShortOptionPrefix();
		private String xorSymbol = SyntaxNotation.DEFAULT.getXorSymbol();

		private Builder() {}

		/**
		 * {@link Builder} for fluently building a {@link SyntaxMetrics}
		 * instances.
		 * 
		 * @return The according constructed {@link SyntaxMetrics} instance.
		 */
		public SyntaxMetrics build() {
			return new SyntaxMetricsImpl( this );
		}

		/**
		 * Sets the symbol for representing an ALL notation.
		 * 
		 * @param aAllSymbol The according ALL symbol.
		 * 
		 * @return This {@link Builder} instance for chaining multiple builder
		 *         methods.
		 */
		public Builder withAllSymbol( String aAllSymbol ) {
			allSymbol = aAllSymbol;
			return this;
		}

		/**
		 * Sets the symbol for representing an AND notation.
		 * 
		 * @param aAndSymbol The according AND symbol.
		 * 
		 * @return This {@link Builder} instance for chaining multiple builder
		 *         methods.
		 */
		public Builder withAndSymbol( String aAndSymbol ) {
			andSymbol = aAndSymbol;
			return this;
		}

		/**
		 * Sets the symbol for representing an ANY notation.
		 * 
		 * @param aAnySymbol The according ANY symbol.
		 * 
		 * @return This {@link Builder} instance for chaining multiple builder
		 *         methods.
		 */
		public Builder withAnySymbol( String aAnySymbol ) {
			anySymbol = aAnySymbol;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withArgumentPrefix( String aArgNotationPrefix ) {
			argNotationPrefix = aArgNotationPrefix;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withArgumentSuffix( String aArgNotationSuffix ) {
			argNotationSuffix = aArgNotationSuffix;
			return this;
		}

		/**
		 * Sets the symbol representing the beginning of an array, e.g. an
		 * opening square brace.
		 * 
		 * @param aBeginArraySymbol The symbol representing the beginning of a
		 *        array.
		 * 
		 * @return This {@link Builder} instance for chaining multiple builder
		 *         methods.
		 */
		public Builder withBeginArraySymbol( String aBeginArraySymbol ) {
			beginArraySymbol = aBeginArraySymbol;
			return this;
		}

		/**
		 * Sets the symbol representing the beginning of a list, e.g. an opening
		 * brace.
		 * 
		 * @param aBeginListSymbol The symbol representing the beginning of a
		 *        list.
		 * 
		 * @return This {@link Builder} instance for chaining multiple builder
		 *         methods.
		 */
		public Builder withBeginListSymbol( String aBeginListSymbol ) {
			beginListSymbol = aBeginListSymbol;
			return this;
		}

		/**
		 * Sets the symbol representing the beginning optional elements, e.g. an
		 * opening square brace.
		 * 
		 * @param aBeginOptionalSymbol The symbol representing the beginning of
		 *        optional elements.
		 * 
		 * @return This {@link Builder} instance for chaining multiple builder
		 *         methods.
		 */
		public Builder withBeginOptionalSymbol( String aBeginOptionalSymbol ) {
			beginOptionalSymbol = aBeginOptionalSymbol;
			return this;
		}

		/**
		 * Sets the symbol representing the beginning of a range, e.g. an
		 * opening square brace.
		 * 
		 * @param aBeginRangeSymbol The symbol representing the beginning of a
		 *        range.
		 * 
		 * @return This {@link Builder} instance for chaining multiple builder
		 *         methods.
		 */
		public Builder withBeginRangeSymbol( String aBeginRangeSymbol ) {
			beginRangeSymbol = aBeginRangeSymbol;
			return this;
		}

		/**
		 * Sets the symbol for representing an EMPTY notation.
		 * 
		 * @param aEmptySymbol The according EMPTY symbol.
		 * 
		 * @return This {@link Builder} instance for chaining multiple builder
		 *         methods.
		 */
		public Builder withEMptySymbol( String aEmptySymbol ) {
			emptySymbol = aEmptySymbol;
			return this;
		}

		/**
		 * Sets the symbol representing the end of an array, e.g. a closing
		 * square brace.
		 * 
		 * @param aEndArraySymbol The symbol representing the end of a array.
		 * 
		 * @return This {@link Builder} instance for chaining multiple builder
		 *         methods.
		 */
		public Builder withEndArraySymbol( String aEndArraySymbol ) {
			endArraySymbol = aEndArraySymbol;
			return this;
		}

		/**
		 * Sets the symbol representing the end of a list, e.g. a closing brace.
		 * 
		 * @param aEndListSymbol The symbol representing the end of a list.
		 * 
		 * @return This {@link Builder} instance for chaining multiple builder
		 *         methods.
		 */
		public Builder withEndListSymbol( String aEndListSymbol ) {
			endListSymbol = aEndListSymbol;
			return this;
		}

		/**
		 * Sets the symbol representing the end of optional elements, e.g. a
		 * closing square brace.
		 * 
		 * @param aEndOptionalSymbol The symbol representing the end of optional
		 *        elements.
		 * 
		 * @return This {@link Builder} instance for chaining multiple builder
		 *         methods.
		 */
		public Builder withEndOptionalSymbol( String aEndOptionalSymbol ) {
			endOptionalSymbol = aEndOptionalSymbol;
			return this;
		}

		/**
		 * Sets the symbol representing the end of a range, e.g. a closing
		 * square brace.
		 * 
		 * @param aEndRangeSymbol The symbol representing the end of a range.
		 * 
		 * @return This {@link Builder} instance for chaining multiple builder
		 *         methods.
		 */
		public Builder withEndRangeSymbol( String aEndRangeSymbol ) {
			endRangeSymbol = aEndRangeSymbol;
			return this;
		}

		/**
		 * Sets the symbol representing an interval, e.g. "..." or "-".
		 * 
		 * @param aIntervalSymbol The symbol representing an interval.
		 * 
		 * @return This {@link Builder} instance for chaining multiple builder
		 *         methods.
		 */
		public Builder withIntervalSymbol( String aIntervalSymbol ) {
			intervalSymbol = aIntervalSymbol;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withLongOptionPrefix( String aLongOptionPrefix ) {
			longOptionPrefix = aLongOptionPrefix;
			return this;
		}

		/**
		 * Sets the symbol for representing an OR notation.
		 * 
		 * @param aOrSymbol The according OR symbol.
		 * 
		 * @return This {@link Builder} instance for chaining multiple builder
		 *         methods.
		 */
		public Builder withOrSymbol( String aOrSymbol ) {
			orSymbol = aOrSymbol;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withShortOptionPrefix( Character aShortOptionPrefix ) {
			shortOptionPrefix = aShortOptionPrefix;
			return this;
		}

		/**
		 * Sets the symbol for representing an XOR notation.
		 * 
		 * @param aXorSymbol The according XOR symbol.
		 * 
		 * @return This {@link Builder} instance for chaining multiple builder
		 *         methods.
		 */
		public Builder withXorSymbol( String aXorSymbol ) {
			xorSymbol = aXorSymbol;
			return this;
		}
	}

	private String _allSymbol = SyntaxNotation.DEFAULT.getAllSymbol();
	private String _andSymbol = SyntaxNotation.DEFAULT.getAndSymbol();
	private String _anySymbol = SyntaxNotation.DEFAULT.getAndSymbol();
	private String _argumentNotationPrefix = SyntaxNotation.DEFAULT.getArgumentPrefix();
	private String _argumentNotationSuffix = SyntaxNotation.DEFAULT.getArgumentSuffix();
	private String _beginArraySymbol = SyntaxNotation.DEFAULT.getBeginArraySymbol();
	private String _beginListSymbol = SyntaxNotation.DEFAULT.getBeginListSymbol();
	private String _beginOptionalSymbol = SyntaxNotation.DEFAULT.getBeginOptionalSymbol();
	private String _beginRangeSymbol = SyntaxNotation.DEFAULT.getBeginRangeSymbol();
	private String _emptySymbol = SyntaxNotation.DEFAULT.getEmptySymbol();
	private String _endArraySymbol = SyntaxNotation.DEFAULT.getEndArraySymbol();
	private String _endListSymbol = SyntaxNotation.DEFAULT.getEndListSymbol();
	private String _endOptionalSymbol = SyntaxNotation.DEFAULT.getEndOptionalSymbol();
	private String _endRangeSymbol = SyntaxNotation.DEFAULT.getEndRangeSymbol();
	private String _intervalSymbol = SyntaxNotation.DEFAULT.getIntervalSymbol();
	private String _longOptionPrefix = SyntaxNotation.DEFAULT.getLongOptionPrefix();
	private String _orSymbol = SyntaxNotation.DEFAULT.getOrSymbol();
	private Character _shortOptionPrefix = SyntaxNotation.DEFAULT.getShortOptionPrefix();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private String _xorSymbol = SyntaxNotation.DEFAULT.getXorSymbol();

	/**
	 * Constructs an accordingly configured {@link SyntaxMetricsImpl} instance.
	 *
	 * @param aShortOptionPrefix The short option prefix.
	 * @param aLongOptionPrefix The long option prefix.
	 * @param aArgumentPrefix The argument notation prefix.
	 * @param aArgumentSuffix The argument notation suffix.
	 * @param aBeginListSymbol The symbol representing the beginning of a list.
	 * @param aEndListSymbol The symbol representing the end of a list.
	 * @param aBeginArraySymbol The symbol representing the beginning of an
	 *        array.
	 * @param aEndArraySymbol The symbol representing the end of an array.
	 * @param aBeginOptionalSymbol The symbol representing the beginning of a
	 *        optional elements.
	 * @param aEndOptionalSymbol The symbol representing the end of a optional
	 *        elements.
	 * @param aBeginRangeSymbol The symbol representing the beginning of a
	 *        range.
	 * @param aEndRangeSymbol The symbol representing the end of a range.
	 * @param aIntervalSymbol The symbol representing an interval.
	 * @param aEmptySymbol the EMPTY symbol to be used.
	 * @param aAllSymbol the ALL symbol to be used.
	 * @param aAnySymbol the ANY symbol to be used.
	 * @param aAndSymbol the AND symbol to be used.
	 * @param aOrSymbol the OR symbol to be used.
	 * @param aXorSymbol the XOR symbol to be used.
	 */
	public SyntaxMetricsImpl( Character aShortOptionPrefix, String aLongOptionPrefix, String aArgumentPrefix, String aArgumentSuffix, String aBeginListSymbol, String aEndListSymbol, String aBeginArraySymbol, String aEndArraySymbol, String aBeginOptionalSymbol, String aEndOptionalSymbol, String aBeginRangeSymbol, String aEndRangeSymbol, String aIntervalSymbol, String aEmptySymbol, String aAllSymbol, String aAnySymbol, String aAndSymbol, String aOrSymbol, String aXorSymbol ) {
		_shortOptionPrefix = aShortOptionPrefix;
		_longOptionPrefix = aLongOptionPrefix;
		_argumentNotationPrefix = aArgumentPrefix;
		_argumentNotationSuffix = aArgumentSuffix;
		_beginListSymbol = aBeginListSymbol;
		_endListSymbol = aEndListSymbol;
		_beginArraySymbol = aBeginArraySymbol;
		_endArraySymbol = aEndArraySymbol;
		_beginRangeSymbol = aBeginRangeSymbol;
		_endRangeSymbol = aEndRangeSymbol;
		_intervalSymbol = aIntervalSymbol;
		_allSymbol = aAllSymbol;
		_xorSymbol = aXorSymbol;
		_andSymbol = aAndSymbol;
		_anySymbol = aAndSymbol;
		_emptySymbol = aEmptySymbol;
		_beginOptionalSymbol = aBeginOptionalSymbol;
		_endOptionalSymbol = aEndOptionalSymbol;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	private SyntaxMetricsImpl( Builder builder ) {
		_argumentNotationPrefix = builder.argNotationPrefix;
		_argumentNotationSuffix = builder.argNotationSuffix;
		_shortOptionPrefix = builder.shortOptionPrefix;
		_longOptionPrefix = builder.longOptionPrefix;
		_xorSymbol = builder.xorSymbol;
		_allSymbol = builder.allSymbol;
		_beginListSymbol = builder.beginListSymbol;
		_endListSymbol = builder.endListSymbol;
		_beginArraySymbol = builder.beginArraySymbol;
		_endArraySymbol = builder.endArraySymbol;
		_beginRangeSymbol = builder.beginRangeSymbol;
		_endRangeSymbol = builder.endRangeSymbol;
		_intervalSymbol = builder.intervalSymbol;
		_andSymbol = builder.andSymbol;
		_emptySymbol = builder.emptySymbol;
		_anySymbol = builder.anySymbol;
		_orSymbol = builder.orSymbol;
		_beginOptionalSymbol = builder.beginOptionalSymbol;
		_endOptionalSymbol = builder.endOptionalSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAllSymbol() {
		return _allSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAndSymbol() {
		return _andSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAnySymbol() {
		return _anySymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getArgumentPrefix() {
		return _argumentNotationPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getArgumentSuffix() {
		return _argumentNotationSuffix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getBeginArraySymbol() {
		return _beginArraySymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getBeginListSymbol() {
		return _beginListSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getBeginOptionalSymbol() {
		return _beginOptionalSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getBeginRangeSymbol() {
		return _beginRangeSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEmptySymbol() {
		return _emptySymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEndArraySymbol() {
		return _endArraySymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEndListSymbol() {
		return _endListSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEndOptionalSymbol() {
		return _endOptionalSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEndRangeSymbol() {
		return _endRangeSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getIntervalSymbol() {
		return _intervalSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLongOptionPrefix() {
		return _longOptionPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getOrSymbol() {
		return _orSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getShortOptionPrefix() {
		return _shortOptionPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getXorSymbol() {
		return _xorSymbol;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a {@link Builder} used to fluently construct
	 * {@link SyntaxMetrics} instances.
	 * 
	 * @return The {@link Builder} for constructing {@link SyntaxMetrics}
	 *         instances.
	 */
	public static Builder builder() {
		return new Builder();
	}
}
