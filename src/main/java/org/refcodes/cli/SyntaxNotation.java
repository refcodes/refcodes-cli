// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import org.refcodes.data.ArgsPrefix;

/**
 * The {@link SyntaxNotation} is used by a the {@link Term#toSyntax(CliContext)}
 * method to determine which notation to be used for the generated syntax.
 */
public enum SyntaxNotation implements SyntaxMetrics {

	/**
	 * A syntax notation derived from GNU and POSIX utility conventions and
	 * utility argument syntax.
	 * 
	 * @see "http://www.gnu.org/software/libc/manual/html_node/Argument-Syntax.html"
	 * @see "http://pubs.opengroup.org/onlinepubs/009695399/basedefs/xbd_chap12.html"
	 */
	GNU_POSIX(ArgsPrefix.POSIX_SHORT_OPTION.getPrefix().charAt( 0 ), ArgsPrefix.POSIX_LONG_OPTION.getPrefix(), "<", ">", "{", "}", "[", "]", "[", "]", "{", "}", "...", "{}", null, null, null, "?", "|"),

	/**
	 * A notation which the author of this code believes to unambiguously
	 * describe a command line utility's argument syntax; as defined by the
	 * means of this <code>refcodes-cli</code> artifact.
	 */
	LOGICAL(ArgsPrefix.POSIX_SHORT_OPTION.getPrefix().charAt( 0 ), ArgsPrefix.POSIX_LONG_OPTION.getPrefix(), "<", ">", "(", ")", "[", "]", "[", "]", "{", "}", "…", "∅", "∀", "?", "&", "|", "^"),

	/**
	 * A verbose notation using words instead of single character symbols.
	 */
	VERBOSE(ArgsPrefix.POSIX_SHORT_OPTION.getPrefix().charAt( 0 ), ArgsPrefix.POSIX_LONG_OPTION.getPrefix(), "<", ">", "{", "}", "{", "}", "[", "]", "{", "}", "-", "EMPTY", "ALL", "ANY", "AND", "OR", "XOR"),

	/**
	 * A notation which tries to mimic <code>CMD</code>'s notation and in case
	 * of lack of <code>CMD</code> notation conventions, the {@link #GNU_POSIX}
	 * equivalent is used.
	 */
	WINDOWS(ArgsPrefix.WINDOWS_SHORT_OPTION.getPrefix().charAt( 0 ), ArgsPrefix.WINDOWS_LONG_OPTION.getPrefix(), "$", "", "{", "}", "[", "]", "[", "]", "{", "}", "...", "{}", null, null, null, "?", "|");

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final SyntaxNotation DEFAULT = SyntaxNotation.GNU_POSIX;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String _allSymString;
	private String _andSymbol;
	private String _anySymbol;
	private String _argumentPrefix;
	private String _argumentSuffix;
	private String _beginArraySymbol;
	private String _beginListSymbol;
	private String _beginOptionalSymbol;
	private String _beginRangeSymbol;
	private String _emptySymbol;
	private String _endArraySymbol;
	private String _endListSymbol;
	private String _endOptionalSymbol;
	private String _endRangeSymbol;
	private String _intervalSymbol;
	private String _longOptionPrefix;
	private String _orSymbol;
	private Character _shortOptionPrefix;
	private String _xorSymString;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private SyntaxNotation( Character aShortOptionPrefix, String aLongOptionPrefix, String aArgumentPrefix, String aArgumentSuffix, String aBeginListSymbol, String aEndListSymbol, String aBeginArraySymbol, String aEndArraySymbol, String aBeginOptionalSymbol, String aEndOptionalSymbol, String aBeginRangeSymbol, String aEndRangeSymbol, String aIntervalSymbol, String aEmptySymbol, String aAllSymbol, String aAnySymbol, String aAndSymbol, String aOrSymbol, String aXorSymbol ) {
		_argumentPrefix = aArgumentPrefix;
		_argumentSuffix = aArgumentSuffix;
		_shortOptionPrefix = aShortOptionPrefix;
		_longOptionPrefix = aLongOptionPrefix;
		_beginListSymbol = aBeginListSymbol;
		_endListSymbol = aEndListSymbol;
		_xorSymString = aXorSymbol;
		_orSymbol = aOrSymbol;
		_allSymString = aAllSymbol;
		_andSymbol = aAndSymbol;
		_anySymbol = aAnySymbol;
		_beginOptionalSymbol = aBeginOptionalSymbol;
		_endOptionalSymbol = aEndOptionalSymbol;
		_intervalSymbol = aIntervalSymbol;
		_beginRangeSymbol = aBeginRangeSymbol;
		_endRangeSymbol = aEndRangeSymbol;
		_beginArraySymbol = aBeginArraySymbol;
		_endArraySymbol = aEndArraySymbol;
		_emptySymbol = aEmptySymbol;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAllSymbol() {
		return _allSymString;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAndSymbol() {
		return _andSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAnySymbol() {
		return _anySymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getArgumentPrefix() {
		return _argumentPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getArgumentSuffix() {
		return _argumentSuffix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getBeginArraySymbol() {
		return _beginArraySymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getBeginListSymbol() {
		return _beginListSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getBeginOptionalSymbol() {
		return _beginOptionalSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getBeginRangeSymbol() {
		return _beginRangeSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEmptySymbol() {
		return _emptySymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEndArraySymbol() {
		return _endArraySymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEndListSymbol() {
		return _endListSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEndOptionalSymbol() {
		return _endOptionalSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEndRangeSymbol() {
		return _endRangeSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getIntervalSymbol() {
		return _intervalSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLongOptionPrefix() {
		return _longOptionPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getOrSymbol() {
		return _orSymbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Character getShortOptionPrefix() {
		return _shortOptionPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getXorSymbol() {
		return _xorSymString;
	}

	/**
	 * Retrieves a {@link SyntaxNotation} depending on the given string,
	 * ignoring the case as well as being graceful regarding "-" and "_",.
	 *
	 * @param aValue The name of the {@link SyntaxNotation} to be interpreted
	 *        graceful.
	 * 
	 * @return The {@link SyntaxNotation} being determined or null if none was
	 *         found.
	 */
	public static SyntaxNotation toSyntaxNotation( String aValue ) {
		aValue = aValue != null ? aValue.replaceAll( "-", "" ).replaceAll( "_", "" ) : aValue;
		for ( SyntaxNotation eValue : values() ) {
			if ( eValue.name().replaceAll( "-", "" ).replaceAll( "_", "" ).equalsIgnoreCase( aValue ) ) {
				return eValue;
			}
		}
		return null;
	}
}
