// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import org.refcodes.exception.ExceptionAccessor;
import org.refcodes.graphical.VisibleAccessor.VisibleBuilder;
import org.refcodes.graphical.VisibleAccessor.VisibleProperty;
import org.refcodes.mixin.DescriptionAccessor;
import org.refcodes.mixin.Resetable;
import org.refcodes.schema.Schemable;

/**
 * A {@link Term} defines the methods at least required when building a command
 * line arguments syntax tree for traversing the syntax tree; either for parsing
 * command line arguments or for constructing the command line arguments syntax.
 * By providing various implementations of the {@link Term}'s subclasses such as
 * {@link Operand}, {@link Option} or {@link Condition}, a command line
 * arguments syntax tree can be constructed. This syntax tree can be use to
 * create a human readable (verbose) command line arguments syntax and to parse
 * an array of command line arguments for determining the {@link Operand}s', the
 * {@link Flag}es' or the {@link Option}s' values.
 */
public interface Term extends Resetable, Synopsisable, DescriptionAccessor, Schemable, ExceptionAccessor<ArgsSyntaxException>, MatchCountAccessor, VisibleProperty, VisibleBuilder<Term> {

	/**
	 * Parses the provided command line arguments and determines the according
	 * values by evaluating this {@link Term} instance or, in case of being a
	 * node in the syntax tree (such as a {@link Condition}, traversing the
	 * child {@link Term} instances'
	 * {@link #parseArgs(String[], String[], CliContext)} methods. In case of
	 * successfully parsing this {@link Term} and / or the child {@link Term}
	 * instances, the evaluated command line arguments are returned: Depending
	 * on the {@link Term} subclasses representing the evaluated command line
	 * arguments, instances of {@link Flag} classes, {@link Option} classes or
	 * {@link Operand} classes may be found in the result. In case of parsing
	 * failure, an according exception is thrown. ATTENTION: This method does
	 * not test for superfluous command line arguments being passed; e.g.
	 * command line arguments not being evaluated by any of the {@link Term}
	 * instance being traversed. This method is commonly used by a root
	 * {@link Term}'s {@link ParseArgs#evalArgs(String[])} method, which
	 * delegates to the {@link #parseArgs(String[], String[], CliContext)}
	 * method and after which it determines whether there are superfluous
	 * arguments to be taken care of (by throwing an according exception).
	 * Business logic therefore should invoke the root node's
	 * {@link #parseArgs(String[], String[], CliContext)} method as ignoring
	 * superfluous command line arguments will aCause unexpected behavior from
	 * the point of view of the invoker.
	 * 
	 * @param aArgs The command line arguments to be parsed.
	 * @param aCliCtx The {@link CliContext} to use when parsing the arguments.
	 * 
	 * @return The list of evaluated command line arguments being instances of
	 *         the {@link Operand} interfaces or its sub-types.
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	default Operand<?>[] parseArgs( String[] aArgs, CliContext aCliCtx ) throws ArgsSyntaxException {
		final String theOptions[] = aCliCtx.toAllOptions( this );
		return parseArgs( aArgs, theOptions, aCliCtx );
	}

	/**
	 * Parses the provided command line arguments and determines the according
	 * values by evaluating this {@link Term} instance or, in case of being a
	 * node in the syntax tree (such as a {@link Condition}, traversing the
	 * child {@link Term} instances'
	 * {@link #parseArgs(String[], String[], CliContext)} methods. In case of
	 * successfully parsing this {@link Term} and / or the child {@link Term}
	 * instances, the evaluated command line arguments are returned: Depending
	 * on the {@link Term} subclasses representing the evaluated command line
	 * arguments, instances of {@link Flag} classes, {@link Option} classes or
	 * {@link Operand} classes may be found in the result. In case of parsing
	 * failure, an according exception is thrown. ATTENTION: This method does
	 * not test for superfluous command line arguments being passed; e.g.
	 * command line arguments not being evaluated by any of the {@link Term}
	 * instance being traversed. This method is commonly used by a root
	 * {@link Term}'s {@link ParseArgs#evalArgs(String[])} method, which
	 * delegates to the {@link #parseArgs(String[], String[], CliContext)}
	 * method and after which it determines whether there are superfluous
	 * arguments to be taken care of (by throwing an according exception).
	 * Business logic therefore should invoke the root node's
	 * {@link #parseArgs(String[], String[], CliContext)} method as ignoring
	 * superfluous command line arguments will aCause unexpected behavior from
	 * the point of view of the invoker.
	 *
	 * @param aArgs The command line arguments to be parsed.
	 * @param aOptions The list of options (short and as well as long) which are
	 *        reserved and cannot be used as value.
	 * @param aCliContext the cli context
	 * 
	 * @return The list of evaluated command line arguments being instances of
	 *         the {@link Operand} interfaces or its sub-types.
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	Operand<?>[] parseArgs( String[] aArgs, String[] aOptions, CliContext aCliContext ) throws ArgsSyntaxException;

	/**
	 * {@inheritDoc}
	 */
	@Override
	CliSchema toSchema();

	/**
	 * {@inheritDoc}
	 */
	@Override
	default String toSynopsis( CliContext aCliCtx ) {
		return toSyntax( aCliCtx );
	}

	/**
	 * This method is to be called from inside the {@link Term} hierarchy; use
	 * the method {@link #toSynopsis(SyntaxNotation)} in case you invoke syntax
	 * retrieval from the root {@link Term}. Returns the human readable
	 * (verbose) syntax of this {@link Term} instance including, in case of
	 * being a node in the syntax tree (such as a {@link Condition}, the syntax
	 * of the child {@link Term} instances. ATTENTION: As of different
	 * parenthesis settings for some notations regarding the root {@link Term}
	 * and the child {@link Term}s, the method {@link #toSyntax(CliContext)} is
	 * called from inside the {@link Term} hierarchy. In case the syntax is to
	 * be retrieved from the root {@link Term}, then the applicable method to be
	 * called is {@link #toSynopsis(SyntaxNotation)}, as for some notations it
	 * will for example not create the most outer braces.
	 * 
	 * This method uses the {@link SyntaxNotation#LOGICAL} be default!
	 * 
	 * @return The human readable (verbose) command line arguments syntax.
	 */
	default String toSyntax() {
		return toSyntax( new CliContext() );
	}

	/**
	 * This method is to be called from inside the {@link Term} hierarchy; use
	 * the method {@link #toSynopsis(SyntaxNotation)} in case you invoke syntax
	 * retrieval from the root {@link Term}. Returns the human readable
	 * (verbose) syntax of this {@link Term} instance including, in case of
	 * being a node in the syntax tree (such as a {@link Condition}, the syntax
	 * of the child {@link Term} instances. ATTENTION: As of different
	 * parenthesis settings for some notations regarding the root {@link Term}
	 * and the child {@link Term}s, the method {@link #toSyntax(CliContext)} is
	 * called from inside the {@link Term} hierarchy. In case the syntax is to
	 * be retrieved from the root {@link Term}, then the applicable method to be
	 * called is {@link #toSynopsis(SyntaxNotation)}, as for some notations it
	 * will for example not create the most outer braces.
	 * 
	 * @param aCliCtx The {@link CliContext} for which the syntax is being
	 *        generated.
	 * 
	 * @return The human readable (verbose) command line arguments syntax.
	 */
	String toSyntax( CliContext aCliCtx );

	/**
	 * This method is to be called from inside the {@link Term} hierarchy; use
	 * the method {@link #toSynopsis(SyntaxNotation)} in case you invoke syntax
	 * retrieval from the root {@link Term}. Returns the human readable
	 * (verbose) syntax of this {@link Term} instance including, in case of
	 * being a node in the syntax tree (such as a {@link Condition}, the syntax
	 * of the child {@link Term} instances. ATTENTION: As of different
	 * parenthesis settings for some notations regarding the root {@link Term}
	 * and the child {@link Term}s, the method {@link #toSyntax(CliContext)} is
	 * called from inside the {@link Term} hierarchy. In case the syntax is to
	 * be retrieved from the root {@link Term}, then the applicable method to be
	 * called is {@link #toSynopsis(SyntaxNotation)}, as for some notations it
	 * will for example not create the most outer braces.
	 * 
	 * This method uses the {@link SyntaxNotation#LOGICAL} be default!
	 * 
	 * @param aOptEscCode The escape code to be used when processing an option,
	 *        e.g. this can be an ANSI Escape-Code to highlight the option.
	 * @param aParamEscCode The escape code to be used when processing a
	 *        keyword, e.g. this can be an ANSI Escape-Code to highlight the
	 *        option and/or parameters.
	 * @param aResetEscCode The escape code to close (reset) any Escape-Code
	 *        being set before.
	 * 
	 * @return The human readable (verbose) command line arguments syntax.
	 */
	default String toSyntax( String aOptEscCode, String aParamEscCode, String aResetEscCode ) {
		return toSyntax( new CliContext( aOptEscCode, aParamEscCode, aResetEscCode ) );
	}

	/**
	 * This method is to be called from inside the {@link Term} hierarchy; use
	 * the method {@link #toSynopsis(SyntaxNotation)} in case you invoke syntax
	 * retrieval from the root {@link Term}. Returns the human readable
	 * (verbose) syntax of this {@link Term} instance including, in case of
	 * being a node in the syntax tree (such as a {@link Condition}, the syntax
	 * of the child {@link Term} instances. ATTENTION: As of different
	 * parenthesis settings for some notations regarding the root {@link Term}
	 * and the child {@link Term}s, the method {@link #toSyntax(CliContext)} is
	 * called from inside the {@link Term} hierarchy. In case the syntax is to
	 * be retrieved from the root {@link Term}, then the applicable method to be
	 * called is {@link #toSynopsis(SyntaxNotation)}, as for some notations it
	 * will for example not create the most outer braces.
	 * 
	 * @param aSyntaxNotation The syntax notation used for generating the
	 *        command line arguments syntax.
	 * 
	 * @return The human readable (verbose) command line arguments syntax.
	 */
	default String toSyntax( SyntaxNotation aSyntaxNotation ) {
		return toSyntax( new CliContext( aSyntaxNotation ) );
	}

	/**
	 * This method is to be called from inside the {@link Term} hierarchy; use
	 * the method {@link #toSynopsis(SyntaxNotation)} in case you invoke syntax
	 * retrieval from the root {@link Term}. Returns the human readable
	 * (verbose) syntax of this {@link Term} instance including, in case of
	 * being a node in the syntax tree (such as a {@link Condition}, the syntax
	 * of the child {@link Term} instances. ATTENTION: As of different
	 * parenthesis settings for some notations regarding the root {@link Term}
	 * and the child {@link Term}s, the method
	 * {@link #toSyntax(SyntaxNotation, String, String, String)} is called from
	 * inside the {@link Term} hierarchy. In case the syntax is to be retrieved
	 * from the root {@link Term}, then the applicable method to be called is
	 * {@link #toSynopsis(SyntaxNotation)}, as for some notations it will for
	 * example not create the most outer braces.
	 * 
	 * @param aSyntaxNotation The syntax notation used for generating the
	 *        command line arguments syntax.
	 * @param aOptEscCode The escape code to be used when processing an option,
	 *        e.g. this can be an ANSI Escape-Code to highlight the option.
	 * @param aParamEscCode The escape code to be used when processing a
	 *        keyword, e.g. this can be an ANSI Escape-Code to highlight the
	 *        option and/or parameters.
	 * @param aResetEscCode The escape code to close (reset) any Escape-Code
	 *        being set before.
	 * 
	 * @return The human readable (verbose) command line arguments syntax.
	 */
	default String toSyntax( SyntaxNotation aSyntaxNotation, String aOptEscCode, String aParamEscCode, String aResetEscCode ) {
		return toSyntax( new CliContext( aSyntaxNotation, aOptEscCode, aParamEscCode, aResetEscCode ) );
	}

	/**
	 * Searches for a value being set for the given for given alias in the
	 * syntax tree parsing the command line arguments; if a node in the syntax
	 * tree has a value for the given alias, then this value is returned.
	 * 
	 * @param <V> The type of the value being expected.
	 * @param aAlias The alias for which to seek for a value being set.
	 * 
	 * @return The according value or null if none has been set for the given
	 *         alias.
	 */
	<V> V toValue( String aAlias );

	/**
	 * Determines the number of args being matched by the
	 * {@link #parseArgs(String[], CliContext)} (and similar) method(s) of the
	 * according syntax. This way we can determine which part of the syntax
	 * (tree) had most matches.
	 * 
	 * @return The number of args matching the according syntax (tree).
	 */
	@Override
	int getMatchCount();

	/**
	 * Retrieves the {@link ArgsSyntaxException} exception in case invoking
	 * {@link #parseArgs(String[], CliContext)} failed. The exception (and the
	 * according suppressed ones alongside the causes) does not necessarily have
	 * been thrown by the {@link #parseArgs(String[], CliContext)} method
	 * depending on the semantics of the {@link Term} handing the exception (the
	 * {@link AnyCondition} provides any exceptions it caught even though it did
	 * not throw them as of its semantics).
	 *
	 * @return The {@link ArgsSyntaxException} exception occurring while parsing
	 *         the arguments.
	 */
	@Override
	ArgsSyntaxException getException();

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	void setVisible( boolean isVisible );

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	boolean isVisible();
}
