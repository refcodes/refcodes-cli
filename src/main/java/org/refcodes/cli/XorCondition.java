// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import java.util.ArrayList;
import java.util.List;

/**
 * An {@link XorCondition} represents a list of {@link Condition} ({@link Term})
 * instances of which only one is allowed to be parsed successfully when the
 * {@link Condition#parseArgs(String[], String[], CliContext)} methods are
 * invoked. The command line arguments syntax <code>{ -a ^ -b ^ -c }</code>
 * specifies that only "-a", only "-b" or only "-c" must be set. In case more
 * then one is set or none, then the {@link XorCondition} will terminate the
 * {@link #parseArgs(String[], String[], CliContext)} method with an exception.
 */
public class XorCondition extends AbstractCondition {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link XorCondition} with the {@link Term}
	 * ({@link Condition}) instances to be nested.
	 *
	 * @param aArgs The {@link Term} ({@link Condition}) instances to be nested
	 */
	public XorCondition( Term... aArgs ) {
		super( "Exactly one (XOR) syntax branch must match from the command line arguments.", aArgs );
	}

	/**
	 * Instantiates a new {@link XorCondition} with the {@link Term}
	 * ({@link Condition}) instances to be nested.
	 * 
	 * @param aDescription The description of this {@link Condition}.
	 *
	 * @param aArgs The {@link Term} ({@link Condition}) instances to be nested
	 */
	protected XorCondition( String aDescription, Term... aArgs ) {
		super( aDescription, aArgs );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Operand<?>[] parseArgs( String[] aArgs, String[] aOptions, CliContext aCliCtx ) throws ArgsSyntaxException {
		final List<Condition> theOptionals = new ArrayList<>();
		Operand<?>[] theResult = null;
		Synopsisable theResultSyntaxable = null;
		Operand<?>[] eOperands = null;
		int theMatchCount = _children.length;
		ArgsSyntaxException theCause = null;
		for ( Term eChild : _children ) {
			if ( eChild instanceof AnyCondition ) {
				theOptionals.add( (AnyCondition) eChild );
			}
			try {
				if ( theResult != null && eChild instanceof Operand && theResultSyntaxable instanceof Option ) {
					eOperands = eChild.parseArgs( toArgsDiff( aArgs, theResult ), aOptions, aCliCtx );
				}
				else {
					eOperands = eChild.parseArgs( aArgs, aOptions, aCliCtx );
				}
				if ( eOperands != null && eOperands.length == 0 ) {
					eOperands = null;
				}

			}
			catch ( ArgsSyntaxException e ) {
				if ( theCause == null ) {
					theCause = e;
				}
				else {
					theCause.addSuppressed( e );
				}
				theMatchCount--;
			}
			// Same operands in different children |-->
			theResult = removeDuplicates( theResult, eOperands );
			// Same operands in different children <--|
			if ( theResult != null && eOperands != null ) {
				throw _exception = new AmbiguousArgsException( "More than one syntax branch matched the command line arguments, though exactly one (XOR) syntax branch must match!", aArgs, this, theCause );
			}
			if ( eOperands != null && eOperands.length != 0 && theResult == null ) {
				theResult = eOperands;
				theResultSyntaxable = eChild;
			}
			eOperands = null;
		}
		if ( theMatchCount == 0 ) {
			throw _exception = new UnknownArgsException( "No syntax branch (fully) matched the command line arguments, though one (XOR) syntax branch must match!", aArgs, this, theCause );
		}
		if ( theMatchCount > 1 ) {
			if ( theOptionals.size() > 1 && ( aArgs != null && aArgs.length > 0 ) ) {
				throw _exception = new AmbiguousArgsException( "No syntax branch (fully) matched the command line arguments causing exclusive (XOR) ambiguity for the according syntax branch!", aArgs, this, theCause );
			}
			theMatchCount -= theOptionals.size();
			if ( theMatchCount > 1 ) {
				throw _exception = new AmbiguousArgsException( "No syntax branch (fully) matched the command line arguments causing exclusive (XOR) ambiguity for the according syntax branch!", aArgs, this, theCause );
			}
		}
		if ( theResult != null ) {
			return theResult;
		}
		if ( theOptionals.size() == 0 ) {
			return new Operand<?>[] {};
		}
		throw _exception = new UnknownArgsException( "No syntax branch (fully) matched the provided command line arguments, though one (XOR) syntax branch must match!", aArgs, this, theCause );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSynopsis( CliContext aCliCtx ) {
		String theSynopsis = "";
		for ( Term eChild : _children ) {
			if ( eChild.isVisible() ) {
				if ( theSynopsis.length() != 0 ) {
					theSynopsis += aCliCtx.getSyntaxMetrics().getXorSymbol() != null && aCliCtx.getSyntaxMetrics().getXorSymbol().length() != 0 ? " " + aCliCtx.getSyntaxMetrics().getXorSymbol() + " " : " ";
				}
				theSynopsis += eChild.toSyntax( aCliCtx );
			}
		}
		return theSynopsis;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Visibility in this context means displaying or hiding this {@link Term}'s
	 * existence to the user (defaults to <code>true</code>).
	 */
	@Override
	public XorCondition withVisible( boolean isVisible ) {
		setVisible( isVisible );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static Operand<?>[] removeDuplicates( Operand<?>[] aOperands, Operand<?>[] aDuplicates ) {
		if ( aOperands != null && aDuplicates != null ) {
			final List<Operand<?>> theResult = new ArrayList<>();
			for ( Operand<?> eOperand : aOperands ) {
				theResult.add( eOperand );
			}
			for ( Operand<?> eOperand : aDuplicates ) {
				if ( theResult.contains( eOperand ) ) {
					theResult.remove( eOperand );
				}
			}
			aOperands = theResult.isEmpty() ? null : theResult.toArray( new Operand<?>[theResult.size()] );
		}
		return aOperands;
	}
}
