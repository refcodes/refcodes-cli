// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class ArgsFilterTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testNoneFilter1() {
		final String[] theArgs = { "-Dconsole.width=130", "-a", "-b", "<someB>", "-Dlog.debug=true", "<somePath>" };
		final String[] theExpected = { "-Dconsole.width=130", "-a", "-b", "<someB>", "-Dlog.debug=true", "<somePath>" };
		validate( ArgsFilter.NONE, theArgs, theExpected );
	}

	@Test
	public void testNoneFilter2() {
		final String[] theArgs = { "-Dconsole.width=130\n", "-a", "-b", "<someB>", "-Dlog.debug=true", "<somePath>" };
		final String[] theExpected = { "-Dconsole.width=130\n", "-a", "-b", "<someB>", "-Dlog.debug=true", "<somePath>" };
		validate( ArgsFilter.NONE, theArgs, theExpected );
	}

	@Test
	public void testDFilter1() {
		final String[] theArgs = { "-Dconsole.width=130", "-a", "-b", "<someB>", "-Dlog.debug=true", "<somePath>" };
		final String[] theExpected = { "-a", "-b", "<someB>", "<somePath>" };
		validate( ArgsFilter.D, theArgs, theExpected );
	}

	@Test
	public void testDFilter2() {
		final String[] theArgs = { "-a", "-b", "<someB>", "-Dlog.debug=true", "<somePath>" };
		final String[] theExpected = { "-a", "-b", "<someB>", "<somePath>" };
		validate( ArgsFilter.D, theArgs, theExpected );
	}

	@Test
	public void testDFilter3() {
		final String[] theArgs = { "-a", "-b", "<someB>", "<somePath>" };
		final String[] theExpected = { "-a", "-b", "<someB>", "<somePath>" };
		validate( ArgsFilter.D, theArgs, theExpected );
	}

	@Test
	public void testDFilter4() {
		final String[] theArgs = { "-a", "-b", "<someB>", "--Dlog.debug=true", "<somePath>" };
		final String[] theExpected = { "-a", "-b", "<someB>", "--Dlog.debug=true", "<somePath>" };
		validate( ArgsFilter.D, theArgs, theExpected );
	}

	@Test
	public void testDFilter5() {
		final String[] theArgs = { "-a", "-b", "<someB>", "Dlog.debug=true", "<somePath>" };
		final String[] theExpected = { "-a", "-b", "<someB>", "Dlog.debug=true", "<somePath>" };
		validate( ArgsFilter.D, theArgs, theExpected );
	}

	@Test
	public void testDFilter6() {
		final String[] theArgs = { "-Dconsole.width=130", "-Dconsole.linebreak=\n", "-a", "-b", "<someB>", "-Dlog.debug=true", "<somePath>" };
		final String[] theExpected = { "-a", "-b", "<someB>", "<somePath>" };
		validate( ArgsFilter.D, theArgs, theExpected );
	}

	@Test
	public void testXXFilter1() {
		final String[] theArgs = { "-XX:codecachetotal", "-a", "-b", "<someB>", "-Dlog.debug=true", "<somePath>" };
		final String[] theExpected = { "-a", "-b", "<someB>", "-Dlog.debug=true", "<somePath>" };
		validate( ArgsFilter.XX, theArgs, theExpected );
	}

	@Test
	public void testXXFilter2() {
		final String[] theArgs = { "-XXwhatever", "-a", "-b", "<someB>", "-Dlog.debug=true", "<somePath>" };
		final String[] theExpected = { "-XXwhatever", "-a", "-b", "<someB>", "-Dlog.debug=true", "<somePath>" };
		validate( ArgsFilter.XX, theArgs, theExpected );
	}

	@Test
	public void testDXXFilter1() {
		final String[] theArgs = { "-XX:codecachetotal", "-a", "-b", "<someB>", "-Dlog.debug=true", "<somePath>" };
		final String[] theExpected = { "-a", "-b", "<someB>", "<somePath>" };
		validate( ArgsFilter.D_XX, theArgs, theExpected );
	}

	@Test
	public void testDXXFilter2() {
		final String[] theArgs = { "-XXwhatever", "-XX:codecachetotal", "-a", "-b", "<someB>", "-Dlog.debug=true", "<somePath>" };
		final String[] theExpected = { "-XXwhatever", "-a", "-b", "<someB>", "<somePath>" };
		validate( ArgsFilter.D_XX, theArgs, theExpected );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static void validate( ArgsFilter aArgsFilter, String[] aArgs, String[] aExpected ) {
		final String[] theResult = aArgsFilter.toFiltered( aArgs );
		final List<String> theList = Arrays.asList( aArgs );
		final List<String> theFiltered = aArgsFilter.toFiltered( theList );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Args := " + Arrays.toString( aArgs ) );
			System.out.println( "Expected := " + Arrays.toString( aExpected ) );
			System.out.println( "Result := " + Arrays.toString( theResult ) );
			System.out.println( "Filtered := " + theFiltered );
			System.out.println();
		}
		assertArrayEquals( aExpected, theResult );
		assertArrayEquals( aExpected, theFiltered.toArray( new String[theFiltered.size()] ) );
	}
}
