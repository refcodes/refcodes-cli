// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.cli.CliSugar.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.runtime.Execution;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.struct.Property;
import org.refcodes.struct.PropertyImpl;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.VerboseTextBuilder;

public class ArgsParserTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	// @formatter:off
	private static final String[][] ARGS = new String[][] { 
		{ "--name", "FILE", "--boolean", "--active" }, 
		{ "--name", "FILE", "--boolean", "--alias", "ALIAS" }, 
		{ "--name", "FILE", "--boolean", "--alias", "ALIAS", "--active" }, 
		{ "--name", "FILE", "--boolean", "--alias", "ALIAS", "NULL" }, 
		{ "NULL", "--name", "FILE", "--boolean", "--alias", "ALIAS" }, 
		{ "NULL", "--name", "FILE", "--boolean", "--alias", "ALIAS", "NIL" }, 
		{ "NULL", "--name", "NAME0", "--name", "NAME1", "--boolean", "--alias", "ALIAS", "NIL" }, 
		{ "NULL", "/name", "NAME0", "/name", "NAME1", "/boolean", "/alias", "ALIAS", "NIL" }, 
		{ "NULL", "-name", "NAME0", "-name", "NAME1", "-boolean", "-alias", "ALIAS", "NIL" }, 
		{ "NULL", "/name", "NAME0", "-name", "NAME1", "--boolean", "/alias", "ALIAS", "NIL" }
	};
	private static final String[][] PROPERTIES = new String[][] { 
		{ "name=FILE", "boolean=true", "active=true" }, 
		{ "name=FILE", "boolean=true", "alias=ALIAS" }, 
		{ "name=FILE", "boolean=true", "alias=ALIAS", "active=true" }, 
		{ "name=FILE", "boolean=true", "alias=ALIAS", "null=NULL" }, 
		{ "null=NULL", "name=FILE", "boolean=true", "alias=ALIAS" }, 
		{ "name=FILE", "boolean=true", "alias=ALIAS", "null#0=NULL", "null#1=NIL" }, 
		{ "name#0=NAME0", "name#1=NAME1", "boolean=true", "alias=ALIAS", "null#0=NULL", "null#1=NIL" }, 
		{ "name#0=NAME0", "name#1=NAME1", "boolean=true", "alias=ALIAS", "null#0=NULL", "null#1=NIL" }, 
		{ "name#0=NAME0", "name#1=NAME1", "boolean=true", "alias=ALIAS", "null#0=NULL", "null#1=NIL" }, 
		{ "name#0=NAME0", "name#1=NAME1", "boolean=true", "alias=ALIAS", "null#0=NULL", "null#1=NIL" } 
	};
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testEdgeCase1() throws ArgsSyntaxException {
		final StringOption theInputFileArg = stringOption( 'i', "input-file", "INPUTFILE_PROPERTY", "The input file which to process from." );
		final StringOption theOutputFileArg = stringOption( 'o', "output-file", "OUTPUTFILE_PROPERTY", "The output file which to process to." );
		final Flag theDebugFlag = debugFlag( false );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();
		// @formatter:off
		final Condition theCondition = cases(
			and( theInputFileArg, theOutputFileArg, any( theDebugFlag ) ), 
			xor( theHelpFlag, and( theSysInfoFlag, any ( theVerboseFlag ) ) )
		);	
		// @formatter:on
		final ParseArgs theParser = new ArgsParser( theCondition );
		theParser.evalArgs( new String[] { "-i", "in", "-o", "out" } );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theCondition.toSchema() );
		}
	}

	@Test
	public void testEdgeCase2() throws ArgsSyntaxException {
		final StringOption theInputFileArg = stringOption( 'i', "input-file", "INPUTFILE_PROPERTY", "The input file which to process from." );
		final StringOption theOutputFileArg = stringOption( 'o', "output-file", "OUTPUTFILE_PROPERTY", "The output file which to process to." );
		final Flag theDebugFlag = debugFlag( false );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();
		// @formatter:off
		final Condition theCondition = cases(
			and( theInputFileArg, theOutputFileArg, any( theDebugFlag ) ), 
			xor( theHelpFlag, and( theSysInfoFlag, any ( theVerboseFlag ) ) )
		);	
		// @formatter:on
		final ParseArgs theParser = new ArgsParser( theCondition );
		try {
			theParser.evalArgs( new String[0] );
			fail( "Expected an <" + UnknownArgsException.class.getName() + ">!" );
		}
		catch ( UnknownArgsException expected ) {}
	}

	enum BaseMetricsConfig {
		A, B, C
	}

	@Test
	public void testEdgeCase3() throws ArgsSyntaxException {
		final EnumOption<BaseMetricsConfig> theEncodingArg = enumOption( "encoding", BaseMetricsConfig.class, "ENCODING_PROPERTY", "The BASE (e.g. BASE64) encoding/decoding to be applied for handling encrypted data: " + VerboseTextBuilder.asString( BaseMetricsConfig.values() ) );
		final Flag theBase64Flag = flag( "base64", "BASE64_PROPERTY", "Use BASE64 encoding/decoding to be applied for handling encrypted data." );
		final Flag theDecryptFlag = flag( 'd', "decrypt", "DECRYPT_PROPERTY", "Decrypts the aMessage (stream, file, text or bytes)." );
		final Flag theEncryptFlag = flag( 'e', "encrypt", "ENCRYPT_PROPERTY", "Encrypts the aMessage (stream, file, text or bytes)." );
		final Flag theVerifyFlag = flag( "verify", "VERIFY_PROPERTY", "Verify the encryption process to make sure encryption decrypts flawlessly." );
		final IntOption theLineWidthArg = intOption( "line-width", "LINE_WIDTH_PROPERTY", "The line width for base64 encoded ASCII output." );
		final StringOption theInputFileArg = stringOption( 'i', "input-file", "INPUT_FILE_PROPERTY", "The input file which to process from." );
		final StringOption theOutputFileArg = stringOption( 'o', "output-file", "OUTPUT_FILE_PROPERTY", "The output file which to process to." );
		// @formatter:off
		final Condition theCondition =  
			// I + O
			and(
				xor(
					and( theEncryptFlag, any( theVerifyFlag, xor( and( theBase64Flag, any (theLineWidthArg) ), and( theEncodingArg, any (theLineWidthArg) ) ) ) ), 
					theDecryptFlag  
				),
				any(
					theInputFileArg, theOutputFileArg 
				)
			);
		// @formatter:on
		final String[] args = new String[] { "-e", "-i", "input.dat", "-o", "output.txt" };
		final ParseArgs theParser = new ArgsParser( theCondition );
		theParser.evalArgs( args );
	}

	enum ChaosMode {
		X, Y, Z
	}

	enum SystemContext {
		S0, S1, S2
	}

	@Test
	public void testEdgeCase4() throws ArgsSyntaxException {
		final EnumOption<ChaosMode> theChaosModeArg = enumOption( "chaos-mode", ChaosMode.class, "CHAOS_MODE_PROPERTY", "The mode to be used when encrypting/decrypting: " + VerboseTextBuilder.asString( ChaosMode.values() ) );
		final EnumOption<SystemContext> theContextArg = enumOption( "context", SystemContext.class, "CONTEXT_PROPERTY", "The system context providing the password: " + VerboseTextBuilder.asString( SystemContext.values() ) );
		final Flag thePromptPasswordFlag = flag( "prompt", "PROMPT_PASSWORD_PROPERTY", "Prompt for the password to use for encryption or decryption." );
		final StringOption theCertFileArg = stringOption( 'c', "cert-file", "CERT_FILE_PROPERTY", "The cert file file which to use." );
		final StringOption thePasswordArg = stringOption( 'p', "password", "PASSWORD_PROPERTY", "The password to use for encryption or decryption." );
		// @formatter:off
		final Condition theCondition = cases(
			// T + B
				xor( 
					any( xor( thePromptPasswordFlag, thePasswordArg, theContextArg ), theChaosModeArg ),
					and( theCertFileArg, any (xor( thePromptPasswordFlag, thePasswordArg, theContextArg ) ) ) 
				)
		);
		// @formatter:on
		final String[] args = new String[] { "-p", "Hallo Welt!", "--cert-file", "C" };
		final ParseArgs theParser = new ArgsParser( theCondition );
		theParser.evalArgs( args );
	}

	@Test
	public void testEdgeCase5() throws ArgsSyntaxException {
		final StringOption theEchoOption = stringOption( 'e', "echo", "TEXT_PROPERTY", "Echoes the provided aMessage to the standard out stream." );
		final ConfigOption theConfigOption = configOption();
		final Flag theInitFlag = initFlag();
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag();
		final Flag theHelpFlag = helpFlag();
		final Flag theDebugFlag = debugFlag();
		// @formatter:off
		final Condition theCondition = cases(
			and( theEchoOption, any( theConfigOption, theVerboseFlag, theDebugFlag ) ),
			and( theInitFlag, any( theConfigOption, theVerboseFlag) ),
			xor( theHelpFlag, and( theSysInfoFlag, any( theVerboseFlag) ) )
		);
		// @formatter:on
		final String[] args = new String[] {};
		final ParseArgs theParser = new ArgsParser( theCondition );
		try {
			theParser.evalArgs( args );
			fail( "Expected a <" + UnknownArgsException.class.getSimpleName() + "> exception!" );
		}
		catch ( UnknownArgsException expected ) {}
	}

	@Test
	public void testEdgeCase6() throws ArgsSyntaxException {
		final StringOption theInputFileArg = stringOption( 'i', "input-file", "INPUTFILE_PROPERTY", "The input file which to process from." );
		final StringOption theOutputFileArg = stringOption( 'o', "output-file", "OUTPUTFILE_PROPERTY", "The output file which to process to." );
		final Flag theDebugFlag = debugFlag( false );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();
		// @formatter:off
		final Condition theCondition = cases(
			any(
				theInputFileArg, and( theOutputFileArg, any(  theVerboseFlag ) ), theDebugFlag 
			),
			and(
				theInputFileArg, and(  theDebugFlag, any ( theVerboseFlag ) )
			),
			xor( theHelpFlag, and( theSysInfoFlag, any ( theVerboseFlag ) ) )
		);
		// @formatter:on
		final String[] args = new String[] { "-i", "someInputFile", "-o", "someOutputFile" };
		final ParseArgs theParser = new ArgsParser( theCondition );
		theParser.evalArgs( args );
	}

	@Test
	public void testEdgeCase7() throws ArgsSyntaxException {
		final StringOption theInputFileArg = stringOption( 'i', "input-file", "INPUTFILE_PROPERTY", "The input file which to process from." );
		final StringOption theOutputFileArg = stringOption( 'o', "output-file", "OUTPUTFILE_PROPERTY", "The output file which to process to." );
		final Flag theDebugFlag = debugFlag( false );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();
		// @formatter:off
		final Condition theCondition = cases(
			any(
				theInputFileArg, and( theOutputFileArg, any(  theVerboseFlag ) ), theDebugFlag 
			),
			and(
				theInputFileArg, and(  theDebugFlag, any ( theVerboseFlag ) )
			),
			xor( theHelpFlag, and( theSysInfoFlag, any ( theVerboseFlag ) ) )
		);
		// @formatter:on
		final String[] args = new String[] { "-i", "someInputFile" };
		final ParseArgs theParser = new ArgsParser( theCondition );
		final var theOperands = theParser.evalArgs( args );
		assertEquals( 1, theOperands.length );
		assertEquals( "someInputFile", theInputFileArg.getValue() );
	}

	@Test
	public void testEdgeCase8() {
		final StringProperty theFileNameArg = stringProperty( 'f', "filename", "The kernel's project file" );
		final StringProperty theCsvNameArg = stringProperty( "csv", "The kernel's poulation CSV file" );
		// @formatter:off
		final Term theArgsSyntax = cases(
			and( theFileNameArg, 
				optional( theCsvNameArg ) 
			)
		);
		// @formatter:on
		final ArgsParser theParser = new ArgsParser( theArgsSyntax ).withName( null ).withSyntaxMetrics( SyntaxNotation.LOGICAL ).withEscapeCodesEnabled( false ).withConsoleWidth( 80 ).withBannerFontPalette( AsciiColorPalette.MAX_LEVEL_GRAY ).withLicense( null ).withCopyright( null ).withTitle( "CROWD:IT" ).withDescription( null );
		try {
			theParser.evalArgs( new String[] { "-f=filename", "--csv=csv" } );
		}
		catch ( ArgsSyntaxException e ) {
			theParser.printBody();
			System.out.println( e.toMessage() );
		}
	}

	@Test
	public void testAnyCase1() {
		final Flag theDebugFlag = debugFlag();
		final Flag theVerboseFlag = verboseFlag();
		final Flag theQuietFlag = quietFlag();
		final Condition theRoot = cases( any( theDebugFlag, theVerboseFlag ), theQuietFlag );
		final String[] args = new String[] {};
		final ParseArgs theParser = new ArgsParser( theRoot );
		try {
			theParser.evalArgs( args );
		}
		catch ( ArgsSyntaxException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( e.toMessage() );
				e.printStackTrace();
			}
			fail( e.getMessage() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theDebugFlag.getAlias() + " := " + theDebugFlag.isEnabled() );
			System.out.println( theVerboseFlag.getAlias() + " := " + theVerboseFlag.isEnabled() );
			System.out.println( theQuietFlag.getAlias() + " := " + theQuietFlag.isEnabled() );
		}
		assertFalse( theDebugFlag.isEnabled() );
		assertFalse( theVerboseFlag.isEnabled() );
		assertFalse( theQuietFlag.isEnabled() );
	}

	@Test
	public void testAnyCase2() {
		final Flag theDebugFlag = debugFlag( true );
		final Flag theVerboseFlag = verboseFlag( true );
		final Flag theQuietFlag = quietFlag( true );
		final Condition theRoot = cases( any( theDebugFlag, theVerboseFlag ), theQuietFlag );
		final String[] args = new String[] { "-q" };
		final ParseArgs theParser = new ArgsParser( theRoot );
		try {
			theParser.evalArgs( args );
		}
		catch ( ArgsSyntaxException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( e.toMessage() );
				e.printStackTrace();
			}
			fail( e.getMessage() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theDebugFlag.getAlias() + " := " + theDebugFlag.isEnabled() );
			System.out.println( theVerboseFlag.getAlias() + " := " + theVerboseFlag.isEnabled() );
			System.out.println( theQuietFlag.getAlias() + " := " + theQuietFlag.isEnabled() );
		}
		assertFalse( theDebugFlag.isEnabled() );
		assertFalse( theVerboseFlag.isEnabled() );
		assertTrue( theQuietFlag.isEnabled() );
	}

	@Test
	public void testAnyCase3() {
		final Flag theDebugFlag = debugFlag( true );
		final Flag theVerboseFlag = verboseFlag( true );
		final Flag theQuietFlag = quietFlag( true );
		final Condition theRoot = cases( any( theDebugFlag, theVerboseFlag ), theQuietFlag );
		final String[] args = new String[] { "-d", "-v" };
		final ParseArgs theParser = new ArgsParser( theRoot );
		try {
			theParser.evalArgs( args );
		}
		catch ( ArgsSyntaxException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( e.toMessage() );
				e.printStackTrace();
			}
			fail( e.getMessage() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theDebugFlag.getAlias() + " := " + theDebugFlag.isEnabled() );
			System.out.println( theVerboseFlag.getAlias() + " := " + theVerboseFlag.isEnabled() );
			System.out.println( theQuietFlag.getAlias() + " := " + theQuietFlag.isEnabled() );
		}
		assertTrue( theDebugFlag.isEnabled() );
		assertTrue( theVerboseFlag.isEnabled() );
		assertFalse( theQuietFlag.isEnabled() );
	}

	@Test
	public void testAnyCase4() {
		final Flag theDebugFlag = debugFlag( true );
		final Flag theVerboseFlag = verboseFlag( true );
		final Flag theQuietFlag = quietFlag( true );
		final Condition theRoot = cases( any( theDebugFlag, theVerboseFlag ), theQuietFlag );
		final String[] args = new String[] { "-d" };
		final ParseArgs theParser = new ArgsParser( theRoot );
		try {
			theParser.evalArgs( args );
		}
		catch ( ArgsSyntaxException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( e.toMessage() );
				e.printStackTrace();
			}
			fail( e.getMessage() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theDebugFlag.getAlias() + " := " + theDebugFlag.isEnabled() );
			System.out.println( theVerboseFlag.getAlias() + " := " + theVerboseFlag.isEnabled() );
			System.out.println( theQuietFlag.getAlias() + " := " + theQuietFlag.isEnabled() );
		}
		assertTrue( theDebugFlag.isEnabled() );
		assertFalse( theVerboseFlag.isEnabled() );
		assertFalse( theQuietFlag.isEnabled() );
	}

	@Test
	public void testAnyCase5() throws ArgsSyntaxException {
		final Flag theDebugFlag = debugFlag();
		final Flag theVerboseFlag = verboseFlag();
		final Flag theQuietFlag = quietFlag();
		final Condition theRoot = cases( any( theDebugFlag, theVerboseFlag ), theQuietFlag );
		final String[] args = new String[] { "-q", "-d" };
		final ParseArgs theParser = new ArgsParser( theRoot );
		try {
			theParser.evalArgs( args );
			fail( "Expected a <" + UnknownArgsException.class.getName() + "> exception!" );
		}
		catch ( UnknownArgsException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( e.toMessage() );
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theDebugFlag.getAlias() + " := " + theDebugFlag.isEnabled() );
			System.out.println( theVerboseFlag.getAlias() + " := " + theVerboseFlag.isEnabled() );
			System.out.println( theQuietFlag.getAlias() + " := " + theQuietFlag.isEnabled() );
		}
	}

	@Test
	public void testAnyCase6() throws ArgsSyntaxException {
		final Flag theDebugFlag = debugFlag();
		final Flag theVerboseFlag = verboseFlag();
		final Flag theQuietFlag = quietFlag();
		final Condition theRoot = cases( any( theDebugFlag, theVerboseFlag ), theQuietFlag );
		final String[] args = new String[] { "-v", "-q" };
		final ParseArgs theParser = new ArgsParser( theRoot );
		try {
			theParser.evalArgs( args );
			fail( "Expected a <" + UnknownArgsException.class.getName() + "> exception!" );
		}
		catch ( UnknownArgsException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( e.toMessage() );
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theDebugFlag.getAlias() + " := " + theDebugFlag.isEnabled() );
			System.out.println( theVerboseFlag.getAlias() + " := " + theVerboseFlag.isEnabled() );
			System.out.println( theQuietFlag.getAlias() + " := " + theQuietFlag.isEnabled() );
		}
	}

	@Test
	public void testAnyCase7() throws ArgsSyntaxException {
		final Flag theDebugFlag = debugFlag();
		final Flag theVerboseFlag = verboseFlag();
		final Flag theQuietFlag = quietFlag();
		final Condition theRoot = cases( any( theDebugFlag, theVerboseFlag ), theQuietFlag );
		final String[] args = new String[] { "-d", "-v", "-q" };
		final ParseArgs theParser = new ArgsParser( theRoot );
		try {
			theParser.evalArgs( args );
			fail( "Expected a <" + UnknownArgsException.class.getName() + "> exception!" );
		}
		catch ( UnknownArgsException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( e.toMessage() );
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theDebugFlag.getAlias() + " := " + theDebugFlag.isEnabled() );
			System.out.println( theVerboseFlag.getAlias() + " := " + theVerboseFlag.isEnabled() );
			System.out.println( theQuietFlag.getAlias() + " := " + theQuietFlag.isEnabled() );
		}
	}

	@Test
	public void testAnyXor1() throws ArgsSyntaxException {
		final Flag theDebugFlag = debugFlag( true );
		final Flag theVerboseFlag = verboseFlag( true );
		final Flag theQuietFlag = quietFlag( true );
		final Condition theRoot = xor( any( theDebugFlag, theVerboseFlag ), theQuietFlag );
		final String[] args = new String[] { "-q", "-d" };
		final ParseArgs theParser = new ArgsParser( theRoot );
		try {
			theParser.evalArgs( args );
			fail( "Expected a <" + AmbiguousArgsException.class.getName() + "> exception!" );
		}
		catch ( AmbiguousArgsException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( e.toMessage() );
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theDebugFlag.getAlias() + " := " + theDebugFlag.isEnabled() );
			System.out.println( theVerboseFlag.getAlias() + " := " + theVerboseFlag.isEnabled() );
			System.out.println( theQuietFlag.getAlias() + " := " + theQuietFlag.isEnabled() );
		}
	}

	@Test
	public void testParser1() throws ArgsSyntaxException {
		final Option<String> theFile = new StringOption( 'f', "file", "file", "The file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Condition theXor = new XorCondition( theAdd, theDelete );
		final Condition theAnd = new AndCondition( theXor, theFile );
		final String[] args = new String[] { "-f", "someFile", "-a" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertTrue( theAdd.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParser2() throws ArgsSyntaxException {
		final Option<String> theFile = new StringOption( 'f', "file", "file", "The file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Condition theXor = new XorCondition( theAdd, theDelete );
		final Term theOptional = new AnyCondition( theXor );
		final Condition theAnd = new AndCondition( theOptional, theFile );
		final String[] args = new String[] { "-f", "someFile", "-d" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theAnd.toSynopsis() );
		}

		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertFalse( theAdd.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParser3() throws ArgsSyntaxException {
		final Option<String> theFile = new StringOption( 'f', "file", "file", "The file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed" );
		final Condition theXor = new XorCondition( theAdd, theDelete );
		final Condition theAnd = new AndCondition( theXor, theFile, theOperand );
		final String[] args = new String[] { "-f", "someFile", "-a", "anOperand" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertTrue( theAdd.getValue() );
		assertEquals( "anOperand", theOperand.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParser4() throws ArgsSyntaxException {
		final Option<String> theFile = new StringOption( 'f', "file", "file", "The file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed" );
		final Condition theXor = new XorCondition( theAdd, theDelete );
		final Condition theAnd = new AndCondition( theXor, theFile, theOperand );
		final String[] args = new String[] { "-f", "someFile", "-a" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		try {
			theParseArgs.evalArgs( args );
			// theArgsParser.evalArgs( args, ArgsFilter.D_XX ).
			fail( "Expecting an exception" );
		}
		catch ( ArgsSyntaxException e ) {}
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParser5() throws ArgsSyntaxException {
		final Option<String> theFile = new StringOption( 'f', "file", "file", "The file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed" );
		final Condition theXor = new XorCondition( theAdd, theDelete );
		final Condition theAnd = new AndCondition( theXor, theFile, theOperand );
		final String[] args = new String[] { "-f", "someFile", "-a", "anOperand", "isSuperfluous" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		try {
			theParseArgs.evalArgs( args );
			// theArgsParser.evalArgs( args, ArgsFilter.D_XX ).
			fail( "Expecting an exception" );
		}
		catch ( SuperfluousArgsException e ) {
			assertEquals( 1, e.getArgs().length );
			assertEquals( "isSuperfluous", e.getArgs()[0] );
		}
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParser6() throws ArgsSyntaxException {
		final Option<String> theFromFile = new StringOption( 'f', "from", "from_file", "The source file to be processed" );
		final Option<String> theToFile = new StringOption( 't', "to", "to_file", "The destination file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed" );
		final Condition theXor = new XorCondition( theAdd, theDelete );
		final Condition theAnd = new AndCondition( theXor, theFromFile, theToFile, theOperand );
		final String[] args = new String[] { "-f", "fromFile", "-t", "toFile", "-a", "anOperand" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertTrue( theAdd.getValue() );
		assertEquals( "anOperand", theOperand.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParser7() throws ArgsSyntaxException {
		final Option<String> theFromFile = new StringOption( 'f', "from", "from_file", "The source file to be processed" );
		final Option<String> theToFile = new StringOption( 't', "to", "to_file", "The destination file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed" );
		final Condition theXor = new XorCondition( theAdd, theDelete );
		final Condition theAnd = new AndCondition( theXor, theFromFile, theToFile, theOperand );
		final String[] args = new String[] { "-t", "toFile", "anOperand", "-f", "fromFile", "-a", };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertTrue( theAdd.getValue() );
		assertEquals( "anOperand", theOperand.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParser8() {
		final Option<String> theFromFile = new StringOption( 'f', "from", "from_file", "The source file to be processed" );
		final Option<String> theToFile = new StringOption( 't', "to", "to_file", "The destination file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed" );
		final Condition theXor = new XorCondition( theDelete, theAdd );
		final Condition theAnd = new AndCondition( theOperand, theXor, theFromFile, theToFile );
		final Operand<?>[] theOperands = theAnd.toOperands();
		doLogOperands( theOperands );
	}

	@Test
	public void testParser9() throws ArgsSyntaxException {
		final Option<String> theFromFile = new StringOption( 'f', "from", "from_file", "The source file to be processed, please provide a relative path as absolute paths are not permitted because absolute pahts may prevent path jail braking." );
		final Option<String> theToFile = new StringOption( 't', "to", "to_file", "The destination file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theCreate = new Flag( 'c', null, null, "Creates the specified file" );
		final Condition theOrArgsSyntax = new OrCondition( theAdd, theCreate );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed, it can be a complex artihmetic expression or a plain placeholder representing an environment variable." );
		final Condition theXor = new XorCondition( theOrArgsSyntax, theDelete );
		final Condition theAnd = new AndCondition( theXor, theFromFile, theToFile, theOperand );
		final String[] args = new String[] { "-f", "fromFile", "-t", "toFile", "-a", "anOperand" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertTrue( theAdd.getValue() );
		assertEquals( "anOperand", theOperand.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParser10() throws ArgsSyntaxException {
		final BooleanOption theBool1 = new BooleanOption( "bool1", "The 1st boolean" );
		final BooleanOption theBool2 = new BooleanOption( "bool2", "The 2nd boolean" );
		final BooleanOption theBool3 = new BooleanOption( "bool3", "The 3rd boolean" );
		final BooleanOption theBool4 = new BooleanOption( "bool4", "The 4th boolean" );
		final AndCondition theAnd = new AndCondition( theBool1, theBool2, theBool3, theBool4 );
		final String[] args = new String[] { "--bool1", "true", "--bool2", "on", "--bool3", "yes", "--bool4", "1" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertTrue( theBool1.getValue() );
		assertTrue( theBool2.getValue() );
		assertTrue( theBool3.getValue() );
		assertTrue( theBool4.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParser11() throws ArgsSyntaxException {
		final BooleanOption theBool1 = booleanOption( "bool1", "The 1st boolean" );
		final BooleanOption theBool2 = booleanOption( "bool2", "The 2nd boolean" );
		final BooleanOption theBool3 = booleanOption( "bool3", "The 3rd boolean" );
		final BooleanOption theBool4 = booleanOption( "bool4", "The 4th boolean" );
		final AndCondition theAnd = and( theBool1, theBool2, theBool3, theBool4 );
		final String[] args = new String[] { "--bool1", "false", "--bool2", "off", "--bool3", "no", "--bool4", "0" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertFalse( theBool1.getValue() );
		assertFalse( theBool2.getValue() );
		assertFalse( theBool3.getValue() );
		assertFalse( theBool4.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParserWithProperties1() throws ArgsSyntaxException {
		final Option<String> theFile = new StringProperty( 'f', "file", "file", "The file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Condition theXor = new XorCondition( theAdd, theDelete );
		final Condition theAnd = new AndCondition( theXor, theFile );
		final String[] args = new String[] { "-f=someFile", "-a" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertTrue( theAdd.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParserWithProperties2() throws ArgsSyntaxException {
		final Option<String> theFile = new StringProperty( 'f', "file", "file", "The file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Condition theXor = new XorCondition( theAdd, theDelete );
		final Term theOptional = new AnyCondition( theXor );
		final Condition theAnd = new AndCondition( theOptional, theFile );
		final String[] args = new String[] { "-f=someFile", "-d" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theAnd.toSynopsis() );
		}

		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertFalse( theAdd.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParserWithProperties3() throws ArgsSyntaxException {
		final Option<String> theFile = new StringProperty( 'f', "file", "file", "The file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed" );
		final Condition theXor = new XorCondition( theAdd, theDelete );
		final Condition theAnd = new AndCondition( theXor, theFile, theOperand );
		final String[] args = new String[] { "-f=someFile", "-a", "anOperand" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertTrue( theAdd.getValue() );
		assertEquals( "anOperand", theOperand.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParserWithProperties4() throws ArgsSyntaxException {
		final Option<String> theFile = new StringProperty( 'f', "file", "file", "The file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed" );
		final Condition theXor = new XorCondition( theAdd, theDelete );
		final Condition theAnd = new AndCondition( theXor, theFile, theOperand );
		final String[] args = new String[] { "-f=someFile", "-a" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		try {
			theParseArgs.evalArgs( args );
			// theArgsParser.evalArgs( args, ArgsFilter.D_XX ).
			fail( "Expecting an exception" );
		}
		catch ( ArgsSyntaxException e ) {}
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParserWithProperties5() throws ArgsSyntaxException {
		final Option<String> theFile = new StringProperty( 'f', "file", "file", "The file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed" );
		final Condition theXor = new XorCondition( theAdd, theDelete );
		final Condition theAnd = new AndCondition( theXor, theFile, theOperand );
		final String[] args = new String[] { "-f=someFile", "-a", "anOperand", "isSuperfluous" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		try {
			theParseArgs.evalArgs( args );
			// theArgsParser.evalArgs( args, ArgsFilter.D_XX ).
			fail( "Expecting an exception" );
		}
		catch ( SuperfluousArgsException e ) {
			assertEquals( 1, e.getArgs().length );
			assertEquals( "isSuperfluous", e.getArgs()[0] );
		}
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParserWithProperties6() throws ArgsSyntaxException {
		final Option<String> theFromFile = new StringProperty( 'f', "from", "from_file", "The source file to be processed" );
		final Option<String> theToFile = new StringProperty( 't', "to", "to_file", "The destination file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed" );
		final Condition theXor = new XorCondition( theAdd, theDelete );
		final Condition theAnd = new AndCondition( theXor, theFromFile, theToFile, theOperand );
		final String[] args = new String[] { "-f=fromFile", "-t=toFile", "-a", "anOperand" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertTrue( theAdd.getValue() );
		assertEquals( "anOperand", theOperand.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParserWithProperties7() throws ArgsSyntaxException {
		final Option<String> theFromFile = new StringProperty( 'f', "from", "from_file", "The source file to be processed" );
		final Option<String> theToFile = new StringProperty( 't', "to", "to_file", "The destination file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed" );
		final Condition theXor = new XorCondition( theAdd, theDelete );
		final Condition theAnd = new AndCondition( theXor, theFromFile, theToFile, theOperand );
		final String[] args = new String[] { "-t=toFile", "anOperand", "-f=fromFile", "-a", };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertTrue( theAdd.getValue() );
		assertEquals( "anOperand", theOperand.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParserWithProperties8() {
		final Option<String> theFromFile = new StringProperty( 'f', "from", "from_file", "The source file to be processed" );
		final Option<String> theToFile = new StringProperty( 't', "to", "to_file", "The destination file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed" );
		final Condition theXor = new XorCondition( theDelete, theAdd );
		final Condition theAnd = new AndCondition( theOperand, theXor, theFromFile, theToFile );
		final Operand<?>[] theOperands = theAnd.toOperands();
		doLogOperands( theOperands );
	}

	@Test
	public void testParserWithProperties9() throws ArgsSyntaxException {
		final Option<String> theFromFile = new StringProperty( 'f', "from", "from_file", "The source file to be processed, please provide a relative path as absolute paths are not permitted because absolute pahts may prevent path jail braking." );
		final Option<String> theToFile = new StringProperty( 't', "to", "to_file", "The destination file to be processed" );
		final Flag theAdd = new Flag( 'a', null, null, "Add the specified file" );
		final Flag theCreate = new Flag( 'c', null, null, "Creates the specified file" );
		final Condition theOrArgsSyntax = new OrCondition( theAdd, theCreate );
		final Flag theDelete = new Flag( 'd', null, null, "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed, it can be a complex artihmetic expression or a plain placeholder representing an environment variable." );
		final Condition theXor = new XorCondition( theOrArgsSyntax, theDelete );
		final Condition theAnd = new AndCondition( theXor, theFromFile, theToFile, theOperand );
		final String[] args = new String[] { "-f=fromFile", "-t=toFile", "-a", "anOperand" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertTrue( theAdd.getValue() );
		assertEquals( "anOperand", theOperand.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParserWithProperties10() throws ArgsSyntaxException {
		final BooleanProperty theBool1 = new BooleanProperty( "bool1", "The 1st boolean" );
		final BooleanProperty theBool2 = new BooleanProperty( "bool2", "The 2nd boolean" );
		final BooleanProperty theBool3 = new BooleanProperty( "bool3", "The 3rd boolean" );
		final BooleanProperty theBool4 = new BooleanProperty( "bool4", "The 4th boolean" );
		final AndCondition theAnd = new AndCondition( theBool1, theBool2, theBool3, theBool4 );
		final String[] args = new String[] { "--bool1=true", "--bool2=on", "--bool3=yes", "--bool4=1" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertTrue( theBool1.getValue() );
		assertTrue( theBool2.getValue() );
		assertTrue( theBool3.getValue() );
		assertTrue( theBool4.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testParserWithProperties11() throws ArgsSyntaxException {
		final BooleanProperty theBool1 = booleanProperty( "bool1", "The 1st boolean" );
		final BooleanProperty theBool2 = booleanProperty( "bool2", "The 2nd boolean" );
		final BooleanProperty theBool3 = booleanProperty( "bool3", "The 3rd boolean" );
		final BooleanProperty theBool4 = booleanProperty( "bool4", "The 4th boolean" );
		final AndCondition theAnd = and( theBool1, theBool2, theBool3, theBool4 );
		final String[] args = new String[] { "--bool1=false", "--bool2=off", "--bool3=no", "--bool4=0" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		@SuppressWarnings("unused")
		final Operand<?>[] theResult = theParseArgs.evalArgs( args );
		assertFalse( theBool1.getValue() );
		assertFalse( theBool2.getValue() );
		assertFalse( theBool3.getValue() );
		assertFalse( theBool4.getValue() );
		doLogArgs( theParseArgs, args );
	}

	@Test
	public void testAliasSyntaxTreeLookup() throws ArgsSyntaxException {
		final Option<String> theFromFile = new StringOption( 'f', "from", "from", "The source file to be processed, please provide a relative path as absolute paths are not permitted because absolute pahts may prevent path jail braking." );
		final Option<String> theToFile = new StringOption( 't', "to", "to", "The destination file to be processed" );
		final Flag theAddFile = new Flag( 'a', "--add", "add", "Add the specified file" );
		final Flag theCreateFile = new Flag( 'c', "create", "create", "Creates the specified file" );
		final Condition theOrArgsSyntax = new OrCondition( theAddFile, theCreateFile );
		final Flag theDeleteFile = new Flag( 'd', "delete", "delete", "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed, it can be a complex artihmetic expression or a plain placeholder representing an environment variable." );
		final Condition theXor = new XorCondition( theOrArgsSyntax, theDeleteFile );
		final Condition theAnd = new AndCondition( theXor, theFromFile, theToFile, theOperand );
		final String[] args = new String[] { "-f", "fromFile", "-t", "toFile", "-a", "anOperand" };
		final ParseArgs theParseArgs = new ArgsParser( theAnd );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theAnd.toSynopsis() );
		}
		theParseArgs.evalArgs( args );
		final String theFrom = theAnd.toValue( "from" );
		final String theTo = theAnd.toValue( "to" );
		final boolean theAdd = theAnd.toValue( "add" );
		final boolean theCreate = theAnd.toValue( "create" );
		final boolean theDelete = theAnd.toValue( "delete" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "From := " + theFrom );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "To := " + theTo );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Add := " + theAdd );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Create := " + theCreate );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Delete := " + theDelete );
		}
		assertEquals( "fromFile", theFrom );
		assertEquals( "toFile", theTo );
		assertTrue( theAdd );
		assertFalse( theCreate );
		assertFalse( theDelete );
	}

	@Test
	public void testArgsProperties() throws ArgsSyntaxException {
		for ( int i = 0; i < ARGS.length; i++ ) {
			testArgs( i );
		}
	}

	enum TrigonometricFunction {
		SINE, COSINE
	}

	@Test
	public void testProductionError() throws ArgsSyntaxException {
		final String[] args = { "-l", "1", "-s", "100", "-f", "1" };
		final Option<Double> theFrequencyOpt = doubleOption( 'f', "frequency", "FREQUENCY_HZ", "The frequency (Hz) to use when generating the values." );
		final Option<Integer> theSamplingRateOpt = intOption( 's', "sampling-rate", "SAMPLING_RATE", "The sample rate (per second) for the generated values (defaults to DEFAULT_SAMPLING_RATE_PER_SEC samples/second)." );
		final Option<Double> theLengthOpt = doubleOption( 'l', "length", "LENGTH_SEC", "The length (in seconds) for the generated values (defaults to " + "DFAULT_LENGTH_SEC" + " seconds)." );
		// @formatter:off
		final Condition theRoot = and( theFrequencyOpt, any( theLengthOpt, theSamplingRateOpt ) );
		// @formatter:on
		final ParseArgs theParseArgs = new ArgsParser( theRoot );
		theParseArgs.withSyntaxMetrics( SyntaxNotation.LOGICAL );
		theParseArgs.withName( "Waves" ).withTitle( "~waves~" ).withCopyright( "Copyright (c) by FUNCODES.CLUB, Munich, Germany." ).withLicense( "Licensed under GNU General Public License, v3.0 and Apache License, v2.0" );
		theParseArgs.withBannerFont( new Font( FontFamily.DIALOG, FontStyle.BOLD ) ).withBannerFontPalette( AsciiColorPalette.MAX_LEVEL_GRAY.getPalette() );
		theParseArgs.withDescription( "Generate (sound) wave tables for given frequencies and amplitudes. Export them as CSV for further processing. Makes heavy use of the  REFCODES.ORG artifacts found together with the FUNCODES.CLUB sources at <http://bitbucket.org/refcodes>." );
		theParseArgs.evalArgs( args );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theRoot.toSchema() );
		}
	}

	@Test
	public void testXorEdgeCase1() throws ArgsSyntaxException {
		final StringOption theTextArg = stringOption( 't', "text", "TEXT_PROPERTY", "The text aMessage which to process." );
		final Flag theEncryptFlag = flag( 'e', "encrypt", "ENCRYPT_PROPERTY", "Encrypts the given aMessage." );
		final Condition theCondition = xor( and( theEncryptFlag, theTextArg ), new AllCondition( theEncryptFlag ) );
		final String[] args = { "-t", "text", "-e" };
		final ParseArgs theParseArgs = new ArgsParser( theCondition );
		theParseArgs.withSyntaxMetrics( SyntaxNotation.LOGICAL );
		theParseArgs.evalArgs( args );
		assertEquals( "text", theTextArg.getValue() );
	}

	@Test
	public void testXorEdgeCase2() throws ArgsSyntaxException {
		final StringOption theTextArg = stringOption( 't', "text", "TEXT_PROPERTY", "The text aMessage which to process." );
		final Flag theEncryptFlag = flag( 'e', "encrypt", "ENCRYPT_PROPERTY", "Encrypts the given aMessage." );
		final Condition theCondition = cases( and( theEncryptFlag, theTextArg ), theEncryptFlag );
		final String[] args = { "-t", "text", "-e" };
		final ParseArgs theParseArgs = new ArgsParser( theCondition );
		theParseArgs.withSyntaxMetrics( SyntaxNotation.LOGICAL );
		theParseArgs.evalArgs( args );
		assertEquals( "text", theTextArg.getValue() );
	}

	@Test
	public void testArrayOption1() throws ArgsSyntaxException {
		final String[] args = { "-t", "text1", "-t", "text2", "-t", "text3" };
		final ArrayOption<String> theArray = new ArrayOption<>( new StringOption( 't', "text", "text", "A text" ), 1, 3 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theArray.toSynopsis() );
		}
		final ParseArgs theParseArgs = new ArgsParser( theArray );
		theParseArgs.withSyntaxMetrics( SyntaxNotation.LOGICAL );
		theParseArgs.evalArgs( args );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theArray.getValue() ) );
		}
		for ( int i = 0; i < theArray.getValue().length; i++ ) {
			assertEquals( args[i * 2 + 1], theArray.getValue()[i] );
		}
	}

	@Test
	public void testArrayOption2() throws ArgsSyntaxException {
		final String[] args = { "-t", "text1", "-t", "text2" };
		final ArrayOption<String> theArray = new ArrayOption<>( new StringOption( 't', "text", "text", "A text" ), 3 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theArray.toSynopsis() );
		}
		final ParseArgs theParseArgs = new ArgsParser( theArray );
		theParseArgs.withSyntaxMetrics( SyntaxNotation.LOGICAL );
		try {
			theParseArgs.evalArgs( args );
			// theArgsParser.evalArgs( args, ArgsFilter.D_XX ).
			fail( "Expected an <UnknownArgsException>!" );
		}
		catch ( UnknownArgsException expected ) {
			// Expected!
		}
	}

	@Test
	public void testArrayOption3() throws ArgsSyntaxException {
		final String[] args = { "-t", "text1", "-t", "text2", "-t", "text3", "-t", "text4" };
		final ArrayOption<String> theArray = new ArrayOption<>( new StringOption( 't', "text", "text", "A text" ), 1, 3 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theArray.toSynopsis() );
		}
		final ParseArgs theParseArgs = new ArgsParser( theArray );
		theParseArgs.withSyntaxMetrics( SyntaxNotation.LOGICAL );
		try {
			theParseArgs.evalArgs( args );
			// theArgsParser.evalArgs( args, ArgsFilter.D_XX ).
			fail( "Expected an <SuperfluousArgsException>!" );
		}
		catch ( SuperfluousArgsException expected ) {
			// Expected!
		}
	}

	@Test
	public void testArrayOption4() throws ArgsSyntaxException {
		final String[] args = {};
		final ArrayOption<String> theArray = new ArrayOption<>( new StringOption( 't', "text", "text", "A text" ), 1, 3 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theArray.toSynopsis() );
		}
		final ParseArgs theParseArgs = new ArgsParser( theArray );
		theParseArgs.withSyntaxMetrics( SyntaxNotation.LOGICAL );
		try {
			theParseArgs.evalArgs( args );
			// theArgsParser.evalArgs( args, ArgsFilter.D_XX ).
			fail( "Expected an <UnknownArgsException>!" );
		}
		catch ( UnknownArgsException expected ) {
			// Expected!
		}
	}

	@Test
	public void testArrayOption5() throws ArgsSyntaxException {
		final String[] args = {};
		final ArrayOption<String> theArray = new ArrayOption<>( new StringOption( 't', "text", "text", "A text" ) );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theArray.toSynopsis() );
		}
		final ParseArgs theParseArgs = new ArgsParser( new AnyCondition( theArray ) );
		theParseArgs.withSyntaxMetrics( SyntaxNotation.LOGICAL );
		theParseArgs.evalArgs( args );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theArray.getValue() ) );
		}
		assertFalse( theArray.hasValue() );
		assertNull( theArray.getValue() );
	}

	@Test
	public void testArrayOption6() throws ArgsSyntaxException {
		final String[] args = { "-t", "text1" };
		final ArrayOption<String> theArray = new ArrayOption<>( new StringOption( 't', "text", "text", "A text" ) );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theArray.toSynopsis() );
		}
		final ParseArgs theParseArgs = new ArgsParser( theArray );
		theParseArgs.withSyntaxMetrics( SyntaxNotation.LOGICAL );
		theParseArgs.evalArgs( args );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theArray.getValue() ) );
		}
		for ( int i = 0; i < theArray.getValue().length; i++ ) {
			assertEquals( args[i * 2 + 1], theArray.getValue()[i] );
		}
	}

	@Test
	public void testArrayOption7() throws ArgsSyntaxException {
		final String[] args = { "-t", "text1", "-t", "text2", "-t", "text3" };
		final ArrayOption<String> theArray = new ArrayOption<>( new StringOption( 't', "text", "text", "A text" ) );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theArray.toSynopsis() );
		}
		final ParseArgs theParseArgs = new ArgsParser( new AnyCondition( theArray ) );
		theParseArgs.withSyntaxMetrics( SyntaxNotation.LOGICAL );
		theParseArgs.evalArgs( args );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Arrays.toString( theArray.getValue() ) );
		}
		for ( int i = 0; i < theArray.getValue().length; i++ ) {
			assertEquals( args[i * 2 + 1], theArray.getValue()[i] );
		}
		assertEquals( args.length / 2, theArray.getValue().length );
	}

	@Test
	public void testOperation1() throws ArgsSyntaxException {
		final Operation ls = new Operation( "ls", "List all entries" );
		final Operation cd = new Operation( "cd", "Change direcory" );
		final Flag verbose = new VerboseFlag();
		final Condition theRoot = new XorCondition( new AndCondition( ls, verbose ), cd );
		final ParseArgs theParseArgs = new ArgsParser( theRoot );
		String[] theArgs = { "ls", "--verbose" };
		theParseArgs.evalArgs( theArgs );
		assertTrue( ls.isEnabled() );
		assertTrue( verbose.isEnabled() );
		assertFalse( cd.isEnabled() );
		theRoot.reset();
		theArgs = new String[] { "cd" };
		theParseArgs.evalArgs( theArgs );
		assertFalse( ls.isEnabled() );
		assertFalse( verbose.isEnabled() );
		assertTrue( cd.isEnabled() );
	}

	@Test
	public void testNone1() throws ArgsSyntaxException {
		final Operation ls = new Operation( "ls", "List all entries" );
		final Operation cd = new Operation( "cd", "Change direcory" );
		final NoneOperand none = new NoneOperand( "none", "No arguments at all" );
		final Flag verbose = new VerboseFlag( true );
		final Condition theRoot = new XorCondition( none, new AndCondition( ls, verbose ), cd );
		final ParseArgs theParseArgs = new ArgsParser( theRoot );
		String[] theArgs = { "ls", "--verbose" };
		theParseArgs.evalArgs( theArgs );
		assertTrue( ls.isEnabled() );
		assertTrue( verbose.isEnabled() );
		assertFalse( cd.isEnabled() );
		theRoot.reset();
		theArgs = new String[] { "cd" };
		theParseArgs.evalArgs( theArgs );
		assertFalse( ls.isEnabled() );
		assertFalse( verbose.isEnabled() );
		assertTrue( cd.isEnabled() );
		assertFalse( none.isEnabled() );
	}

	@Test
	public void testNone2() throws ArgsSyntaxException {
		final Operation ls = new Operation( "ls", "List all entries" );
		final Operation cd = new Operation( "cd", "Change direcory" );
		final NoneOperand none = new NoneOperand( "none", "No arguments at all" );
		final Flag verbose = new VerboseFlag();
		final Condition theRoot = new XorCondition( none, new AndCondition( ls, verbose ), cd );
		final String[] theArgs = {};
		final ParseArgs theParseArgs = new ArgsParser( theRoot );
		theParseArgs.evalArgs( theArgs );
		assertFalse( ls.isEnabled() );
		assertFalse( verbose.isEnabled() );
		assertFalse( cd.isEnabled() );
		assertTrue( none.isEnabled() );
	}

	@Test
	public void testNone3() throws ArgsSyntaxException {
		final boolean isConsumed[] = { false };
		final NoneOperand theNone = none( "Some Description", o -> { isConsumed[0] = true; } );
		final Flag theHelp = helpFlag();
		final Flag theSysInfo = sysInfoFlag();
		final Term theRoot = cases( theSysInfo, xor( theNone, theHelp ) );
		final ArgsParser theParser = new ArgsParser( theRoot );
		theParser.evalArgs( new String[] { "--help" } );
		assertFalse( theNone.isEnabled() );
		assertFalse( isConsumed[0] );
		assertTrue( theHelp.isEnabled() );
	}

	@Test
	public void testNone4() throws ArgsSyntaxException {
		final boolean isConsumed[] = { false };
		final NoneOperand theNone = none( "Some Description", o -> { isConsumed[0] = true; } );
		final Flag theHelp = helpFlag();
		final Flag theSysInfo = sysInfoFlag();
		final Term theRoot = cases( theSysInfo, xor( theNone, theHelp ) );
		final ArgsParser theParser = new ArgsParser( theRoot );
		theParser.evalArgs( new String[] {} );
		assertTrue( theNone.isEnabled() );
		assertTrue( isConsumed[0] );
		assertFalse( theHelp.isEnabled() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void testArgs( int i ) throws ArgsSyntaxException {
		final ParseArgs theParseArgs = new ArgsParser();
		final Map<String, String> theProperties = new HashMap<>();
		final String[] theArgs = ARGS[i];
		final Operand<?>[] theOperands = theParseArgs.evalArgs( theArgs );
		for ( Operand<?> eOperand : theOperands ) {
			theProperties.put( eOperand.getAlias(), "" + eOperand.getValue() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Line <" + i + ">:" );
		}
		for ( String eKey : theProperties.keySet() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eKey + " := " + theProperties.get( eKey ) );
			}
		}
		final String[] theExcepcted = PROPERTIES[i];
		for ( String aTheExcepcted : theExcepcted ) {
			Property eProperty = new PropertyImpl( aTheExcepcted );
			if ( "null".equals( eProperty.getKey() ) ) {
				eProperty = new PropertyImpl( null, eProperty.getValue() );
			}
			assertEquals( eProperty.getValue(), theProperties.get( eProperty.getKey() ), "Line <" + i + "> (" + eProperty.toString() + ")" );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "--------------------------------------------------------------------------------" );
		}
	}

	private void doLogArgs( ParseArgs aParseArgs, String[] aArgs ) {
		final Term theArgsSyntax = aParseArgs.getArgsSyntax();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Execution.getCallerStackTraceElement().getMethodName() );
		}
		aParseArgs.withSyntaxMetrics( SyntaxNotation.GNU_POSIX ).printHelp();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( SyntaxNotation eNotation : SyntaxNotation.values() ) {
				System.out.println( "[" + eNotation + " SYNTAX NOTATION]  " + theArgsSyntax.toSynopsis( eNotation ) );
			}
			System.out.println( "[COMMAND LINE ARGS] " + new VerboseTextBuilder().withElements( aArgs ).toString() );
			System.out.println( "[INTERNAL STATUS] " + theArgsSyntax.toString() );
			System.out.println( "--------------------------------------------------------------------------------" );
		}
	}

	private void doLogOperands( Operand<?>[] aOperands ) {
		Arrays.sort( aOperands );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( Execution.getCallerStackTraceElement().getMethodName() );
		}
		for ( Operand<?> eOperand : aOperands ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "[OPERAND] " + eOperand.toSyntax( SyntaxNotation.LOGICAL ) );
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "--------------------------------------------------------------------------------" );
		}
	}
}
