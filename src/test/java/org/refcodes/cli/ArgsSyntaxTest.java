// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.cli.CliSugar.*;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.refcodes.cli.ArgsParserTest.BaseMetricsConfig;
import org.refcodes.cli.ArgsParserTest.ChaosMode;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.runtime.SystemContext;
import org.refcodes.textual.VerboseTextBuilder;

public class ArgsSyntaxTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testConditionsByArgs() {
		final String[] theArgs = new String[] { "--string1=someString", "--email=someEmail", "--token=1234567890" };
		final ArgsParser theParser = new ArgsParser();
		final Flag theHelpFlag = flag( 'h', "help", "Shows the help ..." );
		final Flag theVersionFlag = flag( 'v', "version", "Shows the version ..." );
		final StringProperty theString1Arg = stringProperty( "string1", "..." );
		final StringProperty theString2Arg = stringProperty( "string2", "..." );
		final BooleanProperty theboolArg = booleanProperty( "bool", "..." );
		final Flag theBoolFlag = flag( "bool", "..." );
		final StringProperty theEMailArg = stringProperty( "email", "..." );
		final StringProperty theNameArg = stringProperty( "name", "..." );
		final StringProperty theTokenArg = stringProperty( "token", "..." );
		final IntProperty theIntArg = intProperty( "int", "..." );
		final DoubleProperty theDoubleArg = doubleProperty( "double", "..." );
		final AndCondition theAnd;
		// @formatter:off
		final Term theArgsSyntax = cases(
			and( theString1Arg, 
				optional( theString2Arg, 
					xor( theboolArg, theBoolFlag, 
						theAnd = and( theEMailArg, theNameArg, theTokenArg ) 
					),
					theIntArg, theDoubleArg
				)
			),
			theHelpFlag,
			theVersionFlag
		);
		// @formatter:on
		theParser.withArgsSyntax( theArgsSyntax ).withName( null ).withSyntaxMetrics( SyntaxNotation.LOGICAL ).withEscapeCodesEnabled( false ).withConsoleWidth( 80 ).withBannerFontPalette( AsciiColorPalette.MAX_LEVEL_GRAY ).withLicense( null ).withCopyright( null ).withTitle( "CROWD:IT" ).withDescription( null );
		try {
			theParser.evalArgs( theArgs );
			fail( "Expecting an <" + UnknownArgsException.class.getSimpleName() + ">!" );
		}
		catch ( ArgsSyntaxException e ) {
			final Condition[] theParents = ( (Condition) theParser.getArgsSyntax() ).toConditions( e.getArgs() );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theParents[0] );
			}
			assertEquals( 1, theParents.length );
			assertEquals( theAnd, theParents[0] );
		}
	}

	@Test
	public void testOperandsByArgs() {
		final String[] theArgs = new String[] { "--string1=someString", "--email=someEmail", "--token=1234567890" };
		final ArgsParser theParser = new ArgsParser();
		final Flag theHelpFlag = flag( 'h', "help", "Shows the help ..." );
		final Flag theVersionFlag = flag( 'v', "version", "Shows the version ..." );
		final StringProperty theString1Arg = stringProperty( "string1", "..." );
		final StringProperty theString2Arg = stringProperty( "string2", "..." );
		final BooleanProperty theboolArg = booleanProperty( "bool", "..." );
		final Flag theBoolFlag = flag( "bool", "..." );
		final StringProperty theEMailArg = stringProperty( "email", "..." );
		final StringProperty theNameArg = stringProperty( "name", "..." );
		final StringProperty theTokenArg = stringProperty( "token", "..." );
		final IntProperty theIntArg = intProperty( "int", "..." );
		final DoubleProperty theDoubleArg = doubleProperty( "double", "..." );
		// @formatter:off
		final Term theArgsSyntax = cases(
			and( theString1Arg, 
				optional( theString2Arg, 
					xor( theboolArg, theBoolFlag, 
						and( theEMailArg, theNameArg, theTokenArg ) 
					),
					theIntArg, theDoubleArg
				)
			),
			theHelpFlag,
			theVersionFlag
		);
		// @formatter:on
		theParser.withArgsSyntax( theArgsSyntax ).withName( null ).withSyntaxMetrics( SyntaxNotation.LOGICAL ).withEscapeCodesEnabled( false ).withConsoleWidth( 80 ).withBannerFontPalette( AsciiColorPalette.MAX_LEVEL_GRAY ).withLicense( null ).withCopyright( null ).withTitle( "CROWD:IT" ).withDescription( null );
		try {
			theParser.evalArgs( theArgs );
			fail( "Expecting an <" + UnknownArgsException.class.getSimpleName() + ">!" );
		}
		catch ( ArgsSyntaxException e ) {
			final Operand<?>[] theOperands = ( (Condition) theParser.getArgsSyntax() ).toOperands( e.getArgs() );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				for ( var eOperand : theOperands ) {
					System.out.println( eOperand );
				}
			}
			assertEquals( 3, theOperands.length );
			final List<Operand<?>> theList = Arrays.asList( theOperands );
			assertTrue( theList.contains( theString1Arg ) );
			assertTrue( theList.contains( theEMailArg ) );
			assertTrue( theList.contains( theTokenArg ) );
		}
	}

	@Test
	public void testEdgeCase1() {
		final Flag theEncryptFlag = flag( 'e', "encrypt", "ENCRYPT_PROPERTY", "Encrypts the message (stream, file, text or bytes)." );
		final StringOption theInputFileArg = stringOption( 'i', "input-file", "INPUT_FILE_PROPERTY", "The input file which to process from." );
		final StringOption theOutputFileArg = stringOption( 'o', "output-file", "OUTPUT_FILE_PROPERTY", "The output file which to process to." );
		final StringOption thePasswordArg = stringOption( 'p', "password", "PASSWORD_PROPERTY", "The password to use for encryption or decryption." );
		final Term theArgsSyntax = createEdgeCaseSyntax( theEncryptFlag, theInputFileArg, theOutputFileArg, thePasswordArg );
		final String[] theArgs = new String[] { "-e", "-i", "in.txt", "-o", "out.txt", "-p", "Secret123!" };
		final ArgsParser theParser = new ArgsParser( theArgsSyntax );
		try {
			final var theOperands = theParser.evalArgs( theArgs );
			assertEquals( 4, theOperands.length );
		}
		catch ( ArgsSyntaxException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theArgsSyntax.toSchema() );
				System.out.println( e.toHeuristicMessage() );
			}
			fail( "Expecting the arguments syntax to be parsed correctly!" );
		}
		assertTrue( theEncryptFlag.isEnabled() );
		assertEquals( "in.txt", theInputFileArg.getValue() );
		assertEquals( "out.txt", theOutputFileArg.getValue() );
		assertEquals( "Secret123!", thePasswordArg.getValue() );
	}

	@Test
	public void testEdgeCase2() {
		final Flag theEncryptFlag = flag( 'e', "encrypt", "ENCRYPT_PROPERTY", "Encrypts the message (stream, file, text or bytes)." );
		final StringOption theInputFileArg = stringOption( 'i', "input-file", "INPUT_FILE_PROPERTY", "The input file which to process from." );
		final StringOption theOutputFileArg = stringOption( 'o', "output-file", "OUTPUT_FILE_PROPERTY", "The output file which to process to." );
		final StringOption thePasswordArg = stringOption( 'p', "password", "PASSWORD_PROPERTY", "The password to use for encryption or decryption." );
		final Term theArgsSyntax = createEdgeCaseSyntax( theEncryptFlag, theInputFileArg, theOutputFileArg, thePasswordArg );
		final String[] theArgs = new String[] { "-d", "-e", "-i", "in.txt", "-o", "out.txt", "-p", "Secret123!" };
		final ArgsParser theParser = new ArgsParser( theArgsSyntax );
		try {
			theParser.evalArgs( theArgs );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theArgsSyntax.toSchema() );
			}
			fail( "Expecting an <" + UnknownArgsException.class.getSimpleName() + ">!" );
		}
		catch ( ArgsSyntaxException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( e.toHeuristicMessage() );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private Term createEdgeCaseSyntax( Flag aEncryptFlag, StringOption aInputFileArg, StringOption aOutputFileArg, StringOption aPasswordArg ) {
		final EnumOption<BaseMetricsConfig> theEncodingArg = enumOption( "encoding", BaseMetricsConfig.class, "ENCODING_PROPERTY", "The BASE (e.g. BASE64) encoding/decoding to be applied for handling encrypted data: " + VerboseTextBuilder.asString( BaseMetricsConfig.values() ) );
		final EnumOption<ChaosMode> theCertModeArg = enumOption( "cert-mode", ChaosMode.class, "CERT_MODE_PROPERTY", "The chaos options (mode) to be used for the cert itself: " + VerboseTextBuilder.asString( ChaosMode.values() ) );
		final EnumOption<ChaosMode> theChaosModeArg = enumOption( "chaos-mode", ChaosMode.class, "CHAOS_MODE_PROPERTY", "The mode to be used when encrypting/decrypting: " + VerboseTextBuilder.asString( ChaosMode.values() ) );
		final EnumOption<SystemContext> theContextArg = enumOption( "context", SystemContext.class, "CONTEXT_PROPERTY", "The system context providing the password: " + VerboseTextBuilder.asString( SystemContext.values() ) );
		final Flag theBase64Flag = flag( "base64", "BASE64_PROPERTY", "Use BASE64 encoding/decoding to be applied for handling encrypted data." );
		final Flag theCertInfoFlag = flag( "cert-info", "CERT_INFO_PROPERTY", "Prints out information of a given cert." );
		final Flag theCreateCertFlag = flag( "create-cert", "CREATE_CERT_PROPERTY", "Create an according cert record (file)." );
		final Flag theCreateSpecFlag = flag( "create-spec", "CREATE_SPEC_PROPERTY", "Create an according spec record (file)." );
		final Flag theDebugFlag = debugFlag( false );
		final Flag theDecryptFlag = flag( 'd', "decrypt", "DECRYPT_PROPERTY", "Decrypts the message (stream, file, text or bytes)." );
		final Flag theEncodedFlag = flag( "encoded-length", "ENCODED_LENGTH_PROPERTY", "Prints length in bytes of a single chaos key encoding (being the addional length of a salted encryption)." );
		final Flag theHelpFlag = helpFlag();
		final Flag theHexFlag = flag( "hex", "HEX_PROPERTY", "Use a hexadecimal representation of (binary) output." );
		final Flag thePromptPasswordFlag = flag( "prompt", "PROMPT_PASSWORD_PROPERTY", "Prompt for the password to use for encryption or decryption." );
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theVerifyFlag = flag( "verify", "VERIFY_PROPERTY", "Verify the encryption process to make sure encryption decrypts flawlessly." );
		final Flag theCopyToFlag = flag( "copy-to", "COPY_TO_PROPERTY", "Copy the processed output data to the clipboard." );
		final Flag thePasteFromFlag = flag( "paste-from", "PASTE_FROM_PROPERTY", "Paste the input data to be processed from the clipboard." );
		final IntOption theChainLengthArg = intOption( "chain-length", "CHAIN_LENGTH_PROPERTY", "The length of the chaos key chain (e.g. number of nested chaos keys, defaults to <...>)" );
		final IntOption theLineWidthArg = intOption( "line-width", "LINE_WIDTH_PROPERTY", "The line width for base64 encoded ASCII output." );
		final DoubleOption theX0Arg = doubleOption( 'x', "start-value", "X0_PROPERTY", "The chaos key's <x0> start value to use: ..." );
		final DoubleOption theAArg = doubleOption( 'a', "parable-coefficient", "A_PROPERTY", "The chaos key's parable coefficient <a> to use: ..." );
		final LongOption theSArg = longOption( 's', "expansion-factor", "S_PROPERTY", "The chaos key's expansion factor <s> to use: ..." );
		final StringOption theBytesArg = stringOption( 'b', "bytes", "BYTES_PROPERTY", "The message in bytes (e.g. \"127, 128, 0x10, 0xFF\") which to process." );
		final StringOption theCertFileArg = stringOption( 'c', "cert-file", "CERT_FILE_PROPERTY", "The cert file file which to use." );
		final StringOption theSpecFileArg = stringOption( "spec-file", "SPEC_FILE_PROPERTY", "The spec file file which to use." );
		final StringOption theTextArg = stringOption( 't', "text", "TEXT_PROPERTY", "The text message which to process." );
		// @formatter:off
		final Condition theArgsSyntax = cases(
			// T + B
			and( 
				and( aEncryptFlag, any( theVerifyFlag ) ), 
				and( 
					xor( theTextArg, theBytesArg )
				),
				any(
					xor( theHexFlag, theBase64Flag, theEncodingArg ),
					xor( 
						any( xor( thePromptPasswordFlag, aPasswordArg, theContextArg, and ( theX0Arg, theAArg, theSArg ) ), theChaosModeArg ),
						and( theCertFileArg, any (xor( thePromptPasswordFlag, aPasswordArg, theContextArg ) ) )
					),
					theVerboseFlag, theDebugFlag
				)
			),
			and( 
				theDecryptFlag, 
				and( 
					xor( theTextArg, theBytesArg )
				),
				any(
					xor( theBase64Flag, theEncodingArg ),  theHexFlag,
					xor( 
						any( xor( thePromptPasswordFlag, aPasswordArg, theContextArg, and ( theX0Arg, theAArg, theSArg ) ), theChaosModeArg ),
						and( theCertFileArg, any (xor( thePromptPasswordFlag, aPasswordArg, theContextArg ) ) )
					),
					theVerboseFlag, theDebugFlag
				)
			),
			// I + O
			and(
				xor(
					and( aEncryptFlag, 
						any(
							xor( aInputFileArg, thePasteFromFlag ), xor( aOutputFileArg, theCopyToFlag ),
							and( xor( theBase64Flag, theEncodingArg ), any ( theLineWidthArg ) ),
							theVerifyFlag
						) 
					), 
					and( theDecryptFlag, 
						any(
							xor( aInputFileArg, thePasteFromFlag ), xor( aOutputFileArg, theCopyToFlag ),
							xor( theBase64Flag, theEncodingArg )
						) 
					) 
				),
				any(
					xor( 
						any( xor( thePromptPasswordFlag, aPasswordArg, theContextArg, and ( theX0Arg, theAArg, theSArg ) ), theChaosModeArg ), 
						and( theCertFileArg, any (xor( thePromptPasswordFlag, aPasswordArg, theContextArg ) ) ) 
					),
					theVerboseFlag, theDebugFlag
				)
			),
			// CERT
			and( 
				theCreateCertFlag, any ( theLineWidthArg, theCertFileArg, and( xor( thePromptPasswordFlag, aPasswordArg, theContextArg ), any( theCertModeArg ) ), xor( or( theChainLengthArg, theChaosModeArg), theSpecFileArg ), theVerboseFlag, theDebugFlag ) 
			),
			and(
				theCertInfoFlag, theCertFileArg, any(  xor( thePromptPasswordFlag, aPasswordArg, theContextArg ), theVerboseFlag, theDebugFlag )
			),
			and( 
				theCreateSpecFlag, any ( theSpecFileArg , theVerboseFlag, theDebugFlag ) 
			),
			xor(
				theHelpFlag, and( xor( theEncodedFlag, theSysInfoFlag ), any ( theVerboseFlag, theDebugFlag ) )
			)
		);
		// @formatter:on
		return theArgsSyntax;
	}
}
