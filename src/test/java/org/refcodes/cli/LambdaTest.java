// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.cli.CliSugar.*;

import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class LambdaTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testStringOptionLambda1() throws ArgsSyntaxException {
		final String theExpected = "Hello World!";
		// @formatter:off
		final StringOption theStringOption;
		final String[] theResult = new String[1];
		final Term theArgsSyntax = cases(
			theStringOption = stringOption( 's', "string", "STRING", "Invoke lambda upon parsing", s -> {
				System.out.println( s.getAlias() + "=" + s.getValue() );
				theResult[0] = s.getValue();
			} )
		);
		// @formatter:on
		final String[] theArgs = { "-s", theExpected };
		final ArgsParser theParser = new ArgsParser( theArgsSyntax );
		theParser.evalArgs( theArgs );
		assertEquals( theExpected, theResult[0] );
		assertEquals( theStringOption.getValue(), theResult[0] );
	}

	@Test
	public void testStringOptionLambda2() throws ArgsSyntaxException {
		final String theExpected = "Hello World!";
		// @formatter:off
		final StringOption theStringOption1;
		final String[] theResult = new String[1];
		final Term theArgsSyntax = cases(
			any(
				theStringOption1 = stringOption( 's', "string", "STRING", "Invoke lambda upon parsing", s -> {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( s.getAlias() + "=" + s.getValue() );
					}
					theResult[0] = s.getValue();
				} ),
				stringOption( 't', "text", "TEXT", "Don't invoke lambda upon parsing", t -> {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( t.getAlias() + "=" + t.getValue() );
					}
					fail("This option did NOT match any of the provided arguments!");
				} )
			)
		);
		// @formatter:on
		final String[] theArgs = { "-s", theExpected };
		final ArgsParser theParser = new ArgsParser( theArgsSyntax );
		theParser.evalArgs( theArgs );
		assertEquals( theExpected, theResult[0] );
		assertEquals( theStringOption1.getValue(), theResult[0] );
	}

	@Test
	public void testStringOptionLambda3() throws ArgsSyntaxException {
		final String theExpected1 = "Hello World!";
		final String theExpected2 = "Hallo Welt!";
		// @formatter:off
		final StringOption theStringOption1;
		final StringOption theStringOption2;
		final String[] theResult = new String[2];
		final Term theArgsSyntax = cases(
			any(
				theStringOption1 = stringOption( 's', "string", "STRING", "Invoke lambda upon parsing", s -> {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( s.getAlias() + "=" + s.getValue() );
					}
					theResult[0] = s.getValue();
				} ),
				theStringOption2 = stringOption( 't', "text", "TEXT", "Invoke lambda upon parsing", t -> {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( t.getAlias() + "=" + t.getValue() );
					}
					theResult[1] = t.getValue();
				} )
			)
		);
		// @formatter:on
		final String[] theArgs = { "-s", theExpected1, "-t", theExpected2 };
		final ArgsParser theParser = new ArgsParser( theArgsSyntax );
		theParser.evalArgs( theArgs );
		assertEquals( theExpected1, theResult[0] );
		assertEquals( theExpected2, theResult[1] );
		assertEquals( theStringOption1.getValue(), theResult[0] );
		assertEquals( theStringOption2.getValue(), theResult[1] );
	}

	@Test
	public void testStringPropertyLambda1() throws ArgsSyntaxException {
		final String theExpected = "Hello World!";
		// @formatter:off
		final StringProperty theStringProperty;
		final String[] theResult = new String[1];
		final Term theArgsSyntax = cases(
			theStringProperty = stringProperty( 's', "string", "STRING", "Invoke lambda upon parsing", s -> {
				System.out.println( s.getAlias() + "=" + s.getValue() );
				theResult[0] = s.getValue();
			} )
		);
		// @formatter:on
		final String[] theArgs = { "-s=" + theExpected };
		final ArgsParser theParser = new ArgsParser( theArgsSyntax );
		theParser.evalArgs( theArgs );
		assertEquals( theExpected, theResult[0] );
		assertEquals( theStringProperty.getValue(), theResult[0] );
	}

	@Test
	public void testStringPropertyLambda2() throws ArgsSyntaxException {
		final String theExpected = "Hello World!";
		// @formatter:off
		final StringProperty theStringProperty1;
		final String[] theResult = new String[1];
		final Term theArgsSyntax = cases(
			any(
				theStringProperty1 = stringProperty( 's', "string", "STRING", "Invoke lambda upon parsing", s -> {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( s.getAlias() + "=" + s.getValue() );
					}
					theResult[0] = s.getValue();
				} ),
				stringProperty( 't', "text", "TEXT", "Don't invoke lambda upon parsing", t -> {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( t.getAlias() + "=" + t.getValue() );
					}
					fail("This option did NOT match any of the provided arguments!");
				} )
			)
		);
		// @formatter:on
		final String[] theArgs = { "-s=" + theExpected };
		final ArgsParser theParser = new ArgsParser( theArgsSyntax );
		theParser.evalArgs( theArgs );
		assertEquals( theExpected, theResult[0] );
		assertEquals( theStringProperty1.getValue(), theResult[0] );
	}

	@Test
	public void testStringPropertyLambda3() throws ArgsSyntaxException {
		final String theExpected1 = "Hello World!";
		final String theExpected2 = "Hallo Welt!";
		// @formatter:off
		final StringProperty theStringProperty1;
		final StringProperty theStringProperty2;
		final String[] theResult = new String[2];
		final Term theArgsSyntax = cases(
			any(
				theStringProperty1 = stringProperty( 's', "string", "STRING", "Invoke lambda upon parsing", s -> {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( s.getAlias() + "=" + s.getValue() );
					}
					theResult[0] = s.getValue();
				} ),
				theStringProperty2 = stringProperty( 't', "text", "TEXT", "Invoke lambda upon parsing", t -> {
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( t.getAlias() + "=" + t.getValue() );
					}
					theResult[1] = t.getValue();
				} )
			)
		);
		// @formatter:on
		final String[] theArgs = { "-s=" + theExpected1, "-t=" + theExpected2 };
		final ArgsParser theParser = new ArgsParser( theArgsSyntax );
		theParser.evalArgs( theArgs );
		assertEquals( theExpected1, theResult[0] );
		assertEquals( theExpected2, theResult[1] );
		assertEquals( theStringProperty1.getValue(), theResult[0] );
		assertEquals( theStringProperty2.getValue(), theResult[1] );
	}

	@Test
	public void testIllegalArgumentLambda() throws ArgsSyntaxException {
		String theExpected = "Unable to parse provided argument(s) [\"-s\", \"Hello World!\"] for operand \"-s --string <STRING>\"! Caused by: Bad input!";
		// @formatter:off
		final Term theArgsSyntax = cases(
			stringOption( 's', "string", "STRING", "Invoke lambda upon parsing", s -> {
				throw new IllegalArgumentException("Bad input!");
			} )
		);
		// @formatter:on
		final String[] theArgs = { "-s", "Hello World!" };
		final ArgsParser theParser = new ArgsParser( theArgsSyntax ).withEscapeCodesEnabled( false );
		try {
			theParser.evalArgs( theArgs );
			fail( "Expecting an exception of type <" + ArgsSyntaxException.class.getName() + ">!" );
		}
		catch ( ArgsSyntaxException expected ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Expected  = " + theExpected );
				System.out.println( "Exception = " + expected.toHeuristicMessage() );
			}
			assertEquals( theExpected, expected.toHeuristicMessage() );
		}
	}
}
