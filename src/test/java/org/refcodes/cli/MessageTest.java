// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.cli.CliSugar.*;

import org.junit.jupiter.api.Test;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.runtime.Execution;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.VerboseTextBuilder;

public class MessageTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testHeuristicMessageA1() {
		final String[] theArgs = new String[] { "--string=someString", "--token=1234567890" };
		final String theExpected = "No syntax branch (fully) matched the command line arguments, though one (XOR) syntax branch must match! Caused by: Failed as of superfluous \"--token=1234567890\" argument, though the arguments must be matched (ALL) without any surplus by the syntax branches! Possible cause: No option \"--email\" was found in the command line arguments, provide it to match the according syntax branch! Also notable: No option \"--name\" was found in the command line arguments, provide it to match the according syntax branch! Also notable: Neither the short-option \"-h\" nor the long-option \"--help\" were found in the command line arguments, provide at least one of them to match the according syntax branch! Neither the short-option \"-v\" nor the long-option \"--version\" were found in the command line arguments, provide at least one of them to match the according syntax branch!";
		final String theHeurisiticExprected = "Failed as of superfluous \"--token=1234567890\" argument, though the arguments must be matched (ALL) without any surplus by the syntax branches! Possible cause: No option \"--email\" was found in the command line arguments, provide it to match the according syntax branch! Also notable: No option \"--name\" was found in the command line arguments, provide it to match the according syntax branch! Also notable: Neither the short-option \"-h\" nor the long-option \"--help\" were found in the command line arguments, provide at least one of them to match the according syntax branch! Neither the short-option \"-v\" nor the long-option \"--version\" were found in the command line arguments, provide at least one of them to match the according syntax branch!";
		final ArgsParser theParser = createArgsParserA();
		try {
			theParser.evalArgs( theArgs );
			fail( "Expected an exception of type <" + ArgsSyntaxException.class.getSimpleName() + ">!" );
		}
		catch ( ArgsSyntaxException e ) {
			final String theMessage = e.toMessage();
			final String theHeuristicMessage = e.toHeuristicMessage();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "toMessage() = " + theMessage );
				System.out.println( "toHeuristicMessage() = " + theHeuristicMessage );
			}
			assertEquals( theExpected, theMessage );
			assertEquals( theHeurisiticExprected, theHeuristicMessage );
		}
	}

	@Test
	public void testHeuristicMessageA2() {
		final String[] theArgs = new String[] { "--string=someString", "--email=someEmail", "--token=1234567890" };
		final String theExpected = "No syntax branch (fully) matched the command line arguments, though one (XOR) syntax branch must match! Caused by: Failed as of (mutual) superfluous \"--email=someEmail\", \"--token=1234567890\" arguments, though the arguments must be matched (ALL) without any surplus by the syntax branches! Possible cause: No option \"--name\" was found in the command line arguments, provide it to match the according syntax branch! Also notable: Neither the short-option \"-h\" nor the long-option \"--help\" were found in the command line arguments, provide at least one of them to match the according syntax branch! Neither the short-option \"-v\" nor the long-option \"--version\" were found in the command line arguments, provide at least one of them to match the according syntax branch!";
		final String theHeurisiticExprected = "Failed as of (mutual) superfluous \"--email=someEmail\", \"--token=1234567890\" arguments, though the arguments must be matched (ALL) without any surplus by the syntax branches! Possible cause: No option \"--name\" was found in the command line arguments, provide it to match the according syntax branch! Also notable: Neither the short-option \"-h\" nor the long-option \"--help\" were found in the command line arguments, provide at least one of them to match the according syntax branch! Neither the short-option \"-v\" nor the long-option \"--version\" were found in the command line arguments, provide at least one of them to match the according syntax branch!";
		final ArgsParser theParser = createArgsParserA();
		try {
			theParser.evalArgs( theArgs );
			fail( "Expected an exception of type <" + ArgsSyntaxException.class.getSimpleName() + ">!" );
		}
		catch ( ArgsSyntaxException e ) {
			final String theMessage = e.toMessage();
			final String theHeuristicMessage = e.toHeuristicMessage();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "toMessage() = " + theMessage );
				System.out.println( "toHeuristicMessage() = " + theHeuristicMessage );
			}
			assertEquals( theExpected, theMessage );
			assertEquals( theHeurisiticExprected, theHeuristicMessage );
		}
	}

	@Test
	public void testHeuristicMessageB1() {
		final String[] theArgs = new String[] { "-d", "-t" };
		final String theExpected = "No syntax branch (fully) matched the command line arguments, though one (XOR) syntax branch must match! Caused by: At least one syntax branch did not match the command line arguments, though all (AND) syntax branches must be matched by the command line arguments! Also notable: At least one syntax branch did not match the command line arguments, though all (AND) syntax branches must be matched by the command line arguments! Caused by: No syntax branch (fully) matched the command line arguments, though one (XOR) syntax branch must match! Caused by: Missing value for option \"-t\" requiring a value. Also notable: Neither the short-option \"-b\" nor the long-option \"--bytes\" were found in the command line arguments, provide at least one of them to match the according syntax branch! At least one syntax branch did not match the command line arguments, though all (AND) syntax branches must be matched by the command line arguments! Caused by: Neither the short-option \"-e\" nor the long-option \"--encrypt\" were found in the command line arguments, provide at least one of them to match the according syntax branch! Failed as of superfluous \"-t\" argument, though the arguments must be matched (ALL) without any surplus by the syntax branches! At least one syntax branch did not match the command line arguments, though all (AND) syntax branches must be matched by the command line arguments! Caused by: No option \"--create-cert\" was found in the command line arguments, provide it to match the according syntax branch! At least one syntax branch did not match the command line arguments, though all (AND) syntax branches must be matched by the command line arguments! Caused by: No option \"--cert-info\" was found in the command line arguments, provide it to match the according syntax branch! Also notable: Neither the short-option \"-c\" nor the long-option \"--cert-file\" were found in the command line arguments, provide at least one of them to match the according syntax branch! At least one syntax branch did not match the command line arguments, though all (AND) syntax branches must be matched by the command line arguments! Caused by: No option \"--create-spec\" was found in the command line arguments, provide it to match the according syntax branch! No syntax branch (fully) matched the command line arguments, though one (XOR) syntax branch must match! Caused by: No option \"--help\" was found in the command line arguments, provide it to match the according syntax branch! Also notable: At least one syntax branch did not match the command line arguments, though all (AND) syntax branches must be matched by the command line arguments! Caused by: No syntax branch (fully) matched the command line arguments, though one (XOR) syntax branch must match! Caused by: No option \"--encoded-length\" was found in the command line arguments, provide it to match the according syntax branch! Also notable: No option \"--sysinfo\" was found in the command line arguments, provide it to match the according syntax branch! Caused by: Neither the short-option \"-e\" nor the long-option \"--encrypt\" were found in the command line arguments, provide at least one of them to match the according syntax branch! Also notable: No syntax branch (fully) matched the command line arguments, though one (XOR) syntax branch must match! Caused by: Missing value for option \"-t\" requiring a value. Also notable: Neither the short-option \"-b\" nor the long-option \"--bytes\" were found in the command line arguments, provide at least one of them to match the according syntax branch!";
		final String theHeurisiticExprected = "Missing value for option \"-t\" requiring a value. Also notable: Neither the short-option \"-b\" nor the long-option \"--bytes\" were found in the command line arguments, provide at least one of them to match the according syntax branch! Possible cause: Failed as of superfluous \"-t\" argument, though the arguments must be matched (ALL) without any surplus by the syntax branches!";
		final ArgsParser theParser = createArgsParserB();
		try {
			theParser.evalArgs( theArgs );
			fail( "Expected an exception of type <" + UnknownArgsException.class.getSimpleName() + ">!" );
		}
		catch ( ArgsSyntaxException e ) {
			final String theMessage = e.toMessage();
			final String theHeuristicMessage = e.toHeuristicMessage();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "toMessage() = " + theMessage );
				System.out.println( "toHeuristicMessage() = " + theHeuristicMessage );
			}
			assertEquals( theExpected, theMessage );
			assertEquals( theHeurisiticExprected, theHeuristicMessage );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private static ArgsParser createArgsParserA() {
		final ArgsParser theParser = new ArgsParser();
		final Flag theHelpFlag = flag( 'h', "help", "Shows the help ..." );
		final Flag theVersionFlag = flag( 'v', "version", "Shows the version ..." );
		final StringProperty theString1Arg = stringProperty( "string", "..." );
		final StringProperty theString2Arg = stringProperty( "string", "..." );
		final BooleanProperty theboolArg = booleanProperty( "bool", "..." );
		final Flag theBoolFlag = flag( "bool", "..." );
		final StringProperty theEMailArg = stringProperty( "email", "..." );
		final StringProperty theNameArg = stringProperty( "name", "..." );
		final StringProperty theTokenArg = stringProperty( "token", "..." );
		final IntProperty theIntArg = intProperty( "int", "..." );
		final DoubleProperty theDoubleArg = doubleProperty( "double", "..." );
		// @formatter:off
		final Term theArgsSyntax = cases(
			and( theString1Arg, 
				optional( theString2Arg, 
					xor( theboolArg, theBoolFlag, 
						and( theEMailArg, theNameArg, theTokenArg ) 
					),
					theIntArg, theDoubleArg
				)
			),
			theHelpFlag,
			theVersionFlag
		);
		// @formatter:on
		return theParser.withArgsSyntax( theArgsSyntax ).withName( null ).withSyntaxMetrics( SyntaxNotation.LOGICAL ).withEscapeCodesEnabled( false ).withConsoleWidth( 80 ).withBannerFontPalette( AsciiColorPalette.MAX_LEVEL_GRAY ).withLicense( null ).withCopyright( null ).withTitle( "CROWD:IT" ).withDescription( null );
	}

	public enum BaseMetricsConfig {
		CONF_A, CONF_B, CONF_C
	}

	enum ChaosMode {
		X, Y, Z
	}

	enum SystemContext {
		S0, S1, S2
	}

	private static ArgsParser createArgsParserB() {
		final ArgsParser theParser = new ArgsParser();
		final EnumOption<BaseMetricsConfig> theEncodingArg = enumOption( "encoding", BaseMetricsConfig.class, "ENCODING_PROPERTY", "The BASE (e.g. BASE64) encoding/decoding to be applied for handling encrypted data: " + VerboseTextBuilder.asString( BaseMetricsConfig.values() ) );
		final EnumOption<ChaosMode> theCertModeArg = enumOption( "cert-mode", ChaosMode.class, "CERT_MODE_PROPERTY", "The chaos options (mode) to be used for the cert itself: " + VerboseTextBuilder.asString( ChaosMode.values() ) );
		final EnumOption<ChaosMode> theChaosModeArg = enumOption( "chaos-mode", ChaosMode.class, "CERT_MODE_PROPERTY", "The mode to be used when encrypting/decrypting: " + VerboseTextBuilder.asString( ChaosMode.values() ) );
		final EnumOption<SystemContext> theContextArg = enumOption( "context", SystemContext.class, "CONTEXT_PROPERTY", "The system context providing the password: " + VerboseTextBuilder.asString( SystemContext.values() ) );
		final Flag theBase64Flag = flag( "base64", "BASE64_PROPERTY", "Use BASE64 encoding/decoding to be applied for handling encrypted data." );
		final Flag theCertInfoFlag = flag( "cert-info", "CERT_INFO_PROPERTY", "Prints out information of a given cert." );
		final Flag theCreateCertFlag = flag( "create-cert", "CREATE_CERT_PROPERTY", "Create an according cert record (file)." );
		final Flag theCreateSpecFlag = flag( "create-spec", "CREATE_SPEC_PROPERTY", "Create an according spec record (file)." );
		final Flag theDebugFlag = debugFlag( false );
		final Flag theDecryptFlag = flag( 'd', "decrypt", "DECRYPT_PROPERTY", "Decrypts the message (stream, file, text or bytes)." );
		final Flag theEncodedFlag = flag( "encoded-length", "ENCODED_LENGTH_PROPERTY", "Prints length in bytes of a single chaos key encoding (being the addional length of a salted encryption)." );
		final Flag theEncryptFlag = flag( 'e', "encrypt", "ENCRYPT_PROPERTY", "Encrypts the message (stream, file, text or bytes)." );
		final Flag theHelpFlag = helpFlag();
		final Flag theHexFlag = flag( "hex", "HEX_PROPERTY", "Use a hexadecimal representation of (binary) output." );
		final Flag thePromptPasswordFlag = flag( "prompt", "PROMPT_PASSWORD_PROPERTY", "Prompt for the password to use for encryption or decryption." );
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theVerifyFlag = flag( "verify", "VERIFY_PROPERTY", "Verify the encryption process to make sure encryption decrypts flawlessly." );
		final Flag theCopyToFlag = flag( "copy-to", "COPY_TO_PROPERTY", "Copy the processed output data to the clipboard" + ( Execution.isNativeImage() ? " (without warranty)" : "" ) + "." );
		final Flag thePasteFromFlag = flag( "paste-from", "PASTE_FROM_PROPERTY", "Paste the input data to be processed from the clipboard" + ( Execution.isNativeImage() ? " (without warranty)" : "" ) + "." );
		final IntOption theChainLengthArg = intOption( "chain-length", "CHAIN_LENGTH_PROPERTY", "The length of the chaos key chain (e.g. number of nested chaos keys, defaults to <>)" );
		final IntOption theLineWidthArg = intOption( "line-width", "LINE_WIDTH_PROPERTY", "The line width for base64 encoded ASCII output." );
		final DoubleOption theX0Arg = doubleOption( 'x', "start-value", "X0_PROPERTY", "The chaos key's <x0> start value to use: < x0 ≤ " );
		final DoubleOption theAArg = doubleOption( 'a', "parable-coefficient", "A_PROPERTY", "The chaos key's parable coefficient <a> to use: ≤ a ≤ " );
		final LongOption theSArg = longOption( 's', "expansion-factor", "S_PROPERTY", "The chaos key's expansion factor <s> to use: ≤ s ≤ or ≤ s ≤ " );
		final StringOption theBytesArg = stringOption( 'b', "bytes", "BYTES_PROPERTY", "The message in bytes (e.g. \"127, 128, 0x10, 0xFF\") which to process." );
		final StringOption theCertFileArg = stringOption( 'c', "cert-file", "CERT_FILE_PROPERTY", "The cert file file which to use." );
		final StringOption theInputFileArg = stringOption( 'i', "input-file", "INPUT_FILE_PROPERTY", "The input file which to process from." );
		final StringOption theOutputFileArg = stringOption( 'o', "output-file", "OUTPUT_FILE_PROPERTY", "The output file which to process to." );
		final StringOption thePasswordArg = stringOption( 'p', "password", "PASSWORD_PROPERTY", "The password to use for encryption or decryption." );
		final StringOption theSpecFileArg = stringOption( "spec-file", "SPEC_FILE_PROPERTY", "The spec file file which to use." );
		final StringOption theTextArg = stringOption( 't', "text", "TEXT_PROPERTY", "The text message which to process." );

		// @formatter:off
		final Condition theArgsSyntax = cases(
			// Text + Bytes
			and( // Encrypt:
				theEncryptFlag, 
				xor( theTextArg, theBytesArg ),
				any(
					xor( theHexFlag, theBase64Flag, theEncodingArg ),
					xor( 
						any( xor( thePromptPasswordFlag, thePasswordArg, theContextArg, and ( theX0Arg, theAArg, theSArg ) ), theChaosModeArg ),
						and( theCertFileArg, any (xor( thePromptPasswordFlag, thePasswordArg, theContextArg ) ) )
					),
					theCopyToFlag, theVerifyFlag, theVerboseFlag, theDebugFlag
				)
			),
			and( // Decrypt: 
				theDecryptFlag, 
				 xor( theTextArg, theBytesArg ),
				 any(
					xor( theBase64Flag, theEncodingArg ),  theHexFlag,
					xor( 
						any( xor( thePromptPasswordFlag, thePasswordArg, theContextArg, and ( theX0Arg, theAArg, theSArg ) ), theChaosModeArg ),
						and( theCertFileArg, any (xor( thePromptPasswordFlag, thePasswordArg, theContextArg ) ) )
					),
					theCopyToFlag, theVerboseFlag, theDebugFlag
				)
			),
			// Input + Output
			and( // Encrypt
				theEncryptFlag, 
				any(
					xor( theInputFileArg, thePasteFromFlag ), xor( theOutputFileArg, theCopyToFlag ),
					and( xor( theBase64Flag, theEncodingArg ), any ( theLineWidthArg ) ),
					xor( 
						any( xor( thePromptPasswordFlag, thePasswordArg, theContextArg, and ( theX0Arg, theAArg, theSArg ) ), theChaosModeArg ), 
						and( theCertFileArg, any (xor( thePromptPasswordFlag, thePasswordArg, theContextArg ) ) ) 
					),
					theVerifyFlag, theVerboseFlag, theDebugFlag
				)
			),
			and( // Decrypt
				 theDecryptFlag, 
				any(
					xor( theInputFileArg, thePasteFromFlag ), xor( theOutputFileArg, theCopyToFlag ),
					xor( theBase64Flag, theEncodingArg ),
					xor( 
						any( xor( thePromptPasswordFlag, thePasswordArg, theContextArg, and ( theX0Arg, theAArg, theSArg ) ), theChaosModeArg ), 
						and( theCertFileArg, any (xor( thePromptPasswordFlag, thePasswordArg, theContextArg ) ) ) 
					),
					theVerboseFlag, theDebugFlag
				)
			),
			// CERT
			and( 
				theCreateCertFlag, any ( theLineWidthArg, theCertFileArg, and( xor( thePromptPasswordFlag, thePasswordArg, theContextArg ), any( theCertModeArg ) ), xor( or( theChainLengthArg, theChaosModeArg), theSpecFileArg ), theVerboseFlag, theDebugFlag ) 
			),
			and(
				theCertInfoFlag, theCertFileArg, any(  xor( thePromptPasswordFlag, thePasswordArg, theContextArg ), theVerboseFlag, theDebugFlag )
			),
			and( 
				theCreateSpecFlag, any ( theSpecFileArg , theVerboseFlag, theDebugFlag ) 
			),
			xor(
				theHelpFlag, and( xor( theEncodedFlag, theSysInfoFlag ), any ( theVerboseFlag, theDebugFlag ) )
			)
		);
		return theParser.withArgsSyntax( theArgsSyntax ).withName( null ).withSyntaxMetrics( SyntaxNotation.LOGICAL ).withEscapeCodesEnabled( false ).withConsoleWidth( 80 ).withBannerFontPalette( AsciiColorPalette.MAX_LEVEL_GRAY ).withLicense( null ).withCopyright( null ).withTitle( "CROWD:IT" ).withDescription( null );
	}
}
