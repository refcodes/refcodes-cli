package org.refcodes.cli;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.cli.CliSugar.*;
import org.junit.jupiter.api.Test;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.VerboseTextBuilder;

public class OptionalConditionTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testOptionalEdgeCase() {
		final Option<Integer> theWidth = intOption( 'w', "width", "width", "Sets the console width" );
		final Option<Integer> thePortOption = intOption( 'p', "port", "port", "Sets the port for the server" );
		final Option<Integer> theMaxConns = intOption( 'c', "connections", "connections", "Sets the number of max. connections" );
		final Option<String> theUsername = stringOption( 'u', "user", "username", "The username for HTTP Basic-Authentication" );
		final Option<String> theSecret = stringOption( 's', "secret", "secret", "The password for HTTP Basic-Authentication" );
		final Flag theSysInfo = flag( null, "sysinfo", "sysInfo", "Shows some system information" );
		final Option<Integer> theMgmPortOption = intOption( 'm', "management-port", "management-port", "The management-port on which to listen for shutdown..." );
		final Flag theHelp = helpFlag( "Shows this help" );
		// @formatter:off
		final Condition theRoot = any ( xor( 
			any( thePortOption, theMaxConns, and( theUsername, theSecret ), theWidth, theMgmPortOption ),
			any( xor( theHelp, theSysInfo ) ) )
		);
		// @formatter:on
		final ParseArgs theParseArgs = new ArgsParser( theRoot );
		theParseArgs.withSyntaxMetrics( SyntaxNotation.LOGICAL );
		theParseArgs.withName( "TinyRestful" ).withTitle( "TINYRESTFUL" ).withCopyright( "Copyright (c) by FUNCODES.CLUB, Munich, Germany." ).withLicense( "Licensed under GNU General Public License, v3.0 and Apache License, v2.0" );
		theParseArgs.withBannerFont( new Font( FontFamily.DIALOG, FontStyle.BOLD ) ).withBannerFontPalette( AsciiColorPalette.MAX_LEVEL_GRAY.getPalette() );
		theParseArgs.withDescription( "Tiny evil RESTful server. TinyRestfulServer makes heavy use of the REFCODES.ORG artifacts found together with the FUNCODES.CLUB sources at <http://bitbucket.org/refcodes>." );
		final String[] args = new String[] { "-p", "8080", "--sysinfo" };
		try {
			theParseArgs.evalArgs( args );
			fail( "Expecting a <" + SuperfluousArgsException.class.getSimpleName() + ">!" );
		}
		catch ( ArgsSyntaxException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( new VerboseTextBuilder().withElements( e.getArgs() ).toString() );
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( e.toHeuristicMessage() );
			}
			assertEquals( 3, e.getArgs().length );
		}
	}
}
