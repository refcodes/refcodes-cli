// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
// under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import static org.refcodes.cli.CliSugar.*;

import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class SchemaTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	// @Disabled("Just for looking if output is beautifully formatted")
	@Test
	public void testSchema() {
		final StringOption theEchoOption = stringOption( 'e', "echo", "Echoes the provided message to the standard out stream." );
		final StringProperty theEchoProperty = stringProperty( 'e', "echo", "Echoes the provided message to the standard out stream." );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag();
		final Flag theHelpFlag = helpFlag();
		final Flag theDebugFlag = debugFlag();
		// @formatter:off
		final Term theArgsSyntax = cases(
			optional( theEchoOption, theEchoProperty, theVerboseFlag, theDebugFlag ),
			xor( theHelpFlag, and( theSysInfoFlag, any ( theVerboseFlag ) ) )
		);
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theArgsSyntax.toSchema() );
		}
		
	}
}
