// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.cli.CliSugar.*;
import org.junit.jupiter.api.Test;
import org.refcodes.exception.MessageDetails;
import org.refcodes.runtime.SystemProperty;

public class StackOverflowTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * "java MyProgram -r opt1 -S opt2 arg1 arg2 arg3 arg4 --test -A opt3"
	 * "http://stackoverflow.com/questions/7341683/parsing-arguments-to-a-java-command-line-program"
	 *
	 * @throws UnknownArgsException the unknown args exception
	 * @throws AmbiguousArgsException the ambiguous args exception
	 * @throws SuperfluousArgsException the superfluous args exception
	 * @throws ParseArgsException the parse args exception
	 */
	@Test
	public void testParserStackOverflow1() throws ArgsSyntaxException {
		final Option<String> r = new StringOption( 'r', null, "opt1", "Your description for option r with argument opt1" );
		final Option<String> s = new StringOption( 'S', null, "opt2", "Your description for option S with argument opt2" );
		// Best to put operands after options to prevent them fetching away the
		// option's args:
		final Operand<String> arg1 = new StringOperand( "arg1", "Your description for arg1" );
		final Operand<String> arg2 = new StringOperand( "arg2", "Your description for arg2" );
		final Operand<String> arg3 = new StringOperand( "arg3", "Your description for arg3" );
		final Operand<String> arg4 = new StringOperand( "arg4", "Your description for arg4" );
		final Flag test = new Flag( "test", null, "Your description for test" );
		final Option<String> a = new StringOption( 'A', null, "opt3", "Your description for option A with argument opt3" );
		final Condition theRoot;
		final ParseArgs theParseArgs;
		// All arguments must be provided, else an exception is thrown:
		theRoot = new AndCondition( r, s, a, arg1, arg2, arg3, arg4, test );
		theParseArgs = new ArgsParser( theRoot );
		theParseArgs.withName( "MyProgramm" );
		theParseArgs.withSyntaxMetrics( SyntaxNotation.GNU_POSIX );
		theParseArgs.printSynopsis();
		theParseArgs.printSeparatorLn();
		theParseArgs.printOptions();
		theParseArgs.evalArgs( new String[] { "-r", "RRRRR", "-S", "SSSSS", "11111", "22222", "33333", "44444", "--test", "-A", "AAAAA" } );
		// Let's see:
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "r    :=" + r.getValue() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "S    :=" + s.getValue() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "arg1 :=" + arg1.getValue() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "arg2 :=" + arg2.getValue() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "arg3 :=" + arg3.getValue() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "arg4 :=" + arg4.getValue() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "test :=" + test.getValue() + "" );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "A    :=" + a.getValue() );
		}
		assertEquals( r.getValue(), "RRRRR" );
		assertEquals( s.getValue(), "SSSSS" );
		assertEquals( arg1.getValue(), "11111" );
		assertEquals( arg2.getValue(), "22222" );
		assertEquals( arg3.getValue(), "33333" );
		assertEquals( arg4.getValue(), "44444" );
		assertEquals( test.getValue(), true );
		assertEquals( a.getValue(), "AAAAA" );
	}

	@Test
	public void testParserStackOverflow2() throws ArgsSyntaxException {
		final Option<String> r = new StringOption( 'r', null, "opt1", "Your description for option r with argument opt1" );
		final Option<String> s = new StringOption( 'S', null, "opt2", "Your description for option S with argument opt2" );
		// Best to put operands after options to prevent them fetching away the
		// option's args:
		final Operand<String> arg1 = new StringOperand( "arg1", "Your description for arg1" );
		final Operand<String> arg2 = new StringOperand( "arg2", "Your description for arg2" );
		final Operand<String> arg3 = new StringOperand( "arg3", "Your description for arg3" );
		final Operand<String> arg4 = new StringOperand( "arg4", "Your description for arg4" );
		final Flag test = new Flag( null, "test", null, "Your description for test" );
		final Option<String> a = new StringOption( 'A', null, "opt3", "Your description for option A with argument opt3" );
		final Condition theRoot;
		final ParseArgs theParseArgs;
		// --test is optional:
		theRoot = new AndCondition( r, s, a, arg1, arg2, arg3, arg4, new AnyCondition( test ) );
		theParseArgs = new ArgsParser( theRoot );
		theParseArgs.withName( "MyProgramm" );
		theParseArgs.withSyntaxMetrics( SyntaxNotation.GNU_POSIX );
		theParseArgs.printSynopsis();
		theParseArgs.printSeparatorLn();
		theParseArgs.printOptions();
		theParseArgs.evalArgs( new String[] { "-r", "RRRRR", "-S", "SSSSS", "11111", "22222", "33333", "44444", "-A", "AAAAA" } );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "r    :=" + r.getValue() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "S    :=" + s.getValue() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "arg1 :=" + arg1.getValue() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "arg2 :=" + arg2.getValue() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "arg3 :=" + arg3.getValue() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "arg4 :=" + arg4.getValue() );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "test :=" + test.getValue() + "" );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "A    :=" + a.getValue() );
		}
		assertEquals( r.getValue(), "RRRRR" );
		assertEquals( s.getValue(), "SSSSS" );
		assertEquals( arg1.getValue(), "11111" );
		assertEquals( arg2.getValue(), "22222" );
		assertEquals( arg3.getValue(), "33333" );
		assertEquals( arg4.getValue(), "44444" );
		assertEquals( test.getValue(), false );
		assertEquals( a.getValue(), "AAAAA" );
	}

	@Test
	public void testParserStackOverflow3() throws ArgsSyntaxException {
		Term syntax = cases( CliSugar.none( "none" ), optional( helpFlag() ), //ambiguous
				stringOperand( "operand", "description" ) ); //needed for stack overflow to happen
		ArgsParser parser = new ArgsParser().withArgsSyntax( syntax );
		try {
			parser.evalArgs( new String[] {} );
			fail( "Expecting a <" + AmbiguousArgsException.class.getSimpleName() + ">!" );
		}
		catch ( ArgsSyntaxException e ) {
			String eMessage = e.toMessage();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "toMessage(): " + eMessage ); //StackOverflowError happens in here
			}
			eMessage = e.toHeuristicMessage();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "toHeuristicMessage(): " + eMessage ); //StackOverflowError happens in here
			}
			for ( MessageDetails eDetails : MessageDetails.values() ) {
				eMessage = e.toMessage( eDetails );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "toMessage(" + eDetails + "): " + eMessage ); //StackOverflowError happens in here
				}
			}
		}
	}
}
