// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.cli;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.cli.CliSugar.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;
import org.refcodes.cli.ArgsParserTest.BaseMetricsConfig;
import org.refcodes.data.AnsiEscapeCode;
import org.refcodes.runtime.OperatingSystem;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.textual.VerboseTextBuilder;

public class SyntaxNotationTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testVisibility1() throws ArgsSyntaxException {
		final EnumOption<BaseMetricsConfig> theEncodingArg = enumOption( "encoding", BaseMetricsConfig.class, "ENCODING_PROPERTY", "The BASE (e.g. BASE64) encoding/decoding to be applied for handling encrypted data: " + VerboseTextBuilder.asString( BaseMetricsConfig.values() ) );
		final Flag theBase64Flag = flag( "base64", "BASE64_PROPERTY", "Use BASE64 encoding/decoding to be applied for handling encrypted data." );
		final Flag theDecryptFlag = flag( 'd', "decrypt", "DECRYPT_PROPERTY", "Decrypts the aMessage (stream, file, text or bytes)." );
		final Flag theEncryptFlag = flag( 'e', "encrypt", "ENCRYPT_PROPERTY", "Encrypts the aMessage (stream, file, text or bytes)." );
		final Flag theVerifyFlag = flag( "verify", "VERIFY_PROPERTY", "Verify the encryption process to make sure encryption decrypts flawlessly." );
		final IntOption theLineWidthArg = intOption( "line-width", "LINE_WIDTH_PROPERTY", "The line width for base64 encoded ASCII output." ).withVisible( true );
		final StringOption theInputFileArg = stringOption( 'i', "input-file", "INPUT_FILE_PROPERTY", "The input file which to process from." );
		final StringOption theOutputFileArg = stringOption( 'o', "output-file", "OUTPUT_FILE_PROPERTY", "The output file which to process to." );
		// @formatter:off
		final Condition theCondition =  
			// I + O
			and(
				xor(
					and( theEncryptFlag, any( theVerifyFlag, xor( and( theBase64Flag, any (theLineWidthArg) ), and( theEncodingArg, any (theLineWidthArg) ) ) ) ), 
					theDecryptFlag  
				),
				any(
					theInputFileArg, theOutputFileArg 
				)
			);
		// @formatter:on
		final ArgsParser theParser = new ArgsParser( theCondition ).withConsoleWidth( 180 ).withEscapeCodesEnabled( false );
		final ByteArrayOutputStream theBytes = new ByteArrayOutputStream();
		final PrintStream theOutHook = new PrintStream( theBytes );
		theParser.printSynopsis( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printDescription( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printOptions( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printExamples( theOutHook );
		final String theResult = theBytes.toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theResult );
		}
		assertTrue( theResult.toLowerCase().contains( "line-width" ) );
		assertEquals( 2495, theResult.replaceAll( "[\\n\\r]+", "\\n" ).length() );
	}

	@Test
	public void testVisibility2() throws ArgsSyntaxException {
		final EnumOption<BaseMetricsConfig> theEncodingArg = enumOption( "encoding", BaseMetricsConfig.class, "ENCODING_PROPERTY", "The BASE (e.g. BASE64) encoding/decoding to be applied for handling encrypted data: " + VerboseTextBuilder.asString( BaseMetricsConfig.values() ) );
		final Flag theBase64Flag = flag( "base64", "BASE64_PROPERTY", "Use BASE64 encoding/decoding to be applied for handling encrypted data." );
		final Flag theDecryptFlag = flag( 'd', "decrypt", "DECRYPT_PROPERTY", "Decrypts the aMessage (stream, file, text or bytes)." );
		final Flag theEncryptFlag = flag( 'e', "encrypt", "ENCRYPT_PROPERTY", "Encrypts the aMessage (stream, file, text or bytes)." );
		final Flag theVerifyFlag = flag( "verify", "VERIFY_PROPERTY", "Verify the encryption process to make sure encryption decrypts flawlessly." );
		final IntOption theLineWidthArg = intOption( "line-width", "LINE_WIDTH_PROPERTY", "The line width for base64 encoded ASCII output." ).withVisible( false );
		final StringOption theInputFileArg = stringOption( 'i', "input-file", "INPUT_FILE_PROPERTY", "The input file which to process from." );
		final StringOption theOutputFileArg = stringOption( 'o', "output-file", "OUTPUT_FILE_PROPERTY", "The output file which to process to." );
		// @formatter:off
		final Condition theCondition =  
			// I + O
			and(
				xor(
					and( theEncryptFlag, any( theVerifyFlag, xor( and( theBase64Flag, any (theLineWidthArg) ), and( theEncodingArg, any (theLineWidthArg) ) ) ) ), 
					theDecryptFlag  
				),
				any(
					theInputFileArg, theOutputFileArg 
				)
			);
		// @formatter:on
		final ArgsParser theParser = new ArgsParser( theCondition ).withConsoleWidth( 180 ).withEscapeCodesEnabled( false );
		final ByteArrayOutputStream theBytes = new ByteArrayOutputStream();
		final PrintStream theOutHook = new PrintStream( theBytes );
		theParser.printSynopsis( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printDescription( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printOptions( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printExamples( theOutHook );
		final String theResult = theBytes.toString();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theResult );
		}
		assertFalse( theResult.toLowerCase().contains( "line-width" ) );
		assertEquals( 2133, theResult.replaceAll( "[\\n\\r]+", "\\n" ).length() );
	}

	@Test
	public void testEscapeCodes1() {
		final Condition theArgsSyntax = cases( and( debugFlag(), verboseFlag() ), xor( helpFlag(), and( sysInfoFlag(), any( verboseFlag() ) ) ) );
		final ArgsParser theParser = new ArgsParser( theArgsSyntax ).withEscapeCodesEnabled( true ).withConsoleWidth( 180 );
		final ByteArrayOutputStream theBytes = new ByteArrayOutputStream();
		final PrintStream theOutHook = new PrintStream( theBytes );
		theParser.printBanner( theOutHook );
		theParser.printLicense( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printSynopsis( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printDescription( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printOptions( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printExamples( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printCopyright( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		final String theResult = theBytes.toString();
		int count = 0;
		for ( int i = 0; i < theResult.length(); i++ ) {
			if ( theResult.charAt( i ) == AnsiEscapeCode.ESCAPE ) {
				count++;
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theResult );
		}

		final int theExpected = OperatingSystem.WINDOWS.isDetected() ? 342 : 330;
		assertEquals( theExpected, count );
	}

	@Test
	public void testEscapeCodes2() {
		final Condition theArgsSyntax = cases( and( debugFlag(), verboseFlag() ), xor( helpFlag(), and( sysInfoFlag(), any( verboseFlag() ) ) ) );
		final ArgsParser theParser = new ArgsParser( theArgsSyntax ).withEscapeCodesEnabled( false ).withConsoleWidth( 180 );
		final ByteArrayOutputStream theBytes = new ByteArrayOutputStream();
		final PrintStream theOutHook = new PrintStream( theBytes );
		theParser.printBanner( theOutHook );
		theParser.printLicense( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printSynopsis( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printDescription( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printOptions( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printExamples( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		theParser.printCopyright( theOutHook );
		theParser.printSeparatorLn( theOutHook );
		final String theResult = theBytes.toString();
		int count = 0;
		for ( int i = 0; i < theResult.length(); i++ ) {
			if ( theResult.charAt( i ) == AnsiEscapeCode.ESCAPE ) {
				count++;
			}
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theResult );
		}
		assertEquals( 0, count );
	}

	@Test
	public void testSyntaxNotations() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			for ( SyntaxNotation eNotation : SyntaxNotation.values() ) {
				System.out.println( eNotation + ".emptySymbol=" + eNotation.getEmptySymbol() );
				System.out.println( eNotation + ".allSymbol=" + eNotation.getAllSymbol() );
				System.out.println( eNotation + ".andSymbol=" + eNotation.getAndSymbol() );
				System.out.println( eNotation + ".anySymbol=" + eNotation.getAnySymbol() );
				System.out.println( eNotation + ".orSymbol=" + eNotation.getOrSymbol() );
				System.out.println( eNotation + ".xorSymbol=" + eNotation.getXorSymbol() );
				System.out.println( eNotation + ".argumentPrefix=" + eNotation.getArgumentPrefix() );
				System.out.println( eNotation + ".argumentSuffix=" + eNotation.getArgumentSuffix() );
				System.out.println( eNotation + ".beginArraySymbol=" + eNotation.getBeginArraySymbol() );
				System.out.println( eNotation + ".beginListSymbol=" + eNotation.getBeginListSymbol() );
				System.out.println( eNotation + ".beginOptionalSymbol=" + eNotation.getBeginOptionalSymbol() );
				System.out.println( eNotation + ".beginRangeSymbol=" + eNotation.getBeginRangeSymbol() );
				System.out.println( eNotation + ".endArraySymbol=" + eNotation.getEndArraySymbol() );
				System.out.println( eNotation + ".endListSymbol=" + eNotation.getEndListSymbol() );
				System.out.println( eNotation + ".endOptionalSymbol=" + eNotation.getEndOptionalSymbol() );
				System.out.println( eNotation + ".endRangeSymbol=" + eNotation.getEndRangeSymbol() );
				System.out.println( eNotation + ".intervalSymbol=" + eNotation.getIntervalSymbol() );
				System.out.println( eNotation + ".longOptionPrefix=" + eNotation.getLongOptionPrefix() );
				System.out.println( eNotation + ".shortOptionPrefix=" + eNotation.getShortOptionPrefix() );
				System.out.println();
			}
		}
	}

	@Test
	public void testSynopsis() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final Condition theArgsSyntax = cases( and( debugFlag(), verboseFlag() ), xor( helpFlag(), and( sysInfoFlag(), any( verboseFlag() ) ) ) );
			System.out.println( theArgsSyntax.toSynopsis( SyntaxNotation.GNU_POSIX ) );
			System.out.println();
			System.out.println( theArgsSyntax.toSchema() );
		}
	}
}
